<?php

namespace Amocrm\Exception;

/**
 * Исключения для работы с загрузкой виджетов в amoCRM через хак. Если во время
 * создания или загрузки, что-то пошло не так, то оно выкинется.
 */
class WidgetException extends \Exception
{

}
