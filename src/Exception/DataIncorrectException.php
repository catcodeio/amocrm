<?php

namespace Amocrm\Exception;

/**
 * Исключение выбрасываемое при обнаружении, что при запросе в API amoCRM мы
 * подали некорректные данные.
 */
class DataIncorrectException extends AmocrmApiException
{

}
