<?php

namespace Amocrm\Exception;

use Exception;

/**
 * Исключение выбрасываемое при невозможности авторизации по паролю.
 */
class AmocrmPasswordException extends Exception
{

}
