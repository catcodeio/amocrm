<?php

namespace Amocrm\Exception;

/**
 * Исключение выбрасываемое при обнаружении, что у Менеджера недостаточно прав
 * т.к. нам нужны только админы.
 */
class ManagerPermissionException extends AmocrmApiException
{

}
