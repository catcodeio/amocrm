<?php

namespace Amocrm\Exception;

/**
 * Исключение выбрасываемое при обнаружении, что под любыми реквизитами
 * менеджеров аккаунта не попасть в API amoCRM.
 */
class AccountUnavailableException extends AmocrmApiException
{

}
