<?php

namespace Amocrm\Exception;

/**
 * Исключение выбрасываемое при обнаружении, что у менеджера неверные реквизиты
 * для доступа в API amoCRM.
 */
class ManagerUnavailableException extends AmocrmApiException
{

}
