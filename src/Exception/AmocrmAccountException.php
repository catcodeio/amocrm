<?php

namespace Amocrm\Exception;

use Exception;

/**
 * Исключение выбрасываемое при ошибке при попытке отправить curl
 * запрос на amocrm
 */
class AmocrmAccountException extends Exception
{

}
