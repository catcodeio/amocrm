<?php

namespace Amocrm\Exception;

use Exception;

/**
 * Исключение выбрасываемое при неудачной попытке получить файл
 */
class CurlFileException extends Exception
{

}
