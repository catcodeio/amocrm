<?php

namespace Amocrm\Exception;

/**
 * Исключение выбрасываемое при невозможности обновить access_token.
 */
class UpdateTokenFailedException extends AmocrmApiException
{

}
