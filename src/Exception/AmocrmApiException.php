<?php

namespace Amocrm\Exception;

use Exception;

/**
 * Исключение выбрасываемое при невозможности понять что или кто виноват в
 * возникновении ошибки при работе с API amoCRM.
 */
class AmocrmApiException extends Exception
{

}
