<?php

namespace Amocrm\Exception;

/**
 * Исключение выбрасываемое при обнаружении, что в аккаунте не обнаружено
 * какое-то нужное для работы поле. Это выбрасывается на основе ответа amoCRM,
 * а значит мы не знаем что за поле.
 */
class CustomFieldIncorrectException extends AmocrmApiException
{

}
