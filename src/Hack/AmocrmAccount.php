<?php

namespace Amocrm\Hack;

use Amocrm\Exception\AmocrmAccountException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\AmocrmPasswordException;
use Amocrm\Exception\AmocrmCapchaException;
use PhpQuery\PhpQuery;
use Psr\Log\LoggerInterface;

/**
 * Класс для работы с аккаунтом амо
 */
class AmocrmAccount
{
    /**
     * Ссылка главной страницы amoCRM
     *
     * @var string
     */
    private const URL_AMOCRM = 'https://amocrm.ru';

    /**
     * Ссылка для авторизации в amocrm по паролю
     *
     * @var string
     */
    private const URL_OAUTH = 'https://www.amocrm.ru/oauth2/authorize';

    /**
     * Ссылка для захода в аккаунт
     *
     * @var string
     */
    private const URL_AUTH = '/?USER_LOGIN=%s&USER_HASH=%s';

    /**
     * USERAGENT
     *
     * @var string
     */
    private const USERAGENT_DEFAULT = 'Mozilla/4.0 (Windows; U; Windows NT 5.0; En; rv:1.8.0.2) Gecko/20070306 Firefox/1.0.0.4';

    /**
     * Хранение ресурса для CURL
     *
     * @var resource
     */
    private $curl;

    /**
     * Ссылка для работы с amoCRM для текущего пользователя
     *
     * @var string
     */
    private $amoDomain;

    /**
     * Мыло для авторизации в amoCRM
     *
     * @var string
     */
    private $amoEmail;

    /**
     * API-ключ для авторизации в amoCRM
     *
     * @var string
     */
    private $amoKey;

    /**
     * Пароль для авторизации в amoCRM
     *
     * @var string
     */
    private $amoPasswordEmail;

    /**
     * Пароль для авторизации в amoCRM
     *
     * @var string
     */
    private $amoPassword;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Подготовка состояния для запросов напрямую к amoCRM через хак.
     *
     * @param string          $amoDomain
     * @param string          $amoEmail
     * @param string          $amoKey
     * @param string          $amoPasswordEmail
     * @param string          $amoPassword
     * @param LoggerInterface $logger
     */
    public function __construct($amoDomain, $amoEmail = '', $amoKey = '', $amoPasswordEmail = '', $amoPassword = '', $logger = null)
    {
        $this->amoDomain        = 'https://' . $amoDomain;
        $this->amoEmail         = $amoEmail;
        $this->amoKey           = $amoKey;
        $this->amoPasswordEmail = $amoPasswordEmail;
        $this->amoPassword      = $amoPassword;
        $this->logger           = $logger;

        /* ---- Конфигурация запросов ---- */
        $this->curl = curl_init();

        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($this->curl, CURLOPT_USERAGENT, self::USERAGENT_DEFAULT);
        curl_setopt($this->curl, CURLOPT_HEADER, 1);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_COOKIEJAR, '/dev/null');
        /* ---- /Конфигурация запросов ---- */
    }

    /**
     * Авторизация в amoCRM по api ключу или паролю
     *
     * @return AmocrmAccount
     *
     * @throws AmocrmAccountException
     * @throws AmocrmApiException
     * @throws AmocrmPasswordException
     * @throws AmocrmCapchaException
     */
    public function authorize()
    {
        if ($this->amoKey) {
            // Авторизация через апи ключ в приоритете, если есть
            $urlAuth = $this->amoDomain . sprintf(self::URL_AUTH, $this->amoEmail, $this->amoKey);

            // Открываем страницу
            curl_setopt($this->curl, CURLOPT_POST, 0);
            curl_setopt($this->curl, CURLOPT_URL, $urlAuth);
            curl_setopt($this->curl, CURLOPT_REFERER, $urlAuth);

            curl_exec($this->curl);

            if (!in_array(curl_getinfo($this->curl, CURLINFO_HTTP_CODE), [200, 302])) {
                // Если не получилось, то пробуем по паролю
                if ($this->amoPassword) {
                    $this->authPassword();
                } else {
                    // Если пароля нет, кидаем ошибку по авторизации
                    throw new AmocrmApiException('Не удалось получить данные по api ключу.');
                }
            }
        } elseif ($this->amoPassword) {
            // Пробуем по паролю
            $this->authPassword();
        } else {
            throw new AmocrmAccountException('Не удалось получить данные.');
        }

        return $this;
    }

    /**
     * Проверка авторизации в amoCRM по api ключу или паролю
     *
     * @return AmocrmAccount
     *
     * @throws AmocrmPasswordException
     * @throws AmocrmCapchaException
     */
    public function checkPassword()
    {
        $this->authPassword();

        return $this;
    }

    /**
     * Метод авторизации по паролю
     *
     * @return AmocrmAccount
     *
     * @throws AmocrmPasswordException
     * @throws AmocrmCapchaException
     */
    private function authPassword()
    {
        // Открываем страницу
        curl_setopt($this->curl, CURLOPT_POST, 0);
        curl_setopt($this->curl, CURLOPT_URL, self::URL_AMOCRM);
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);
        $page = curl_exec($this->curl);

        // Получаем токен
        $pq = new PhpQuery;
        $pq->load_str($page);

        $csrf_token = null;
        foreach ($pq->query('#csrf_token')[0]->attributes as $attribute) {
            if ($attribute->name == 'value') {
                $csrf_token = $attribute->value;
            }
        }

        if (!$csrf_token) {
            throw new AmocrmPasswordException('Не удалось получить токен для авторизации.');
        }

        // Авторизируемся
        curl_setopt($this->curl, CURLOPT_POST, 1);
        curl_setopt($this->curl, CURLOPT_HEADER, 0);
        curl_setopt($this->curl, CURLOPT_URL, self::URL_OAUTH);
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, 'username=' . $this->amoPasswordEmail . '&password=' . $this->amoPassword . '&csrf_token=' . $csrf_token);

        $result = curl_exec($this->curl);

        if (curl_getinfo($this->curl, CURLINFO_HTTP_CODE) == 400) {
            $result = json_decode($result, true);

            if ($this->logger) {
                $this->logger->notice('Result of request AuthPassword:', [
                    'result' => $result,
                ]);
            }

            if ($result['invalid-params'][0]['name'] == 'captcha') {
                throw new AmocrmCapchaException('Необходимо пройти капчу.');
            }

            throw new AmocrmPasswordException('Не удалось получить данные по паролю.');
        }

        if (curl_getinfo($this->curl, CURLINFO_HTTP_CODE) !== 200) {
            throw new AmocrmPasswordException('Не удалось получить данные по паролю.');
        }

        return $this;
    }

    /**
     * Отправляет запрос на файл по ссылке. Если в ответ пришёл код 301 или 302,
     * то возвращает новую ссылку. Иначе вернёт исходную.
     *
     * @param string $link
     *
     * @return array
     *
     * @throws AmocrmAccountException
     */
    public function getFileUrl($link)
    {
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, true);

        $this->requestCurl($link, false);
        $code   = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

        // Если были редиректы, надо взять последнюю ссылку
        if (curl_getinfo($this->curl, CURLINFO_REDIRECT_COUNT) > 0) {
            $link = curl_getinfo($this->curl, CURLINFO_EFFECTIVE_URL);
            $code = 302;
        }

        // Проверяем код ответа
        switch ($code) {
            case 301:
            case 302:
            case 200:
                return [
                    'link' => $link,
                    'code' => $code,
                ];
            default:
                throw new AmocrmAccountException('Ошибка при получении файла.');
        }
    }

    /**
     * Возвращает файл по переданной ссылке.
     *
     * @param $link
     *
     * @return string
     *
     * @throws AmocrmAccountException
     */
    public function getFile($link)
    {
        $result = $this->requestCurl($link, false);
        $code   = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

        // Проверяем код ответа
        if ($code == 200) {
            return $result;
        } else {
            throw new AmocrmAccountException('Ошибка при получении файла.');
        }
    }

    /**
     * Запрос к странице аналитики Анализ продаж
     *
     * @link /stats/
     *
     * @param array $data - параметры фильтрации
     *
     * @return array
     *
     * @throws AmocrmAccountException
     */
    public function getPipelineStats(array $data = [])
    {
        return $this->requestJson('/ajax/stats/pipeline/?', true, $data);
    }

    /**
     * Запрос к странице отчета по сотрудникам
     *
     * @link /stats/by_activities/
     *
     * @param array $data - параметры фильтрации
     *
     * @return array
     *
     * @throws AmocrmAccountException
     */
    public function getUserStats(array $data = [])
    {
        return $this->requestJson('/ajax/stats/by_activities/?', false, $data);
    }

    /**
     * Запрос к рабочему столу
     *
     * @link /dashboard/
     *
     * @param array $data - параметры фильтрации
     *
     * @return array
     *
     * @throws AmocrmAccountException
     */
    public function getDashboardStats(array $data = [])
    {
        return $this->requestJson('/ajax/dashboard/show/?', false, $data);
    }

    /**
     * Вызов запроса с json ответом
     *
     * @param string $url
     * @param bool   $isPost
     * @param array  $data
     *
     * @return array
     *
     * @throws AmocrmAccountException
     */
    private function requestJson($url, $isPost, array $data)
    {
        // !!! Параметры надо передавать как в параметрах post, так и в url
        $data = http_build_query($data);

        $result = $this->requestCurl($this->amoDomain . $url . $data, $isPost, $data, ['Accept: application/json']);
        $result = json_decode($result, true);

        return $result['response'];
    }

    /**
     * Отправляет curl запрос в amoCRM
     *
     * @param string $url
     * @param bool   $isPost
     * @param string $data
     * @param array  $headers
     *
     * @return string
     *
     * @throws AmocrmAccountException
     */
    private function requestCurl($url, $isPost = false, $data = null, array $headers = [])
    {
        if (!$this->curl) {
            throw new AmocrmAccountException('Необходимо пройти авторизацию перед использованием данного метода.');
        }

        // Может прийти ссылка без домена, тогда добавляем
        if ($url[0] == '/') {
            $url = $this->amoDomain . $url;
        }

        $headersDefault = [
            'X-Requested-With:XMLHttpRequest',
            'Cache-Control:no-cache',
            'Pragma:no-cache',
        ];

        if ($data) {
            curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
        }

        curl_setopt($this->curl, CURLOPT_URL, $url);
        curl_setopt($this->curl, CURLOPT_POST, $isPost);
        curl_setopt($this->curl, CURLOPT_HEADER , false);
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, array_merge($headersDefault, $headers));

        return curl_exec($this->curl);
    }
}
