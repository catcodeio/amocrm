<?php

namespace Amocrm\Hack;

use Amocrm\Exception\WidgetException;

/**
 * Класс для создания и загрузки пользовательских виджетов через хак.
 */
class AmocrmWidget
{
    /**
     * Ссылка для получения данных о виджетах
     *
     * @var string
     */
    const URL_DEV = 'https://%s.%s/ajax/settings/dev/';

    /**
     * Ссылка для удаления виджета
     *
     * @var string
     */
    const URL_DELETE = 'https://widgets.%s/%s/delete/';

    /**
     * Ссылка для загрузки виджета
     *
     * @var string
     */
    const URL_UPLOAD = 'https://widgets.%s/%s/upload/';

    /**
     * Хранение ресурса для CURL
     *
     * @var resource
     */
    private $curl;

    /**
     * Мыло для авторизации в amoCRM
     *
     * @var string
     */
    private $amoEmail;

    /**
     * API-ключ для авторизации в amoCRM
     *
     * @var string
     */
    private $amoKey;

    /**
     * Поддомен пользователя
     *
     * @var string
     */
    private $amoDomain;

    /**
     * Основной домен для общения с API
     *
     * @var string
     */
    private $mainDomain;

    /**
     * Подготовка состояния для запросов напрямую к amoCRM через хак.
     *
     * @param string $mainDomain
     * @param string $amoDomain
     * @param string $amoEmail
     * @param string $amoKey
     *
     * @return void
     */
    public function __construct($mainDomain, $amoDomain, $amoEmail, $amoKey)
    {
        $this->amoDomain  = $amoDomain;
        $this->mainDomain = $mainDomain;

        $this->amoEmail   = $amoEmail;
        $this->amoKey     = $amoKey;

        $this->curl       = curl_init();

        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYHOST, 0);

        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->curl, CURLOPT_REFERER, sprintf(self::URL_DEV, $this->amoDomain, $this->mainDomain));
    }

    /**
     * Получить данные виджета по его названию, если он существует.
     *
     * @param string $widgetCode
     *
     * @return array|false
     *
     * @throws WidgetException
     */
    public function get($widgetCode)
    {
        $domain     = sprintf(self::URL_DEV, $this->amoDomain, $this->mainDomain);
        $response   = $this->request('GET', $domain);

        if (isset($response['response']['widgets']['items'])) {
            foreach (array_reverse($response['response']['widgets']['items']) as $item) {
                if ($item['code'] == $widgetCode) {
                    return $item;
                }
            }
        }

        return false;
    }

    /**
     * Создание виджета в аккаунте для получения code, secret key и др.
     *
     * @param string $widgetCode
     *
     * @return array
     *
     * @throws WidgetException
     */
    public function create($widgetCode)
    {
        $domain = sprintf(self::URL_DEV, $this->amoDomain, $this->mainDomain);

        // Добавляем новый виджет (не загружаем пока)
        $response = $this->request(
            'POST',
            $domain,
            [
                'action'    => 'create',
                'PAGEN_1'   => 1,
                'code'      => $widgetCode,
            ]
        );

        if (isset($response['error'])) {
            throw new WidgetException('Установить виджет не удалось: ' . $response['error']);
        }

        // Получаем весь список текущих виджетов и ищим среди них свой, чтобы
        // запомнить secret key.
        $response = $this->request('GET', $domain);

        if (isset($response['response']['widgets']['items'])) {
            foreach (array_reverse($response['response']['widgets']['items']) as $item) {
                if ($item['code'] == $widgetCode) {
                    return $item;
                }
            }
        }

        throw new WidgetException('Установить виджет не удалось т.к. после добавления его не удаётся найти.');
    }

    /**
     * Загрузка архива с виджетом, но перед этим формируется архив с
     * определённым manifest.json с code и secret_key для этого пользователя.
     *
     * @param string          $widgetCode
     * @param string          $secret
     * @param resource|string $archive
     *
     * @return array|false
     *
     * @throws WidgetException
     */
    public function upload($widgetCode, $secret, $archive)
    {
        $delimiter  = '----WebKitFormBoundary'.uniqid();

        $data = $this->createMultipartRequestWithFile(
            $delimiter,
            [
                'widget' => $widgetCode,
                'amouser' => $this->amoEmail,
                'amohash' => $this->amoKey,
                'secret' => $secret,
            ],
            [[
                'content' => $archive,
                'name' => 'widget',
                'filename' => 'widget.zip',
                'type' => 'application/zip'
            ]]
        );

        $headers = [
            'Content-Type: multipart/form-data; ' . 'boundary=' . $delimiter,
            'Cache-Control: no-cache',
            // 'DNT: 1',
            // 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            // 'Origin: https://widget8.amocrm.ru',
        ];

        $response = $this->request(
            'POST',
            sprintf(self::URL_UPLOAD, $this->mainDomain, $this->amoDomain),
            $data,
            $headers
        );

        return $response;
    }

    /**
     * Удаление виджета.
     *
     * @param string $widgetCode
     * @param string $secret
     *
     * @return true
     *
     * @throws WidgetException
     */
    public function remove($widgetCode, $secret)
    {
        $this->request(
            'POST',
            sprintf(self::URL_DELETE, $this->mainDomain, $this->amoDomain),
            [
                'widget' => $widgetCode,
                'amouser' => $this->amoEmail,
                'amohash' => $this->amoKey,
                'secret' => $secret,
            ]
        );

        return true;
    }

    /**
     * Метод для формирования multipart-запроса с разделением частей через
     * WebKitFormBoundary, как это делает браузер.
     *
     * @see https://gist.github.com/mcorrigan/f7032dc9ca5d78034b18
     *
     * @param array $delimiter
     * @param array $postFields
     * @param array $fileFields
     *
     * @return string
     */
    private function createMultipartRequestWithFile($delimiter, array $postFields, array $fileFields)
    {
        $eol        = "\r\n";
        $data       = '';

        foreach ($postFields as $name => $content) {
            $data .= "--$delimiter" . $eol;
            $data .= 'Content-Disposition: form-data; name="' . $name . '"';
            $data .= $eol.$eol; // note: double endline
            $data .= $content;
            $data .= $eol;
        }

        foreach ($fileFields as $file) {
            $content    = $file['content'];
            $name       = $file['name'];
            $filename   = $file['filename'];
            $type       = isset($file['type']) ? $file['type'] : null;

            $data .= "--$delimiter" . $eol;

            // Атрибут "filename" является не обязательным, но мы его будем требовать
            $data .= 'Content-Disposition: form-data; name="' . $name . '";' .
                ' filename="' . $filename . '"' . $eol;

            // Также является необзятальным, но рекомендован для включения
            if ($type) {
                $data .= 'Content-Type: ' . $type . $eol;
            }

            $data .= $eol;

            // Непосредственно сам файл (без кодирования)
            if (is_resource($content)) {
                rewind($content);
                // Считываем контент файла
                while (!feof($content)) {
                    $data .= fgets($content);
                }

                $data .= $eol;
            } else {
                // Проверим не загружается ли файл по полному пути
                if (strpos($content, '@') === 0) {
                    $filePath = substr($content, 1);
                    $fh = fopen(realpath($filePath), 'rb');

                    if ($fh) {
                        while (!feof($fh)) {
                            $data .= fgets($fh);
                        }
                        $data .= $eol;
                        fclose($fh);
                    }
                } else {
                    // Или это уже raw-контент файла
                    $data .= $content . $eol;
                }
            }
        }

        $data .= "--" . $delimiter . "--$eol";

        return $data;
    }

    /**
     * Прямой запрос в amoCRM через веб-интерфейс с авторизацией по ключу и
     * мылу.
     *
     * @param string       $method
     * @param string       $url
     * @param array|string $params
     * @param array        $headers
     *
     * @return array|false
     *
     * @throws WidgetException
     */
    private function request($method, $url, $params = null, array $headers = [])
    {
        $paramsQuery = [
            'USER_LOGIN' => $this->amoEmail,
            'USER_HASH'  => $this->amoKey,
        ];

        $headersRequest = [
            'X-Requested-With: XMLHttpRequest'
        ];

        if ($method == 'POST') {
            curl_setopt($this->curl, CURLOPT_POST, 1);

            if ($params) {
                if (is_array($params)) {
                    $params = http_build_query($params);
                }

                curl_setopt($this->curl, CURLOPT_POSTFIELDS, $params);
            }
        } elseif ($method == 'GET') {
            curl_setopt($this->curl, CURLOPT_POST, 0);

            if ($params && is_array($params)) {
                $paramsQuery = array_merge($paramsQuery, $params);
            }
        }

        if ($headers) {
            $headersRequest = array_merge($headersRequest, $headers);
        }

        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headersRequest);
        curl_setopt($this->curl, CURLOPT_URL, $url . '?' . http_build_query($paramsQuery));

        $response = curl_exec($this->curl);
        $statusCode = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
        $errno = curl_errno($this->curl);
        $error = curl_error($this->curl);

        if ($statusCode != 200) {
            throw new WidgetException(
                'Ошибка при попытке создать виджет: ' . "\n"
                . 'errno: ' . $errno . "\n"
                . 'error: ' . $error . "\n"
                . 'HTTP status: ' . $statusCode . "\n"
                . 'response: ' . print_r($response, true)
            );
        }

        $return = json_decode($response, true);

        if (!$return) {
            // В ответ может быть не только нормальный json, но и HTML и в перемешку,
            // потому пытаемся удалить <script>, который обычно идёт в само начале:
            // <script type="text/javascript">document.domain = "amocrm.ru";</script>{"error":"Setup archive missing"}
            $return = preg_replace('/\<script.+<\/script>/ui', '', $response);
            $return = json_decode($return, true);
        }

        return $return;
    }
}
