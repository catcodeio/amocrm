<?php

namespace Amocrm;

use Amocrm\Api\Client\RestClientOAuth2;
use Amocrm\Api\Client\RestClientToken;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmAccountException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerPermissionException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Api\AmocrmApi;
use Amocrm\Exception\ManagerUnavailableException;
use Amocrm\Hack\AmocrmWidget;
use Amocrm\Hack\AmocrmAccount;
use AmoCRM\OAuth2\Client\Provider\AmoCRM;
use League\OAuth2\Client\Token\AccessToken;

class AmocrmFactory
{
    /**
     * Создание объекта для работы с API amoCRM без проверки на валидность с OAuth2-авторизацией.
     *
     * @param string           $clientId
     * @param string           $clientSecret
     * @param string           $redirectUri
     * @param string|null      $domain Домен вида *subdomain.amocrm.ru*.
     * @param AccessToken|null $accessToken
     * @param callable|null    $refreshCallback
     * @param int              $version
     *
     * @return AmocrmApi
     */
    public static function createApi(
        string $clientId,
        string $clientSecret,
        string $redirectUri,
        string $domain = null,
        AccessToken $accessToken = null,
        callable $refreshCallback = null,
        $version = AmocrmApi::VERSION_2
    ) {
        return new AmocrmApi(new RestClientOAuth2(new AmoCRM([
            'clientId'     => $clientId,
            'clientSecret' => $clientSecret,
            'redirectUri'  => $redirectUri,
            'baseDomain'   => $domain,
        ]), $accessToken, $refreshCallback), $version);
    }

    /**
     * Создание объекта для работы с API amoCRM без проверки на валидность с авторизацией по токену.
     *
     * @param string $domain
     * @param string $email
     * @param string $token
     * @param int    $version
     *
     * @return AmocrmApi
     */
    public static function createApiToken(
        string $domain,
        string $email,
        string $token,
        $version = AmocrmApi::VERSION_1
    ) {
        return new AmocrmApi(new RestClientToken($domain, $email, $token), $version);
    }

    /**
     * Создание объекта для недокументированной работы с виджетами amoCRM без проверки валидности реквизитов.
     *
     * @param string $domain Домен вида *subdomain.amocrm.ru*.
     * @param string $email
     * @param string $token
     *
     * @return AmocrmWidget
     */
    public static function createWidget($domain, $email, $token)
    {
        $mainDomain = explode('.', $domain);
        $subdomain = $mainDomain[0];
        unset($mainDomain[0]);
        $mainDomain = implode('.', $mainDomain);

        return new AmocrmWidget(
            $mainDomain,
            $subdomain,
            $email,
            $token
        );
    }

    /**
     * Создание объекта для недокументированной работы с аккаунтом amoCRM без проверки валидности реквизитов.
     *
     * @param string $mainDomain
     * @param string $subdomain
     * @param string $email
     * @param string $token
     *
     * @return AmocrmAccount
     */
    public static function createAccount($mainDomain, $subdomain, $email, $token)
    {
        return new AmocrmAccount(
            $mainDomain,
            $subdomain,
            $email,
            $token
        );
    }

    /**
     * Возвращает клиент для работы с API amoCRM с проверкой на валидность OAuth2-авторизации.
     *
     * @param string           $clientId
     * @param string           $clientSecret
     * @param string           $redirectUri
     * @param string|null      $domain Домен вида *subdomain.amocrm.ru*.
     * @param AccessToken|null $accessToken
     * @param callable|null    $refreshCallback
     * @param int              $version
     *
     * @return AmocrmApi
     *
     * @throws ManagerUnavailableException
     * @throws AccountUnavailableException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws AmocrmApiException
     */
    public static function getApi(
        string $clientId,
        string $clientSecret,
        string $redirectUri,
        string $domain = null,
        AccessToken $accessToken = null,
        callable $refreshCallback = null,
        $version = AmocrmApi::VERSION_2
    ) {
        $amocrmApiClient = self::createApi($clientId, $clientSecret, $redirectUri, $domain, $accessToken, $refreshCallback, $version);

        // Вызов данных аккаунта в случае чего выкинет исклюение.
        $amocrmApiClient->account()->getId();

        return $amocrmApiClient;
    }

    /**
     * Возвращает клиент для работы с API amoCRM с проверкой на валидность авторизации по токену.
     *
     * @param string $domain
     * @param string $email
     * @param string $token
     * @param int    $version
     *
     * @return AmocrmApi
     *
     * @throws ManagerUnavailableException
     * @throws AccountUnavailableException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws AmocrmApiException
     */
    public static function getApiToken(
        string $domain,
        string $email,
        string $token,
        $version = AmocrmApi::VERSION_1
    ) {
        $amocrmApiClient = self::createApiToken($domain, $email, $token, $version);

        // Вызов данных аккаунта в случае чего выкинет исклюение.
        $amocrmApiClient->account()->getId();

        return $amocrmApiClient;
    }

    /**
     * Возвращает клиент для работы с API amoCRM или исключение, если Менеджера не является админом, с OAuth2-авторизацией.
     *
     * @param string           $clientId
     * @param string           $clientSecret
     * @param string           $redirectUri
     * @param string|null      $domain Домен вида *subdomain.amocrm.ru*.
     * @param AccessToken|null $accessToken
     * @param callable|null    $refreshCallback
     * @param int              $version
     *
     * @return AmocrmApi
     *
     * @throws ManagerUnavailableException
     * @throws AccountUnavailableException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws AmocrmApiException
     * @throws ManagerPermissionException
     */
    public static function getApiWithAdmin(
        string $clientId,
        string $clientSecret,
        string $redirectUri,
        string $domain = null,
        AccessToken $accessToken = null,
        callable $refreshCallback = null,
        $version = AmocrmApi::VERSION_2
    ) {
        $amocrmApiClient = self::createApi($clientId, $clientSecret, $redirectUri, $domain, $accessToken, $refreshCallback, $version);

        self::throwPermission($amocrmApiClient, $email);

        return $amocrmApiClient;
    }

    /**
     * Возвращает клиент для работы с API amoCRM или исключение, если Менеджера не
     * является админом, с авторизацией по токену.
     *
     * @param string $domain
     * @param string $email
     * @param string $token
     * @param int    $version
     *
     * @return AmocrmApi
     *
     * @throws ManagerUnavailableException
     * @throws AccountUnavailableException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws AmocrmApiException
     * @throws ManagerPermissionException
     */
    public static function getApiWithAdminToken($domain, $email, $token, $version = AmocrmApi::VERSION_1)
    {
        $amocrmApiClient = self::createApiToken($domain, $email, $token, $version);

        self::throwPermission($amocrmApiClient, $email);

        return $amocrmApiClient;
    }

    /**
     * Возвращает клиент для недокументированной работы с виджетами amoCRM.
     * Необходимы реквизиты именно администратора, другие не имеют право в
     * принципе работать с настройками аккаунта, а потому тут проверяется
     * является ли Менеджера админом или нет.
     *
     * @param string $domain
     * @param string $email
     * @param string $token
     *
     * @return AmocrmWidget
     *
     * @throws ManagerUnavailableException
     * @throws AccountUnavailableException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws AmocrmApiException
     * @throws ManagerPermissionException
     */
    public static function getWidget($domain, $email, $token)
    {
        // В начале проверяем реквизиты с помощью обычного API ...
        $amocrmApiClient = self::createApiToken($domain, $email, $token);

        self::throwPermission($amocrmApiClient, $email);

        // ... а потом формируем объект для работы с виджетами.
        return self::createWidget($domain, $email, $token);
    }

    /**
     * Возвращает клиент для недокументированной работы с аккаунтом amoCRM.
     * Пытается авторизоваться, иначе выбрасывает ошибку.
     *
     * @param string $mainDomain
     * @param string $subdomain
     * @param string $email
     * @param string $token
     *
     * @return AmocrmAccount
     *
     * @throws AmocrmAccountException
     */
    public static function getAccount($mainDomain, $subdomain, $email, $token)
    {
        $account = self::createAccount($mainDomain, $subdomain, $email, $token);

        return $account->authorize();
    }

    /**
     * Проверяет админские права доступа, и если что кидает исключение.
     *
     * @param AmocrmApi $amocrmApiClient
     * @param string    $email
     *
     * @return true
     *
     * @throws ManagerPermissionException
     */
    private static function throwPermission(AmocrmApi $amocrmApiClient, $email)
    {
        $user = $amocrmApiClient->account()->getUsers()->getOneByEmail($email);

        if (!$user->isAdmin()) {
            throw new ManagerPermissionException(
                'У менеджера недостаточно прав для использования, необходим администратор.'
            );
        }

        return true;
    }
}
