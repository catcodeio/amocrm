<?php

namespace Amocrm\Api\Helper;

/**
 * Класс, который служит для определения того кто виноват в возникшей ошибке при
 * работе с amoCRM. Это может потребоваться, чтобы заблокировать менеджера, если
 * у него реквизиты сменились или всю компанию в целом, если услуги не оплачены.
 * Также можно определить является ли ошибка в наборе посланных нами данных.
 *
 * Списко ошибок https://github.com/dotzero/amocrm-php/blob/master/src/Exception.php
 */
class AmocrmResponseAnalyzer
{
    /**
     * Список ответов-ошибок API amoCRM, в которых виноват менеджер. Если
     * срабатывает один из них, то менеджер блокируется и считается, что в API
     * через него не попасть.
     */
    private const RESPONSES_OF_MANAGER = [
        110, 111, 112, 202, 244, 288,
    ];

    /**
     * Список ответов-ошибок API amoCRM, в которых виновата весь аккаунт. Если
     * срабатывает один из них, то весь аккаунт блокируется и считается, что ни
     * через одного её менеджера в API не попасть.
     */
    private const RESPONSES_OF_ACCOUNT = [
        101, 113, 403, 429, 2002, 402,
    ];

    /**
     * Список ответов-ошибок API amoCRM, в которых виновата настройка полей
     * аккаунта Компании. Если срабатывает, то возможно текущий функционал не
     * может работать корректно (конкретный виджет), но другой сможет т.к. не
     * завязан на этом поле. В этом случае компания и менеджер не блокируются,
     * но может выключиться виджет с целью не юзать его больше.
     *
     * Эта фича пока неприменима т.к. у каждого виджет свой бекенд и он сам
     * реагирует на это всё. Но пусть тут будет.
     */
    private const RESPONSES_OF_FIELD = [
        204, 211, 401,
    ];

    /**
     * Список ответов-ошибок API amoCRM, в которых виноват посланные данные.
     */
    private const RESPONSES_OF_DATA = [
        102, 103, 104, 201, 203, 205, 206, 207, 208, 209, 210, 211, 212, 213,
        214, 215, 216, 217, 218, 219, 221, 222, 223, 224, 225, 227, 228, 229,
        230, 231, 232, 233, 234, 235, 236, 237, 238, 240, 400,
    ];

    /**
     * @param int $code
     *
     * @return bool
     */
    public static function isManager($code)
    {
        return array_search($code, self::RESPONSES_OF_MANAGER) !== false;
    }

    /**
     * @param int $code
     *
     * @return bool
     */
    public static function isAccount($code)
    {
        return array_search($code, self::RESPONSES_OF_ACCOUNT) !== false;
    }

    /**
     * @param int $code
     *
     * @return bool
     */
    public static function isField($code)
    {
        return array_search($code, self::RESPONSES_OF_FIELD) !== false;
    }

    /**
     * @param int $code
     *
     * @return bool
     */
    public static function isData($code)
    {
        return array_search($code, self::RESPONSES_OF_DATA) !== false;
    }
}
