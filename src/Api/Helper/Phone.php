<?php

namespace Amocrm\Api\Helper;

/**
 * Класс-помощник для работы с телефонными номерами.
 */
class Phone
{
    /**
     * Преобразует номер телефона к единому формату для поиска через API amoCRM.
     * Убирает код и берёт последние 10 цифр т.к. amoCRM так лучше ищет:
     *
     * 89051234578          -> 9051234578
     * +7 905 123-45-78     -> 9051234578
     * +33 (123) 213 23 3   -> 3123213233
     * 123 4567             -> 1234567
     * 1234567              -> 1234567
     *
     * Если передать true вторым параметром, то код страны будет оставлен. Этот
     * вариант подходит для записи телефонов в доп. поле:
     *
     * 89051234578        -> 79051234578
     * +7 905 123-45-78   -> 79051234578
     * +33 (123) 213 23 3 -> 33123213233
     *
     * @param string $phone
     * @param bool   $code
     *
     * @return string
     */
    public static function clear($phone, $code = false)
    {
        // Проверяем на наличие чего-то явно не телефонного и пустой строки
        if (!strlen(trim($phone)) || strlen(trim($phone)) < 7) {
            return $phone;
        }

        // Убираем пробелы и дефисы со скобками
        $trimmed = preg_replace('/[^\d]/', '', $phone);

        // Берем 'основной' номер (7 цифр с конца)
        preg_match('/.{7}$/', $trimmed, $main);
        if (array_key_exists(0, $main)) {
            $main = $main[0];
        } else {
            return $trimmed;
        }

        // Получаем префиксы
        $prefix = substr($trimmed, 0, strpos($trimmed, $main));

        // Выделяем среди префиксов код города
        preg_match('/\d{3}$/', $prefix, $cityCode);

        if (array_key_exists(0, $cityCode)) {
            $cityCode = $cityCode[0];
        } else {
            return $trimmed;
        }

        $countryCode = '';

        // Если кроме кода города в префиксе что-то есть, то это код страны
        if (strlen($prefix) - strlen($cityCode)) {
            $countryCode = substr($prefix, 0, strpos($prefix, $cityCode));
            $countryCode = ($countryCode == 8) ? '+7' : $countryCode;

            if (preg_match('/^\+/', $countryCode) && strlen($countryCode)) {
                $countryCode = preg_replace('/^\+/', '', $countryCode);
            }
        }

        return ($code ? $countryCode : '') . $cityCode . $main;
    }
}
