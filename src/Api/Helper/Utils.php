<?php

namespace Amocrm\Api\Helper;

class Utils
{
    /**
     * Конвертирует 'CamelCase' в 'camel_case'.
     *
     * @param string $word
     *
     * @return string $word
     */
    public static function toSnakeCase(string $word): string
    {
        return strtolower(preg_replace('~(?<=\\w)([A-Z])~', '_$1', $word));
    }

    /**
     * Конвертирует 'snake_case' в 'SnakeCase'.
     *
     * @param string $word
     *
     * @return string $word
     */
    public static function toCamelCase(string $word): string
    {
        return str_replace(' ', '', ucwords(strtr($word, '_-', '  ')));
    }
}