<?php

namespace Amocrm\Api\Helper;

/**
 * Типы неразбранных.
 */
class UnsortedType
{
    /**
     * @var string
     */
    const SIP = 'sip';

    /**
     * @var string
     */
    const MAIL = 'mail';

    /**
     * @var string
     */
    const FORM = 'form';
}