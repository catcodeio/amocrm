<?php

namespace Amocrm\Api\Helper;

/**
 * Специальный класс-хелпер-костыль для того, чтобы покрыть все варианты типов
 * сущностей в API, которых несколько: то цифровые, то символьные.
 *
 * Получается везде используем константы для передачи в параметры типа элемента,
 * а уже внутри каждый класс как надо будет преоразовывать переданный тип. Также
 * при возврате значения каждый класс будет обратно преобразовывать значение.
 */
class ElementType
{
    /**
     * @var string
     */
    const CONTACT = 'contact';

    /**
     * @var string
     */
    const LEAD = 'lead';

    /**
     * @var string
     */
    const COMPANY = 'company';

    /**
     * @var string
     */
    const TASK = 'task';

    /**
     * @var string
     */
    const CUSTOMER = 'customer';

    /**
     * @var string
     */
    const ELEMENT = 'element';

    /**
     * @var string[]
     */
    private static $plurals = [
        self::CONTACT  => 'contacts',
        self::LEAD     => 'leads',
        self::COMPANY  => 'companies',
        self::TASK     => 'tasks',
        self::CUSTOMER => 'customers',
        self::ELEMENT  => 'catalog_elements',
    ];

    /**
     * @var int[]
     */
    private static $numbers = [
        self::CONTACT  => 1,
        self::LEAD     => 2,
        self::COMPANY  => 3,
        self::TASK     => 4,
        self::CUSTOMER => 12,
        self::ELEMENT  => 5, // Не используется
    ];

    /**
     * Возвращает множественное число типа. Нужен, например, в Link.
     *
     * @param mixed $type
     *
     * @return mixed
     */
    public static function toPlural($type)
    {
        if (!isset(self::$plurals[$type])) {
            return $type;
        }

        return self::$plurals[$type];
    }

    /**
     * Возвращает стандартное строкое представление типа по множественному числу.
     *
     * @param mixed $type
     *
     * @return mixed
     */
    public static function fromPlural($type)
    {
        $key = array_search($type, self::$plurals);

        if ($key === false) {
            return $type;
        }

        return $key;
    }

    /**
     * Возвращает числовое представление типа. Нужен, например, в создании задач
     * и примечаний.
     *
     * @param mixed $type
     *
     * @return mixed
     */
    public static function toNumber($type)
    {
        if (!isset(self::$numbers[$type])) {
            return $type;
        }

        return self::$numbers[$type];
    }

    /**
     * Возвращает стандартное строкое представление типа по числовому.
     *
     * @param mixed $type
     *
     * @return mixed
     */
    public static function fromNumber($type)
    {
        $key = array_search($type, self::$numbers);

        if ($key === false) {
            return $type;
        }

        return $key;
    }

    /**
     * Для особых случаев, когда множественное число надо преобразовать в цифру.
     *
     * @param mixed $type
     *
     * @return mixed
     */
    public static function pluralToNumber($type)
    {
        $key = array_search($type, self::$plurals);

        if ($key === false) {
            return $type;
        }

        return self::$numbers[$key];
    }
}