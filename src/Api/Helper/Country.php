<?php

namespace Amocrm\Api\Helper;

/**
 * Класс-помощник для работы с IATA-кодами стран.
 */
class Country
{
    /**
     * @param string $iata
     * @param string $locale
     *
     * @return string|null
     */
    public static function getName(?string $iata, string $locale = 'ru'): ?string
    {
        if (!$iata) {
            return null;
        }

        $iata   = strtolower($iata);
        $locale = strtolower($locale);

        return self::$countries[$iata][$locale]
            ?? self::$countries[$iata]['ru']
            ?? null;
    }

    // Соответствие iata-кодов странам на двух локалях.
    private static $countries = [
        'us' => [
            'ru' => 'Соединённые Штаты Америки',
            'en' => 'United States of America',
        ],
        'za' => [
            'ru' => 'Южно-Африканская Республика',
            'en' => 'South Africa',
        ],
        'gb' => [
            'ru' => 'Великобритания',
            'en' => 'Great Britain',
        ],
        'ru' => [
            'ru' => 'Россия',
            'en' => 'Russia',
        ],
        'th' => [
            'ru' => 'Таиланд',
            'en' => 'Thailand',
        ],
        'ca' => [
            'ru' => 'Канада',
            'en' => 'Canada',
        ],
        'au' => [
            'ru' => 'Австралия',
            'en' => 'Australia',
        ],
        'sg' => [
            'ru' => 'Сингапур',
            'en' => 'Singapore',
        ],
        'be' => [
            'ru' => 'Бельгия',
            'en' => 'Belgium',
        ],
        'mx' => [
            'ru' => 'Мексика',
            'en' => 'Mexico',
        ],
        'fr' => [
            'ru' => 'Франция',
            'en' => 'France',
        ],
        'ae' => [
            'ru' => 'Объединённые Арабские Эмираты',
            'en' => 'United Arab Emirates',
        ],
        'pk' => [
            'ru' => 'Пакистан',
            'en' => 'Pakistan',
        ],
        'ly' => [
            'ru' => 'Ливия',
            'en' => 'Libya',
        ],
        'gm' => [
            'ru' => 'Гамбия',
            'en' => 'Gambia',
        ],
        'ua' => [
            'ru' => 'Украина',
            'en' => 'Ukraine',
        ],
        'ir' => [
            'ru' => 'Иран',
            'en' => 'Iran',
        ],
        'fi' => [
            'ru' => 'Финляндия',
            'en' => 'Finland',
        ],
        'br' => [
            'ru' => 'Бразилия',
            'en' => 'Brazil',
        ],
        'co' => [
            'ru' => 'Колумбия',
            'en' => 'Colombia',
        ],
        'gh' => [
            'ru' => 'Гана',
            'en' => 'Ghana',
        ],
        'ke' => [
            'ru' => 'Кения',
            'en' => 'Kenya',
        ],
        'lr' => [
            'ru' => 'Либерия',
            'en' => 'Liberia',
        ],
        'tg' => [
            'ru' => 'Того',
            'en' => 'Togo',
        ],
        'ma' => [
            'ru' => 'Марокко',
            'en' => 'Morocco',
        ],
        'do' => [
            'ru' => 'Доминиканская Республика',
            'en' => 'Dominican Republic',
        ],
        'jp' => [
            'ru' => 'Япония',
            'en' => 'Japan',
        ],
        'al' => [
            'ru' => 'Албания',
            'en' => 'Albania',
        ],
        'es' => [
            'ru' => 'Испания',
            'en' => 'Spain',
        ],
        'ng' => [
            'ru' => 'Нигерия',
            'en' => 'Nigeria',
        ],
        'de' => [
            'ru' => 'Германия',
            'en' => 'Germany',
        ],
        'si' => [
            'ru' => 'Словения',
            'en' => 'Slovenia',
        ],
        'cz' => [
            'ru' => 'Чехия',
            'en' => 'Czech',
        ],
        'bj' => [
            'ru' => 'Бенин',
            'en' => 'Benin',
        ],
        'gr' => [
            'ru' => 'Греция',
            'en' => 'Greece',
        ],
        'cl' => [
            'ru' => 'Чили',
            'en' => 'Chile',
        ],
        'sd' => [
            'ru' => 'Судан',
            'en' => 'Sudan',
        ],
        'bo' => [
            'ru' => 'Боливия',
            'en' => 'Bolivia',
        ],
        'it' => [
            'ru' => 'Италия',
            'en' => 'Italy',
        ],
        'ar' => [
            'ru' => 'Аргентина',
            'en' => 'Argentina',
        ],
        'sl' => [
            'ru' => 'Сьерра-Леоне',
            'en' => 'Sierra Leone',
        ],
        'id' => [
            'ru' => 'Индонезия',
            'en' => 'Indonesia',
        ],
        'sn' => [
            'ru' => 'Сенегал',
            'en' => 'Senegal',
        ],
        'af' => [
            'ru' => 'Афганистан',
            'en' => 'Afghanistan',
        ],
        'ug' => [
            'ru' => 'Уганда',
            'en' => 'Uganda',
        ],
        'ba' => [
            'ru' => 'Босния и Герцеговина',
            'en' => 'Bosnia and Herzegovina',
        ],
        'ga' => [
            'ru' => 'Габон',
            'en' => 'Gabon',
        ],
        'ao' => [
            'ru' => 'Ангола',
            'en' => 'Angola',
        ],
        'uz' => [
            'ru' => 'Узбекистан',
            'en' => 'Uzbekistan',
        ],
        'na' => [
            'ru' => 'Намибия',
            'en' => 'Namibia',
        ],
        'tr' => [
            'ru' => 'Турция',
            'en' => 'Turkey',
        ],
        'vn' => [
            'ru' => 'Вьетнам',
            'en' => 'Vietnam',
        ],
        'zm' => [
            'ru' => 'Замбия',
            'en' => 'Zambia',
        ],
        've' => [
            'ru' => 'Венесуэла',
            'en' => 'Venezuela',
        ],
        'eg' => [
            'ru' => 'Египет',
            'en' => 'Egypt',
        ],
        'ie' => [
            'ru' => 'Ирландия',
            'en' => 'Ireland',
        ],
        'ch' => [
            'ru' => 'Швейцария',
            'en' => 'Switzerland',
        ],
        'rs' => [
            'ru' => 'Сербия',
            'en' => 'Serbia',
        ],
        'pe' => [
            'ru' => 'Перу',
            'en' => 'Peru',
        ],
        'sk' => [
            'ru' => 'Словакия',
            'en' => 'Slovakia',
        ],
        'dk' => [
            'ru' => 'Дания',
            'en' => 'Denmark',
        ],
        'az' => [
            'ru' => 'Азербайджан',
            'en' => 'Azerbaijan',
        ],
        'hk' => [
            'ru' => 'Гонконг',
            'en' => 'Hong Kong',
        ],
        'hr' => [
            'ru' => 'Хорватия',
            'en' => 'Croatia',
        ],
        'hu' => [
            'ru' => 'Венгрия',
            'en' => 'Hungary',
        ],
        'ee' => [
            'ru' => 'Эстония',
            'en' => 'Estonia',
        ],
        'sz' => [
            'ru' => 'Свазиленд',
            'en' => 'Swaziland',
        ],
        'in' => [
            'ru' => 'Индия',
            'en' => 'India',
        ],
        're' => [
            'ru' => 'Реюньон',
            'en' => 'Reunion',
        ],
        'is' => [
            'ru' => 'Исландия',
            'en' => 'Iceland',
        ],
        'il' => [
            'ru' => 'Израиль',
            'en' => 'Israel',
        ],
        'at' => [
            'ru' => 'Австрия',
            'en' => 'Austria',
        ],
        'jm' => [
            'ru' => 'Ямайка',
            'en' => 'Jamaica',
        ],
        'mt' => [
            'ru' => 'Мальта',
            'en' => 'Malta',
        ],
        'pt' => [
            'ru' => 'Португалия',
            'en' => 'Portugal',
        ],
        'cy' => [
            'ru' => 'Кипр',
            'en' => 'Cyprus',
        ],
        'kz' => [
            'ru' => 'Казахстан',
            'en' => 'Kazakhstan',
        ],
        'kg' => [
            'ru' => 'Кыргызстан',
            'en' => 'Kyrgyzstan',
        ],
        'tm' => [
            'ru' => 'Туркменистан',
            'en' => 'Turkmenistan',
        ],
        'ki' => [
            'ru' => 'Кирибати',
            'en' => 'Kiribati',
        ],
        'kh' => [
            'ru' => 'Камбоджа',
            'en' => 'Cambodia',
        ],
        'an' => [
            'ru' => 'Нидерландские Антильские острова',
            'en' => 'Netherlands Antilles',
        ],
        'st' => [
            'ru' => 'Сан-Томе и Принсипи',
            'en' => 'Sao Tome and Principe',
        ],
        'lt' => [
            'ru' => 'Литва',
            'en' => 'Lithuania',
        ],
        'mv' => [
            'ru' => 'Мальдивы',
            'en' => 'Maldives',
        ],
        'mw' => [
            'ru' => 'Малави',
            'en' => 'Malawi',
        ],
        'md' => [
            'ru' => 'Молдавия',
            'en' => 'Moldavia',
        ],
        'me' => [
            'ru' => 'Черногория',
            'en' => 'Montenegro',
        ],
        'sc' => [
            'ru' => 'Сейшельские Острова',
            'en' => 'Seychelles',
        ],
        'bg' => [
            'ru' => 'Болгария',
            'en' => 'Bulgaria',
        ],
        'pg' => [
            'ru' => 'Папуа-Новая Гвинея',
            'en' => 'Papua New Guinea',
        ],
        'lv' => [
            'ru' => 'Латвия',
            'en' => 'Latvia',
        ],
        'nz' => [
            'ru' => 'Новая Зеландия',
            'en' => 'New Zealand',
        ],
        'ph' => [
            'ru' => 'Филиппины',
            'en' => 'Philippines',
        ],
        'se' => [
            'ru' => 'Швеция',
            'en' => 'Sweden',
        ],
        'aw' => [
            'ru' => 'Аруба',
            'en' => 'Aruba',
        ],
        'am' => [
            'ru' => 'Армения',
            'en' => 'Armenia',
        ],
        'td' => [
            'ru' => 'Чад',
            'en' => 'Chad',
        ],
        'np' => [
            'ru' => 'Непал',
            'en' => 'Nepal',
        ],
        'pa' => [
            'ru' => 'Панама',
            'en' => 'Panama',
        ],
        'gw' => [
            'ru' => 'Гвинея-Бисау',
            'en' => 'Guinea bissau',
        ],
        'tz' => [
            'ru' => 'Танзания',
            'en' => 'Tanzania',
        ],
        'bf' => [
            'ru' => 'Буркина-Фасо',
            'en' => 'Burkina Faso',
        ],
        'nl' => [
            'ru' => 'Нидерланды',
            'en' => 'Netherlands',
        ],
        'ec' => [
            'ru' => 'Эквадор',
            'en' => 'Ecuador',
        ],
        'uy' => [
            'ru' => 'Уругвай',
            'en' => 'Uruguay',
        ],
        'vu' => [
            'ru' => 'Вануату',
            'en' => 'Vanuatu',
        ],
        'bd' => [
            'ru' => 'Бангладеш',
            'en' => 'Bangladesh',
        ],
        'ge' => [
            'ru' => 'Грузия',
            'en' => 'Georgia',
        ],
        'sv' => [
            'ru' => 'Эль-Сальвадор',
            'en' => 'El salvador',
        ],
        'ne' => [
            'ru' => 'Нигер',
            'en' => 'Niger',
        ],
        'jo' => [
            'ru' => 'Иордания',
            'en' => 'Jordan',
        ],
        'my' => [
            'ru' => 'Малайзия',
            'en' => 'Malaysia',
        ],
        'mk' => [
            'ru' => 'Республика Македония',
            'en' => 'Republic of Macedonia',
        ],
        'zw' => [
            'ru' => 'Зимбабве',
            'en' => 'Zimbabwe',
        ],
        'lb' => [
            'ru' => 'Ливан',
            'en' => 'Lebanon',
        ],
        'gt' => [
            'ru' => 'Гватемала',
            'en' => 'Guatemala',
        ],
        'bh' => [
            'ru' => 'Бахрейн',
            'en' => 'Bahrain',
        ],
        'bb' => [
            'ru' => 'Барбадос',
            'en' => 'Barbados',
        ],
        'bw' => [
            'ru' => 'Ботсвана',
            'en' => 'Botswana',
        ],
        'pf' => [
            'ru' => 'Французская Полинезия',
            'en' => 'French polynesia',
        ],
        'cn' => [
            'ru' => 'Китай',
            'en' => 'China',
        ],
        'bz' => [
            'ru' => 'Белиз',
            'en' => 'Belize',
        ],
        'mz' => [
            'ru' => 'Мозамбик',
            'en' => 'Mozambique',
        ],
        'mh' => [
            'ru' => 'Маршалловы острова',
            'en' => 'Marshall Islands',
        ],
        'dz' => [
            'ru' => 'Алжир',
            'en' => 'Algeria',
        ],
        'dj' => [
            'ru' => 'Джибути',
            'en' => 'Djibouti',
        ],
        'et' => [
            'ru' => 'Эфиопия',
            'en' => 'Ethiopia',
        ],
        'fj' => [
            'ru' => 'Фиджи',
            'en' => 'Fiji',
        ],
        'ml' => [
            'ru' => 'Мали',
            'en' => 'Mali',
        ],
        'fo' => [
            'ru' => 'Фарерские острова',
            'en' => 'Faroe islands',
        ],
        'mr' => [
            'ru' => 'Мавритания',
            'en' => 'Mauritania',
        ],
        'cm' => [
            'ru' => 'Камерун',
            'en' => 'Cameroon',
        ],
        'gn' => [
            'ru' => 'Гвинея',
            'en' => 'Guinea',
        ],
        'by' => [
            'ru' => 'Белоруссия',
            'en' => 'Belorussia',
        ],
        'gf' => [
            'ru' => 'Французская Гвиана',
            'en' => 'French Guiana',
        ],
        'ht' => [
            'ru' => 'Гаити',
            'en' => 'Haiti',
        ],
        'km' => [
            'ru' => 'Коморы',
            'en' => 'Comoros',
        ],
        'hn' => [
            'ru' => 'Гондурас',
            'en' => 'Honduras',
        ],
        'no' => [
            'ru' => 'Норвегия',
            'en' => 'Norway',
        ],
        'mu' => [
            'ru' => 'Маврикий',
            'en' => 'Mauritius',
        ],
        'mg' => [
            'ru' => 'Мадагаскар',
            'en' => 'Madagascar',
        ],
        'mn' => [
            'ru' => 'Монголия',
            'en' => 'Mongolia',
        ],
        'bi' => [
            'ru' => 'Бурунди',
            'en' => 'Burundi',
        ],
        'lk' => [
            'ru' => 'Шри-Ланка',
            'en' => 'Sri Lanka',
        ],
        'ro' => [
            'ru' => 'Румыния',
            'en' => 'Romania',
        ],
        'rw' => [
            'ru' => 'Руанда',
            'en' => 'Rwanda',
        ],
        'ni' => [
            'ru' => 'Никарагуа',
            'en' => 'Nicaragua',
        ],
        'tc' => [
            'ru' => 'Теркс и Кайкос',
            'en' => 'Turks and Caicos',
        ],
        'bs' => [
            'ru' => 'Багамские Острова',
            'en' => 'Bahamas',
        ],
        'sr' => [
            'ru' => 'Суринам',
            'en' => 'Suriname',
        ],
        'cv' => [
            'ru' => 'Кабо-Верде',
            'en' => 'Cape Verde',
        ],
        'lu' => [
            'ru' => 'Люксембург',
            'en' => 'Luxembourg',
        ],
        'om' => [
            'ru' => 'Оман',
            'en' => 'Oman',
        ],
        'ag' => [
            'ru' => 'Антигуа и Барбуда',
            'en' => 'Antigua and Barbuda',
        ],
        'tt' => [
            'ru' => 'Тринидад и Тобаго',
            'en' => 'Trinidad and Tobago',
        ],
        'ky' => [
            'ru' => 'Острова Кайман',
            'en' => 'Cayman Islands',
        ],
        'cf' => [
            'ru' => 'Центральноафриканская Республика',
            'en' => 'Central African Republic',
        ],
        'pl' => [
            'ru' => 'Польша',
            'en' => 'Poland',
        ],
        'tw' => [
            'ru' => 'Китайская Республика',
            'en' => 'Republic of China',
        ],
        'py' => [
            'ru' => 'Парагвай',
            'en' => 'Paraguay',
        ],
        'cu' => [
            'ru' => 'Куба',
            'en' => 'Cuba',
        ],
        'bt' => [
            'ru' => 'Бутан',
            'en' => 'Butane',
        ],
        'gq' => [
            'ru' => 'Экваториальная Гвинея',
            'en' => 'Equatorial Guinea',
        ],
        'er' => [
            'ru' => 'Эритрея',
            'en' => 'Eritrea',
        ],
        'tn' => [
            'ru' => 'Тунис',
            'en' => 'Tunisia',
        ],
        'cr' => [
            'ru' => 'Коста-Рика',
            'en' => 'Costa Rica',
        ],
        'lc' => [
            'ru' => 'Сент-Люсия',
            'en' => 'Saint Lucia',
        ],
        'mc' => [
            'ru' => 'Монако',
            'en' => 'Monaco',
        ],
        'sb' => [
            'ru' => 'Соломоновы острова',
            'en' => 'Solomon islands',
        ],
        'iq' => [
            'ru' => 'Ирак',
            'en' => 'Iraq',
        ],
        'kw' => [
            'ru' => 'Кувейт',
            'en' => 'Kuwait',
        ],
        'bm' => [
            'ru' => 'Бермудские Острова',
            'en' => 'Bermuda',
        ],
        'ms' => [
            'ru' => 'Монтсеррат',
            'en' => 'Montserrat',
        ],
        'sa' => [
            'ru' => 'Саудовская Аравия',
            'en' => 'Saudi Arabia',
        ],
        'nr' => [
            'ru' => 'Науру',
            'en' => 'Nauru',
        ],
        'pw' => [
            'ru' => 'Палау',
            'en' => 'Palau',
        ],
        'to' => [
            'ru' => 'Тонга',
            'en' => 'Tonga',
        ],
        'ws' => [
            'ru' => 'Самоа',
            'en' => 'Samoa',
        ],
        'qa' => [
            'ru' => 'Катар',
            'en' => 'Qatar',
        ],
        'gy' => [
            'ru' => 'Гайана',
            'en' => 'Guyana',
        ],
        'ck' => [
            'ru' => 'Острова Кука',
            'en' => 'Cook Islands',
        ],
        'bn' => [
            'ru' => 'Бруней-Даруссалам',
            'en' => 'Brunei Darussalam',
        ],
        'vc' => [
            'ru' => 'Сент-Винсент и Гренадины',
            'en' => 'Saint Vincent and the Grenadines',
        ],
        'ye' => [
            'ru' => 'Йемен',
            'en' => 'Yemen',
        ],
        'tj' => [
            'ru' => 'Таджикистан',
            'en' => 'Tajikistan',
        ],
        'kn' => [
            'ru' => 'Сент-Китс и Невис',
            'en' => 'Saint Kitts and Nevis',
        ],
        'ci' => [
            'ru' => 'Кот-д\'Ивуар',
            'en' => 'Ivory Coast',
        ],
        'mm' => [
            'ru' => 'Мьянма',
            'en' => 'Myanmar',
        ],
        'kr' => [
            'ru' => 'Республика Корея',
            'en' => 'The Republic of Korea',
        ],
        'gp' => [
            'ru' => 'Гваделупа',
            'en' => 'Guadeloupe',
        ],
        'cd' => [
            'ru' => 'Демократическая Республика Конго',
            'en' => 'Democratic Republic of Congo',
        ],
        'sy' => [
            'ru' => 'Сирия',
            'en' => 'Syria',
        ],
        'as' => [
            'ru' => 'Американское Самоа',
            'en' => 'American Samoa',
        ],
        'vg' => [
            'ru' => 'Британские Виргинские Острова',
            'en' => 'British Virgin Islands',
        ],
        'pr' => [
            'ru' => 'Пуэрто-Рико',
            'en' => 'Puerto rico',
        ],
        'la' => [
            'ru' => 'Лаос',
            'en' => 'Laos',
        ],
        'gi' => [
            'ru' => 'Гибралтар',
            'en' => 'Gibraltar',
        ],
        'mq' => [
            'ru' => 'Мартиника',
            'en' => 'Martinique',
        ],
        'ai' => [
            'ru' => 'Ангилья',
            'en' => 'Anguilla',
        ],
        'aq' => [
            'ru' => 'Антарктида',
            'en' => 'Antarctica',
        ],
        'cg' => [
            'ru' => 'Республика Конго',
            'en' => 'Republic of the Congo',
        ],
        'cc' => [
            'ru' => 'Кокосовые острова',
            'en' => 'Cocos Islands',
        ],
        'mp' => [
            'ru' => 'Северные Марианские острова',
            'en' => 'Northern Mariana Islands',
        ],
        'dm' => [
            'ru' => 'Доминика',
            'en' => 'Dominica',
        ],
        'fk' => [
            'ru' => 'Фолклендские острова',
            'en' => 'Falkland islands',
        ],
        'fm' => [
            'ru' => 'Федеративные Штаты Микронезии',
            'en' => 'Federated States of Micronesia',
        ],
        'gd' => [
            'ru' => 'Гренада',
            'en' => 'Grenada',
        ],
        'gg' => [
            'ru' => 'Гернси',
            'en' => 'Guernsey',
        ],
        'gl' => [
            'ru' => 'Гренландия',
            'en' => 'Greenland',
        ],
        'gu' => [
            'ru' => 'Гуам',
            'en' => 'Guam',
        ],
        'im' => [
            'ru' => 'Остров Мэн',
            'en' => 'Isle Of Man',
        ],
        'io' => [
            'ru' => 'Британская Территория в Индийском Океане',
            'en' => 'British Indian Ocean Territory',
        ],
        'je' => [
            'ru' => 'Остров Джерси',
            'en' => 'Jersey Island',
        ],
        'kp' => [
            'ru' => 'Корейская Народно-Демократическая Республика',
            'en' => 'Democratic People\'s Republic of Korea',
        ],
        'cx' => [
            'ru' => 'Остров Рождества',
            'en' => 'Christmas Island',
        ],
        'ls' => [
            'ru' => 'Лесото',
            'en' => 'Lesotho',
        ],
        'mo' => [
            'ru' => 'Макао',
            'en' => 'Macau',
        ],
        'yt' => [
            'ru' => 'Майотта',
            'en' => 'Mayotte',
        ],
        'nc' => [
            'ru' => 'Новая Каледония',
            'en' => 'New Caledonia',
        ],
        'nu' => [
            'ru' => 'Ниуэ',
            'en' => 'Niue',
        ],
        'nf' => [
            'ru' => 'Остров Норфолк',
            'en' => 'Norfolk Island',
        ],
        'pm' => [
            'ru' => 'Сен-Пьер и Микелон',
            'en' => 'St. Pierre and Miquelon',
        ],
        'sh' => [
            'ru' => 'Острова Святой Елены, Вознесения и Тристан-да-Кунья',
            'en' => 'Saint Helena, Ascension and Tristan da Cunha',
        ],
        'so' => [
            'ru' => 'Сомали',
            'en' => 'Somalia',
        ],
        'gs' => [
            'ru' => 'Южная Георгия и Южные Сандвичевы Острова',
            'en' => 'South Georgia and the South Sandwich Islands',
        ],
        'tv' => [
            'ru' => 'Тувалу',
            'en' => 'Tuvalu',
        ],
        'vi' => [
            'ru' => 'Американские Виргинские Острова',
            'en' => 'United States Virgin Islands',
        ],
        'wf' => [
            'ru' => 'Уоллис и Футуна',
            'en' => 'Wallis and Futuna',
        ],
        'eh' => [
            'ru' => 'Западная Сахара',
            'en' => 'West Sahara',
        ],
        'ss' => [
            'ru' => 'Южный Судан',
            'en' => 'South Sudan',
        ],
        'ps' => [
            'ru' => 'Палестина',
            'en' => 'Palestine',
        ],
        'tl' => [
            'ru' => 'Восточный Тимор',
            'en' => 'East Timor',
        ],
        'ad' => [
            'ru' => 'Андорра',
            'en' => 'Andorra',
        ],
        'um' => [
            'ru' => 'Внешние малые острова США',
            'en' => 'US Outside Small Islands',
        ],
        'sx' => [
            'ru' => 'Нидерландские Антильские острова',
            'en' => 'Netherlands Antilles',
        ],
        'li' => [
            'ru' => 'Лихтенштейн',
            'en' => 'Liechtenstein',
        ],
        'sm' => [
            'ru' => 'Сан-Марино',
            'en' => 'San marino',
        ],
        'va' => [
            'ru' => 'Ватикан',
            'en' => 'Vatican',
        ],
        'hm' => [
            'ru' => 'Острова Херд и Макдональд',
            'en' => 'Heard and McDonald Islands',
        ],
        'pn' => [
            'ru' => 'Острова Питкэрн',
            'en' => 'Pitcairn Islands',
        ],
        'tk' => [
            'ru' => 'Токелау',
            'en' => 'Tokelau',
        ],
        'tf' => [
            'ru' => 'Французские Южные и Антарктические территории',
            'en' => 'French Southern and Antarctic Territories',
        ],
    ];
}
