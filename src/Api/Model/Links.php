<?php

namespace Amocrm\Api\Model;

/**
 * Коллекция ссылок.
 *
 * @method Link first()
 * @method Link last()
 * @method Link current()
 * @method Link get($key)
 * @method Link filterOneByField($name, $value)
 */
class Links extends AbstractModelCollection
{
    /**
     * Возвращает массив IDs из from-элементов.
     *
     * @return array
     */
    public function getFromIds(): array
    {
        return array_map(function (Link $link) {
            return $link->getFromId();
        }, $this->getValues());
    }

    /**
     * Возвращает массив IDs из to-элементов.
     *
     * @return array
     */
    public function getToIds(): array
    {
        return array_map(function (Link $link) {
            return $link->getToId();
        }, $this->getValues());
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Link::class;
    }
}