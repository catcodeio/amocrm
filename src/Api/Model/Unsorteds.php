<?php

namespace Amocrm\Api\Model;

/**
 * Коллекция неразобранных.
 *
 * @method Webhook first()
 * @method Webhook last()
 * @method Webhook current()
 * @method Webhook get($key)
 * @method Webhook filterOneByField($name, $value)
 */
class Unsorteds extends AbstractModelCollection
{
    /**
     * Возвращает массив IDs задач в коллекции.
     *
     * @return array
     */
    public function getUids(): array
    {
        return array_map(function (Unsorted $webhook) {
            return $webhook->getUid();
        }, $this->getValues());
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Unsorted::class;
    }
}