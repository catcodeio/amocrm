<?php

namespace Amocrm\Api\Model;

use Amocrm\Api\Model\Common\LastModifiedTrait;
use DateTime;

/**
 * @method int getContactId()
 * @method int getLeadId()
 */
class ContactLink extends AbstractModel
{
    use LastModifiedTrait;
}
