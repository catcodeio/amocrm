<?php

namespace Amocrm\Api\Model;

/**
 * @method int    getId()
 * @method self   setId(int $id)
 * @method int    getRequestId()
 * @method self   setRequestId(int $requestId)
 * @method string getName()
 * @method self   setName(string $name)
 */
class AbstractEntity extends AbstractModel
{
    /**
     * @inheritdoc
     */
    public function __clone()
    {
        parent::__clone();

        if (isset($this->modified['id'])) {
            $this->modified['id'] = null;
            $this->initial['id']  = null;
        }
    }
}
