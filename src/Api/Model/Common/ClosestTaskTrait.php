<?php

namespace Amocrm\Api\Model\Common;

use DateTime;

/**
 * Трейт для общего метода.
 *
 * @method mixed __call(string $method, array $params = null)
 */
trait ClosestTaskTrait
{
    /**
     * @return DateTime|null
     */
    public function getClosestTask(): ?DateTime
    {
        $result = $this->__call(__FUNCTION__);

        if (!$result) {
            // В API v2 иначе называется, проверим его.
            $result = $this->__call('getClosestTaskAt');

            if (!$result) {
                return null;
            }
        }

        return (new DateTime())->setTimestamp($result);
    }
}