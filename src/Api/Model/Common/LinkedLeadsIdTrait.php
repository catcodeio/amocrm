<?php

namespace Amocrm\Api\Model\Common;

/**
 * Трейт для методов работы со списком сделок.
 *
 * @method array getLinkedLeadsId()
 * @method self  setLinkedLeadsId(array $linkedLeadsId)
 */
trait LinkedLeadsIdTrait
{
    /**
     * Добавляет в список связанных сделок ещё один ID.
     *
     * @param int $leadId
     *
     * @return $this
     */
    public function addLeadId(int $leadId)
    {
        $leadsId = $this->getLinkedLeadsId() ?? [];

        // Добавляем только если это не повтор.
        if (array_search($leadId, $leadsId) !== false) {
            return $this;
        }

        $leadsId[] = $leadId;

        $this->setLinkedLeadsId($leadsId);

        return $this;
    }

    /**
     * Удаляет из списка связанных сделок один ID.
     *
     * @param int $leadId
     *
     * @return $this
     */
    public function removeLeadId(int $leadId)
    {
        $leadsId = $this->getLinkedLeadsId() ?? [];

        $key = array_search($leadId, $leadsId);

        if ($key !== false) {
            unset($leadsId[$key]);
            $leadsId = array_values($leadsId);
        }

        $this->setLinkedLeadsId($leadsId);

        return $this;
    }
}