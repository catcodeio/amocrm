<?php

namespace Amocrm\Api\Model\Common;

use DateTime;

/**
 * Трейт для общих методов.
 *
 * @method mixed __call(string $method, array $params = null)
 */
trait LastModifiedTrait
{
    /**
     * Из amoCRM приходит timestamp в временной зоне UTC.
     *
     * @return DateTime
     */
    public function getLastModified(): DateTime
    {
        return (new DateTime())->setTimestamp($this->__call(__FUNCTION__));
    }

    /**
     * @param DateTime $lastModified
     *
     * @return $this
     */
    public function setLastModified(DateTime $lastModified)
    {
        $this->__call(__FUNCTION__, [$lastModified->format('U')]);

        return $this;
    }
}