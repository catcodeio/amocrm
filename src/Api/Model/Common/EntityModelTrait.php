<?php

namespace Amocrm\Api\Model\Common;

use DateTime;

/**
 * Трейт для общих методов основных сущностей.
 */
trait EntityModelTrait
{
    /**
     * @inheritdoc
     */
    public function getModifiedForApi(): array
    {
        $result = parent::getModifiedForApi();

        if (!isset($result['id']) && isset($this->initial['id'])) {
            $result['id'] = $this->initial['id'];
        }

        if (!isset($result['last_modified'])) {
            $result['last_modified'] = (new DateTime('now'))->format('U');
        }

        return $result;
    }
}