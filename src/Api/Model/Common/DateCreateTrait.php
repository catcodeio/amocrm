<?php

namespace Amocrm\Api\Model\Common;

use DateTime;

/**
 * Трейт для общих методов.
 *
 * @method mixed __call(string $method, array $params = null)
 */
trait DateCreateTrait
{
    /**
     * Из amoCRM приходит timestamp в временной зоне UTC.
     *
     * @return DateTime
     */
    public function getDateCreate(): DateTime
    {
        return (new DateTime())->setTimestamp($this->__call(__FUNCTION__));
    }

    /**
     * @param DateTime $dateCreate
     *
     * @return $this
     */
    public function setDateCreate(DateTime $dateCreate)
    {
        $this->__call(__FUNCTION__, [$dateCreate->format('U')]);

        return $this;
    }
}