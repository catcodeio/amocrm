<?php

namespace Amocrm\Api\Model\Common;

use Amocrm\Api\Helper\ElementType;

/**
 * Трейт для общих методов.
 *
 * @method mixed  __call(string $method, array $params = null)
 * @method static setElementId(int $elementId)
 */
trait ElementTypeTrait
{
    /**
     * Тип сущности, для которого создаётся поле. Принимает константы из ElementType.
     *
     * @param string|null $elementType
     *
     * @return static
     */
    public function setElementType(?string $elementType)
    {
        $this->__call(__FUNCTION__, [ElementType::toNumber($elementType)]);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getElementType(): ?string
    {
        return ElementType::fromNumber($this->__call(__FUNCTION__));
    }

    /**
     * Устанавливает создаваемую сущность для указанной сделки.
     *
     * @param int $leadId
     *
     * @return static
     */
    public function setLeadId(int $leadId)
    {
        $this->setElementId($leadId);
        $this->setElementType(ElementType::LEAD);

        return $this;
    }

    /**
     * Устанавливает создаваемую сущность для указанной компании.
     *
     * @param int $companyId
     *
     * @return static
     */
    public function setCompanyId(int $companyId)
    {
        $this->setElementId($companyId);
        $this->setElementType(ElementType::COMPANY);

        return $this;
    }

    /**
     * Устанавливает создаваемую сущность для указанного контакта.
     *
     * @param int $contactId
     *
     * @return static
     */
    public function setContactId(int $contactId)
    {
        $this->setElementId($contactId);
        $this->setElementType(ElementType::CONTACT);

        return $this;
    }

    /**
     * Устанавливает создаваемую сущность для указанного покупателя.
     *
     * @param int $customerId
     *
     * @return static
     */
    public function setCustomerId(int $customerId)
    {
        $this->setElementId($customerId);
        $this->setElementType(ElementType::CUSTOMER);

        return $this;
    }
}