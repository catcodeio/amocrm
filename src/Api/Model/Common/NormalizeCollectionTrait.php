<?php

namespace Amocrm\Api\Model\Common;

use Amocrm\Api\Collection\CollectionInterface;
use Amocrm\Api\Model\AbstractModelCollection;

/**
 * Трейт только для общего метода нормализации.
 */
trait NormalizeCollectionTrait
{
    /**
     * Нормализация входных параметров при сохранении, чтобы как минимум там был массив из значений т.к. можно посылать
     * один элемент, массив или коллекцию.
     *
     * @param mixed  $collection
     * @param string $classCollection Классы имплементирующие CollectionInterface.
     *
     * @return mixed
     */
    protected function normalizeCollection($collection, $classCollection = null)
    {
        if ($collection && !($collection instanceof AbstractModelCollection)) {
            $collection = is_array($collection) ? $collection : [$collection];

            if ($classCollection && in_array(CollectionInterface::class, class_implements($classCollection))) {
                $collection = call_user_func($classCollection .'::create', $collection);
            }
        }

        return $collection;
    }
}