<?php

namespace Amocrm\Api\Model\Common;

/**
 * Трейт для общих методов контактов и компаний.
 *
 * @method mixed __call(string $method, array $params = null)
 */
trait ContactTrait
{
    /**
     * @return array|null
     */
    public function getLeadsId(): ?array
    {
        $result = $this->__call('getLeads');

        if (!isset($result['id'])) {
            return null;
        }

        sort($result['id']);

        return $result['id'];
    }

    /**
     * @return array|null
     */
    public function getCustomersId(): ?array
    {
        $result = $this->__call('getCustomers');

        if (!isset($result['id'])) {
            return null;
        }

        sort($result['id']);

        return $result['id'];
    }
}