<?php

namespace Amocrm\Api\Model\Common;

/**
 * Трейт для методов-синономов.
 *
 * @method mixed __call(string $method, array $params = null)
 */
trait ResponsibleUserTrait
{
    /**
     * Синоним getResponsibleUserId() для краткости.
     *
     * @return int
     */
    public function getUserId(): int
    {
        return $this->__call('getResponsibleUserId');
    }

    /**
     * Синоним setResponsibleUserId() для краткости.
     *
     * @param int $userId
     *
     * @return $this
     */
    public function setUserId(int $userId)
    {
        $this->__call('setResponsibleUserId', [$userId]);

        return $this;
    }
}