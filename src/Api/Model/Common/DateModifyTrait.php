<?php

namespace Amocrm\Api\Model\Common;

use DateTime;

/**
 * Трейт для общих методов.
 *
 * @method mixed __call(string $method, array $params = null)
 */
trait DateModifyTrait
{
    /**
     * Из amoCRM приходит timestamp в временной зоне UTC.
     *
     * @return DateTime
     */
    public function getDateModify(): DateTime
    {
        return (new DateTime())->setTimestamp($this->__call(__FUNCTION__));
    }

    /**
     * @param DateTime $dateModify
     *
     * @return $this
     */
    public function setDateModify(DateTime $dateModify)
    {
        $this->__call(__FUNCTION__, [$dateModify->format('U')]);

        return $this;
    }
}