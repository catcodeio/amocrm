<?php

namespace Amocrm\Api\Model;

use Amocrm\Api\Helper\ElementType;

/**
 * @method int  getFromId()
 * @method Link setFromId(int $fromId)
 * @method int  getToId()
 * @method Link setToId(int $toId)
 * @method int  getFromRequestId()
 * @method Link setFromRequestId(int $fromRequestId)
 * @method int  getToRequestId()
 * @method Link setToRequestId(int $toRequestId)
 * @method int  getFromCatalogId()
 * @method Link setFromCatalogId(int $fromCatalogId)
 * @method int  getToCatalogId()
 * @method Link setToCatalogId(int $toCatalogId)
 * @method int  getQuantity()
 * @method Link setQuantity(int $quantity)
 */
class Link extends AbstractModel
{
    /**
     * Тип сущности, к которой осуществленна привязка. Принимает константы из
     * ElementType. Тут используются все типы сущностей: leads, contacts,
     * companies, customers, catalog_elements.
     *
     * @param string|null $elementType
     *
     * @return Link
     */
    public function setFrom(?string $elementType): Link
    {
        $this->__call(__FUNCTION__, [ElementType::toPlural($elementType)]);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFrom(): ?string
    {
        return ElementType::fromPlural($this->__call(__FUNCTION__));
    }

    /**
     * Тип приязанной сущности. Принимает константы из ElementType. Тут
     * используются все типы сущностей: leads, contacts, companies, customers,
     * catalog_elements.
     *
     * @param string|null $elementType
     *
     * @return Link
     */
    public function setTo(?string $elementType): Link
    {
        $this->__call(__FUNCTION__, [ElementType::toPlural($elementType)]);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTo(): ?string
    {
        return ElementType::fromPlural($this->__call(__FUNCTION__));
    }
}
