<?php

namespace Amocrm\Api\Model;

use Amocrm\Api\Model\Common\ClosestTaskTrait;
use Amocrm\Api\Model\Common\NormalizeCollectionTrait;
use Amocrm\Api\Model\Common\EntityModelTrait;
use Amocrm\Api\Model\Common\ResponsibleUserTrait;
use Amocrm\Api\Model\CustomField\CustomFieldTrait;
use Amocrm\Api\Model\Common\DateCreateTrait;
use Amocrm\Api\Model\Common\LastModifiedTrait;
use Amocrm\Api\Model\Tag\TagTrait;
use DateTime;

/**
 * @method Lead setPrice(int $price)
 * @method int  getResponsibleUserId()
 * @method Lead setResponsibleUserId(int $responsibleUserId)
 * @method Lead setLinkedCompanyId(int $linkedCompanyId)
 * @method int  getStatusId()
 * @method Lead setStatusId(int $statusId)
 * @method int  getMainContactId()
 * @method Lead setMainContactId(int $mainContactId)
 * @method int  getPipelineId()
 * @method Lead setPipelineId(int $pipelineId)
 * @method int  getLossReasonId()
 * @method int  getDeleted()
 * @method int  getCreatedUserId()
 * @method int  getModifiedUserId()
 * @method int  getGroupId()
 * @method int  getAccountId()
 */
class Lead extends AbstractEntity
{
    use CustomFieldTrait;
    use TagTrait;
    use DateCreateTrait;
    use LastModifiedTrait;
    use ResponsibleUserTrait;
    use EntityModelTrait;
    use ClosestTaskTrait;
    use NormalizeCollectionTrait;

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return (int)$this->__call(__FUNCTION__) ?? 0;
    }

    /**
     * @return int|null
     */
    public function getLinkedCompanyId(): ?int
    {
        $result = $this->__call(__FUNCTION__);

        // Иногда тут может быть '0' и тогда тоже венём null.
        if (!$result) {
            return null;
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return (bool)$this->__call('getDeleted');
    }

    /**
     * @return DateTime|null
     */
    public function getDateClose(): ?DateTime
    {
        $result = $this->__call(__FUNCTION__);

        if (!$result) {
            return null;
        }

        return (new DateTime())->setTimestamp($result);
    }
}
