<?php

namespace Amocrm\Api\Model;

/**
 * Коллекция контактов.
 *
 * @method Contact first()
 * @method Contact last()
 * @method Contact current()
 * @method Contact get($key)
 * @method Contact filterOneByField($name, $value)
 */
class Contacts extends AbstractModelCollection
{
    /**
     * Возвращает массив IDs контактов в коллекции.
     *
     * @return array
     */
    public function getIds(): array
    {
        return array_map(function (Contact $contact) {
            return $contact->getId();
        }, $this->getValues());
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Contact::class;
    }
}