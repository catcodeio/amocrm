<?php

namespace Amocrm\Api\Model;

/**
 * Коллекция примечаний.
 *
 * @method Note first()
 * @method Note last()
 * @method Note current()
 * @method Note get($key)
 * @method Note filterOneByField($name, $value)
 */
class Notes extends AbstractModelCollection
{
    /**
     * Возвращает массив IDs примечаний в коллекции.
     *
     * @return array
     */
    public function getIds(): array
    {
        return array_map(function (Note $note) {
            return $note->getId();
        }, $this->getValues());
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Note::class;
    }
}