<?php

namespace Amocrm\Api\Model;

/**
 * Коллекция компаний.
 *
 * @method Company first()
 * @method Company last()
 * @method Company current()
 * @method Company get($key)
 * @method Company filterOneByField($name, $value)
 */
class Companies extends AbstractModelCollection
{
    /**
     * Возвращает массив IDs компаний в коллекции.
     *
     * @return array
     */
    public function getIds(): array
    {
        return array_map(function (Company $company) {
            return $company->getId();
        }, $this->getValues());
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Company::class;
    }
}