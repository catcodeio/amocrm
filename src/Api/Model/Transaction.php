<?php

namespace Amocrm\Api\Model;

use Amocrm\Api\Model\Common\DateCreateTrait;
use Amocrm\Api\Model\Common\DateModifyTrait;
use DateTime;

/**
 * @method int         getCustomerRequestId()
 * @method Transaction setCustomerRequestId(int $customerRequestId) Используется для element/sync
 * @method int         getCreatedBy()
 * @method int         getModifiedBy()
 * @method Transaction setModifiedBy(int $modifiedBy)
 * @method int         getAccountId()
 * @method int         getCustomerId()
 * @method Transaction setCustomerId(int $customerId)
 * @method int         getComment()
 * @method Transaction setComment(string $comment)
 * @method int         getDeleted()
 * @method int         getPrice()
 * @method Transaction setPrice(int $price)
 */
class Transaction extends AbstractEntity
{
    use DateCreateTrait;
    use DateModifyTrait;

    /**
     * @return DateTime|null
     */
    public function getDate(): ?DateTime
    {
        $date = $this->__call(__FUNCTION__);

        if ($date) {
            return (new DateTime())->setTimestamp($date);
        }

        return null;
    }

    /**
     * @param DateTime $date
     *
     * @return $this
     */
    public function setDate(DateTime $date)
    {
        $this->__call(__FUNCTION__, [$date->format('U')]);

        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return (bool)$this->__call('getDeleted');
    }

    /**
     * @inheritdoc
     */
    public function getModifiedForApi(): array
    {
        $result = parent::getModifiedForApi();

        if ($result && isset($this->initial['id'])) {
            $result['id'] = $this->initial['id'];
        }

        return $result;
    }
}
