<?php

namespace Amocrm\Api\Model\Account;

use Amocrm\Api\Helper\ElementType;
use Amocrm\Api\Model\AbstractModel;
use Amocrm\Api\Model\Account\CustomField\Subtypes;

/**
 * @method int      getId()
 * @method string   getName()
 * @method int      getTypeId()
 * @method Subtypes getSubtypes() Есть только у полей с типом 13 (адрес)
 * @method string   getCode()
 * @method string   getMultiple() Возвращает Y или N.
 * @method int      getDisabled() Возвращает 1 или 0.
 * @method int      getSort()
 * @method array    getEnums()
 * @method bool     getIsRequired()
 * @method bool     getIsDeletable()
 * @method bool     getIsVisible()
 */
class CustomField extends AbstractModel
{
    /**
     * Возвращает строковые значения типв из констант ElementType, либо ID
     * каталога.
     *
     * @return string|null
     */
    public function getElementType(): ?string
    {
        return ElementType::fromNumber($this->__call(__FUNCTION__));
    }

    /**
     * @return bool
     */
    public function isDisabled(): bool
    {
        return (bool)$this->getDisabled();
    }

    /**
     * @return bool
     */
    public function isMultiple(): bool
    {
        return $this->getMultiple() == 'Y';
    }

    /**
     * Возвращает ID конкретного подтипа из enum по его текстовому отображению.
     *
     * @param string $enumWord
     *
     * @return int|null
     */
    public function getEnumId(string $enumWord): ?int
    {
        // Там в amoCRM они кодируются. Видел как минимум &amp;.
        $enumWord = htmlentities($enumWord);
        $enums    = $this->getEnums();

        if (!$enums) {
            return null;
        }

        foreach ($enums as $id => $enum) {
            if ($enum == $enumWord) {
                return $id;
            }
        }

        return null;
    }
}