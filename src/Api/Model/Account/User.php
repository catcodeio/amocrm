<?php

namespace Amocrm\Api\Model\Account;

use Amocrm\Api\Model\AbstractModel;

/**
 * @method int    getId()
 * @method int    getGroupId()
 * @method string getName()
 * @method string getLastName()
 * @method string getLogin()
 * @method string getMailAdmin()
 * @method string getPhotoUrl()
 * @method string getLanguage()
 * @method string getPhoneNumber()
 * @method string getIsAdmin()
 * @method bool   getActive()
 * @method bool   getFreeUser()
 * @method string getUnsortedAccess()
 * @method string getCatalogsAccess()
 * @method string getRightsLeadAdd()
 * @method string getRightsLeadView()
 * @method string getRightsLeadEdit()
 * @method string getRightsLeadDelete()
 * @method string getRightsLeadExport()
 * @method string getRightsContactAdd()
 * @method string getRightsContactView()
 * @method string getRightsContactEdit()
 * @method string getRightsContactDelete()
 * @method string getRightsContactExport()
 * @method string getRightsCompanyAdd()
 * @method string getRightsCompanyView()
 * @method string getRightsCompanyEdit()
 * @method string getRightsCompanyDelete()
 * @method string getRightsCompanyExport()
 * @method string getRightsTaskEdit()
 * @method string getRightsTaskDelete()
 */
class User extends AbstractModel
{
    /**
     * Возвращает всегда какое-нибудь имя пользователя в зависимости от того
     * что заполнено в полях профиля.
     *
     * @return string
     */
    public function getFullName(): string
    {
        return $this->getName() . ($this->getLastName() ? ' ' . $this->getLastName() : '');
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->getIsAdmin() == 'Y';
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->getActive();
    }

    /**
     * @return bool
     */
    public function isFree(): bool
    {
        return $this->getFreeUser();
    }
}