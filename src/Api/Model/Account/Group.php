<?php

namespace Amocrm\Api\Model\Account;

use Amocrm\Api\Model\AbstractModel;

/**
 * @method int    getId()
 * @method string getName()
 */
class Group extends AbstractModel
{
    // ...
}