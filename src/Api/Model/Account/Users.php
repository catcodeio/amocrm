<?php

namespace Amocrm\Api\Model\Account;

use Amocrm\Api\Model\AbstractModelCollection;
use Amocrm\Api\Collection\CollectionInterface;

/**
 * Коллекция пользователей.
 *
 * @method User first()
 * @method User last()
 * @method User current()
 * @method User get($key)
 * @method User filterOneByField($name, $value)
 *
 * @method User getOneById($id)
 * @method User getOneByLogin($login)
 */
class Users extends AbstractModelCollection
{
    /**
     * Поиск пользователей по ID группе или списку IDs групп.
     *
     * @param int|int[] $groupId
     *
     * @return CollectionInterface|Users
     */
    public function getByGroupId($groupId)
    {
        $groupId = !is_array($groupId) ? [$groupId] : $groupId;

        return $this->filter(function (User $user) use ($groupId) {
            return in_array($user->getGroupId(), $groupId);
        });
    }

    /**
     * Поиск пользователей по его мылу (синоним).
     *
     * @param string $email
     *
     * @return User|null
     */
    public function getOneByEmail(string $email): ?User
    {
        return $this->getOneByLogin($email);
    }

    /**
     * Возвращает массив IDs пользователей в коллекции.
     *
     * @return array
     */
    public function getIds(): array
    {
        return array_map(function (User $user) {
            return $user->getId();
        }, $this->getValues());
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return User::class;
    }
}