<?php

namespace Amocrm\Api\Model\Account;

use Amocrm\Api\Model\AbstractModelCollection;

/**
 * Коллекция типов задач.
 *
 * @method TaskType first()
 * @method TaskType last()
 * @method TaskType current()
 * @method TaskType get($key)
 * @method TaskType filterOneByField($name, $value)
 *
 * @method TaskType getOneById($id)
 * @method TaskType getOneByName($name)
 * @method TaskType getOneByCode($code)
 */
class TaskTypes extends AbstractModelCollection
{
    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return TaskType::class;
    }
}