<?php

namespace Amocrm\Api\Model\Account;

use Amocrm\Api\Model\AbstractModelCollection;

/**
 * Коллекция групп пользователей.
 *
 * @method Group first()
 * @method Group last()
 * @method Group current()
 * @method Group get($key)
 * @method Group filterOneByField($name, $value)
 *
 * @method Group getOneById(int $id)
 */
class Groups extends AbstractModelCollection
{
    /**
     * Поиск пользователей по его ID.
     *
     * @param int $id
     *
     * @return Group|null
     */
    public function getById(int $id): ?Group
    {
        return $this->filterOneByField('id', $id);
    }

    /**
     * Возвращает список найденных IDs групп по названию.
     *
     * @param string[] $names
     *
     * @return array
     */
    public function getIdsByNames(array $names): array
    {
        $names = array_map(function ($item) {
            return mb_strtolower($item, 'utf8');
        }, $names);

        $filtered = $this->filter(function (Group $group) use ($names) {
            return in_array(
                mb_strtolower($group->getName(), 'utf8'),
                $names
            );
        });

        $result = [];
        /**
         * @var Group $group
         */
        foreach ($filtered as $group) {
            $result[] = $group->getId();
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Group::class;
    }
}