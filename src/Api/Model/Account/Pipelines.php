<?php

namespace Amocrm\Api\Model\Account;

use Amocrm\Api\Model\AbstractModelCollection;
use Amocrm\Api\Model\Account\Pipeline\Statuses;

/**
 * Коллекция доп. полей.
 *
 * @method Pipeline first()
 * @method Pipeline last()
 * @method Pipeline current()
 * @method Pipeline get($key)
 * @method Pipeline filterOneByField($name, $value)
 *
 * @method Pipeline getOneById(int $id)
 * @method Pipeline getOneByValue(int $value)
 * @method Pipeline getOneByName(string $name)
 * @method Pipeline getOneByLabel(string $label)
 * @method Pipeline getOneBySort(int $sort)
 * @method Pipeline getOneByIsMain(bool $isMain)
 * @method Pipeline getOneByLeads(int $leads)
 */
class Pipelines extends AbstractModelCollection
{
    /**
     * Возвращает список активных статусов всех воронок вместе взятых.
     *
     * @return Statuses
     */
    public function getStatusesActive(): Statuses
    {
        $statuses = Statuses::create();

        foreach ($this as $pipeline) {
            $statuses->merge($pipeline->getStatusesActive());
        }

        return $statuses;
    }

    /**
     * Возвращает массив IDs воронок в коллекции.
     *
     * @return array
     */
    public function getIds(): array
    {
        return array_map(function (Pipeline $pipeline) {
            return $pipeline->getId();
        }, $this->getValues());
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Pipeline::class;
    }
}