<?php

namespace Amocrm\Api\Model\Account;

use Amocrm\Api\Model\AbstractModel;

/**
 * @method int    getId()
 * @method string getName()
 * @method string getCode()
 * @method string getEditable() Возвращает Y или N.
 */
class NoteType extends AbstractModel
{
    /**
     * Тип примечания "Сделка создана".
     *
     * Примечания приманимают только ID типа, не смотря, что у задач можно и так
     * и так. Плюс при запросе примечаний из API оно возвращает именно именно ID.
     *
     * @var int
     */
    const DEAL_CREATED = 1;

    /**
     * Тип примечания "Контакта создан".
     *
     * @var int
     */
    const CONTACT_CREATED = 2;

    /**
     * Тип примечания "Смена статуса сделки".
     *
     * @var int
     */
    const DEAL_STATUS_CHANGED = 3;

    /**
     * Тип примечания "Стандартное" (обычный текст).
     *
     * @var int
     */
    const COMMON = 4;

    /**
     * Тип примечания "Файл".
     *
     * @var int
     */
    const ATTACHMENT = 5;

    /**
     * Тип примечания "Звонок из приложения на iPhone" (написано в доке).
     *
     * @var int
     */
    const CALL = 6;

    /**
     * Не используется.
     *
     * @var int
     */
    const MAIL_MESSAGE = 7;

    /**
     * Не используется.
     *
     * @var int
     */
    const MAIL_MESSAGE_ATTACHMENT = 8;

    /**
     * Не используется.
     *
     * @var int
     */
    const EXTERNAL_ATTACH = 9;

    /**
     * Тип примечания "Входящий звонок".
     *
     * @var int
     */
    const CALL_IN = 10;

    /**
     * Тип примечания "Исходящий звонок".
     *
     * @var int
     */
    const CALL_OUT = 11;

    /**
     * Тип примечания "Создана компания".
     *
     * @var int
     */
    const COMPANY_CREATED = 12;

    /**
     * Тип примечания "Результат задачи".
     *
     * @var int
     */
    const TASK_RESULT = 13;

    /**
     * Тип примечания "Чат" (есть в данных аккаунта).
     *
     * @var int
     */
    const CHAT = 17;

    /**
     * Тип примечания "Системное сообщение" (из новой доки).
     *
     * @var int
     */
    const SYSTEM = 25;

    /**
     * Неизвестный тип (есть в данных аккаунта).
     *
     * @var int
     */
    const MAX_SYSTEM = 99;

    /**
     * Тип примечания "Ссылка".
     *
     * @var int
     */
    const DROPBOX = 101;

    /**
     * Тип примечания "Входящая СМС".
     *
     * @var int
     */
    const SMS_IN = 102;

    /**
     * Тип примечания "Исходящая СМС".
     *
     * @var int
     */
    const SMS_OUT = 103;

    /**
     * @return bool
     */
    public function isEditable(): bool
    {
        return $this->getEditable() == 'Y';
    }
}