<?php

namespace Amocrm\Api\Model\Account;

use Amocrm\Api\Model\AbstractModel;

/**
 * @method int    getId()
 * @method string getName()
 * @method string getCode()
 */
class TaskType extends AbstractModel
{
    /**
     * Дефолтный тип задачи "Связаться с клиентом".
     *
     * Задачи приманимают как ID типа так и строку, но у FOLLOW_UP и CALL одни и
     * те же ID. При этом CALL никак не обозначается в интерфейсе, что странно.
     * Но выбранно использование строк т.к. при запросе задач из API оно
     * возвращает именно строки или ID кастомных типов.
     *
     * @var string
     */
    const FOLLOW_UP = 'FOLLOW_UP';

    /**
     * Дефолтный тип задачи "Звонок"
     *
     * @var string
     */
    const CALL = 'CALL';

    /**
     * Дефолтный тип задачи "Встреча"
     *
     * @var string
     */
    const MEETING = 'MEETING';

    /**
     * Дефолтный тип задачи "Письмо"
     *
     * @var string
     */
    const LETTER = 'LETTER';
}