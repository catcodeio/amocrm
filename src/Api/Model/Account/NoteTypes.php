<?php

namespace Amocrm\Api\Model\Account;

use Amocrm\Api\Model\AbstractModelCollection;

/**
 * Коллекция типов примечаний.
 *
 * @method NoteType first()
 * @method NoteType last()
 * @method NoteType current()
 * @method NoteType get($key)
 * @method NoteType filterOneByField($name, $value)
 *
 * @method NoteType getOneById($id)
 * @method NoteType getOneByName($name)
 * @method NoteType getOneByCode($code)
 * @method NoteType getOneByEditable($editable)
 */
class NoteTypes extends AbstractModelCollection
{
    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return NoteType::class;
    }
}