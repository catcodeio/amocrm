<?php

namespace Amocrm\Api\Model\Account\CustomField;

use Amocrm\Api\Model\AbstractModelCollection;

/**
 * Коллекция доп. полей.
 *
 * @method Subtype first()
 * @method Subtype last()
 * @method Subtype current()
 * @method Subtype get($key)
 * @method Subtype filterOneByField($name, $value)
 *
 * @method Subtype getOneById($id)
 * @method Subtype getOneByName($name)
 * @method Subtype getOneByTitle($title)
 */
class Subtypes extends AbstractModelCollection
{
    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Subtype::class;
    }
}