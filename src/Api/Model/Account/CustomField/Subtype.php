<?php

namespace Amocrm\Api\Model\Account\CustomField;

use Amocrm\Api\Model\AbstractModel;

/**
 * @method int    getId()
 * @method string getName()
 * @method string getTitle()
 */
class Subtype extends AbstractModel
{
    // ...
}