<?php

namespace Amocrm\Api\Model\Account;

use Amocrm\Api\Collection\CollectionInterface;
use Amocrm\Api\Model\AbstractModelCollection;
use Amocrm\Api\Helper\ElementType;

/**
 * Коллекция доп. полей из данных аккаунта.
 *
 * @method CustomField first()
 * @method CustomField last()
 * @method CustomField current()
 * @method CustomField filterOneByField($name, $value)
 *
 * @method CustomField getOneById(int $id)
 * @method CustomField getOneByCode(string $code)
 * @method CustomField getOneByTypeId(int $typeId)
 * @method CustomField getOneByName(string $name)
 */
class CustomFields extends AbstractModelCollection
{
    /**
     * Фильтрует коллекцию доп. полей по типу сущности, к которой они относяться.
     * Принимает константы из EntityType или id каталогов.
     *
     * @param string $elementType
     *
     * @return CustomFields
     */
    public function getByElementType(string $elementType)
    {
        return $this->filterByField('element_type', $elementType);
    }

    /**
     * @inheritdoc
     */
    public static function create($data = null)
    {
        // Костыль т.к. фильтрованные списки доп. полей уже не будет содержать
        // название сущностей в ключах, а потому они все уйдут в else.
        if (isset($data['contacts'])) {
            // Немного видоизменим массив каждого доп. поля добавив в него сущность,
            // к которой он относится. Это поможет фильтровать потом по ней.
            $items = [];

            foreach ($data as $entity => $item) {
                $items = array_merge(
                    $items,
                    array_map(function ($item) use ($entity) {
                        $item['element_type'] = ElementType::pluralToNumber($entity);

                        return $item;
                    }, $item)
                );
            }

            return parent::create($items);
        } else {
            return parent::create($data);
        }
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return CustomField::class;
    }
}