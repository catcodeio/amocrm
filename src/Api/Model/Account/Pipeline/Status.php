<?php

namespace Amocrm\Api\Model\Account\Pipeline;

use Amocrm\Api\Model\AbstractModel;

/**
 * @method int    getId()
 * @method string getName()
 * @method string getColor()
 * @method int    getSort()
 * @method string getEditable()
 * @method int    getPipelineId()
 */
class Status extends AbstractModel
{
    /**
     * Стандартный статус "Успешно реализовано".
     *
     * @var int
     */
    const SUCCESS = 142;

    /**
     * Стандартный статус "Закрыто и не реализовано".
     *
     * @var int
     */
    const FAILURE = 143;

    /**
     * @return bool
     */
    public function isEditable(): bool
    {
        return $this->getEditable() == 'Y';
    }
}