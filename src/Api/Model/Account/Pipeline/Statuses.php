<?php

namespace Amocrm\Api\Model\Account\Pipeline;

use Amocrm\Api\Model\AbstractModelCollection;
use Amocrm\Api\Collection\CollectionInterface;

/**
 * Коллекция статусов воронки.
 *
 * @method Status first()
 * @method Status last()
 * @method Status current()
 * @method Status get($key)
 * @method Status filterOneByField($name, $value)
 *
 * @method Status getOneById(int $id)
 * @method Status getOneByName(string $name)
 * @method Status getOneByColor(string $color)
 * @method Status getOneBySort(int $sort)
 * @method Status getOneByEditable(string $editable) Принимает Y или N.
 * @method Status getOneByPipelineId(int $pipelineId)
 */
class Statuses extends AbstractModelCollection
{
    /**
     * Возвращает список только активных статусов.
     *
     * @return Statuses|CollectionInterface
     */
    public function getActive(): Statuses
    {
        return $this->filter(function (Status $status) {
            return !in_array($status->getId(), [142, 143]);
        });
    }

    /**
     * Возвращает массив IDs статусов в коллекции.
     *
     * @return array
     */
    public function getIds(): array
    {
        return array_unique(array_map(function (Status $status) {
            return $status->getId();
        }, $this->getValues()));
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Status::class;
    }
}