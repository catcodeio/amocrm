<?php

namespace Amocrm\Api\Model\Account;

use Amocrm\Api\Model\AbstractModel;
use Amocrm\Api\Model\Account\Pipeline\Status;
use Amocrm\Api\Model\Account\Pipeline\Statuses;

/**
 * @method int      getId()
 * @method string   getValue()
 * @method string   getName()
 * @method string   getLabel()
 * @method int      getSort()
 * @method bool     getIsMain()
 * @method int      getLeads()
 * @method Statuses getStatuses()
 */
class Pipeline extends AbstractModel
{
    /**
     * Возвращает сразу данные конктеного статуса.
     *
     * @param int $statusId
     *
     * @return Status
     */
    public function getStatus(int $statusId): Status
    {
        return $this->getStatuses()->get($statusId);
    }

    /**
     * Возвращает список активных статусов текущей воронки.
     *
     * @return Statuses
     */
    public function getStatusesActive(): Statuses
    {
        return $this->getStatuses()->getActive();
    }

    /**
     * @return bool
     */
    public function isMain(): bool
    {
        return $this->getIsMain();
    }
}