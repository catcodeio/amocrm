<?php

namespace Amocrm\Api\Model;

/**
 * Коллекция виджетов.
 *
 * @method Widget first()
 * @method Widget last()
 * @method Widget current()
 * @method Widget get($key)
 * @method Widget filterOneByField($name, $value)
 */
class Widgets extends AbstractModelCollection
{
    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Widget::class;
    }
}