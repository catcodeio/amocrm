<?php

namespace Amocrm\Api\Model\Tag;

/**
 * Трейт для сущностей, которые должны работать с тегами.
 *
 * @method Tags getTags()
 */
trait TagTrait
{
    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasTag(string $name)
    {
        return $this
            ->getTags()
            ->exists(function ($key, Tag $tag) use ($name) {
                return $tag->getName() == $name;
            });
    }

    /**
     * @param string $name
     *
     * @return Tag
     */
    public function getTag(string $name)
    {
        return $this->getTags()->getOneByName($name);
    }

    /**
     * @param string[]|string $names
     *
     * @return static
     */
    public function addTag($names)
    {
        $names = is_array($names) ? $names : [$names];

        foreach ($names as $name) {
            if (!$name) {
                continue;
            }
            /**
             * @var Tag $tag
             */
            $tag = $this->getTags()->mapModel();

            $tag->setName($name);

            $this->getTags()->add($tag);
        }

        return $this;
    }

    /**
     * @param string[]|string $names
     *
     * @return static
     */
    public function removeTag($names)
    {
        $names = is_array($names) ? $names : [$names];

        foreach ($names as $name) {
            $this->getTags()->removeElement($this->getTag($name));
        }

        return $this;
    }
}