<?php

namespace Amocrm\Api\Model\Tag;

use Amocrm\Api\Helper\ElementType;
use Amocrm\Api\Model\AbstractModel;

/**
 * @method int    getId()
 * @method string getName()
 * @method Tag    setName(string $tag)
 */
class Tag extends AbstractModel
{
    /**
     * @return string|null
     */
    public function getElementType(): ?string
    {
        return ElementType::toNumber($this->__call(__FUNCTION__));
    }
}