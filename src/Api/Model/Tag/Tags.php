<?php

namespace Amocrm\Api\Model\Tag;

use Amocrm\Api\Model\AbstractModelCollection;

/**
 * Коллекция тегов сущности.
 *
 * @method Tag first()
 * @method Tag last()
 * @method Tag current()
 * @method Tag get($key)
 * @method Tag filterOneByField($name, $value)
 *
 * @method Tag getOneById(int $id)
 * @method Tag getOneByName(string $name)
 */
class Tags extends AbstractModelCollection
{

    /**
     * @inheritdoc
     */
    public function getModifiedForApi()
    {
        $elements = $this->toArray();
        $result   = [];

        if ($elements) {
            /**
             * @var Tag $element
             */
            foreach ($elements as $element) {
                // Беря сразу название (без вызова getModifiedForApi) объединяем
                // старые теги с новыми, чтобы не перетирать при добавлении новых.
                $result[] = $element->getName();
            }
        }

        return implode(', ', $result);
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Tag::class;
    }
}