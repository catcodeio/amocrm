<?php

namespace Amocrm\Api\Model;

use Amocrm\Api\Model\Common\DateCreateTrait;
use Amocrm\Api\Model\Common\ElementTypeTrait;
use Amocrm\Api\Model\Common\LastModifiedTrait;
use Amocrm\Api\Model\Common\EntityModelTrait;
use Amocrm\Api\Model\Common\ResponsibleUserTrait;
use DateTime;

/**
 * @method int        getId()
 * @method Task       setId(int $id)
 * @method int        getElementId()
 * @method Task       setElementId(int $elementId)
 * @method string|int getTaskType() Может возвращать code стандартных зада либо ID кастомных.
 * @method Task       setTaskType(int $taskType)
 * @method int        getResponsibleUserId()
 * @method Task       setResponsibleUserId(int $responsibleUserId)
 * @method string     getText()
 * @method Task       setText(string $text)
 * @method int        getStatus() @deprecated работает только в V1.
 * @method Task       setStatus(int $status) @deprecated работает только в V1.
 * @method int        getDuration()
 * @method Task       setDuration(string $duration) Продолжительность в секундах, считается от getCompleteTillAt()
 * @method int        getIsCompleted()
 * @method Task       setIsCompleted(int $isCompleted)
 * @method int        getCreatedUserId()
 * @method int        getGroupId()
 * @method int        getAccountId()
 * @method array      getResult()
 */
class Task extends AbstractEntity
{
    use DateCreateTrait;
    use LastModifiedTrait;
    use ResponsibleUserTrait;
    use EntityModelTrait;
    use ElementTypeTrait;

    /**
     * @return bool
     *
     * @deprecated Используется для API v1.
     */
    public function isClosed(): bool
    {
        // В API v2 именуется поле иначе.
        if (is_null($this->getStatus())) {
            return $this->isCompleted();
        }

        return $this->getStatus() != 0;
    }

    /**
     * Использовать только с API v2.
     *
     * @return bool
     */
    public function isCompleted(): bool
    {
        return (bool)$this->getIsCompleted();
    }

    /**
     * @param bool $completed
     *
     * @return Task
     */
    public function setCompleted(bool $completed): Task
    {
        $this->__call('setIsCompleted', [$completed]);

        return $this;
    }

    /**
     * Возвращает сразу текст результата, если он есть. Иначе там массив т.к.
     * насколько я понимаю результат задачи это примечание (note)!
     *
     * @return string|null
     */
    public function getResultText(): ?string
    {
        $result = $this->getResult();

        if (!$result) {
            return null;
        }

        return $result['text'];
    }

    /**
     * @return DateTime|null
     */
    public function getCompleteTillAt(): ?DateTime
    {
        $date = $this->__call(__FUNCTION__);

        if (!$date) {
            return null;
        }

        return (new DateTime())->setTimestamp($date);
    }

    /**
     * @param DateTime $completeTillAt
     *
     * @return $this
     */
    public function setCompleteTillAt(DateTime $completeTillAt)
    {
        $this->__call(__FUNCTION__, [$completeTillAt->format('U')]);

        return $this;
    }

    /**
     * @return DateTime|null
     *
     * @deprecated Используется для API v1.
     */
    public function getCompleteTill(): ?DateTime
    {
        $date = $this->__call(__FUNCTION__);

        if (!$date) {
            return null;
        }

        return (new DateTime())->setTimestamp($date);
    }

    /**
     * @param DateTime $completeTill
     *
     * @return $this
     *
     * @deprecated Используется для API v1.
     */
    public function setCompleteTill(DateTime $completeTill)
    {
        $this->__call(__FUNCTION__, [$completeTill->format('U')]);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getModifiedForApiV1(): array
    {
        $data = $this->getModifiedForApi();

        $data['status'] = $this->isCompleted() ? 1 : 0;

        if ($this->getCompleteTillAt()) {
            $data['complete_till'] = $this->getCompleteTillAt()->format('U');
        }

        return $data;
    }
}
