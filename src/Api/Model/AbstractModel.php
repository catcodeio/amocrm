<?php

namespace Amocrm\Api\Model;

use Amocrm\Api\Model\CustomField\CustomFields;
use Amocrm\Api\Helper\Utils;
use Amocrm\Api\Model\Tag\Tags;
use RuntimeException;

/**
 * Общий функционал модели для получения и присвоения данных.
 */
abstract class AbstractModel implements ModelInterface
{
    /**
     * Первичный массив данных переданный при инициализации. Не обязательный.
     *
     * @var array
     */
    protected $initial;

    /**
     * Все изменённые данные на протяжении всей работы с сущностью. Тут как и
     * простые типы (строки и числа), так и объекты зависимсых подмоделей.
     * Требуется, чтобы сохранять изменённые данные, а потом собирать
     * их воедино обновляя все данные при отправки по API.
     *
     * @var mixed[]|ModelInterface[]
     */
    protected $modified = [];

    /**
     * Запрещаем использовать конструктор, создание только через метод-фабрику.
     *
     * @param array|null $initial
     */
    protected function __construct(?array $initial = [])
    {
        $this->initial = $initial ?? [];
    }

    /**
     * @inheritdoc
     */
    public static function create($data = null)
    {
        return new static($data);
    }

    /**
     * @inheritdoc
     */
    public function getInitial(): array
    {
        return $this->initial;
    }

    /**
     * @inheritdoc
     */
    public function getModified(): array
    {
        $result = $this->initial;

        foreach ($this->modified as $attr => $model) {
            if ($model instanceof ModelInterface) {
                $data = $model->getModified();

                if ($data) {
                    $result[$attr] = $data;
                }
            } else {
                $result[$attr] = $model;
            }
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function isModified(): bool
    {
        foreach ($this->modified as $attr => $model) {
            $modified = null;

            if ($model instanceof ModelInterface) {
                $modified = $model->isModified();
            } else {
                $modified = true;
            }

            if ($modified) {
                return true;
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function getModifiedForApi(): array
    {
        $serialize = function ($items) use (&$serialize) {
            $result = [];

            foreach ($items as $attr => $model) {
                if ($model instanceof ModelInterface) {
                    $data = $model->getModifiedForApi();

                    if ($data) {
                        $result[$attr] = $data;
                    }
                } elseif (is_array($model)) {
                    // Может быть это просто массив объектов типа ModelInterface.
                    $result[$attr] = $serialize($model);
                } else {
                    $result[$attr] = $model;
                }
            }

            return $result;
        };

        return $serialize($this->modified);
    }

    /**
     * @inheritdoc
     */
    public function getModifiedForApiV1(): array
    {
        return $this->getModifiedForApi();
    }

    /**
     * Генерируем сеттеры и геттеры для все обычных свойств объекта. Для
     * уникального функционала нужно определить конкретный метод отдельно.
     *
     * Например, для получения ID статуса сделки надо вызвать getStatusId().
     *
     * @param string     $method
     * @param array|null $params
     *
     * @return mixed
     */
    public function __call(string $method, array $params = null)
    {
        $prefix = substr($method, 0, 3);
        $attr   = Utils::toSnakeCase(substr($method, 3));

        if ($prefix == 'set' && count($params) == 1) {
            $this->setAttr($attr, $params[0]);

            return $this;
        } elseif ($prefix == 'get') {
            return $this->getAttr($attr);
        } else {
            throw new RuntimeException(sprintf('Method %s is not exists in %s class', $method, static::class));
        }
    }

    /**
     * Получает значение поля сущности по имени.
     *
     * @param string $name
     *
     * @return mixed
     */
    private function getAttr(string $name)
    {
        if (array_key_exists($name, $this->modified) === true) {
            return $this->modified[$name];
        }

        $dataForModel = null;
        // Ищем данные для передачи в модель.
        if (isset($this->initial[$name]) && is_array($this->initial[$name])) {
            $dataForModel = $this->initial[$name];
        }
        // Если это массив, то там может быть список сущностей или подсущность.
        // Для того и другого должен быть свой класс. Для первого будет класс-коллекция
        // для второго просто обычный класс для получения доступа к полям.
        $className = static::class . '\\' . Utils::toCamelCase($name);

        if (class_exists($className)) {
            /**
             * @var $className ModelInterface
             */
            $this->modified[$name] = $className::create($dataForModel);

            return $this->modified[$name];
        }
        // Отдельные специальные классы для конкретных сущностей, чтобы не
        // генрировать лишние, железно завести так. Норм.
        switch ($name) {
            case 'custom_fields':
                $this->modified[$name] = CustomFields::create($dataForModel);

                return $this->modified[$name];
            case 'tags':
                $this->modified[$name] = Tags::create($dataForModel);

                return $this->modified[$name];
        }
        // Если не нашли класс-обёртку, но данные есть, то вернём их хотя бы в сыром виде.
        if (array_key_exists($name, $this->initial) === true) {
            return $this->initial[$name];
        }

        return null;
    }

    /**
     * Устанавливает значение поля сущности по имени.
     *
     * @param string $name
     * @param mixed  $value
     */
    private function setAttr(string $name, $value)
    {
        $this->modified[$name] = $value;
    }

    /**
     * Для копирования сущностей с помощью команды clone.
     */
    public function __clone()
    {
        foreach ($this->initial as $key => $value) {
            $valueObject = $this->getAttr($key);

            if ($valueObject instanceof ModelInterface) {
                $valueObject = clone $valueObject;
            }

            $this->setAttr($key, $valueObject);
        }

        $this->initial = null;
    }
}