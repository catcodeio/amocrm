<?php

namespace Amocrm\Api\Model;

use Amocrm\Api\Collection\AbstractArrayCollection;
use Amocrm\Api\Helper\Utils;
use RuntimeException;

/**
 * Класс для оборачивания массивов на основе коллекции Doctrine.
 */
abstract class AbstractModelCollection extends AbstractArrayCollection
{
    /**
     * @inheritdoc
     */
    public static function create($data = null)
    {
        if (is_array($data) && count($data) && !(current($data) instanceof ModelInterface)) {
            foreach ($data as &$element) {
                $element = self::mapModel($element);
            }
        }

        return parent::create($data);
    }

    /**
     * Метод мэппит массив на специальный объект, добавляющий доп. функционал.
     *
     * @param array|null $element
     *
     * @return mixed
     */
    public static function mapModel(array $element = null): ModelInterface
    {
        /**
         * @var ModelInterface
         */
        $modelClass = static::getModelClass();

        return $modelClass::create($element);
    }

    /**
     * Возвращает название класса, в который нужно оборачивать эллементы
     * коллекции. Вовзврщаемый класс должен имплементировать ModelInterface.
     *
     * @return mixed
     */
    protected abstract static function getModelClass(): string;

    /**
     * @param string $method
     * @param array  $params
     *
     * @return mixed
     */
    public function __call(string $method, ?array $params)
    {
        if (strpos($method, 'getBy') === 0 && count($params) == 1) {
            $attr = Utils::toSnakeCase(substr($method, 5));

            return $this->filterByField($attr, $params[0]);
        } elseif (strpos($method, 'getOneBy') === 0 && count($params) == 1) {
            $attr = Utils::toSnakeCase(substr($method, 8));

            return $this->filterOneByField($attr, $params[0]);
        } else {
            throw new RuntimeException(sprintf('Method %s is not exists in %s class', $method, __CLASS__));
        }
    }

    /**
     * Возвращает список отфильтрованных элементов с помощью фильтрации по
     * конкретному полю.
     *
     * @param string $name  Название поля в snake case.
     * @param mixed  $value
     *
     * @return static
     */
    public function filterByField($name, $value)
    {
        $valueLower = mb_strtolower($value, 'utf8');

        $filtered = $this->filter(function (ModelInterface $element) use ($name, $valueLower) {
            $methodName = 'get' . Utils::toCamelCase($name);

            return mb_strtolower($element->{$methodName}(), 'utf8') == $valueLower;
        });

        return $filtered;
    }

    /**
     * Возвращает один элемент с помощью фильтрации по конкретному полю.
     *
     * @param string $name  Название поля в snake case.
     * @param mixed  $value
     *
     * @return mixed|null
     */
    public function filterOneByField($name, $value)
    {
        $filtered = $this->filterByField($name, $value);

        if (!$filtered->count()) {
            return null;
        }

        return $filtered->first();
    }

    /**
     * @inheritdoc
     */
    public function getModified(): array
    {
        $elements = $this->toArray();
        $result   = [];

        if ($elements) {
            /**
             * @var ModelInterface $element
             */
            foreach ($elements as $element) {
                $result[] = $element->getModified();
            }
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function isModified(): bool
    {
        $elements = $this->toArray();

        if ($elements) {
            /**
             * @var ModelInterface $element
             */
            foreach ($elements as $element) {
                if ($element->isModified()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function getModifiedForApi()
    {
        $elements = $this->toArray();
        $result   = [];

        if ($elements) {
            /**
             * @var ModelInterface $element
             */
            foreach ($elements as $element) {
                $result[] = $element->getModifiedForApi();
            }
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getModifiedForApiV1(): array
    {
        return $this->getModifiedForApi();
    }

    /**
     * @inheritdoc
     */
    public function getInitial(): array
    {
        $elements = $this->toArray();
        $result   = [];

        if ($elements) {
            /**
             * @var ModelInterface $element
             */
            foreach ($elements as $element) {
                $result[] = $element->getInitial();
            }
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function __clone()
    {
        foreach ($this->getKeys() as $key) {
            $value = $this->get($key);

            if ($value instanceof ModelInterface) {
                $this->set($key, clone $value);
            }
        }
    }
}