<?php

namespace Amocrm\Api\Model;

/**
 * @method int     getId()
 * @method Webhook setId(int $id)
 * @method int     getUrl()
 * @method Webhook setUrl(string $url)
 * @method array   getEvents()
 * @method Webhook setEvents(array $events)
 * @method array   getResult() Используется только при (от)подписке для проверки успешности т.к. если URL невалиден, то вебхук не будет добавлен.
 * @method Webhook setResult()
 * @method bool    getDisabled() Работает только при получение вебхуков.
 */
class Webhook extends AbstractModel
{
    /**
     * У сделки сменился ответственный.
     *
     * @var string
     */
    const RESPONSIBLE_LEAD = 'responsible_lead';

    /**
     * У контакта сменился ответственный.
     *
     * @var string
     */
    const RESPONSIBLE_CONTACT = 'responsible_contact';

    /**
     * У компании сменился ответственный.
     *
     * @var string
     */
    const RESPONSIBLE_COMPANY = 'responsible_company';

    /**
     * У покупателя сменился ответственный.
     *
     * @var string
     */
    const RESPONSIBLE_CUSTOMER = 'responsible_customer';

    /**
     * У задачи сменился ответственный.
     *
     * @var string
     */
    const RESPONSIBLE_TASK = 'responsible_task';

    /**
     * Сделка восстановлена из корзины.
     *
     * @var string
     */
    const RESTORE_LEAD = 'restore_lead';

    /**
     * Контакт восстановлен из корзины.
     *
     * @var string
     */
    const RESTORE_CONTACT = 'restore_contact';

    /**
     * Компания восстановлена из корзины.
     *
     * @var string
     */
    const RESTORE_COMPANY = 'restore_company';

    /**
     * Добавлена сделка.
     *
     * @var string
     */
    const ADD_LEAD = 'add_lead';

    /**
     * Добавлен контакт.
     *
     * @var string
     */
    const ADD_CONTACT = 'add_contact';

    /**
     * Добавлена компания.
     *
     * @var string
     */
    const ADD_COMPANY = 'add_company';

    /**
     * Добавлен покупатель.
     *
     * @var string
     */
    const ADD_CUSTOMER = 'add_customer';

    /**
     * Добавлена задача.
     *
     * @var string
     */
    const ADD_TASK = 'add_task';

    /**
     * Сделка изменена.
     *
     * @var string
     */
    const UPDATE_LEAD = 'update_lead';

    /**
     * Контакт изменён.
     *
     * @var string
     */
    const UPDATE_CONTACT = 'update_contact';

    /**
     * Компания изменена.
     *
     * @var string
     */
    const UPDATE_COMPANY = 'update_company';

    /**
     * Покупатель изменен.
     *
     * @var string
     */
    const UPDATE_CUSTOMER = 'update_customer';

    /**
     * Задача изменена.
     *
     * @var string
     */
    const UPDATE_TASK = 'update_task';

    /**
     * Удалена сделка.
     *
     * @var string
     */
    const DELETE_LEAD = 'delete_lead';

    /**
     * Удалён контакт.
     *
     * @var string
     */
    const DELETE_CONTACT = 'delete_contact';

    /**
     * Удалена компания.
     *
     * @var string
     */
    const DELETE_COMPANY = 'delete_company';

    /**
     * Удален покупатель.
     *
     * @var string
     */
    const DELETE_CUSTOMER = 'delete_customer';

    /**
     * Удалена задача.
     *
     * @var string
     */
    const DELETE_TASK = 'delete_task';

    /**
     * У сделки сменился статус.
     *
     * @var string
     */
    const STATUS_LEAD = 'status_lead';

    /**
     * Примечание добавлено в сделку.
     *
     * @var string
     */
    const NOTE_LEAD = 'note_lead';

    /**
     * Примечание добавлено в контакт.
     *
     * @var string
     */
    const NOTE_CONTACT = 'note_contact';

    /**
     * Примечание добавлено в компанию.
     *
     * @var string
     */
    const NOTE_COMPANY = 'note_company';

    /**
     * Примечание добавлено в покупателя.
     *
     * @var string
     */
    const NOTE_CUSTOMER = 'note_customer';

    /**
     * @inheritdoc
     */
    public static function create($data = null)
    {
        // У amoCRM в получаении в сущности события называются actions, а в
        // добавлении events. Потому если обнаруживаем actions, то перемещаем в
        // events и не думаем больше об actions.
        if (isset($data['actions'])) {
            $data['events'] = $data['actions'];
            unset($data['actions']);
        }

        return new static($data);
    }

    /**
     * Переопределяем и делаем наподобии getModified() т.к. обновлять вебхуки мы не можем всё равно,
     * но копировать можем. А для действий нужен полный набор данных вебхука, например, мы могли его
     * получить, а потом снова эту же сущность добавить.
     *
     * @inheritdoc
     */
    public function getModifiedForApi(): array
    {
        $result = [];

        if (isset($this->initial['url'])) {
            $result['url'] = $this->initial['url'];
        }

        if (isset($this->initial['events'])) {
            $result['events'] = $this->initial['events'];
        }

        foreach ($this->modified as $attr => $model) {
            if (in_array($attr, ['url', 'events'])) {
                $result[$attr] = $model;
            }
        }

        return $result;
    }
}
