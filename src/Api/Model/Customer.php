<?php

namespace Amocrm\Api\Model;

use Amocrm\Api\Model\Common\ResponsibleUserTrait;
use Amocrm\Api\Model\CustomField\CustomFieldTrait;
use Amocrm\Api\Model\Common\DateCreateTrait;
use Amocrm\Api\Model\Common\DateModifyTrait;
use Amocrm\Api\Model\Tag\TagTrait;
use DateTime;

/**
 * @method int      getCreatedBy()
 * @method int      getModifiedBy()
 * @method Customer setModifiedBy(int $modifiedBy)
 * @method int      getMainUserId()
 * @method Customer setMainUserId(int $mainUserId)
 * @method int      getAccountId()
 * @method int      getNextPrice()
 * @method Customer setNextPrice(int $nextPrice)
 * @method int      getPeriodicity()
 * @method int      setPeriodicity(int $periodicity)
 * @method int      getMainContactId()
 * @method int      getPeriodId()
 * @method Customer setPeriodId(int $periodId)
 * @method int      getDeleted()
 */
class Customer extends AbstractEntity
{
    use CustomFieldTrait;
    use TagTrait;
    use DateCreateTrait;
    use DateModifyTrait;
    use ResponsibleUserTrait;

    /**
     * @return DateTime|null
     */
    public function getNextDate(): ?DateTime
    {
        $date = $this->__call(__FUNCTION__);

        if ($date) {
            return (new DateTime())->setTimestamp($date);
        }

        return null;
    }

    /**
     * @param DateTime $nextDate
     *
     * @return $this
     */
    public function setNextDate(DateTime $nextDate)
    {
        $this->__call(__FUNCTION__, [$nextDate->format('U')]);

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getTaskLastDate(): ?DateTime
    {
        $date = $this->__call(__FUNCTION__);

        if ($date) {
            return (new DateTime())->setTimestamp($date);
        }

        return null;
    }

    /**
     * @param DateTime $taskLastDate
     *
     * @return $this
     */
    public function setTaskLastDate(DateTime $taskLastDate)
    {
        $this->__call(__FUNCTION__, [$taskLastDate->format('U')]);

        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return (bool)$this->__call('getDeleted');
    }

    /**
     * @inheritdoc
     */
    public function getModifiedForApi(): array
    {
        $result = parent::getModifiedForApi();

        if ($result && isset($this->initial['id'])) {
            $result['id'] = $this->initial['id'];
        }

        $result['date_modify'] = (new DateTime('now'))->format('U');

        return $result;
    }
}
