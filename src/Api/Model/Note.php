<?php

namespace Amocrm\Api\Model;

use Amocrm\Api\Helper\ElementType;
use Amocrm\Api\Model\Common\DateCreateTrait;
use Amocrm\Api\Model\Common\ElementTypeTrait;
use Amocrm\Api\Model\Common\LastModifiedTrait;
use Amocrm\Api\Model\Common\EntityModelTrait;
use Amocrm\Api\Model\Common\ResponsibleUserTrait;

/**
 * @method int    getId()
 * @method Note   setId(int $id)
 * @method int    getElementId()
 * @method Note   setElementId(int $elementId)
 * @method int    getNoteType()
 * @method Note   setNoteType(int $noteType)
 * @method int    getResponsibleUserId()
 * @method Note   setResponsibleUserId(int $responsibleUserId)
 * @method string getText()
 * @method Note   setText(string $text)
 * @method int    getEditable() Возвращает Y или N.
 * @method int    getCreatedUserId()
 * @method int    getGroupId()
 * @method int    getAccountId()
 */
class Note extends AbstractModel
{
    use DateCreateTrait;
    use LastModifiedTrait;
    use ResponsibleUserTrait;
    use EntityModelTrait;
    use ElementTypeTrait;

    /**
     * @return bool
     */
    public function isEditable(): bool
    {
        return $this->__call('getEditable') == 'Y';
    }

    /**
     * Дурацкое свойство написанно капсом, да, ещё и с ошибкой. Сеттер для него
     * не надо делать т.к. всё равно нельзя устанавливать, а получать, вот так
     * костыльно только.
     *
     * @return string
     */
    public function getAttachment(): string
    {
        return $this->initial['ATTACHEMENT'];
    }

    /**
     * Устанавливает примечание для указанного покупателя.
     *
     * @param int $taskId
     *
     * @return static
     */
    public function setTaskId(int $taskId)
    {
        $this->setElementId($taskId);
        $this->setElementType(ElementType::TASK);

        return $this;
    }
}
