<?php

namespace Amocrm\Api\Model;

/**
 * Коллекция ссылок между контактами и сделками.
 *
 * @method ContactLink first()
 * @method ContactLink last()
 * @method ContactLink current()
 * @method ContactLink get($key)
 * @method ContactLink filterOneByField($name, $value)
 */
class ContactLinks extends AbstractModelCollection
{
    /**
     * Возвращает массива IDs сделок.
     *
     * @return array
     */
    public function getIdsOfLeads(): array
    {
        $result = [];
        /**
         * @var ContactLink $link
         */
        foreach ($this as $link) {
            $result[] = $link->getLeadId();
        }

        return $result;
    }

    /**
     * Возвращает массива IDs контактов.
     *
     * @return array
     */
    public function getIdsOfContacts(): array
    {
        $result = [];
        /**
         * @var ContactLink $link
         */
        foreach ($this as $link) {
            $result[] = $link->getContactId();
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return ContactLink::class;
    }
}