<?php

namespace Amocrm\Api\Model;

use Amocrm\Api\Model\Account\CustomField;
use Amocrm\Api\Model\Account\Group;
use Amocrm\Api\Model\Account\NoteTypes;
use Amocrm\Api\Model\Account\Pipeline;
use Amocrm\Api\Model\Account\TaskTypes;
use Amocrm\Api\Model\Account\User;
use Amocrm\Api\Model\Account\Users;
use Amocrm\Api\Model\Account\Groups;
use Amocrm\Api\Model\Account\CustomFields;
use Amocrm\Api\Model\Account\Pipelines;

/**
 * @method int          getId()
 * @method string       getName()
 * @method string       getSubdomain()
 * @method string       getTimezone()
 * @method string       getCurrency()
 * @method string       getLanguage()
 * @method string       getDateFormat()
 * @method string       getTimeFormat()
 * @method array        getShortDatePattern()
 * @method string       getCountry()
 * @method string       getUnsortedOn()
 * @method string       getTimezoneoffset()
 * @method Users        getUsers()
 * @method Groups       getGroups()
 * @method NoteTypes    getNoteTypes()
 * @method TaskTypes    getTaskTypes()
 * @method CustomFields getCustomFields()
 * @method Pipelines    getPipelines()
 */
class Account extends AbstractModel
{
    /**
     * @inheritdoc
     */
    protected function __construct(?array $initial = [])
    {
        parent::__construct($initial);
    }

    /**
     * Возвращает сразу данные конкретного доп. поля.
     *
     * @param int $customFieldId
     *
     * @return CustomField|null
     */
    public function getCustomField(int $customFieldId): ?CustomField
    {
        return $this->getCustomFields()->getOneById($customFieldId);
    }

    /**
     * Возвращает сразу данные конктеной воронки.
     *
     * @param int $pipelineId
     *
     * @return Pipeline|null
     */
    public function getPipeline(int $pipelineId): ?Pipeline
    {
        return $this->getPipelines()->get($pipelineId);
    }

    /**
     * Возвращает сразу данные конктеного пользователя.
     *
     * @param int $userId
     *
     * @return User|null
     */
    public function getUser(int $userId): ?User
    {
        return $this->getUsers()->getOneById($userId);
    }

    /**
     * Возвращает сразу данные конктеной группы.
     *
     * @param int $groupId
     *
     * @return Group|null
     */
    public function getGroup(int $groupId): ?Group
    {
        return $this->getGroups()->getOneById($groupId);
    }
}