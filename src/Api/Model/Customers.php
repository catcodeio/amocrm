<?php

namespace Amocrm\Api\Model;

/**
 * Коллекция покупателей.
 *
 * @method Lead first()
 * @method Lead last()
 * @method Lead current()
 * @method Lead get($key)
 * @method Lead filterOneByField($name, $value)
 */
class Customers extends AbstractModelCollection
{
    /**
     * Возвращает массив IDs сделок в коллекции.
     *
     * @return array
     */
    public function getIds(): array
    {
        return array_map(function (Customer $customer) {
            return $customer->getId();
        }, $this->getValues());
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Customer::class;
    }
}