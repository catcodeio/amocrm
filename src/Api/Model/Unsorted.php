<?php

namespace Amocrm\Api\Model;

use Amocrm\Api\Model\Common\EntityModelTrait;
use Amocrm\Api\Model\Common\NormalizeCollectionTrait;
use DateTime;

/**
 * @method string   getSourceName()
 * @method Unsorted setSourceName(string $sourceName)
 * @method int      getPipelineId()
 * @method Unsorted setPipelineId(int $pipelineId)
 * @method string   getSourceUid()
 * @method Unsorted setSourceUid(string $sourceUid)
 * @method string   getUid()
 * @method Unsorted setUid(string $uid)
 * @method string   getCategory()
 * @method array    getIncomingEntities()
 * @method Unsorted setIncomingEntities(array $incomingEntities)
 * @method array    getIncomingLeadInfo()
 * @method Unsorted setIncomingLeadInfo(array $incomingLeadInfo)
 */
class Unsorted extends AbstractModel
{
    use EntityModelTrait;
    use NormalizeCollectionTrait;

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return (new DateTime())->setTimestamp($this->__call(__FUNCTION__));
    }

    /**
     * @param DateTime $createdAt
     *
     * @return Unsorted
     */
    public function setCreatedAt(DateTime $createdAt): Unsorted
    {
        $this->__call(__FUNCTION__, [$createdAt->format('U')]);

        return $this;
    }

    /**
     * @return Leads|Lead[]
     */
    public function getLeads()
    {
        return $this->normalizeCollection($this->getIncomingEntities()['leads'], Leads::class);
    }

    /**
     * @param Leads|Lead[]|Lead $leads
     *
     * @return Unsorted
     */
    public function setLeads($leads): Unsorted
    {
        // TODO в сделках можно сразу примечания отсылать, но пока это не работает, надо сделать.
        $entities = $this->getIncomingEntities();

        $entities['leads'] = $this->normalizeCollection($leads, Leads::class);

        return $this->setIncomingEntities($entities);
    }

    /**
     * @return Contacts|Contact[]
     */
    public function getContacts()
    {
        return $this->normalizeCollection($this->getIncomingEntities()['contacts'], Contacts::class);
    }

    /**
     * @param Contacts|Contact[]|Contact $contacts
     *
     * @return Unsorted
     */
    public function setContacts($contacts): Unsorted
    {
        $entities = $this->getIncomingEntities();

        $entities['contacts'] = $this->normalizeCollection($contacts, Contacts::class);

        return $this->setIncomingEntities($entities);
    }

    /**
     * @return Companies|Company[]
     */
    public function getCompanies()
    {
        return $this->normalizeCollection($this->getIncomingEntities()['companies'], Companies::class);
    }

    /**
     * @param Companies|Company[]|Company $companies
     *
     * @return Unsorted
     */
    public function setCompanies($companies): Unsorted
    {
        $entities = $this->getIncomingEntities();

        $entities['companies'] = $this->normalizeCollection($companies, Companies::class);

        return $this->setIncomingEntities($entities);
    }

    /**
     * @return int
     */
    public function getFormId(): int
    {
        return $this->getIncomingLeadInfo()['form_id'];
    }

    /**
     * @param int $formId
     *
     * @return Unsorted
     */
    public function setFormId(int $formId): Unsorted
    {
        $info = $this->getIncomingLeadInfo();

        $info['form_id'] = $formId;

        return $this->setIncomingLeadInfo($info);
    }

    /**
     * @return string
     */
    public function getFormPage(): string
    {
        return $this->getIncomingLeadInfo()['form_page'];
    }

    /**
     * @param string $formPage
     *
     * @return Unsorted
     */
    public function setFormPage(string $formPage): Unsorted
    {
        $info = $this->getIncomingLeadInfo();

        $info['form_page'] = $formPage;

        return $this->setIncomingLeadInfo($info);
    }

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->getIncomingLeadInfo()['ip'];
    }

    /**
     * @param string $ip
     *
     * @return Unsorted
     */
    public function setIp(string $ip): Unsorted
    {
        $info = $this->getIncomingLeadInfo();

        $info['ip'] = $ip;

        return $this->setIncomingLeadInfo($info);
    }

    /**
     * @return string
     */
    public function getServiceCode(): string
    {
        return $this->getIncomingLeadInfo()['service_code'];
    }

    /**
     * @param string $serviceCode
     *
     * @return Unsorted
     */
    public function setServiceCode(string $serviceCode): Unsorted
    {
        $info = $this->getIncomingLeadInfo();

        $info['service_code'] = $serviceCode;

        return $this->setIncomingLeadInfo($info);
    }

    /**
     * @return string
     */
    public function getFormName(): string
    {
        return $this->getIncomingLeadInfo()['form_name'];
    }

    /**
     * @param string $formName
     *
     * @return Unsorted
     */
    public function setFormName(string $formName): Unsorted
    {
        $info = $this->getIncomingLeadInfo();

        $info['form_name'] = $formName;

        return $this->setIncomingLeadInfo($info);
    }

    /**
     * @return DateTime
     */
    public function getFormSendAt(): DateTime
    {
        return (new DateTime())->setTimestamp($this->getIncomingLeadInfo()['form_send_at']);
    }

    /**
     * @param DateTime $formSendAt
     *
     * @return Unsorted
     */
    public function setFormSendAt(DateTime $formSendAt): Unsorted
    {
        $info = $this->getIncomingLeadInfo();

        $info['form_name'] = $formSendAt->format('U');

        return $this->setIncomingLeadInfo($info);
    }

    /**
     * @return string
     */
    public function getReferer(): string
    {
        return $this->getIncomingLeadInfo()['referer'];
    }

    /**
     * @param string $referer
     *
     * @return Unsorted
     */
    public function setReferer(string $referer): Unsorted
    {
        $info = $this->getIncomingLeadInfo();

        $info['referer'] = $referer;

        return $this->setIncomingLeadInfo($info);
    }
}
