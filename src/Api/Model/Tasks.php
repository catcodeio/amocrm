<?php

namespace Amocrm\Api\Model;

/**
 * Коллекция задач.
 *
 * @method Task first()
 * @method Task last()
 * @method Task current()
 * @method Task get($key)
 * @method Task filterOneByField($name, $value)
 */
class Tasks extends AbstractModelCollection
{
    /**
     * Возвращает массив IDs задач в коллекции.
     *
     * @return array
     */
    public function getIds(): array
    {
        return array_map(function (Task $task) {
            return $task->getId();
        }, $this->getValues());
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Task::class;
    }
}