<?php

namespace Amocrm\Api\Model;

use Amocrm\Api\Model\Common\ClosestTaskTrait;
use Amocrm\Api\Model\Common\ContactTrait;
use Amocrm\Api\Model\Common\NormalizeCollectionTrait;
use Amocrm\Api\Model\Common\EntityModelTrait;
use Amocrm\Api\Model\Common\ResponsibleUserTrait;
use Amocrm\Api\Model\CustomField\CustomFieldTrait;
use Amocrm\Api\Model\Common\DateCreateTrait;
use Amocrm\Api\Model\Common\LastModifiedTrait;
use Amocrm\Api\Model\Common\LinkedLeadsIdTrait;
use Amocrm\Api\Model\Tag\TagTrait;

/**
 * @method int     getResponsibleUserId()
 * @method Contact setResponsibleUserId(int $responsibleUserId)
 * @method Contact setCompanyId(int $companyId)
 * @method Contact setLeadsId(array $leadsId)
 * @method Contact setCustomersId(array $customersId)
 * @method int     getCreatedUserId()
 * @method int     getModifiedUserId()
 * @method int     getGroupId()
 * @method int     getAccountId()
 * @method string  getCompanyName()
 * @method Contact setCompanyName(string $companyName) Создание новой компании и привязка к ней.
 */
class Contact extends AbstractEntity
{
    use CustomFieldTrait;
    use TagTrait;
    use DateCreateTrait;
    use LastModifiedTrait;
    use ResponsibleUserTrait;
    use EntityModelTrait;
    use NormalizeCollectionTrait;
    use ContactTrait;
    use ClosestTaskTrait;
    use LinkedLeadsIdTrait;

    /**
     * @return int|null
     */
    public function getCompanyId(): ?int
    {
        $result = $this->__call(__FUNCTION__);

        // Иногда тут может быть '0' и тогда тоже венём null.
        if (!$result) {
            return null;
        }

        return $result;
    }
}
