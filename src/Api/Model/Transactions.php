<?php

namespace Amocrm\Api\Model;

/**
 * Коллекция транзакий.
 *
 * @method Lead first()
 * @method Lead last()
 * @method Lead current()
 * @method Lead get($key)
 * @method Lead filterOneByField($name, $value)
 */
class Transactions extends AbstractModelCollection
{
    /**
     * Возвращает массив IDs сделок в коллекции.
     *
     * @return array
     */
    public function getIds(): array
    {
        return array_map(function (Transaction $transaction) {
            return $transaction->getId();
        }, $this->getValues());
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Transaction::class;
    }
}