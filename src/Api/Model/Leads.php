<?php

namespace Amocrm\Api\Model;

/**
 * Коллекция сделок.
 *
 * @method Lead first()
 * @method Lead last()
 * @method Lead current()
 * @method Lead get($key)
 * @method Lead filterOneByField($name, $value)
 */
class Leads extends AbstractModelCollection
{
    /**
     * Фильтрует коллекцию сделок по массиву ID переданных воронок.
     *
     * @param array|int $pipelineIds
     *
     * @return Leads
     */
    public function filterByPipelineIds($pipelineIds): Leads
    {
        $pipelineIds = is_array($pipelineIds) ? $pipelineIds : [$pipelineIds];

        return $this->filter(function (Lead $lead) use ($pipelineIds) {
            return in_array($lead->getPipelineId(), $pipelineIds);
        });
    }

    /**
     * Фильтрует коллекцию сделок по массиву ID переданных статусов.
     *
     * @param array|int $statusIds
     *
     * @return Leads
     */
    public function filterByStatusIds($statusIds): Leads
    {
        $statusIds = is_array($statusIds) ? $statusIds : [$statusIds];

        return $this->filter(function (Lead $lead) use ($statusIds) {
            return in_array($lead->getStatusId(), $statusIds);
        });
    }

    /**
     * Возвращает массив IDs сделок в коллекции.
     *
     * @return array
     */
    public function getIds(): array
    {
        return array_map(function (Lead $lead) {
            return $lead->getId();
        }, $this->getValues());
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Lead::class;
    }
}