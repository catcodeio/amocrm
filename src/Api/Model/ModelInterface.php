<?php

namespace Amocrm\Api\Model;

/**
 * Интерфейс для модели данных.
 */
interface ModelInterface
{
    /**
     * Создаёт и инициализирует сущность при необходимости.
     *
     * @param array|mixed $data
     *
     * @return static
     */
    public static function create($data = null);

    /**
     * Возвращает первично заданное состояние данных сущности.
     *
     * @return array
     */
    public function getInitial(): array;

    /**
     * Возвращает массив изменённых данных сущности "как есть".
     *
     * @return array
     */
    public function getModified(): array;

    /**
     * Была ли модифицированна сущность за время её существования или нет. Любое
     * изменение любого поля сбрасывает этот флаг.
     *
     * @return bool
     */
    public function isModified(): bool;

    /**
     * Возвращает массив изменённых данных сущности для отправки по API.
     *
     * @return array|string
     */
    public function getModifiedForApi();

    /**
     * Возвращает массив изменённых данных сущности для отправки по API v1. Зачастую возвращает тоже самое что и метод
     * getModifiedForApi(), но иногда может быть переопределён.
     *
     * @return array|string
     */
    public function getModifiedForApiV1();
}