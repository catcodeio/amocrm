<?php

namespace Amocrm\Api\Model\CustomField;

use Amocrm\Api\Model\CustomField\Type\Checkbox;
use Amocrm\Api\Model\CustomField\Type\Multiselect;
use Amocrm\Api\Model\CustomField\Type\Organization;
use Amocrm\Api\Model\CustomField\Type\Select;
use Amocrm\Api\Model\CustomField\Type\Text;
use Amocrm\Api\Model\CustomField\Type\Numeric;
use Amocrm\Api\Model\CustomField\Type\Date;
use Amocrm\Api\Model\CustomField\Type\Link;
use Amocrm\Api\Model\CustomField\Type\Phone;
use Amocrm\Api\Model\CustomField\Type\Email;
use Amocrm\Api\Model\CustomField\Type\Im;
use Amocrm\Api\Model\CustomField\Type\Textarea;
use Amocrm\Api\Model\CustomField\Type\AddressShort;
use Amocrm\Api\Model\CustomField\Type\RadioButton;
use Amocrm\Api\Model\CustomField\Type\Address;
use Amocrm\Api\Model\CustomField\Type\Birthday;
use Amocrm\Api\Model\CustomField\Type\Entity;
use Amocrm\Api\Model\CustomField\Type\TypeInterface;
use Amocrm\Api\Helper\Utils;
use Amocrm\Api\Model\AbstractModel;
use Amocrm\Api\Model\ModelInterface;
use RuntimeException;

/**
 * Внимание! Для инициализации нового доп. поля удобнее сразу задать id, а потом
 * уже заниматься только присвоением значения. Для этого нужно в create передать
 * массив ['id' => $id].
 *
 * @method int         getId()
 * @method CustomField setId()
 * @method string      getName()
 * @method array       getValues()
 * @method CustomField setValues(array $values)
 *
 * @method Checkbox     checkbox()
 * @method Multiselect  multiselect()
 * @method Select       select()
 * @method Text         text()
 * @method Numeric      numeric()
 * @method Date         date()
 * @method Link         link()
 * @method Phone        phone()
 * @method Email        email()
 * @method Im           im()
 * @method Textarea     textarea()
 * @method AddressShort addressShort()
 * @method RadioButton  radioButton()
 * @method Address      address()
 * @method Birthday     birthday()
 * @method Entity       entity()
 * @method Organization organization()
 */
class CustomField extends AbstractModel
{
    /**
     * @var TypeInterface
     */
    private $cfWrapper;

    /**
     * Необходимо для понимания применяли ли при создании объекта clone или нет.
     *
     * @var bool
     */
    private $clone = false;

    /**
     * Необходимо для понимания отчищается значения поля сейчас или нет.
     *
     * @var bool
     */
    private $clear = false;

    /**
     * Автосоздание класса для работы с кокнретным типом доп. поля.
     *
     * @param string     $method
     * @param array|null $params
     *
     * @return mixed
     */
    public function __call(string $method, array $params = null)
    {
        $className = 'Amocrm\\Api\\Model\\CustomField\\Type\\' . Utils::toCamelCase($method);

        if (class_exists($className)) {
            if ($this->cfWrapper) {
                // Если уже доп. поле инициализировано, то вызывается исключение,
                // при попытке инициализровать в другой тип.
                if (get_class($this->cfWrapper) != $className) {
                    throw new RuntimeException(sprintf(
                        'Custom field %s can not be used as %s in %s yet',
                        get_class($this->cfWrapper),
                        $method,
                        static::class
                    ));
                }
            } else {
                /**
                 * @var $className ModelInterface
                 */
                $this->cfWrapper = $className::create($this);
                $this->clone     = false;
            }

            return $this->cfWrapper;
        }

        return parent::__call($method, $params);
    }

    /**
     * Устанавливает флаг означающий, что поле в amoCRM должно быть отчищено от
     * значений при обновлении сущности. При это без разницы что в него добавили,
     * это всё игнорируется и значение в amoCRM перетирается.
     *
     * Это не зависит от типа, всегда если послать в values null, то значение из
     * поля будет удалено.
     *
     * @return self
     */
    public function clear()
    {
        $this->clear = true;

        return $this;
    }

    /**
     * @return bool
     */
    public function isClear(): bool
    {
        return $this->clear;
    }

    /**
     * Проверяет содержит ли какое-то значение доп. поле или нет.
     *
     * @return bool
     */
    public function isEmpty(): bool
    {
        $values = $this->getValues();

        if (!(bool)$values) {
            return true;
        }
        // Отсеиваем те значения, где в value null, пустая строка или false.
        return !isset($values[0]['value'])
            || $values[0]['value'] === ''
            || $values[0]['value'] === false;
    }

    /**
     * @inheritdoc
     */
    public function getModifiedForApi(): array
    {
        if ($this->clone) {
            return parent::getModifiedForApi();
        }

        if ($this->clear) {
            return [
                'id'     => $this->getId(),
                'values' => null,
            ];
        }

        return $this->cfWrapper ? $this->cfWrapper->getModifiedForApi() : [];
    }

    /**
     * @inheritdoc
     */
    public function __clone()
    {
        parent::__clone();

        $this->clone = true;
    }
}