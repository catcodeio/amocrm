<?php

namespace Amocrm\Api\Model\CustomField;

use Amocrm\Api\Model\AbstractModelCollection;

/**
 * Коллекция доп. полей.
 *
 * @method CustomField first()
 * @method CustomField last()
 * @method CustomField current()
 * @method CustomField get($key)
 * @method CustomField filterOneByField($name, $value)
 *
 * @method CustomField getOneById(int $id)
 * @method CustomField getOneByName(string $name)
 */
class CustomFields extends AbstractModelCollection
{
    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return CustomField::class;
    }

    /**
     * @inheritdoc
     */
    public function getModifiedForApi()
    {
        // Фильтруем пустые массивы т.к. в доп. поля они не нужны.
        return array_filter(parent::getModifiedForApi());
    }
}