<?php

namespace Amocrm\Api\Model\CustomField;

use Amocrm\Api\Model\AbstractModelCollection;

/**
 * Коллекция непосредственно доп. полей для добавления и удаления через API.
 *
 * @method Field first()
 * @method Field last()
 * @method Field current()
 * @method Field get($key)
 * @method Field filterOneByField($name, $value)
 *
 * @method Field getOneById(int $id)
 * @method Field getOneByName(string $name)
 */
class Fields extends AbstractModelCollection
{
    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Field::class;
    }
}