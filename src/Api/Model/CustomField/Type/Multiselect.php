<?php

namespace Amocrm\Api\Model\CustomField\Type;

/**
 * Функционал доп. поля мультисписок – тип 5.
 */
class Multiselect extends AbstractType
{
    /**
     * @inheritdoc
     */
    public static function getType(): int
    {
        return 5;
    }

    /**
     * @return array|null
     */
    public function get(): ?array
    {
        return $this->customField->getValues();
    }

    /**
     * @param array $values
     *
     * @return Multiselect
     */
    public function set(array $values): Multiselect
    {
        $this->customField->setValues($values);

        return $this;
    }

    /**
     * @param string|null $enumText
     * @param int|null    $enumId
     *
     * @return Multiselect
     */
    public function add(?string $enumText, int $enumId = null): Multiselect
    {
        $values = $this->customField->getValues();
        $item   = [];

        if ($enumText) {
            $item['value'] = $enumText;
        }

        if ($enumId) {
            $item['enum'] = $enumId;
        }

        $values[] = $item;

        $this->customField->setValues($values);

        return $this;
    }

    /**
     * Возвращает массив только значений без типов (без enum).
     *
     * @return array
     */
    public function list(): array
    {
        $values = $this->customField->getValues();

        if (!$values) {
            return [];
        }

        return array_column($values, 'value');
    }

    /**
     * @param string $enumText
     * @param int    $enumId
     *
     * @return Multiselect
     */
    public function remove(?string $enumText, int $enumId = null): Multiselect
    {
        $values = $this->customField->getValues();

        if ($enumText) {
            foreach ($values as $key => $item) {
                if ($item['value'] == $enumText) {
                    unset($values[$key]);

                    break;
                }
            }
        } elseif ($enumId) {
            foreach ($values as $key => $item) {
                if ($item['enum'] == $enumId) {
                    unset($values[$key]);

                    break;
                }
            }
        }

        $this->customField->setValues($values);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getModifiedForApi(): array
    {
        $result = [
            'id'     => $this->customField->getId(),
            'values' => [],
        ];

        foreach ($this->customField->getValues() as $value) {
            if (isset($value['enum']) && $value['enum']) {
                $result['values'][] = $value['enum'];
            } else {
                $result['values'][] = $value['value'];
            }
        }

        return $result;
    }
}