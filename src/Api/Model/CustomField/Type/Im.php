<?php

namespace Amocrm\Api\Model\CustomField\Type;

/**
 * Функционал доп. поля im – тип 8.
 */
class Im extends AbstractCommunications
{
    /**
     * Подтип для Skype (каким файлообменником ты пользуешься?).
     *
     * @const string
     */
    public const ENUM_SKYPE = 'SKYPE';

    /**
     * Подтип для ICQ.
     *
     * @const string
     */
    public const ENUM_ICQ = 'ICQ';

    /**
     * Подтип для Jabber.
     *
     * @const string
     */
    public const ENUM_JABBER = 'JABBER';

    /**
     * Подтип для Google Talk.
     *
     * @const string
     */
    public const ENUM_GTALK = 'GTALK';

    /**
     * Подтип для MSN.
     *
     * @const string
     */
    public const ENUM_MSN = 'MSN';

    /**
     * Подтип для остальных форм связи.
     *
     * @const string
     */
    public const ENUM_OTHER = 'OTHER';

    /**
     * @inheritdoc
     */
    protected function getEnumDefault(): string
    {
        return self::ENUM_OTHER;
    }
}