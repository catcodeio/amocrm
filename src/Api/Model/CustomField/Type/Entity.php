<?php

namespace Amocrm\Api\Model\CustomField\Type;

/**
 * Функционал доп. поля юридическое лицо – тип 15.
 */
class Entity extends AbstractType
{
    /**
     * @var int
     */
    private $key = 0;

    /**
     * @inheritdoc
     */
    public static function getType(): int
    {
        return 15;
    }

    /**
     * @return array|null
     */
    public function get(): ?array
    {
        return $this->customField->getValues();
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->getItem('name');
    }

    /**
     * @return string|null
     */
    public function getEntityType(): ?string
    {
        return $this->getItem('entity_type');
    }

    /**
     * @return string|null
     */
    public function getVatId(): ?string
    {
        return $this->getItem('vat_id');
    }

    /**
     * @return string|null
     */
    public function getTaxRegistrationReasonCode(): ?string
    {
        return $this->getItem('tax_registration_reason_code');
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->getItem('address');
    }

    /**
     * @return string|null
     */
    public function getKpp(): ?string
    {
        return $this->getItem('kpp');
    }

    /**
     * @return string|null
     */
    public function getExternalUid(): ?string
    {
        return $this->getItem('external_uid');
    }

    /**
     * @param array $value
     *
     * @return Entity
     */
    public function set(array $value): Entity
    {
        $this->customField->setValues($value);

        return $this;
    }

    /**
     * @param null|string $name
     * @param null|string $entityType
     * @param null|string $vatId
     * @param null|string $tax
     * @param null|string $address
     * @param null|string $kpp
     * @param null|string $externalUid
     *
     * @return Organization
     */
    public function add(
        ?string $name,
        ?string $entityType = null,
        ?string $vatId = null,
        ?string $tax = null,
        ?string $address = null,
        ?string $kpp = null,
        ?string $externalUid = null
    ): Entity
    {
        $values = $this->customField->getValues();

        $values[] = ['value' => [
            'name'                         => $name,
            'entity_type'                  => $entityType,
            'vat_id'                       => $vatId,
            'tax_registration_reason_code' => $tax,
            'address'                      => $address,
            'kpp'                          => $kpp,
            'external_uid'                 => $externalUid,
        ]];

        $this->set($values);

        return $this;
    }

    /**
     * @param string|null $value
     *
     * @return Entity
     */
    public function setName(?string $value): Entity
    {
        return $this->setItem('name', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Entity
     */
    public function setEntityType(?string $value): Entity
    {
        return $this->setItem('entity_type', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Entity
     */
    public function setVatId(?string $value): Entity
    {
        return $this->setItem('vat_id', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Entity
     */
    public function setTaxRegistrationReasonCode(?string $value): Entity
    {
        return $this->setItem('tax_registration_reason_code', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Entity
     */
    public function setAddress(?string $value): Entity
    {
        return $this->setItem('address', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Entity
     */
    public function setKpp(?string $value): Entity
    {
        return $this->setItem('kpp', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Entity
     */
    public function setExternalUid(?string $value): Entity
    {
        return $this->setItem('external_uid', $value);
    }

    /**
     * @param string $key
     *
     * @return null|string
     */
    private function getItem(string $key): ?string
    {
        $values = $this->customField->getValues();

        if (isset($values[$this->key]['value'][$key])) {
            return $values[$this->key]['value'][$key];
        }

        return null;
    }

    /**
     * @param string $key
     * @param string $value
     *
     * @return Entity
     */
    private function setItem(string $key, string $value): Entity
    {
        $values = $this->customField->getValues();

        $values[$this->key]['value'][$key] = $value;

        $this->customField->setValues($values);

        return $this;
    }

    /**
     * Возвращает кол-во организаций в поле.
     *
     * @return int
     */
    public function count(): int
    {
        $values = $this->customField->getValues();

        if (!$values) {
            return 0;
        }

        return count($values);
    }

    /**
     * Сбрасывает внутренний счётчик.
     *
     * @return bool Есть ли во внутреннем массиве данные по этому ключ.
     */
    public function rewind()
    {
        $this->key = 0;

        return $this->valid();
    }

    /**
     * Возвращает внутренний счётчик.
     *
     * @return int
     */
    public function key()
    {
        return $this->key;
    }

    /**
     * Итерирует внутренний счётчик.
     *
     * @return bool Есть ли во внутреннем массиве данные по новому ключ.
     */
    public function next()
    {
        $this->key++;

        return $this->valid();
    }

    /**
     * Проверят наличие данных по текущему ключу.
     *
     * @return bool Есть ли во внутреннем массиве данные по текущему ключ.
     */
    public function valid()
    {
        $values = $this->customField->getValues();

        return isset($values[$this->key]);
    }
}