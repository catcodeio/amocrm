<?php

namespace Amocrm\Api\Model\CustomField\Type;

/**
 * Функционал доп. поля число – тип 2.
 */
class Numeric extends AbstractType
{
    /**
     * @inheritdoc
     */
    public static function getType(): int
    {
        return 2;
    }

    /**
     * @return string|null
     */
    public function get(): ?string
    {
        return $this->getFirstValue();
    }

    /**
     * @param string|null $value
     *
     * @return Numeric
     */
    public function set(?string $value): Numeric
    {
        if (!$value) {
            return $this;
        }

        $this->customField->setValues([[
            'value' => $value,
        ]]);

        return $this;
    }
}