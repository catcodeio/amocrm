<?php

namespace Amocrm\Api\Model\CustomField\Type;

use DateTime;

/**
 * Функционал доп. поля день рождения – тип 14.
 */
class Birthday extends AbstractType
{
    /**
     * @inheritdoc
     */
    public static function getType(): int
    {
        return 14;
    }

    /**
     * @return DateTime|null
     */
    public function get(): ?DateTime
    {
        $value = $this->getFirstValue();

        if ($value) {
            return new DateTime($value);
        }

        return null;
    }

    /**
     * @param DateTime $value
     *
     * @return Birthday
     */
    public function set(DateTime $value): Birthday
    {
        $this->customField->setValues([[
            'value' => $value->format('Y-m-d 00:00:00'),
        ]]);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getModifiedForApi(): array
    {
        $result = [
            'id'     => $this->customField->getId(),
            'values' => [],
        ];

        $value = $this->getFirstValue();

        if ($value) {
            $result['values'][] = [
                'value' => (new DateTime($value))->format('d.m.Y'),
            ];
        }

        return $result;
    }
}