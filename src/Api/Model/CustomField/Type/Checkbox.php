<?php

namespace Amocrm\Api\Model\CustomField\Type;

/**
 * Функционал доп. поля флаг – тип 3.
 */
class Checkbox extends AbstractType
{
    /**
     * @inheritdoc
     */
    public static function getType(): int
    {
        return 3;
    }

    /**
     * @return bool
     */
    public function get(): bool
    {
        return (bool)$this->getFirstValue();
    }

    /**
     * @param bool $value
     *
     * @return Checkbox
     */
    public function set(bool $value): Checkbox
    {
        $this->customField->setValues([[
            'value' => (string)(int)$value,
        ]]);

        return $this;
    }
}