<?php

namespace Amocrm\Api\Model\CustomField\Type;

/**
 * Функционал доп. поля список – тип 4.
 */
class Select extends AbstractType
{
    /**
     * @inheritdoc
     */
    public static function getType(): int
    {
        return 4;
    }

    /**
     * @return string|null
     */
    public function get(): ?string
    {
        return $this->getFirstValue();
    }

    /**
     * @param string|null $enumText
     * @param int|null    $enumId
     *
     * @return Select
     */
    public function set(?string $enumText, int $enumId = null): Select
    {
        $item = [];

        if ($enumText) {
            $item['value'] = $enumText;
        }

        if ($enumId) {
            $item['enum'] = $enumId;
        }

        $this->customField->setValues([$item]);

        return $this;
    }

    /**
     * @return Select
     */
    public function remove(): Select
    {
        $this->customField->setValues([
            'value' => null,
        ]);

        return $this;
    }
}