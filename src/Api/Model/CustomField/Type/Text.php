<?php

namespace Amocrm\Api\Model\CustomField\Type;

/**
 * Функционал доп. поля текст – тип 1.
 */
class Text extends AbstractType
{
    /**
     * @inheritdoc
     */
    public static function getType(): int
    {
        return 1;
    }

    /**
     * @return string|null
     */
    public function get(): ?string
    {
        return $this->getFirstValue();
    }

    /**
     * @param string|null $value
     *
     * @return Text
     */
    public function set(?string $value): Text
    {
        if (!$value) {
            return $this;
        }

        $this->customField->setValues([[
            'value' => $value,
        ]]);

        return $this;
    }
}