<?php

namespace Amocrm\Api\Model\CustomField\Type;

use DateTime;

/**
 * Функционал доп. поля дата – тип 6.
 */
class Date extends AbstractType
{
    /**
     * @inheritdoc
     */
    public static function getType(): int
    {
        return 6;
    }

    /**
     * @return DateTime|null
     */
    public function get(): ?DateTime
    {
        $value = $this->getFirstValue();

        if ($value) {
            return new DateTime($value);
        }

        return null;
    }

    /**
     * @param DateTime $value
     *
     * @return Date
     */
    public function set(DateTime $value): Date
    {
        $this->customField->setValues([[
            'value' => $value->format('Y-m-d 00:00:00'),
        ]]);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getModifiedForApi(): array
    {
        $result = [
            'id'     => $this->customField->getId(),
            'values' => [],
        ];

        $value = $this->getFirstValue();

        if ($value) {
            $result['values'][] = [
                'value' => (new DateTime($value))->format('d.m.Y'),
            ];
        }

        return $result;
    }
}