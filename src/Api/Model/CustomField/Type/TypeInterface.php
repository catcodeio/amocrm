<?php

namespace Amocrm\Api\Model\CustomField\Type;

use Amocrm\Api\Model\CustomField\CustomField;
use Amocrm\Api\Model\ModelInterface;

/**
 * Интерфейс для типов доп. полей. Это будут обёртки дающие свой набор методов
 * для работы с каждым типов доп. поля.
 */
interface TypeInterface extends ModelInterface
{
    /**
     * Возвращает CustomField с данными, вокруг которых используется эта обёртка,
     * чтобы присвоить данные в сущность т.к. там ждут именно CustomField.
     *
     * @return CustomField
     */
    public function getCustomField(): CustomField;

    /**
     * Возвращает числовой тип поля. Это внутренний тип полей в amoCRM.
     *
     * @return int
     */
    public static function getType(): int;
}