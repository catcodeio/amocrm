<?php

namespace Amocrm\Api\Model\CustomField\Type;

use Amocrm\Api\Helper\Country;

/**
 * Функционал доп. поля адрес – тип 13.
 */
class Address extends AbstractType
{
    /**
     * @inheritdoc
     */
    public static function getType(): int
    {
        return 13;
    }

    /**
     * @return array|null
     */
    public function get(): ?array
    {
        return $this->customField->getValues();
    }

    /**
     * @return string|null
     */
    public function getAddressLine1(): ?string
    {
        return $this->getItem('address_line_1');
    }

    /**
     * @return string|null
     */
    public function getAddressLine2(): ?string
    {
        return $this->getItem('address_line_2');
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->getItem('city');
    }

    /**
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->getItem('state');
    }

    /**
     * @return string|null
     */
    public function getZip(): ?string
    {
        return $this->getItem('zip');
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->getItem('country');
    }

    /**
     * @param string $locale
     *
     * @return string|null
     */
    public function getCountryName(string $locale): ?string
    {
        $iata = $this->getCountry();

        return Country::getName($iata, $locale);
    }

    /**
     * @param string      $subtype
     * @param string|null $locale Требуется только для поля country. Если не передано, то возвратится IATA, иначе
     *                            название страны. Во всех остальных типах полей игнорируется.
     *
     * @return string|null
     */
    public function getBySubtype(string $subtype, string $locale = null): ?string
    {
        if ($subtype == 'country') {
            return $locale ? $this->getCountryName($locale) : $this->getCountry();
        }

        return $this->getItem($subtype);
    }

    /**
     * @param array $value
     *
     * @return Address
     */
    public function set(array $value): Address
    {
        $this->customField->setValues($value);

        return $this;
    }

    /**
     * @param string|null $value
     *
     * @return Address
     */
    public function setAddressLine1(?string $value): Address
    {
        return $this->setItem('address_line_1', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Address
     */
    public function setAddressLine2(?string $value): Address
    {
        return $this->setItem('address_line_2', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Address
     */
    public function setCity(?string $value): Address
    {
        return $this->setItem('city', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Address
     */
    public function setState(?string $value): Address
    {
        return $this->setItem('state', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Address
     */
    public function setZip(?string $value): Address
    {
        return $this->setItem('zip', $value);
    }

    /**
     * Принимает страны в виде двузначных IATA-кодов.
     *
     * @param string|null $value
     *
     * @return Address
     */
    public function setCountry(?string $value): Address
    {
        return $this->setItem('country', $value);
    }

    /**
     * @param string $subtype
     *
     * @return null|string
     */
    private function getItem(string $subtype): ?string
    {
        $values = $this->customField->getValues();

        if (!$values) {
            return null;
        }

        foreach ($values as $value) {
            if ($value['subtype'] == $subtype) {
                return $value['value'];
            }
        }

        return null;
    }

    /**
     * @param string      $subtype
     * @param string|null $value
     *
     * @return Address
     */
    private function setItem(string $subtype, ?string $value): Address
    {
        $values = $this->customField->getValues() ?? [];
        $found  = false;

        foreach ($values as $key => &$item) {
            if ($item['subtype'] == $subtype) {
                if ($value === null) {
                    unset($values[$key]);
                } else {
                    $item['value'] = $value;
                }

                $found = true;
            }
        }

        if (!$found && $value) {
            $values[] = [
                'value'   => $value,
                'subtype' => $subtype,
            ];
        }

        $this->customField->setValues($values);

        return $this;
    }
}