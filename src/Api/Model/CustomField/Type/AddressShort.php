<?php

namespace Amocrm\Api\Model\CustomField\Type;

/**
 * Функционал доп. поля короткий адрес – тип 11.
 */
class AddressShort extends AbstractType
{
    /**
     * @inheritdoc
     */
    public static function getType(): int
    {
        return 11;
    }

    /**
     * @return string|null
     */
    public function get(): ?string
    {
        return $this->getFirstValue();
    }

    /**
     * @param string|null $value
     *
     * @return AddressShort
     */
    public function set(?string $value): AddressShort
    {
        if (!$value) {
            return $this;
        }

        $this->customField->setValues([[
            'value' => $value,
        ]]);

        return $this;
    }
}