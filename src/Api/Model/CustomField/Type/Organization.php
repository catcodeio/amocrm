<?php

namespace Amocrm\Api\Model\CustomField\Type;

use Amocrm\Api\Helper\Country;

/**
 * Функционал доп. поля юридическое лицо – тип 17.
 */
class Organization extends AbstractType
{
    /**
     * @var int
     */
    private $key = 0;

    /**
     * @inheritdoc
     */
    public static function getType(): int
    {
        return 17;
    }

    /**
     * @return array|null
     */
    public function get(): ?array
    {
        return $this->customField->getValues();
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->getItem('name');
    }

    /**
     * @return string|null
     */
    public function getEntityType(): ?string
    {
        return $this->getItem('entity_type');
    }

    /**
     * @return string|null
     */
    public function getVatId(): ?string
    {
        return $this->getItem('vat_id');
    }

    /**
     * @return string|null
     */
    public function getTaxRegistrationReasonCode(): ?string
    {
        return $this->getItem('tax_registration_reason_code');
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->getItem('address');
    }

    /**
     * @return string|null
     */
    public function getKpp(): ?string
    {
        return $this->getItem('kpp');
    }

    /**
     * @return string|null
     */
    public function getExternalUid(): ?string
    {
        return $this->getItem('external_uid');
    }

    /**
     * @return string|null
     */
    public function getLine1(): ?string
    {
        return $this->getItem('line1');
    }

    /**
     * @return string|null
     */
    public function getLine2(): ?string
    {
        return $this->getItem('line2');
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->getItem('city');
    }

    /**
     * @return string|null
     */
    public function getState(): ?string
    {
        return $this->getItem('state');
    }

    /**
     * @return string|null
     */
    public function getZip(): ?string
    {
        return $this->getItem('zip');
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->getItem('country');
    }

    /**
     * @param string $locale
     *
     * @return string|null
     */
    public function getCountryName(string $locale): ?string
    {
        $iata = $this->getCountry();

        return Country::getName($iata, $locale);
    }

    /**
     * @param array $values
     *
     * @return Organization
     */
    public function set(array $values): Organization
    {
        $this->customField->setValues($values);

        return $this;
    }

    /**
     * @param null|string $name
     * @param null|string $entityType
     * @param null|string $vatId
     * @param null|string $tax
     * @param null|string $address
     * @param null|string $kpp
     * @param null|string $externalUid
     * @param null|string $line1
     * @param null|string $line2
     * @param null|string $city
     * @param null|string $state
     * @param null|string $zip
     * @param null|string $country
     *
     * @return Organization
     */
    public function add(
        ?string $name,
        ?string $entityType = null,
        ?string $vatId = null,
        ?string $tax = null,
        ?string $address = null,
        ?string $kpp = null,
        ?string $externalUid = null,
        ?string $line1 = null,
        ?string $line2 = null,
        ?string $city = null,
        ?string $state = null,
        ?string $zip = null,
        ?string $country = null
    ): Organization
    {
        $values = $this->customField->getValues();

        $values[] = ['value' => [
            'name'                         => $name,
            'entity_type'                  => $entityType,
            'vat_id'                       => $vatId,
            'tax_registration_reason_code' => $tax,
            'address'                      => $address,
            'kpp'                          => $kpp,
            'external_uid'                 => $externalUid,
            'line1'                        => $line1,
            'line2'                        => $line2,
            'city'                         => $city,
            'state'                        => $state,
            'zip'                          => $zip,
            'country'                      => $country,
        ]];

        $this->set($values);

        return $this;
    }

    /**
     * @param string|null $value
     *
     * @return Organization
     */
    public function setName(?string $value): Organization
    {
        return $this->setItem('name', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Organization
     */
    public function setEntityType(?string $value): Organization
    {
        return $this->setItem('entity_type', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Organization
     */
    public function setVatId(?string $value): Organization
    {
        return $this->setItem('vat_id', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Organization
     */
    public function setTaxRegistrationReasonCode(?string $value): Organization
    {
        return $this->setItem('tax_registration_reason_code', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Organization
     */
    public function setAddress(?string $value): Organization
    {
        return $this->setItem('address', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Organization
     */
    public function setKpp(?string $value): Organization
    {
        return $this->setItem('kpp', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Organization
     */
    public function setExternalUid(?string $value): Organization
    {
        return $this->setItem('external_uid', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Organization
     */
    public function setLine1(?string $value): Organization
    {
        return $this->setItem('line1', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Organization
     */
    public function setLine2(?string $value): Organization
    {
        return $this->setItem('line2', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Organization
     */
    public function setCity(?string $value): Organization
    {
        return $this->setItem('city', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Organization
     */
    public function setState(?string $value): Organization
    {
        return $this->setItem('state', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Organization
     */
    public function setZip(?string $value): Organization
    {
        return $this->setItem('zip', $value);
    }

    /**
     * @param string|null $value
     *
     * @return Organization
     */
    public function setCountry(?string $value): Organization
    {
        return $this->setItem('country', $value);
    }

    /**
     * @param string $key
     *
     * @return null|string
     */
    private function getItem(string $key): ?string
    {
        $values = $this->customField->getValues();

        if (isset($values[$this->key]['value'][$key])) {
            return $values[$this->key]['value'][$key];
        }

        return null;
    }

    /**
     * @param string $key
     * @param string $value
     *
     * @return Organization
     */
    private function setItem(string $key, string $value): Organization
    {
        $values = $this->customField->getValues();

        $values[$this->key]['value'][$key] = $value;

        $this->customField->setValues($values);

        return $this;
    }

    /**
     * Возвращает кол-во организаций в поле.
     *
     * @return int
     */
    public function count(): int
    {
        $values = $this->customField->getValues();

        if (!$values) {
            return 0;
        }

        return count($values);
    }

    /**
     * Сбрасывает внутренний счётчик.
     *
     * @return bool Есть ли во внутреннем массиве данные по этому ключ.
     */
    public function rewind()
    {
        $this->key = 0;

        return $this->valid();
    }

    /**
     * Возвращает внутренний счётчик.
     *
     * @return int
     */
    public function key()
    {
        return $this->key;
    }

    /**
     * Итерирует внутренний счётчик.
     *
     * @return bool Есть ли во внутреннем массиве данные по новому ключ.
     */
    public function next()
    {
        $this->key++;

        return $this->valid();
    }

    /**
     * Проверят наличие данных по текущему ключу.
     *
     * @return bool Есть ли во внутреннем массиве данные по текущему ключ.
     */
    public function valid()
    {
        $values = $this->customField->getValues();

        return isset($values[$this->key]);
    }
}