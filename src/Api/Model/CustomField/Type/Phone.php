<?php

namespace Amocrm\Api\Model\CustomField\Type;

use Amocrm\Api\Helper\Phone as PhoneHelper;

/**
 * Функционал доп. поля телефон – тип 8.
 */
class Phone extends AbstractCommunications
{
    /**
     * Подтип для рабочего телефона.
     *
     * @const string
     */
    public const ENUM_WORK = 'WORK';

    /**
     * Подтип для рабочего приямого телефона.
     *
     * @const string
     */
    public const ENUM_WORKDD = 'WORKDD';

    /**
     * Подтип для мобильного телефона.
     *
     * @const string
     */
    public const ENUM_MOB = 'MOB';

    /**
     * Подтип для факса.
     *
     * @const string
     */
    public const ENUM_FAX = 'FAX';

    /**
     * Подтип для домашнего телефона.
     *
     * @const string
     */
    public const ENUM_HOME = 'HOME';

    /**
     * Подтип для остальных телефонов.
     *
     * @const string
     */
    public const ENUM_OTHER = 'OTHER';

    /**
     * @inheritdoc
     */
    protected function getEnumDefault(): string
    {
        return self::ENUM_MOB;
    }

    /**
     * @inheritdoc
     */
    public function add(?string $value, $enum = null)
    {
        return parent::add(PhoneHelper::clear($value, true), $enum);
    }
}