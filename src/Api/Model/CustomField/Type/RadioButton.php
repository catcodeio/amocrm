<?php

namespace Amocrm\Api\Model\CustomField\Type;

/**
 * Функционал доп. поля переключатель – тип 10.
 */
class RadioButton extends AbstractType
{
    /**
     * @inheritdoc
     */
    public static function getType(): int
    {
        return 10;
    }

    /**
     * @return string|null
     */
    public function get(): ?string
    {
        return $this->getFirstValue();
    }

    /**
     * @param string|null $enumText
     * @param int|null    $enumId
     *
     * @return RadioButton
     */
    public function set(?string $enumText, int $enumId = null): RadioButton
    {
        $item = [];

        if ($enumText) {
            $item['value'] = $enumText;
        }

        if ($enumId) {
            $item['enum'] = $enumId;
        }

        $this->customField->setValues([$item]);

        return $this;
    }

    /**
     * @return RadioButton
     */
    public function remove(): RadioButton
    {
        $this->customField->setValues([
            'value' => null,
        ]);

        return $this;
    }
}