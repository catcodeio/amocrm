<?php

namespace Amocrm\Api\Model\CustomField\Type;

/**
 * Общий функционал доп. поля типа 8.
 */
abstract class AbstractCommunications extends AbstractType
{
    /**
     * @inheritdoc
     */
    public static function getType(): int
    {
        return 8;
    }

    /**
     * Тип элемента сущности по умолчанию.
     *
     * @return string
     */
    protected abstract function getEnumDefault(): string;

    /**
     * @return array|null
     */
    public function get(): ?array
    {
        return $this->customField->getValues();
    }

    /**
     * Нюанс! Из amoCRM приходят enum в виде цифер, которые мы можем узнать
     * только из данных аккаунта.
     *
     * @param string|int $enum
     *
     * @return string|null
     */
    public function getByEnum($enum): ?string
    {
        $values = (array)$this->customField->getValues();

        foreach ($values as $vl) {
            if (isset($vl['enum']) && $vl['enum'] == $enum) {
                return $vl['value'];
            }
        }

        return null;
    }

    /**
     * @param array $value
     *
     * @return static
     */
    public function set(array $value)
    {
        $this->customField->setValues($value);

        return $this;
    }

    /**
     * Тут есть серьёзный нюанс! Приходят enum в виде цифер, отправлять надо в
     * виде букв или цифер, но не смешивая одно с другим иначе выходит всегда
     * по-разному. Надо это учитывать. Если enum не указан, то подставится
     * тот, который у первого элемента, либо если его нет, то getEnumDefault().
     *
     * @param string|null     $value
     * @param string|int|null $enum
     *
     * @return static
     */
    public function add(?string $value, $enum = null)
    {
        $value = trim($value);

        if (!$value) {
            return $this;
        }

        $values = (array)$this->customField->getValues();
        $item   = [];

        // Добавляем только если это не повтор.
        foreach ($values as $vl) {
            if (isset($vl['value']) && $vl['value'] == $value) {
                return $this;
            }
        }

        if (!$enum) {
            $enum = isset($values[0]['enum'])
                ? $values[0]['enum']
                : $this->getEnumDefault();
        }

        if ($value) {
            $item['value'] = $value;
        }

        if ($enum) {
            $item['enum'] = $enum;
        }

        $values[] = $item;

        $this->customField->setValues($values);

        return $this;
    }

    /**
     * @param string|null     $value
     * @param string|int|null $enum
     *
     * @return static
     */
    public function remove(?string $value, $enum = null)
    {
        $values = $this->customField->getValues();

        if ($value) {
            foreach ($values as $key => $item) {
                if ($item['value'] == $value) {
                    unset($values[$key]);
                }
            }
        } elseif ($enum) {
            foreach ($values as $key => $item) {
                if ($item['enum'] == $enum) {
                    unset($values[$key]);
                }
            }
        }

        $this->customField->setValues($values);

        return $this;
    }

    /**
     * Возвращает массив только значений без типов (без enum).
     *
     * @return array
     */
    public function list(): array
    {
        $values = $this->customField->getValues();

        if (!$values) {
            return [];
        }

        return array_column($values, 'value');
    }
}