<?php

namespace Amocrm\Api\Model\CustomField\Type;

/**
 * Функционал доп. поля текстовая область – тип 9.
 */
class Textarea extends AbstractType
{
    /**
     * @inheritdoc
     */
    public static function getType(): int
    {
        return 9;
    }

    /**
     * @return string|null
     */
    public function get(): ?string
    {
        return $this->getFirstValue();
    }

    /**
     * @param string|null $value
     *
     * @return Textarea
     */
    public function set(?string $value): Textarea
    {
        if (!$value) {
            return $this;
        }

        $this->customField->setValues([[
            'value' => $value,
        ]]);

        return $this;
    }
}