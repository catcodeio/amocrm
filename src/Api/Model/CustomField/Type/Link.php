<?php

namespace Amocrm\Api\Model\CustomField\Type;

/**
 * Функционал доп. поля ссылка – тип 7.
 */
class Link extends AbstractType
{
    /**
     * @inheritdoc
     */
    public static function getType(): int
    {
        return 7;
    }

    /**
     * @return string|null
     */
    public function get(): ?string
    {
        return $this->getFirstValue();
    }

    /**
     * @param string|null $value
     *
     * @return Link
     */
    public function set(?string $value): Link
    {
        if (!$value) {
            return $this;
        }

        $this->customField->setValues([[
            'value' => $value,
        ]]);

        return $this;
    }
}