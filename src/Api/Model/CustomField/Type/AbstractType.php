<?php

namespace Amocrm\Api\Model\CustomField\Type;

use Amocrm\Api\Model\CustomField\CustomField;

/**
 * Общее поведение поддериваемое большинством типов полей.
 */
abstract class AbstractType implements TypeInterface
{
    /**
     * @var CustomField
     */
    protected $customField;

    /**
     * Запрещаем использовать конструктор, создание только через метод-фабрику.
     *
     * @param CustomField $customField
     */
    private function __construct(CustomField $customField)
    {
        $this->customField = $customField;
    }

    /**
     * @inheritdoc
     *
     * @param CustomField $data
     */
    public static function create($data = null)
    {
        return new static($data);
    }

    /**
     * @inheritdoc
     */
    public function getCustomField(): CustomField
    {
        return $this->customField;
    }

    /**
     * Часто требуется получить первый элемент, потому вынесем.
     *
     * @return string|null
     */
    protected function getFirstValue(): ?string
    {
        $values = $this->customField->getValues();

        return isset($values[0]['value'])
            ? $values[0]['value']
            : null;
    }

    /**
     * @inheritdoc
     */
    public function getInitial(): array
    {
        return $this->customField->getInitial();
    }

    /**
     * @inheritdoc
     */
    public function getModified(): array
    {
        return $this->customField->getModified();
    }

    /**
     * @inheritdoc
     */
    public function isModified(): bool
    {
        return $this->customField->isModified();
    }

    /**
     * @inheritdoc
     */
    public function getModifiedForApi(): array
    {
        $result = [
            'id'     => $this->customField->getId(),
            'values' => [],
        ];

        if ($this->customField->getValues()) {
            foreach ($this->customField->getValues() as $value) {
                $item = [];

                if (isset($value['enum']) && $value['enum']) {
                    $item['enum'] = $value['enum'];
                }

                if (isset($value['value'])) {
                    $item['value'] = $value['value'];
                }

                if (isset($value['subtype']) && $value['subtype']) {
                    $item['subtype'] = $value['subtype'];
                }

                if ($item && array_key_exists('value', $item)) {
                    $result['values'][] = $item;
                }
            }
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getModifiedForApiV1(): array
    {
        return $this->getModifiedForApi();
    }
}