<?php

namespace Amocrm\Api\Model\CustomField\Type;

/**
 * Функционал доп. поля email – тип 8.
 */
class Email extends AbstractCommunications
{
    /**
     * Подтип для рабочего email.
     *
     * @const string
     */
    public const ENUM_WORK = 'WORK';

    /**
     * Подтип для личного email.
     *
     * @const string
     */
    public const ENUM_PRIV = 'PRIV';

    /**
     * Подтип для остальных email.
     *
     * @const string
     */
    public const ENUM_OTHER = 'OTHER';

    /**
     * @inheritdoc
     */
    protected function getEnumDefault(): string
    {
        return self::ENUM_WORK;
    }
}