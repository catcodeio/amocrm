<?php

namespace Amocrm\Api\Model\CustomField;

use Amocrm\Api\Model\CustomField\Type\TypeInterface;

/**
 * Трейт для сущностей, которые должны работать с доп. полями.
 *
 * @method CustomFields getCustomFields()
 */
trait CustomFieldTrait
{
    /**
     * @param int $cfId
     *
     * @return bool
     */
    public function hasCustomField(int $cfId)
    {
        return $this
            ->getCustomFields()
            ->exists(function ($key, CustomField $customField) use ($cfId) {
                return $customField->getId() == $cfId;
            });
    }

    /**
     * Метод возвращает объект работы с доп. полем даже если в сущности данных
     * не оказалось, он возвратит пустой объект, с которым можно работать сразу.
     * Это удобно, чтобы не проверять на null и не создавать поле с нуля,
     * подразумевается, что получаемое поле существует, просто оно пустое.
     *
     * @param int $cfId
     *
     * @return CustomField
     */
    public function getCustomField(int $cfId)
    {
        $cf = $this->getCustomFields()->getOneById($cfId);

        return $cf ?? CustomField::create(['id' => $cfId]);
    }

    /**
     * Синоним метода getCustomField() для краткости.
     *
     * @param int $cfId
     *
     * @return CustomField
     */
    public function getCF(int $cfId)
    {
        return $this->getCustomField($cfId);
    }

    /**
     * @param int $cfId
     *
     * @return CustomField
     *
     * @deprecated
     */
    public function cf(int $cfId)
    {
        return $this->getCF($cfId);
    }

    /**
     * @param TypeInterface $field
     *
     * @return static
     */
    public function setCustomField(TypeInterface $field)
    {
        $cf = $field->getCustomField();

        if ($this->hasCustomField($cf->getId())) {
            $this->removeCustomField($cf->getId());
        }

        $this->getCustomFields()->add($cf);

        return $this;
    }

    /**
     * Синоним для setCustomField() для краткости.
     *
     * @param TypeInterface $field
     *
     * @return static
     */
    public function setCF(TypeInterface $field)
    {
        return $this->setCustomField($field);
    }

    /**
     * Удаляет данные указанного из поля при обновлении сущности.
     *
     * @param CustomField|TypeInterface|int $field Принимает как сформированное поле, так и
     *                                      просто ID, на основе которого будет создан
     *                                      CustomField.
     *
     * @return static
     */
    public function clearCF($field)
    {
        if ($field instanceof TypeInterface) {
            $field = $field->getCustomField();
        }

        if (!$field instanceof CustomField) {
            $field = CustomField::create(['id' => $field]);
        }

        $field->clear();

        if ($this->hasCustomField($field->getId())) {
            $this->removeCustomField($field->getId());
        }

        $this->getCustomFields()->add($field);

        return $this;
    }

    /**
     * @param int $cfId
     *
     * @return static
     */
    public function removeCustomField(int $cfId)
    {
        $this->getCustomFields()->removeElement($this->getCustomField($cfId));

        return $this;
    }
}