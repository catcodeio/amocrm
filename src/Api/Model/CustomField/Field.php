<?php

namespace Amocrm\Api\Model\CustomField;

use Amocrm\Api\Helper\ElementType;
use Amocrm\Api\Model\AbstractModel;
use RuntimeException;

/**
 * Специальная модель для создания и удаления доп. полей. Оно отличается от
 * структуры доп полей из сущностей или из данных аккаунта.
 *
 * @method int    getId()
 * @method Field  setId(int $id)
 * @method string getName()
 * @method Field  setName(int $name)
 * @method int    getType()
 * @method Field  setType(int $type) Циферный тип доп. поля. В описании классов в папке Type можно поглядеть.
 * @method int    getDisabled() Возвращает 1 или 0, как true или false.
 * @method string getOrigin()
 * @method Field  setOrigin(string $origin)
 */
class Field extends AbstractModel
{
    /**
     * Тип поля "Текст".
     *
     * @var int
     */
    const TEXT = 1;

    /**
     * Тип поля "Число".
     *
     * @var int
     */
    const NUMERIC = 2;

    /**
     * Тип поля "Флаг".
     *
     * @var int
     */
    const CHECKBOX = 3;

    /**
     * Тип поля "Список".
     *
     * @var int
     */
    const SELECT = 4;

    /**
     * Тип поля "Мультисписок".
     *
     * @var int
     */
    const MULTISELECT = 5;

    /**
     * Тип поля "Дата".
     *
     * @var int
     */
    const DATE = 6;

    /**
     * Тип поля "Ссылка".
     *
     * @var int
     */
    const LINK = 7;

    /**
     * Тип поля "Мультитекст" (телефоны, мыла, меседжеры).
     *
     * @var int
     */
    const MULTITEXT = 8;

    /**
     * Тип поля "Текстовая область".
     *
     * @var int
     */
    const TEXTAREA = 9;

    /**
     * Тип поля "Переключатель".
     *
     * @var int
     */
    const RADIO_BUTTON = 10;

    /**
     * Тип поля "Короткий адрес".
     *
     * @var int
     */
    const ADDRESS_SHORT = 11;

    /**
     * Тип поля "Адрес".
     *
     * @var int
     */
    const ADDRESS = 13;

    /**
     * Тип поля "День рождения".
     *
     * @var int
     */
    const BIRTHDAY = 14;

    /**
     * Тип поля "Юр. лицо".
     *
     * @var int
     */
    const ENTITY = 15;

    /**
     * Тип сущности, для которого создаётся поле. Принимает константы из ElementType.
     *
     * @param string|null $elementType
     *
     * @return Field
     */
    public function setElementType(?string $elementType): Field
    {
        $this->__call(__FUNCTION__, [ElementType::toNumber($elementType)]);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getElementType(): ?string
    {
        return ElementType::fromNumber($this->__call(__FUNCTION__));
    }

    /**
     * @param bool $disabled
     *
     * @return Field
     */
    public function setDisabled(bool $disabled): Field
    {
        $this->__call(__FUNCTION__, [(string)(int)$disabled]);

        return $this;
    }

    /**
     * @return bool
     */
    public function isDisabled(): bool
    {
        return (bool)$this->getDisabled();
    }

    /**
     * @inheritdoc
     */
    public function getModifiedForApi(): array
    {
        $result = parent::getModifiedForApi();
        // Если не установлен origin, то сгенерируем сами, но для этого нужны все данные.
        if (!isset($result['origin']) || !$result['origin']) {
            if (!$this->getName() || !$this->getType() || !$this->getElementType()) {
                throw new RuntimeException(
                    'To generate an origin for custom field, you need set the name, type and element_type.'
                );
            }

            $result['origin'] = $this->generateOrigin(
                $this->getName(),
                $this->getType(),
                $this->getElementType()
            );
        }

        return $result;
    }

    /**
     * Получения уникальной строки для создаваемого доп. поля.
     *
     * @param string $name
     * @param int    $type
     * @param string $elementType Примание константы из ElementType
     *
     * @return string
     */
    public static function generateOrigin(string $name, int $type, string $elementType)
    {
        return 'catcode_custom_field_' . md5($name . $type . $elementType);
    }
}
