<?php

namespace Amocrm\Api\Model;

/**
 * @method int    getWidgetId()
 * @method Widget setWidgetId(int $widgetId)
 * @method string getWidgetCode()
 * @method Widget setWidgetCode(int $widgetCode)
 * @method array  getSettings()
 * @method Widget setSettings(array $settings)
 * @method bool   getActive()
 * @method Widget setActive(bool $active)
 */
class Widget extends AbstractModel
{
    /**
     * @param array|null $initial
     */
    protected function __construct(?array $initial = [])
    {
        // Проблема в том, что при получении данных виджета используется widget_id и widget_code, а при установке или
        // удалении id и code. Всё пересохраняем в widget_id и widget_code т.к. отправлять всё равно их. Плюс id и code
        // всегда в приоритете и перетирают widget_id и widget_code т.к. могут мержится два массива из разных мест.
        if (isset($initial['id'])) {
            $initial['widget_id'] = $initial['id'];
        }

        if (isset($initial['code'])) {
            $initial['widget_code'] = $initial['code'];
        }

        $this->initial = $initial ?? [];
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return (bool)$this->__call('getActive');
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->__call('getWidgetId');
    }

    /**
     * @param int $id
     *
     * @return Widget
     */
    public function setId(int $id): Widget
    {
        $this->__call('setWidgetId', [$id]);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->__call('getWidgetCode');
    }

    /**
     * @param string $code
     *
     * @return Widget
     */
    public function setCode(string $code): Widget
    {
        $this->__call('setWidgetCode', [$code]);

        return $this;
    }
}
