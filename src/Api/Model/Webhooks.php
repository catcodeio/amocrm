<?php

namespace Amocrm\Api\Model;

/**
 * Коллекция вебхуков.
 *
 * @method Webhook first()
 * @method Webhook last()
 * @method Webhook current()
 * @method Webhook get($key)
 * @method Webhook filterOneByField($name, $value)
 */
class Webhooks extends AbstractModelCollection
{
    /**
     * Возвращает массив IDs задач в коллекции.
     *
     * @return array
     */
    public function getIds(): array
    {
        return array_map(function (Webhook $webhook) {
            return $webhook->getId();
        }, $this->getValues());
    }

    /**
     * @inheritdoc
     */
    protected static function getModelClass(): string
    {
        return Webhook::class;
    }
}