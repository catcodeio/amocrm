<?php

namespace Amocrm\Api\Model;

use Amocrm\Api\Model\Common\ClosestTaskTrait;
use Amocrm\Api\Model\Common\NormalizeCollectionTrait;
use Amocrm\Api\Model\Common\EntityModelTrait;
use Amocrm\Api\Model\Common\ResponsibleUserTrait;
use Amocrm\Api\Model\CustomField\CustomFieldTrait;
use Amocrm\Api\Model\Common\DateCreateTrait;
use Amocrm\Api\Model\Common\LastModifiedTrait;
use Amocrm\Api\Model\Common\LinkedLeadsIdTrait;
use Amocrm\Api\Model\Common\ContactTrait;
use Amocrm\Api\Model\Tag\TagTrait;

/**
 * @method int     getResponsibleUserId()
 * @method Company setResponsibleUserId(int $responsibleUserId)
 * @method Contact setContactsId(array $contactsId)
 * @method Contact setLeadsId(array $leadsId)
 * @method Contact setCustomersId(array $customersId)
 * @method int     getCreatedUserId()
 * @method int     getModifiedUserId()
 * @method int     getGroupId()
 * @method int     getAccountId()
 * @method string  getType() Для компании будет возвращать "company"
 */
class Company extends AbstractEntity
{
    use CustomFieldTrait;
    use TagTrait;
    use DateCreateTrait;
    use LastModifiedTrait;
    use ResponsibleUserTrait;
    use EntityModelTrait;
    use NormalizeCollectionTrait;
    use ContactTrait;
    use ClosestTaskTrait;
    use LinkedLeadsIdTrait;

    /**
     * @return array|null
     */
    public function getContactsId(): ?array
    {
        $result = $this->__call('getContacts');

        if (!isset($result['id'])) {
            return null;
        }

        sort($result['id']);

        return $result['id'];
    }
}
