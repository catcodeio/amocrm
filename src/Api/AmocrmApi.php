<?php

namespace Amocrm\Api;

use Amocrm\Api\Client\RestClientInterface;
use Amocrm\Api\Client\RestClientOAuth2;
use Amocrm\Api\Provider\V1\AccountProvider as AccountProviderV1;
use Amocrm\Api\Provider\V2\AccountProvider as AccountProviderV2;
use Amocrm\Api\Model\CustomField\CustomField;
use Amocrm\Api\Provider\V1\AuthProvider as AuthProviderV1;
use Amocrm\Api\Provider\V2\AuthProvider as AuthProviderV2;
use Amocrm\Api\Provider\V1\CompanyProvider as CompanyProviderV1;
use Amocrm\Api\Provider\V2\CompanyProvider as CompanyProviderV2;
use Amocrm\Api\Provider\V1\ElementProvider as ElementProviderV1;
use Amocrm\Api\Provider\V1\FieldProvider as FieldProviderV1;
use Amocrm\Api\Provider\V1\LeadProvider as LeadProviderV1;
use Amocrm\Api\Provider\V2\LeadProvider as LeadProviderV2;
use Amocrm\Api\Provider\V1\ContactProvider as ContactProviderV1;
use Amocrm\Api\Provider\V2\ContactProvider as ContactProviderV2;
use Amocrm\Api\Provider\V1\LinkProvider as LinkProviderV1;
use Amocrm\Api\Provider\V1\TaskProvider as TaskProviderV1;
use Amocrm\Api\Provider\V2\TaskProvider as TaskProviderV2;
use Amocrm\Api\Provider\V1\NoteProvider as NoteProviderV1;
use Amocrm\Api\Provider\V2\NoteProvider as NoteProviderV2;
use Amocrm\Api\Provider\V2\UnsortedProvider as UnsortedProviderV2;
use Amocrm\Api\Provider\V1\WebhookProvider as WebhookProviderV1;
use Amocrm\Api\Provider\V1\CustomerProvider as CustomerProviderV1;
use Amocrm\Api\Provider\V2\CustomerProvider as CustomerProviderV2;
use Amocrm\Api\Provider\V1\TransactionProvider as TransactionProviderV1;
use Amocrm\Api\Provider\V2\WidgetProvider;
use RuntimeException;

/**
 * Основной класс API, который предоставляет доступ ко всем сущностям и провайдерам.
 */
class AmocrmApi
{
    /**
     * @const int
     */
    public const VERSION_1 = 1;

    /**
     * @const int
     */
    public const VERSION_2 = 2;

    /**
     * Клиент для общения с API amoCRM версии 2. Является protected для возможности тестирования.
     *
     * @var RestClientOAuth2
     */
    protected $client;

    /**
     * @var int
     */
    private $version;

    /**
     * Для кеширования запросов данных аккаунта. Как правило они не меняются.
     *
     * @var AccountProviderV1|AccountProviderV2
     */
    private $accountProvider;

    /**
     * @param RestClientInterface $client
     * @param int                 $version Версия API, через которую необходимо работать.
     */
    public function __construct(RestClientInterface $client, $version = self::VERSION_2)
    {
        $this->client  = $client;
        $this->version = $version;
    }

    /**
     * @return RestClientInterface
     */
    public function client(): RestClientInterface
    {
        return $this->client;
    }

    /**
     * @return AuthProviderV1|AuthProviderV2
     */
    public function auth()
    {
        if ($this->client instanceof RestClientOAuth2) {
            return new AuthProviderV2($this->client);
        }

        return new AuthProviderV1($this->client);
    }

    /**
     * @param bool $withoutCache Если необходимо получать данные аккаунта без
     *                           кеширования предыдущего ответа, то true.
     *
     * @return AccountProviderV1|AccountProviderV2
     */
    public function account($withoutCache = false)
    {
        if ($this->version == self::VERSION_1) {
            if ($withoutCache) {
                return new AccountProviderV1($this->client, $this);
            }

            if (!$this->accountProvider) {
                $this->accountProvider = new AccountProviderV1($this->client, $this);
            }

            return $this->accountProvider;
        }

        if ($withoutCache) {
            return new AccountProviderV2($this->client, $this);
        }

        if (!$this->accountProvider) {
            $this->accountProvider = new AccountProviderV2($this->client, $this);
        }

        return $this->accountProvider;
    }

    /**
     * Генерация значения для конкретного доп. поля.
     *
     * @param int $id
     *
     * @return CustomField
     */
    public function cf(int $id): CustomField
    {
        return CustomField::create(['id' => $id]);
    }

    /**
     * Провайдер для создания доп. полей в аккаунте.
     *
     * @return FieldProviderV1
     */
    public function field()
    {
        if ($this->version == self::VERSION_1) {
            return new FieldProviderV1($this->client);
        }

        throw new RuntimeException('Provider "field" not exists on API version 2');
    }

    /**
     * @return LeadProviderV1|LeadProviderV2
     */
    public function lead()
    {
        if ($this->version == self::VERSION_1) {
            return new LeadProviderV1($this->client, $this);
        }

        return new LeadProviderV2($this->client, $this);
    }

    /**
     * @return ContactProviderV1|ContactProviderV2
     */
    public function contact()
    {
        if ($this->version == self::VERSION_1) {
            return new ContactProviderV1($this->client, $this);
        }

        return new ContactProviderV2($this->client, $this);
    }

    /**
     * @return CompanyProviderV1|CompanyProviderV2
     */
    public function company()
    {
        if ($this->version == self::VERSION_1) {
            return new CompanyProviderV1($this->client, $this);
        }

        return new CompanyProviderV2($this->client, $this);
    }

    /**
     * @return TaskProviderV1|TaskProviderV2
     */
    public function task()
    {
        if ($this->version == self::VERSION_1) {
            return new TaskProviderV1($this->client, $this);
        }

        return new TaskProviderV2($this->client, $this);

    }

    /**
     * @return UnsortedProviderV2
     */
    public function unsorted()
    {
        if ($this->version == self::VERSION_1) {
            throw new RuntimeException('Provider "unsorted" not exists on API version 1');
        }

        return new UnsortedProviderV2($this->client);
    }

    /**
     * @return NoteProviderV1|NoteProviderV2
     */
    public function note()
    {
        if ($this->version == self::VERSION_1) {
            return new NoteProviderV1($this->client, $this);
        }

        return new NoteProviderV2($this->client, $this);
    }

    /**
     * @return LinkProviderV1
     */
    public function link()
    {
        if ($this->version == self::VERSION_1) {
            return new LinkProviderV1($this->client, $this);
        }

        throw new RuntimeException('Provider "link" not exists on API version 2');
    }

    /**
     * @return WebhookProviderV1
     */
    public function webhook()
    {
        if ($this->version == self::VERSION_1) {
            return new WebhookProviderV1($this->client);
        }

        throw new RuntimeException('Provider "widget" not exists on API version 2');
    }

    /**
     * @return CustomerProviderV1|CustomerProviderV2
     */
    public function customer()
    {
        if ($this->version == self::VERSION_1) {
            return new CustomerProviderV1($this->client, $this);
        }

        return new CustomerProviderV2($this->client, $this);
    }

    /**
     * @return TransactionProviderV1
     */
    public function transaction()
    {
        if ($this->version == self::VERSION_1) {
            return new TransactionProviderV1($this->client, $this);
        }

        throw new RuntimeException('Provider "transaction" not exists on API version 2');
    }

    /**
     * @return ElementProviderV1
     */
    public function element()
    {
        if ($this->version == self::VERSION_1) {
            return new ElementProviderV1($this->client);
        }

        throw new RuntimeException('Provider "element" not exists on API version 2');
    }

    /**
     * @return WidgetProvider
     */
    public function widget()
    {
        return new WidgetProvider($this->client);
    }
}
