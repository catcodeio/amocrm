<?php

namespace Amocrm\Api\Client;

use Amocrm\Api\Helper\AmocrmResponseAnalyzer;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;

/**
 * HTTP-клиент для работы с API старой версии безщ применения HAL.
 *
 * @see https://developers.amocrm.com/introduction.php
 */
class RestClientToken extends AbstractRestClient
{
    /**
     * Точка входа в виде паттерна для подмены домена в sprintf().
     *
     * @const string
     */
    private const ENDPOINT_PATTERN = 'https://%s/%s?%s';

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $token;

    /**
     * @param string $domain
     * @param string $login
     * @param string $token
     */
    public function __construct(string $domain, string $login, string $token)
    {
        $this->domain = $domain;
        $this->login  = $login;
        $this->token  = $token;
    }

    /**
     * @inheritdoc
     */
    public function get(string $url, array $params = [], array $headers = [])
    {
        return $this->request($url, self::METHOD_GET, $params, $headers);
    }

    /**
     * @inheritdoc
     */
    public function post(string $url, array $params = [], array $headers = [])
    {
        return $this->request($url, self::METHOD_POST, $params, $headers);
    }

    /**
     * Возвращает массив с параметрами для авторизации, которые пойдут в
     * query-параметры.
     *
     * @param string $login
     * @param string $token
     *
     * @return array
     */
    private function getAuthParams(string $login, string $token): array
    {
        return [
            'USER_LOGIN' => $login,
            'USER_HASH'  => $token,
        ];
    }

    /**
     * Подготавливает список заголовков для запроса.
     *
     * @param array $headers
     *
     * @return array
     */
    private function prepareHeaders(array $headers = []): array
    {
        return array_merge(['Content-Type: application/json'], $headers);
    }

    /**
     * Подготавливает URL для HTTP запроса.
     *
     * @param string $url Запрашиваемый метод у API.
     * @param array  $params Доп. параметры для GET-запроса.
     *
     * @return string
     */
    private function prepareEndpoint(string $url, array $params = []): string
    {
        $lang = [];

        if ($this->locale) {
            $lang = ['lang' => $this->locale];
        }

        return sprintf(
            self::ENDPOINT_PATTERN,
            $this->domain,
            $url,
            http_build_query(array_merge(
                $this->getAuthParams($this->login, $this->token),
                $lang,
                $params
            ))
        );
    }

    /**
     * @param string $url
     * @param string $method
     * @param array  $params
     * @param array  $headers
     *
     * @return mixed
     *
     * @throws AmocrmApiException
     */
    private function request(string $url, string $method = self::METHOD_GET, array $params = [], array $headers = [])
    {
        $headers  = $this->prepareHeaders($headers);
        $endpoint = null;

        if ($method == self::METHOD_GET && $params) {
            $endpoint = $this->prepareEndpoint($url, $params);
        } else {
            $endpoint = $this->prepareEndpoint($url);
        }

        return $this->requestCurl(
            $endpoint,
            $method,
            $method != self::METHOD_GET ? $params : [],
            $headers
        );
    }

    /**
     * @param string $endpoint
     * @param string $method
     * @param array  $params
     * @param array  $headers
     *
     * @return mixed
     *
     * @throws AmocrmApiException
     */
    protected function requestCurl(
        string $endpoint,
        string $method = self::METHOD_GET,
        array $params = [],
        array $headers = []
    )
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_FAILONERROR, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_HEADER, false);

        if ($method == self::METHOD_POST && $params) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        }

        if ($this->proxy) {
            curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
        }

        if ($this->logger) {
            // Для логирования, чтобы одновременные запросы из разных потоков не путать.
            $uniq = uniqid();

            $this->logger->notice('['.$uniq.'] '. 'Request to amoCRM' , [
                'url'     => $endpoint,
                'method'  => $method,
                'params'  => $params,
                'headers' => $headers,
            ]);
        }

        if ($this->usleep) {
            $startRequest = microtime(true);
        }

        $response = curl_exec($ch);

        if ($this->usleep) {
            // Если запрос длился меньше, чем отпущено на один запрос, то выжидаем оставшееся время.
            $timeRequest = round(microtime(true) - $startRequest, 6) * 1000000;
            $diff = $this->usleep - $timeRequest;

            if ($diff > 0) {
                usleep($diff);
            }
        }

        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $errno      = curl_errno($ch);
        $error      = curl_error($ch);

        curl_close($ch);

        if ($response === false && !empty($error)) {
            throw new AmocrmApiException('Curl error: ' . $error, $errno);
        }

        $response = json_decode($response, true);

        if ($this->logger) {
            $this->logger->notice('['.$uniq.'] '. 'Result of request:', [
                'code'     => $statusCode,
                'response' => $response,
            ]);
        }

        if (floor($statusCode / 100) >= 3) {
            if (isset($response['response']['error_code'])) {
                $code = $response['response']['error_code'];
            } else {
                $code = $statusCode;
            }

            $message = 'Error in response from API amoCRM: '.
                ($response['response']['error'] ?? $response['detail'] ?? null) .', '.
                'code: '.$code.', '.
                'HTTP status: '.$statusCode.'. '.
                'More info: https://www.amocrm.ru/developers/content/api/errors';

            if ($this->logger) {
                $this->logger->error($message);
            }

            if (AmocrmResponseAnalyzer::isManager($code)) {
                throw new ManagerUnavailableException($message, $code);
            } else if (AmocrmResponseAnalyzer::isAccount($code)) {
                throw new AccountUnavailableException($message, $code);
            } else if (AmocrmResponseAnalyzer::isField($code)) {
                throw new CustomFieldIncorrectException($message, $code);
            } else if (AmocrmResponseAnalyzer::isData($code)) {
                throw new DataIncorrectException($message, $code);
            }

            throw new AmocrmApiException($message, $code);
        } elseif ($statusCode == 204) {
            return null;
        } elseif (!$response) {
            throw new AmocrmApiException('Response is empty', 200);
        }

        return $response;
    }
}