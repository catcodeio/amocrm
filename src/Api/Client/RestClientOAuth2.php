<?php

namespace Amocrm\Api\Client;

use Amocrm\Api\Helper\AmocrmResponseAnalyzer;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;
use Amocrm\Exception\UpdateTokenFailedException;
use AmoCRM\OAuth2\Client\Provider\AmoCRM;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use League\OAuth2\Client\Grant\RefreshToken;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessTokenInterface;

/**
 * HTTP-клиент для авторизации в API через OAuth2.
 *
 * @see https://developers.amocrm.com/introduction.php
 */
class RestClientOAuth2 extends AbstractRestClient
{
    /**
     * Количество секунд до окнчания access_token когда его уже надо бы обновить. На вский случай заложим, чтбы избежать
     * казусов.
     *
     * @var int
     */
    private const ACCESS_TOKEN_UPDATE = 60;

    /**
     * Время жизни refresh_token в секундах, после которого access_token уже не получить и нужно бросать исключение.
     * Сейчай это три месяца.
     *
     * @var int
     */
    private const REFRESH_TOKEN_TIMELIVE = 7776000;

    /**
     * @var AccessTokenInterface
     */
    private $accessToken;

    /**
     * @var callable
     */
    private $refreshCallback;

    /**
     * @var AmoCRM
     */
    private $amocrm;

    /**
     * @param AmoCRM                    $amocrm
     * @param AccessTokenInterface|null $accessToken
     * @param callable|null             $refreshCallback
     */
    public function __construct(
        AmoCRM $amocrm,
        AccessTokenInterface $accessToken = null,
        callable $refreshCallback = null
    ) {
        $this->amocrm          = $amocrm;
        $this->accessToken     = $accessToken;
        $this->refreshCallback = $refreshCallback;
    }

    /**
     * @inheritdoc
     */
    public function get(string $url, array $params = [], array $headers = [])
    {
        return $this->request($url, self::METHOD_GET, $params, $headers);
    }

    /**
     * @inheritdoc
     */
    public function post(string $url, array $params = [], array $headers = [])
    {
        return $this->request($url, self::METHOD_POST, $params, $headers);
    }

    /**
     * @return AmoCRM
     */
    public function getAmocrm(): AmoCRM
    {
        return $this->amocrm;
    }

    /**
     * @return AccessTokenInterface
     */
    public function getAccessToken(): AccessTokenInterface
    {
        return $this->accessToken;
    }

    /**
     * @param AccessTokenInterface $accessToken
     *
     * @return RestClientOAuth2
     */
    public function setAccessToken(AccessTokenInterface $accessToken): RestClientOAuth2
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * @param string $domain
     *
     * @return RestClientOAuth2
     */
    public function setDomain(string $domain): RestClientOAuth2
    {
        $this->amocrm->setBaseDomain($domain);

        return $this;
    }

    /**
     * Колбек, который будет вызываться при обновлении access_token, которому будет передан объект AccessTokenInterface.
     *
     * @param callable $refreshCallback
     *
     * @return RestClientOAuth2
     */
    public function setRefreshCallback(callable $refreshCallback): RestClientOAuth2
    {
        $this->refreshCallback = $refreshCallback;

        return $this;
    }

    /**
     * Пытается получить новый access_token.
     *
     * @throws UpdateTokenFailedException
     */
    public function updateAccessToken()
    {
        try {
            $this->accessToken = $this->amocrm->getAccessToken(new RefreshToken(), [
                'refresh_token' => $this->accessToken->getRefreshToken(),
            ]);

            if ($this->refreshCallback && is_callable($this->refreshCallback)) {
                call_user_func_array($this->refreshCallback, [$this->accessToken]);
            }
        } catch (IdentityProviderException $e) {
            throw new UpdateTokenFailedException($e->getResponseBody(), $e->getCode());
        }
    }

    /**
     * Проверка текущего access_token и получение нового при необходимоти. Если получить не удаётся или refresh_token
     * старше 3 месяцев, то выбрасываем соответсвующее исключение.
     *
     * @throws UpdateTokenFailedException
     */
    private function checkAccessToken()
    {
        $diffInSeconds = time() - $this->accessToken->getExpires();

        if ($diffInSeconds > -self::ACCESS_TOKEN_UPDATE && $diffInSeconds < self::REFRESH_TOKEN_TIMELIVE) {
            $this->updateAccessToken();
        } elseif ($diffInSeconds >= self::REFRESH_TOKEN_TIMELIVE) {
            throw new UpdateTokenFailedException('Invalid refresh token');
        }
    }

    /**
     * Подготавливает список заголовков для запроса.
     *
     * @param array $headers
     *
     * @return array
     *
     * @throws UpdateTokenFailedException
     */
    private function prepareHeaders(array $headers = []): array
    {
        $this->checkAccessToken();

        return $headers + $this->amocrm->getHeaders($this->accessToken);
    }

    /**
     * @param string $url
     * @param string $method
     * @param array  $params
     * @param array  $headers
     *
     * @return mixed
     *
     * @throws AmocrmApiException
     * @throws AccountUnavailableException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    private function request(string $url, string $method = self::METHOD_GET, array $params = [], array $headers = [])
    {
        $headers  = $this->prepareHeaders($headers);
        $endpoint = $this->amocrm->urlAccount() . $url;
        $options  = [
            RequestOptions::HEADERS => $headers,
            RequestOptions::TIMEOUT => $this->timeout,
        ];

        if ($this->proxy) {
            $options[RequestOptions::PROXY] = $this->proxy;
        }

        if ($this->locale) {
            $options[RequestOptions::QUERY] = [
                'lang' => $this->locale,
            ];
        }

        if ($params) {
            if ($method == self::METHOD_GET) {
                if (isset($options[RequestOptions::QUERY])) {
                    $options[RequestOptions::QUERY] += $params;
                } else {
                    $options[RequestOptions::QUERY] = $params;
                }
            } else {
                $options[RequestOptions::JSON] = $params;
            }
        }

        if ($this->logger) {
            // Для логирования, чтобы одновременные запросы из разных потоков не путать.
            $uniq = uniqid();

            $this->logger->notice('['.$uniq.'] '. 'Request to amoCRM' , [
                'url'     => $endpoint,
                'method'  => $method,
                'params'  => $params,
            ]);
        }

        if ($this->usleep) {
            $startRequest = microtime(true);
        }

        try {
            $response = $this->amocrm->getHttpClient()->request($method, $endpoint, $options);

            if ($this->usleep) {
                // Если запрос длился меньше, чем отпущено на один запрос, то выжидаем оставшееся время.
                $timeRequest = round(microtime(true) - $startRequest, 6) * 1000000;
                $diff = $this->usleep - $timeRequest;

                if ($diff > 0) {
                    usleep($diff);
                }
            }

            $parsedBody = json_decode($response->getBody()->getContents(), true);
            $statusCode = $response->getStatusCode();

            if ($this->logger) {
                $this->logger->notice('['.$uniq.'] '. 'Result of request:', [
                    'status_code' => $statusCode,
                    'parsed_body' => $parsedBody,
                ]);
            }
        } catch (GuzzleException $e) {
            if ($e instanceof RequestException && $e->getResponse()) {
                $statusCode = $e->getResponse()->getStatusCode();
                $parsedBody = json_decode($e->getResponse()->getBody()->getContents(), true);

                if (isset($parsedBody['response']['error_code'])) {
                    $code = $parsedBody['response']['error_code'];
                } else {
                    $code = $statusCode;
                }

                $message = 'Error in response from API amoCRM: ' .
                    $parsedBody['response']['error'] . ', ' .
                    'code: ' . $code . ', ' .
                    'HTTP status: ' . $statusCode . '. ' .
                    'More info: https://www.amocrm.ru/developers/content/api/errors';

                if ($this->logger) {
                    $this->logger->error($message);
                }

                if (AmocrmResponseAnalyzer::isManager($code)) {
                    throw new ManagerUnavailableException($message, $code);
                } else if (AmocrmResponseAnalyzer::isAccount($code)) {
                    throw new AccountUnavailableException($message, $code);
                } else if (AmocrmResponseAnalyzer::isField($code)) {
                    throw new CustomFieldIncorrectException($message, $code);
                } else if (AmocrmResponseAnalyzer::isData($code)) {
                    throw new DataIncorrectException($message, $code);
                }

                throw new AmocrmApiException($message, $code);
            } else {
                throw new AmocrmApiException('Guzzle error: ' . $e->getMessage(), $e->getCode());
            }
        }

        if ($statusCode == 204) {
            return null;
        }

        return $parsedBody;
    }
}