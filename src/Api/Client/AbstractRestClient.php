<?php

namespace Amocrm\Api\Client;

use Psr\Log\LoggerInterface;

/**
 * Абстрактный класс для rest-клиентов под разные версии API с общим функционалом.
 */
abstract class AbstractRestClient implements RestClientInterface
{
    /**
     * @const string
     */
    protected const METHOD_GET  = 'GET';

    /**
     * @const string
     */
    protected const METHOD_POST = 'POST';

    /**
     * @const int
     */
    public const TIMEOUT_DEFAULT = 60;

    /**
     * @var string
     */
    protected $domain;

    /**
     * @var string
     */
    protected $locale;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var int
     */
    protected $usleep;

    /**
     * @var string
     */
    protected $proxy;

    /**
     * @var string
     */
    protected $timeout = self::TIMEOUT_DEFAULT;

    /**
     * @inheritdoc
     */
    public function setTimeout(int $timeout)
    {
        $this->timeout = $timeout;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setProxy(string $proxy)
    {
        $this->proxy = $proxy;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setLocale(string $locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setLogger(?LoggerInterface $logger)
    {
        $this->logger = $logger;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setUsleep(int $usleep)
    {
        $this->usleep = $usleep;

        return $this;
    }
}