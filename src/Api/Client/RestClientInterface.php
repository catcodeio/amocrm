<?php

namespace Amocrm\Api\Client;

use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;
use Amocrm\Exception\UpdateTokenFailedException;
use Psr\Log\LoggerInterface;

/**
 * Интерфейс для классов, которые работают как rest-клиенты с авторизацией под
 * конкретную версию API amoCRM. Плюс в зависимости от версии по разному
 * извлекается текст ошибки и формируются запросы.
 *
 * Этот класс сам знает на какой endpoint посылать и как парсить ответ.
 */
interface RestClientInterface
{
    /**
     * Выполнить POST-запрос и вернуть тело ответа
     *
     * @param string $url     URL от корня домена без первого слеша.
     * @param array  $params
     * @param array  $headers
     *
     * @return mixed
     *
     * @throws AmocrmApiException
     * @throws AccountUnavailableException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    public function get(string $url, array $params = [], array $headers = []);

    /**
     * Выполнить POST-запрос и вернуть тело ответа
     *
     * @param string $url     URL от корня домена без первого слеша.
     * @param array  $params
     * @param array  $headers
     *
     * @return mixed
     *
     * @throws AmocrmApiException
     * @throws AccountUnavailableException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    public function post(string $url, array $params = [], array $headers = []);

    /**
     * @param int $timeout
     *
     * @return $this
     */
    public function setTimeout(int $timeout);

    /**
     * @param string $proxy
     *
     * @return $this
     */
    public function setProxy(string $proxy);

    /**
     * @param string $locale
     *
     * @return $this
     */
    public function setLocale(string $locale);

    /**
     * @param LoggerInterface|null $logger
     *
     * @return $this
     */
    public function setLogger(?LoggerInterface $logger);

    /**
     * @param int $usleep
     *
     * @return $this
     */
    public function setUsleep(int $usleep);
}