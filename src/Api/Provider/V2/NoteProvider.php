<?php

namespace Amocrm\Api\Provider\V2;

use Amocrm\Api\Model\Note;
use Amocrm\Api\Provider\V1\NoteProvider as NoteProviderV1;

class NoteProvider extends NoteProviderV1
{
    /**
     * @inheritdoc
     */
    protected function requestSave(&$currentAdd, &$currentUpdate, &$resultUpdate, &$resultAdd, &$result)
    {
        $request = [];

        if ($currentAdd) {
            $request['add'] = $currentAdd;
            $currentAdd = [];
        }

        if ($currentUpdate) {
            $request['update'] = $currentUpdate;
            $currentUpdate = [];
        }

        $response = $this->getClient()->post('api/v2/notes', $request);

        if (isset($response['_embedded']['items']) && count($response['_embedded']['items'])) {
            foreach ($response['_embedded']['items'] as $key => $item) {
                // Если в ответ придёт не то кол-во сущностей, то мы конечно не упадём, но ID никому не проставим.
                if (isset($resultAdd[$key])) {
                    /** @var Note[] $resultAdd */
                    $resultAdd[$key]->setId($item['id']);
                }
            }

            $result       = array_merge($result, $resultAdd, $resultUpdate);
            $resultAdd    = [];
            $resultUpdate = [];
        }
    }

    /**
     * @inheritdoc
     */
    protected function getUrlList()
    {
        return 'api/v2/notes';
    }

    /**
     * @inheritdoc
     */
    protected function getEmbeddedItems($response)
    {
        if (is_null($response)) {
            return null;
        }

        if (!isset($response['_embedded']['items'])) {
            $this->throwWrongResponse();
        }

        return $response['_embedded']['items'];
    }
}