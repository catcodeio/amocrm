<?php

namespace Amocrm\Api\Provider\V2;

use Amocrm\Api\Provider\V1\CompanyProvider as CompanyProviderV1;

class CompanyProvider extends CompanyProviderV1
{
    /**
     * @inheritdoc
     */
    protected function getUrlList()
    {
        return 'api/v2/companies';
    }

    /**
     * @inheritdoc
     */
    protected function getEmbeddedItems($response)
    {
        if (is_null($response)) {
            return null;
        }

        if (!isset($response['_embedded']['items'])) {
            $this->throwWrongResponse();
        }

        return $response['_embedded']['items'];
    }
}