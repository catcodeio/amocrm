<?php

namespace Amocrm\Api\Provider\V2;

use Amocrm\Api\Model\AbstractEntity;
use Amocrm\Api\Model\Unsorted;
use Amocrm\Api\Model\Unsorteds;
use Amocrm\Api\Provider\AbstractProvider;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;
use Amocrm\Exception\UpdateTokenFailedException;
use DateTime;
use InvalidArgumentException;

class UnsortedProvider extends AbstractProvider
{
    /**
     * @return Unsorted
     */
    public function create(): Unsorted
    {
        return Unsorted::create();
    }

    /**
     * Сохраняет неразобранные.
     *
     * Внимание! ID подчинённых сущностей (сделки, конакты, компании) будут проставлены только, если за один раз
     * добавляеяется один неразобранный. В противном случае по ответу не понятно что к чему относится. И то опираемся
     * только на порядок следования возвращённых ID. Хотя может быть можно смепить просто по порядку следования все
     * сущности, но я не уверен, надо проверять.
     *
     * @param string                        $category Категория неразобранного (sip, mail, forms).
     * @param Unsorteds|Unsorted[]|Unsorted $entities
     *
     * @return Unsorteds|Unsorted
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws InvalidArgumentException
     */
    public function save($category, $entities)
    {
        $entities   = $this->normalizeCollection($entities);
        $currentAdd = [];
        $resultAdd  = [];
        $result     = [];
        /** @var AbstractEntity $entity */
        foreach ($entities as $entity) {
            $currentAdd[] = $entity->getModifiedForApi();
            $resultAdd[]  = $entity;

            if (count($currentAdd) >= 200) {
                $this->requestSave($category, $currentAdd, $resultAdd, $result);
            }
        }

        if (count($currentAdd)) {
            $this->requestSave($category, $currentAdd, $resultAdd, $result);
        }

        return Unsorteds::create($result);
    }

    /**
     * Сохраняет все, но возвращает только первую сущность.
     *
     * @param string                        $category Категория неразобранного (sip, mail, forms).
     * @param Unsorteds|Unsorted[]|Unsorted $entities
     *
     * @return Unsorted
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function saveOne($category, $entities): ?Unsorted
    {
        $entities = $this->save($category, $entities);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Гибкий поиск контактов с фильтрацией и без лимита по 500.
     *
     * @param array|string|null $categories Категория неразобранного (sip, mail, forms).
     * @param int|null          $pipelineId
     * @param array|null        $orderBy
     *
     * @return Unsorteds|Unsorted[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function find(
        $categories = null,
        int $pipelineId = null,
        array $orderBy = null
    ): Unsorteds
    {
        // Максимально возможная выборка строк (ограничено amocrm), она же шаг.
        $limit    = 500;
        $end      = false;
        $offset   = 0;
        $entities = Unsorteds::create();

        do {
            $chunk = $this->list(
                $pipelineId,
                $categories,
                $orderBy,
                $limit,
                ($offset / $limit) + 1
            );

            if ($chunk->count() > 0) {
                $entities->merge($chunk);

                if ($chunk->count() < $limit) {
                    $end = true;
                } else {
                    $offset += $limit;
                }
            } else {
                $end = true;
            }
        } while (!$end);

        return $entities;
    }

    /**
     * Список неразобранных заявок.
     *
     * @param array|string|null $categories Категория неразобранного (sip, mail, forms).
     * @param int|null          $pipelineId
     * @param array|null        $orderBy
     * @param int|null          $pageSize
     * @param int|null          $page
     *
     * @return Unsorteds|Unsorted[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function list(
        $categories = null,
        int $pipelineId = null,
        array $orderBy = null,
        int $pageSize = null,
        int $page = null
    ): Unsorteds
    {
        $categories = is_array($categories) ? $categories : $categories;

        $response = $this->getClient()->get('api/v2/incoming_leads', [
            'pipeline_id' => $pipelineId,
            'categories'  => $categories,
            'order_by'    => $orderBy,
            'page_size'   => $pageSize,
            'page'        => $page,
        ]);

        if (is_null($response) || !isset($response['_embedded']['items'])) {
            return Unsorteds::create();
        }

        return Unsorteds::create($response['_embedded']['items']);
    }

    /**
     * Сводная информация о неразобранных заявках.
     *
     * @param DateTime|null $dateFrom
     * @param DateTime|null $dateTo
     *
     * @return array
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function summary(
        DateTime $dateFrom = null,
        DateTime $dateTo = null
    ): array
    {
        $params = [];

        if ($dateFrom || $dateTo) {
            $params = [
                'data' => [
                    'filter' => [
                        'date' => [
                            'from' => $dateFrom ? $dateFrom->format('U') : null,
                            'to'   => $dateTo ? $dateTo->format('U') : null,
                        ]
                    ]
                ]
            ];
        }

        $response = $this->getClient()->get('api/v2/incoming_leads/summary', $params);

        if (is_null($response)) {
            return [];
        }

        if (!isset($response['_embedded']['items'])) {
            $this->throwWrongResponse();
        }

        return $response['_embedded']['items'];
    }

    /**
     * Функция для отправки запроса с подготовкой т.к. она нужна в двух местах для разбиение большого запроса на чанки.
     *
     * @param string $category
     * @param array  $currentAdd
     * @param array  $resultAdd
     * @param array  $result
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    protected function requestSave($category, &$currentAdd, &$resultAdd, &$result)
    {
        $request = [];

        if ($currentAdd) {
            $request['add'] = $currentAdd;
            $currentAdd     = [];
        }

        $response = $this->getClient()->post(sprintf('api/v2/incoming_leads/%s', $category), $request);

        if (isset($response['status'])
            && $response['status'] == 'success'
            && isset($response['data'])
            && count($response['data'])
        ) {
            foreach ($response['data'] as $key => $item) {
                // Если в ответ придёт не то кол-во сущностей, то мы конечно не упадём, но UID никому не проставим.
                if (isset($resultAdd[$key])) {
                    /** @var Unsorted[] $resultAdd */
                    $resultAdd[$key]->setUid($item);
                }
            }

            if (count($response['data']) == 1) {
                foreach ($response['contacts'] ?? [] as $k => $id) {
                    $resultAdd[0]->getContacts()->get($k)->setId($id);
                }

                foreach ($response['companies'] ?? [] as $k => $id) {
                    $resultAdd[0]->getCompanies()->get($k)->setId($id);
                }

                foreach ($response['leads'] ?? [] as $k => $id) {
                    $resultAdd[0]->getLeads()->get($k)->setId($id);
                }
            }

            $result    = array_merge($result, $resultAdd);
            $resultAdd = [];
        }
    }
}