<?php

namespace Amocrm\Api\Provider\V2;

use Amocrm\Api\Model\AbstractEntity;
use Amocrm\Api\Model\Task;
use Amocrm\Api\Provider\V1\TaskProvider as TaskProviderV1;

class TaskProvider extends TaskProviderV1
{
    /**
     * @inheritdoc
     */
    protected function requestSave(&$currentAdd, &$currentUpdate, &$resultUpdate, &$resultAdd, &$result)
    {
        $request = [];

        if ($currentAdd) {
            $request['add'] = $currentAdd;
            $currentAdd     = [];
        }

        if ($currentUpdate) {
            $request['update'] = $currentUpdate;
            $currentUpdate     = [];
        }

        $response = $this->getClient()->post('api/v2/tasks', $request);

        if (isset($response['_embedded']['items']) && count($response['_embedded']['items'])) {
            foreach ($response['_embedded']['items'] as $key => $item) {
                // Если в ответ придёт не то кол-во сущностей, то мы конечно не упадём, но ID никому не проставим.
                if (isset($resultAdd[$key])) {
                    /** @var Task[] $resultAdd */
                    $resultAdd[$key]->setId($item['id']);
                }
            }

            $result       = array_merge($result, $resultAdd, $resultUpdate);
            $resultAdd    = [];
            $resultUpdate = [];
        }
    }

    /**
     * @inheritdoc
     */
    protected function getUrlList()
    {
        return 'private/api/v2/json/tasks/list';
    }

    /**
     * @inheritdoc
     */
    protected function getEmbeddedItems($response)
    {
        if (is_null($response)) {
            return null;
        }

        if (!isset($response['_embedded']['items'])) {
            $this->throwWrongResponse();
        }

        return $response['_embedded']['items'];
    }

    /**
     * @param AbstractEntity $entity
     *
     * @return array
     */
    protected function getEntityData(AbstractEntity $entity)
    {
        return $entity->getModifiedForApi();
    }
}