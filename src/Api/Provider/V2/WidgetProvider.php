<?php

namespace Amocrm\Api\Provider\V2;

use Amocrm\Api\Model\Widget;
use Amocrm\Api\Model\Widgets;
use Amocrm\Api\Provider\AbstractProvider;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;
use Amocrm\Exception\UpdateTokenFailedException;
use InvalidArgumentException;

class WidgetProvider extends AbstractProvider
{
    /**
     * @return Widget
     */
    public function create(): Widget
    {
        return Widget::create();
    }

    /**
     * Устанавливаем все указанные виджеты.
     *
     * @param Widgets|Widget[]|Widget $entities
     *
     * @return Widgets
     *
     * @throws AmocrmApiException
     * @throws AccountUnavailableException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    public function install($entities): Widgets
    {
        $entities = is_array($entities) ? $entities : [$entities];
        $result   = $this->save(['install' => $entities]);

        return $result['install'];
    }

    /**
     * Удаляем все указанные виджеты.
     *
     * @param Widgets|Widget[]|Widget $entities
     *
     * @return Widgets
     *
     * @throws AmocrmApiException
     * @throws AccountUnavailableException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    public function uninstall($entities): Widgets
    {
        $entities = is_array($entities) ? $entities : [$entities];
        $result   = $this->save(['uninstall' => $entities]);

        return $result['uninstall'];
    }

    /**
     * Устанавливаем и удаляет все указанные виджеты.
     *
     * Внимание! На выходе виджеты могу перемешаться, порядок их может измениться.
     *
     * @param Widgets|Widget[]|Widget $entities
     *
     * @return Widgets|Widget[]
     *
     * @throws AmocrmApiException
     * @throws AccountUnavailableException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    public function save($entities)
    {
        if (!isset($entities['install']) && !isset($entities['uninstall'])) {
            throw new InvalidArgumentException('Во входном массиве должен быть один из ключей: install, uninstall.');
        }

        $entities = $this->normalizeCollection($entities);

        $current = [
            'install'   => [],
            'uninstall' => [],
        ];
        $items   = [
            'install'   => [],
            'uninstall' => [],
        ];
        $result  = [
            'install'   => [],
            'uninstall' => [],
        ];

        foreach (['install', 'uninstall'] as $key) {
            if (!isset($entities[$key])) {
                continue;
            }

            $entities[$key] = $this->normalizeCollection($entities[$key]);

            /** @var Widget $entity */
            foreach ($entities[$key] as $entity) {
                $this->validateException($entity, [['widget_id', 'widget_code']]);

                $current[$key][] = $entity->getModifiedForApi();

                if ($entity->getWidgetId()) {
                    $items[$key][$entity->getWidgetId()] = $entity;
                } else {
                    $items[$key][$entity->getWidgetCode()] = $entity;
                }

                if (count($current['install']) + count($current['uninstall']) >= 200) {
                    $this->requestSave($current, $items, $result);
                }
            }
        }

        if (count($current['install']) || count($current['uninstall'])) {
            $this->requestSave($current, $items, $result);
        }

        $newResult = [];

        foreach (['install', 'uninstall'] as $key) {
            if (isset($result[$key]) && count($result[$key])) {
                $newResult[$key] = Widgets::create($result[$key]);
            } else {
                $newResult[$key] = Widgets::create();
            }
        }

        return $newResult;
    }

    /**
     * Получение виджета или множества виджетов по widget_id.
     *
     * @param int|int[] $ids
     *
     * @return Widgets|Widget[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    public function getById($ids): Widgets
    {
        // Пока не работает множественный запрос, то сделаем так.
        if (is_array($ids)) {
            $limit    = 350;
            $entities = Widgets::create();

            foreach (array_chunk($ids, $limit) as $id) {
                $entities->merge(
                    $this->list($id)
                );
            }

            return $entities;
        }

        return $this->list($ids);
    }

    /**
     * Получение виджета или множества виджетов по widget_code.
     *
     * @param int|int[] $codes
     *
     * @return Widgets|Widget[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    public function getByCode($codes): Widgets
    {
        // Пока не работает множественный запрос, то сделаем так.
        if (is_array($codes)) {
            $limit    = 350;
            $entities = Widgets::create();

            foreach (array_chunk($codes, $limit) as $code) {
                $entities->merge(
                    $this->list(null, $code)
                );
            }

            return $entities;
        }

        return $this->list(null, $codes);
    }

    /**
     * Получение одного первого виджета по widget_id.
     *
     * @param int|int[] $ids
     *
     * @return Widget|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    public function getOneById($ids): ?Widget
    {
        $entities = $this->getById($ids);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Получение одного первого виджета по widget_code.
     *
     * @param int|int[] $codes
     *
     * @return Widget|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    public function getOneByCode($codes): ?Widget
    {
        $entities = $this->getByCode($codes);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Самый прямой и подробный метод запроса.
     *
     * @param array|int|null    $widgetId
     * @param array|string|null $widgetCode
     *
     * @return Widgets|Widget[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    public function list(
        $widgetId = null,
        $widgetCode = null
    ): Widgets
    {
        $response = $this->getClient()->get('api/v2/widgets', [
            'widget_id'   => $widgetId,
            'widget_code' => $widgetCode,
        ]);

        if (is_null($response) || !isset($response['_embedded'])) {
            return Widgets::create();
        }

        if (!isset($response['_embedded']['items'])) {
            $this->throwWrongResponse();
        }

        return Widgets::create($response['_embedded']['items']);
    }

    /**
     * Функция для отправки запроса с подготовкой т.к. она нужна в двух местах для разбиение большого запроса на чанки.
     *
     * @param array $current
     * @param array $items
     * @param array $result
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    protected function requestSave(&$current, $items, &$result) {
        $request = ['widgets' => []];

        if (count($current['install'])) {
            $request['widgets']['install'] = $current['install'];
            $current['install']            = [];
        }

        if (count($current['uninstall'])) {
            $request['widgets']['uninstall'] = $current['uninstall'];
            $current['uninstall']            = [];
        }

        $response = $this->getClient()->post('api/v2/widgets', $request);

        foreach ($response['_embedded']['widgets'] as $item) {
            /** @var Widget $entity */
            $entity = null;
            $index  = null;

            foreach (['install', 'uninstall'] as $key) {
                if (isset($items[$key][$item['id']])) {
                    $entity = $items[$key][$item['id']];
                    $index  = $item['id'];
                } elseif (isset($items[$key][$item['code']])) {
                    $entity = $items[$key][$item['code']];
                    $index  = $item['code'];
                }

                if ($entity) {
                    $entity = Widget::create(array_merge($entity->getModified(), $item));

                    $result[$key][$index] = $entity;

                    break;
                }
            }
        }
    }
}