<?php

namespace Amocrm\Api\Provider\V2;

use Amocrm\Api\Model\Lead;
use Amocrm\Api\Model\Leads;
use Amocrm\Api\Provider\V1\LeadProvider as LeadProviderV1;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;
use DateTime;

class LeadProvider extends LeadProviderV1
{
    /**
     * Самый прямой и подробный метод запроса.
     *
     * @param string|null    $query
     * @param int|int[]|null $status
     * @param int|int[]|null $responsibleUserId
     * @param DateTime|null  $dateModified
     * @param int|null       $limitRows
     * @param int|null       $limitOffset
     * @param int|int[]|null $id
     * @param bool           $onlyDeleted
     *
     * @return Leads|Lead[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function list(
        string $query = null,
        $status = null,
        $responsibleUserId = null,
        DateTime $dateModified = null,
        int $limitRows = null,
        int $limitOffset = null,
        $id = null,
        $onlyDeleted = false
    ): Leads
    {
        $headers = [];

        if ($dateModified) {
            $headers = ['if-modified-since' => $dateModified->format('D, d M Y H:i:s')];
        }

        $with = ['is_price_modified_by_robot', 'loss_reason_name'];

        if ($onlyDeleted) {
            $with[] = 'only_deleted';
        }

        $response = $this->getClient()->get('api/v2/leads', [
            'query'               => $query,
            'status'              => $status,
            'responsible_user_id' => $responsibleUserId,
            'limit_rows'          => $limitRows,
            'limit_offset'        => $limitOffset,
            'id'                  => $id,
            'with'                => implode(',', $with),
        ], $headers);

        return Leads::create($this->getEmbeddedItems($response));
    }

    /**
     * @inheritdoc
     */
    protected function requestSave(&$currentAdd, &$currentUpdate, &$resultUpdate, &$resultAdd, &$result)
    {
        $request = [];

        if ($currentAdd) {
            $request['add'] = $currentAdd;
            $currentAdd = [];
        }

        if ($currentUpdate) {
            $request['update'] = $currentUpdate;
            $currentUpdate = [];
        }

        $response = $this->getClient()->post('api/v2/leads', $request);

        if (isset($response['_embedded']['items']) && count($response['_embedded']['items'])) {
            foreach ($response['_embedded']['items'] as $key => $item) {
                // Если в ответ придёт не то кол-во сущностей, то мы конечно не упадём, но ID никому не проставим.
                if (isset($resultAdd[$key])) {
                    /** @var Lead[] $resultAdd */
                    $resultAdd[$key]->setId($item['id']);
                }
            }

            $result       = array_merge($result, $resultAdd, $resultUpdate);
            $resultAdd    = [];
            $resultUpdate = [];
        }
    }

    /**
     * @inheritdoc
     */
    protected function getEmbeddedItems($response)
    {
        if (is_null($response)) {
            return null;
        }

        if (!isset($response['_embedded']['items'])) {
            $this->throwWrongResponse();
        }

        return $response['_embedded']['items'];
    }
}