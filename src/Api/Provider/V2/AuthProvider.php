<?php

namespace Amocrm\Api\Provider\V2;

use Amocrm\Api\Provider\AbstractProvider;
use League\OAuth2\Client\Grant\AuthorizationCode;
use League\OAuth2\Client\Grant\RefreshToken;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessTokenInterface;

/**
 * Провайдер для работы с авторизацией в API и получения токенов.
 */
class AuthProvider extends AbstractProvider
{
    /**
     * Получает актуальный access_token для OAuth-авторизации и устанавливает его в либу.
     *
     * @param string $code
     * @param string $referer
     *
     * @return AccessTokenInterface
     *
     * @throws IdentityProviderException
     */
    public function getAccessTokenByCode(string $code, string $referer)
    {
        $amocrm = $this->getClient()->getAmocrm();

        $amocrm->setBaseDomain($referer);

        $accessToken = $amocrm->getAccessToken(new AuthorizationCode(), [
            'code' => $code,
        ]);

        $this->getClient()->setAccessToken($accessToken);

        return $accessToken;
    }

    /**
     * Обновляет access_token для OAuth-авторизации с помощью refresh_token.
     *
     * @return AccessTokenInterface
     *
     * @throws IdentityProviderException
     */
    public function getRefreshToken()
    {
        $accessToken = $this->getClient()->getAmocrm()->getAccessToken(new RefreshToken(), [
            'refresh_token' => $this->getClient()->getAccessToken()->getRefreshToken(),
        ]);

        $this->getClient()->setAccessToken($accessToken);

        return $accessToken;
    }
}