<?php

namespace Amocrm\Api\Provider\V2;

use Amocrm\Api\Provider\V1\AccountProvider as AccountProviderV1;

/**
 * @inheritdoc
 */
class AccountProvider extends AccountProviderV1
{
    /**
     * @inheritdoc
     */
    protected function getAccountData()
    {
        $response = $this->getClient()->get('api/v2/account', [
            'with'       => implode(',', [
                'custom_fields',
                'users',
                'pipelines',
                'groups',
                'note_types',
                'task_types',
            ]),
            'free_users' => 'Y',
        ]);

        if (isset($response['_embedded'])) {
            $embedded = $response['_embedded'];

            unset($response['_embedded']);

            $response += $embedded;
        }

        return $response;
    }
}