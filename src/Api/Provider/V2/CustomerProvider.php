<?php

namespace Amocrm\Api\Provider\V2;

use Amocrm\Api\Provider\V1\CustomerProvider as CustomerProviderV1;

class CustomerProvider extends CustomerProviderV1
{
    /**
     * @inheritdoc
     */
    protected function getUrlList()
    {
        return 'api/v2/customers';
    }

    /**
     * @inheritdoc
     */
    protected function getEmbeddedItems($response)
    {
        if (is_null($response)) {
            return null;
        }

        if (!isset($response['_embedded']['items'])) {
            $this->throwWrongResponse();
        }

        return $response['_embedded']['items'];
    }
}