<?php

namespace Amocrm\Api\Provider\V2;

use Amocrm\Api\Provider\V1\ContactProvider as ContactProviderV1;

class ContactProvider extends ContactProviderV1
{
    /**
     * @return string
     */
    protected function getUrlList()
    {
        return 'api/v2/contacts';
    }

    /**
     * @inheritdoc
     */
    protected function getEmbeddedItems($response)
    {
        if (is_null($response)) {
            return null;
        }

        if (!isset($response['_embedded']['items'])) {
            $this->throwWrongResponse();
        }

        return $response['_embedded']['items'];
    }
}