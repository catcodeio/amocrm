<?php

namespace Amocrm\Api\Provider;

use Amocrm\Api\AmocrmApi;
use Amocrm\Api\Client\RestClientInterface;
use Amocrm\Api\Helper\Utils;
use Amocrm\Api\Model\AbstractModel;
use Amocrm\Api\Model\Common\NormalizeCollectionTrait;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\DataIncorrectException;
use RuntimeException;

abstract class AbstractProvider
{
    use NormalizeCollectionTrait;

    /**
     * @var RestClientInterface
     */
    private $client;

    /**
     * @var AmocrmApi
     */
    private $amocrmApi;

    /**
     * @param RestClientInterface $client
     * @param AmocrmApi|null      $amocrmApi Если внутри не используется API для доп. запросов, то можно не передавать.
     */
    public function __construct(RestClientInterface $client, AmocrmApi $amocrmApi = null)
    {
        $this->client    = $client;
        $this->amocrmApi = $amocrmApi;
    }

    /**
     * @return AmocrmApi
     */
    protected function getAmocrmApi(): AmocrmApi
    {
        if (is_null($this->amocrmApi)) {
            throw new RuntimeException('Provider have no amoCRM API client but you cat insert this in the constructor');
        }

        return $this->amocrmApi;
    }

    /**
     * @return RestClientInterface
     */
    protected function getClient(): RestClientInterface
    {
        return $this->client;
    }

    /**
     * Проверка заполненности полей сущности.
     *
     * @param AbstractModel $entity
     * @param array         $fields Имена полей в змеинной нотации (может содержать массив в массиве, означает один из
     *                              вариантов, OR).
     *
     * @return void
     *
     * @throws DataIncorrectException
     */
    protected function validateException(AbstractModel $entity, array $fields)
    {
        foreach ($fields as $field) {
            if (is_array($field)) {
                $found = false;

                foreach ($field as $item) {
                    $methodName = 'get' . Utils::toCamelCase($item);

                    if ($entity->{$methodName}() !== null) {
                        $found = true;

                        break;
                    }
                }

                if (!$found) {
                    $this->throwException($entity, $fields);
                }
            } else {
                $methodName = 'get' . Utils::toCamelCase($field);

                if (!$entity->{$methodName}()) {
                    $this->throwException($entity, $fields);
                }
            }
        }
    }

    /**
     * @param AbstractModel $entity
     * @param array         $fields
     *
     * @return void
     *
     * @throws DataIncorrectException
     */
    private function throwException(AbstractModel $entity, array $fields)
    {
        $fieldsText = $fields;

        foreach ($fieldsText as &$fieldText) {
            if (is_array($fieldText)) {
                $fieldText = $fieldText[0] . ' (или ' . implode(', ', array_slice($fieldText, 1)) . ')';
            }
        }

        throw new DataIncorrectException(sprintf(
            'Сущность %s обязательно должна содержать следующие поля: %s',
            get_class($entity),
            implode(', ', $fieldsText)
        ));
    }

    /**
     * Вызывает исключение для невалидного формата ответа.
     *
     * @return void
     *
     * @throws AmocrmApiException
     */
    protected function throwWrongResponse()
    {
        throw new AmocrmApiException('The wrong format response');
    }
}