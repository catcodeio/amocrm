<?php

namespace Amocrm\Api\Provider\V1;

use Amocrm\Api\Model\AbstractEntity;
use Amocrm\Api\Model\Webhook;
use Amocrm\Api\Model\Webhooks;
use Amocrm\Api\Provider\AbstractProvider;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;
use Amocrm\Exception\UpdateTokenFailedException;

class WebhookProvider extends AbstractProvider
{
    /**
     * @return Webhook
     */
    public function create(): Webhook
    {
        return Webhook::create();
    }

    /**
     * Подписка вебхуков.
     *
     * @param Webhooks|Webhook[]|Webhook $entities
     *
     * @return Webhooks
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function subscribe($entities): Webhooks
    {
        return $this->save($entities, 'subscribe');
    }

    /**
     * Отписка вебхуков.
     *
     * @param Webhooks|Webhook[]|Webhook $entities
     *
     * @return Webhooks
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function unsubscribe($entities): Webhooks
    {
        return $this->save($entities, 'unsubscribe');
    }

    /**
     * Сохраняет все, но возвращает только первую сущность.
     *
     * @param Webhooks|Webhook[]|Webhook $entities
     *
     * @return Webhook|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function subscribeOne($entities): ?Webhook
    {
        $entities = $this->subscribe($entities);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }
    /**
     * Сохраняет все, но возвращает только первую сущность.
     *
     * @param Webhooks|Webhook[]|Webhook $entities
     *
     * @return Webhook|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function unsubscribeOne($entities): ?Webhook
    {
        $entities = $this->unsubscribe($entities);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Сохраняет все вебхуки, и которые нужно добавить и которые обновить.
     *
     * Внимание! На выходе вебхуки могу перемешаться, порядок может измениться.
     *
     * @param Webhooks|Webhook[]|Webhook $entities
     * @param string                     $action   Действие, которое нужно произвести: subscribe, unsubscribe.
     *
     * @return Webhooks
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    private function save($entities, $action): Webhooks
    {
        $entities     = $this->normalizeCollection($entities);
        $currentChunk = [];
        $resultChunk  = [];
        $result       = [];
        /** @var AbstractEntity $entity */
        foreach ($entities as $entity) {
            $currentChunk[] = $entity->getModifiedForApi();
            $resultChunk[]  = $entity;

            if (count($currentChunk) >= 200) {
                $this->requestSave($action, $currentChunk, $resultChunk, $result);
            }
        }

        if (count($currentChunk)) {
            $this->requestSave($action, $currentChunk, $resultChunk, $result);
        }

        return Webhooks::create($result);
    }

    /**
     * Гибкий поиск с фильтрацией и без лимита по 500.
     *
     * @param string|null   $url
     * @param string[]|null $events
     *
     * @return Webhooks|Webhook[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function find(
        ?string $url,
        ?array $events = null
    ): Webhooks
    {
        // Максимально возможная выборка строк (ограничено amocrm), она же шаг.
        $limit    = 500;
        $end      = false;
        $offset   = 0;
        $entities = Webhooks::create();

        do {
            $chunk = $this->list(
                $url,
                $events
            );

            if ($chunk->count() > 0) {
                $entities->merge($chunk);

                if ($chunk->count() < $limit) {
                    $end = true;
                } else {
                    $offset += $limit;
                }
            } else {
                $end = true;
            }
        } while (!$end);

        return $entities;
    }

    /**
     * Получение сделки или множества сделок по ID.
     *
     * @param int|int[] $ids
     *
     * @return Webhooks|Webhook[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function get($ids): Webhooks
    {
        $ids = is_array($ids) ? $ids : [$ids];

        // Похоже можно только по одному вебхуку запрашивать по id.
        $limit    = 1;
        $entities = Webhooks::create();

        foreach (array_chunk($ids, $limit) as $id) {
            $entities->merge(
                $this->list(
                    null,
                    null,
                    $id
                )
            );
        }

        return $entities;
    }

    /**
     * Получение одной первой сделки по ID.
     *
     * @param int|int[] $ids
     *
     * @return Webhooks|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function getOne($ids): ?Webhooks
    {
        $entities = $this->get($ids);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Получение вебхуков по части URL.
     *
     * @param string $url
     *
     * @return Webhooks
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findByUrl($url): Webhooks
    {
        return $this->list()->filter(function (Webhook $webhook) use ($url) {
            return strpos($webhook->getUrl(), $url) !== false;
        });
    }

    /**
     * Получение одного первого вебхука по части URL.
     *
     * @param string $url
     *
     * @return Webhook|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findOneByUrl($url): ?Webhook
    {
        $entities = $this->findByUrl($url);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Отписывается от всех вебхуков, которые будут найдены по совпадению с
     * переданной строкой.
     *
     * @param string $url
     *
     * @return bool
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function unsubscribeByUrl($url): bool
    {
        $entities = $this->findByUrl($url);

        if (!$entities->count()) {
            return false;
        }

        $this->unsubscribe($entities);

        return true;
    }

    /**
     * Самый прямой и подробный метод запроса.
     *
     * @param string|null   $url
     * @param string[]|null $events
     * @param int|null      $id
     *
     * @return Webhooks|Webhook[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function list(
        string $url = null,
        ?array $events = null,
        $id = null
    ): Webhooks
    {
        $response = $this->getClient()->get('private/api/v2/json/webhooks/list', [
            'url'    => $url,
            'events' => $events,
            'id'     => $id,
        ]);

        if (is_null($response)) {
            return Webhooks::create();
        }

        if (!isset($response['response']['webhooks'])) {
            $this->throwWrongResponse();
        }

        return Webhooks::create($response['response']['webhooks']);
    }

    /**
     * Функция для отправки запроса с подготовкой т.к. она нужна в двух местах для разбиение большого запроса на чанки.
     *
     * @param string $action
     * @param array  $currentChunk
     * @param array  $resultChunk
     * @param array  $result
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    protected function requestSave($action, &$currentChunk, &$resultChunk, &$result)
    {
        $request = ['request' => ['webhooks' => []]];

        if ($currentChunk) {
            $request['request']['webhooks'][$action] = $currentChunk;
            $currentChunk = [];
        }

        $response = $this->getClient()->post('private/api/v2/json/webhooks/' . $action, $request);

        if (isset($response['response']['webhooks'][$action]) && count($response['response']['webhooks'][$action])) {
            foreach ($response['response']['webhooks'][$action] as $key => $item) {
                // Если в ответ придёт не то кол-во сущностей, то мы конечно не упадём,  но ID никому не проставим.
                if (isset($resultChunk[$key])) {
                    /** @var Webhook[] $resultChunk */
                    $resultChunk[$key]->setResult($item['result']);
                }
            }

            $result      = array_merge($result, $resultChunk);
            $resultChunk = [];
        }
    }
}