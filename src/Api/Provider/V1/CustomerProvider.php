<?php

namespace Amocrm\Api\Provider\V1;

use Amocrm\Api\Model\CustomField\Type\AbstractCommunications;
use Amocrm\Api\Model\CustomField\Type\AbstractType;
use Amocrm\Api\Model\CustomField\Type\Birthday;
use Amocrm\Api\Model\CustomField\Type\Checkbox;
use Amocrm\Api\Model\CustomField\Type\Date;
use Amocrm\Api\Model\CustomField\Type\Multiselect;
use Amocrm\Api\Model\Customer;
use Amocrm\Api\Model\Customers;
use Amocrm\Api\Provider\AbstractProvider;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;
use Amocrm\Exception\UpdateTokenFailedException;
use DateTime;
use InvalidArgumentException;

class CustomerProvider extends AbstractProvider
{
    /**
     * @return Customer
     */
    public function create(): Customer
    {
        return Customer::create();
    }

    /**
     * Сохраняет всех покупателей, и которые нужно добавить, обновить или удалить.
     *
     * Внимание! На выходе сущности могу перемешаться, порядок их может измениться.
     *
     * @param array $entities Массив, где под ключами add, update и delete соответсвующие массивы с сущностями.
     *                        В delete могжет быть просто массив id (в ответе уже будут сущности).
     *
     * @return array Массив с ключами add, update и delete соответсвующий входному массиву.
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws InvalidArgumentException
     */
    public function save(array $entities)
    {
        if (!isset($entities['add']) && !isset($entities['update']) && !isset($entities['delete'])) {
            throw new InvalidArgumentException('Во входном массиве должен быть один из ключей: add, update, delete.');
        }

        $current = [
            'add'    => [],
            'update' => [],
            'delete' => [],
        ];
        $result  = [
            'add'    => [],
            'update' => [],
            'delete' => [],
        ];

        foreach (['add', 'update', 'delete'] as $key) {
            if (!isset($entities[$key])) {
                continue;
            }

            $entities[$key] = $this->normalizeCollection($entities[$key]);

            // Для delete генерируем сущности если там ID, чтобы потом вернуть нормальные сущности.
            if ($key === 'delete') {
                foreach ($entities[$key] as &$id) {
                    if (!($id instanceof Customer)) {
                        $id = $this->create()->setId($id);
                    }
                }
            }
            unset($id);
            /**
             * @var Customer $entity
             */
            foreach ($entities[$key] as $entity) {
                // Для delete массив немного по-другому составляется.
                if ($key === 'delete') {
                    $id                = $entity->getId();
                    $current[$key][]   = $id;
                    $result[$key][$id] = $entity;
                } elseif ($key === 'add') {
                    // Надо помнить, что в Переодических покупках поле next_date будт обязательным.
                    $this->validateException($entity, ['name']);
                    // Добавляем каждому request_id, если у него его нет.
                    if ($entity->getRequestId() === null) {
                        $entity->setRequestId(uniqid('customer_', true));
                    }

                    $current[$key][]                       = $entity->getModifiedForApi();
                    $result[$key][$entity->getRequestId()] = $entity;
                } else {
                    $current[$key][]                = $entity->getModifiedForApi();
                    $result[$key][$entity->getId()] = $entity;
                }

                if (count($current['delete']) + count($current['update']) + count($current['add']) >= 200) {
                    $this->requestSave($current, $result);
                }
            }
        }

        if (count($current['delete']) || count($current['update']) || count($current['add'])) {
            $this->requestSave($current, $result);
        }

        $newResult = [];

        foreach (['add', 'update', 'delete'] as $key) {
            if (isset($result[$key]) && count($result[$key])) {
                $newResult[$key] = Customers::create($result[$key]);
            }
        }

        return $newResult;
    }

    /**
     * Добавляет все, но возвращает только первую сущность.
     *
     * @param Customers|Customer[]|Customer $entities
     *
     * @return Customer|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function addOne($entities): ?Customer
    {
        $entities = $this->save(['add' => $entities]);

        if (!$entities['add']->count()) {
            return null;
        }

        return $entities['add']->first();
    }

    /**
     * Обновляет все, но возвращает только первую сущность.
     *
     * @param Customers|Customer[]|Customer $entities
     *
     * @return Customer|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function updateOne($entities): ?Customer
    {
        $entities = $this->save(['update' => $entities]);

        if (!$entities['update']->count()) {
            return null;
        }

        return $entities['update']->first();
    }

    /**
     * Удаляет все, но возвращает только первую сущность.
     *
     * @param Customers|Customer[]|Customer|int|int[] $entities
     *
     * @return Customer|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function deleteOne($entities): ?Customer
    {
        $entities = $this->save(['delete' => $entities]);

        if (!$entities['delete']->count()) {
            return null;
        }

        return $entities['delete']->first();
    }

    /**
     * Гибкий поиск сущностей с фильтрацией и без лимита по 500.
     *
     * @param string|null       $dateType
     * @param DateTime|null     $dateFrom
     * @param DateTime|null     $dateTo
     * @param DateTime|null     $dateNextFrom
     * @param DateTime|null     $dateNextTo
     * @param array|null        $cfs
     * @param array|null        $cfsDate
     * @param int|null          $mainUser
     * @param string|array|null $tasks
     *
     * @return Customers
     *
     * @throws AmocrmApiException
     */
    public function find(
        string $dateType = null,
        DateTime $dateFrom = null,
        DateTime $dateTo = null,
        DateTime $dateNextFrom = null,
        DateTime $dateNextTo = null,
        array $cfs = null,
        array $cfsDate = null,
        int $mainUser = null,
        $tasks = null
    ): Customers
    {
        // Максимально возможная выборка строк (ограничено amocrm), она же шаг.
        $limit    = 500;
        $end      = false;
        $offset   = 0;
        $entities = Customers::create();

        do {
            $chunk = $this->list(
                $dateType,
                $dateFrom,
                $dateTo,
                $dateNextFrom,
                $dateNextTo,
                $cfs,
                $cfsDate,
                $mainUser,
                $tasks,
                $limit,
                $offset
            );

            if ($chunk->count() > 0) {
                $entities->merge($chunk);

                if ($chunk->count() < $limit) {
                    $end = true;
                } else {
                    $offset += $limit;
                }
            } else {
                $end = true;
            }
        } while (!$end);

        return $entities;
    }

    /**
     * Получение сущности или множества сущностей по ID.
     *
     * @param int|int[] $ids
     *
     * @return Customers|Customer[]
     *
     * @throws AmocrmApiException
     */
    public function get($ids): Customers
    {
        $ids = is_array($ids) ? $ids : [$ids];

        // Тут берётся ограничение длинны URL для GET-запроса. Обычно это ограничение сервера (nginx).
        // По умолчанию в nginx 8k (8192 символов), но берём немного меньше, исходим из того что на один
        // id уходит символов 15, и тогда 15 * 350 = 5250. Вроде помещаемся, будем бить их так.
        $limit    = 350;
        $entities = Customers::create();

        foreach (array_chunk($ids, $limit) as $id) {
            $customers = $this->list(
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                $id
            );

            $entities->merge($customers);
        }

        return $entities;
    }

    /**
     * Получение одной первой сущности по ID.
     *
     * @param int|int[] $ids
     *
     * @return Customer|null
     *
     * @throws AmocrmApiException
     */
    public function getOne($ids): ?Customer
    {
        $entities = $this->get($ids);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Самый прямой и подробный метод запроса.
     *
     * @param string|null       $dateType По какой дате искать: create или modify. При этом
     *                                    $dateFrom или $dateTo обязательным к заполнению.
     * @param DateTime|null     $dateFrom
     * @param DateTime|null     $dateTo
     * @param DateTime|null     $dateNextFrom
     * @param DateTime|null     $dateNextTo
     * @param int|null          $mainUser Если указать не существующего в аккаунте пользователя, то вернутся все сущности.
     * @param string|array|null $tasks Фильтрация по задачам сущности: today, tomorrow,
     *                          week, month, quarter, no_tasks, failed_tasks.
     * @param int|null          $limitRows
     * @param int|null          $limitOffset
     * @param array|null        $cfs      Массив с доп. полями, классы наследники AbstractType.
     * @param array|null        $cfsDate  Массив для филтрации по диапазону доп. полей с датой.
     *                                    Должены быть ключи from и / или to содержащие Date или Birthday.
     * @param array|int|null    $id
     *
     * @return Customers
     * @throws AmocrmApiException
     * @throws InvalidArgumentException
     */
    public function list(
        string $dateType = null,
        DateTime $dateFrom = null,
        DateTime $dateTo = null,
        DateTime $dateNextFrom = null,
        DateTime $dateNextTo = null,
        array $cfs = null,
        array $cfsDate = null,
        int $mainUser = null,
        $tasks = null,
        int $limitRows = null,
        int $limitOffset = null,
        $id = null
    ): Customers
    {
        $params = [];

        if ($dateType) {
            if (!in_array($dateType, ['create', 'modify'])) {
                throw new InvalidArgumentException('Параметр date_type может принимать только следующие значения: create и modify.');
            }

            if (!$dateFrom && !$dateTo) {
                throw new InvalidArgumentException('При использовании date_type обязательно к заполнению date_from или date_to.');
            }

            $params['filter']['date']['type'] = $dateType;

            if ($dateFrom) {
                $params['filter']['date']['from'] = $dateFrom->format('d/m/Y');
            }

            if ($dateTo) {
                $params['filter']['date']['to'] = $dateTo->format('d/m/Y');
            }
        }

        if ($dateNextFrom) {
            $params['filter']['next_date']['from'] = $dateNextFrom->format('d/m/Y');
        }

        if ($dateNextTo) {
            $params['filter']['next_date']['to'] = $dateNextTo->format('d/m/Y');
        }

        if ($cfs) {
            $cfs = is_array($cfs) ? $cfs : [$cfs];

            foreach ($cfs as $cf) {
                if ($cf instanceof AbstractType) {
                    $idCf = $cf->getCustomField()->getId();

                    if ($cf instanceof Multiselect) {
                        $params['filter']['cf'][$idCf] = array_column($cf->get(), 'value');
                    } elseif ($cf instanceof Checkbox) {
                        $params['filter']['cf'][$idCf] = $cf->get() ? 'Y' : 'N';
                    } elseif ($cf instanceof AbstractCommunications) {
                        $list = $cf->list();
                        if (count($list)) {
                            $params['filter']['cf'][$idCf] = $list[0];
                        }
                    } else {
                        $params['filter']['cf'][$idCf] = $cf->get();
                    }
                }
            }
        }

        if ($cfsDate) {
            foreach ($cfsDate as $cf) {
                if (isset($cf['from'])
                    && ($cf['from'] instanceof Birthday || $cf['from'] instanceof Date)
                ) {
                    $idCf = $cf['from']->getCustomField()->getId();

                    $params['filter']['cf'][$idCf]['from'] = $cf['from']->get()->format('d/m/Y');
                }

                if (isset($cf['to'])
                    && ($cf['to'] instanceof Birthday || $cf['to'] instanceof Date)
                ) {
                    $idCf = $cf['to']->getCustomField()->getId();

                    $params['filter']['cf'][$idCf]['to'] = $cf['to']->get()->format('d/m/Y');
                }
            }
        }

        if ($mainUser) {
            $params['filter']['main_user'] = $mainUser;
        }

        if ($tasks) {
            $tasks     = is_array($tasks) ? $tasks : [$tasks];
            $tasksType = ['today', 'tomorrow', 'week', 'month', 'quarter', 'no_tasks', 'failed_tasks'];

            if (count(array_unique(array_merge($tasks, $tasksType))) !== count($tasksType)) {
                throw new InvalidArgumentException(
                    'Параметр tasks может принимать только следующие значения: today, tomorrow, week, month, quarter, '.
                    'no_tasks, failed_tasks.'
                );
            }

            $params['filter']['tasks'] = $tasks;
        }

        if ($limitOffset) {
            $params['limit_offset'] = $limitOffset;
        }

        if ($limitRows) {
            $params['limit_rows'] = $limitRows;
        }

        if ($id) {
            $params['filter']['id'] = is_array($id) ? $id : [$id];
        }

        $response = $this->getClient()->get($this->getUrlList(), $params);

        return Customers::create($this->getEmbeddedItems($response));
    }

    /**
     * Функция для отправки запроса с подготовкой т.к. она нужна в двух местах для разбиение большого запроса на чанки.
     *
     * @param array $current
     * @param array $result
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    protected function requestSave(&$current, &$result)
    {
        $request = ['request' => ['customers' => []]];

        if (count($current['add'])) {
            $request['request']['customers']['add'] = $current['add'];
            $current['add']                         = [];
        }

        if (count($current['update'])) {
            $request['request']['customers']['update'] = $current['update'];
            $current['update']                         = [];
        }

        if (count($current['delete'])) {
            $request['request']['customers']['delete'] = $current['delete'];
            $current['delete']                         = [];
        }

        $response = $this->getClient()->post('private/api/v2/json/customers/set', $request);

        foreach (['add', 'update', 'delete'] as $key) {
            if (isset($response['response']['customers'][$key]['errors']) && count($response['response']['customers'][$key]['errors'])) {
                throw new DataIncorrectException(sprintf(
                    'В одной из сущностей ошибка: %s',
                    json_encode($response['response']['customers'][$key]['errors'])
                ));
            }

            if (!isset($response['response']['customers'][$key]['customers'])
                || !count($response['response']['customers'][$key]['customers'])
            ) {
                continue;
            }

            foreach ($response['response']['customers'][$key]['customers'] as $item) {
                /** @var Customer $entity */
                $entity = null;

                if ($key === 'add') {
                    if (isset($result[$key][$item['request_id']])) {
                        $entity = &$result[$key][$item['request_id']];
                    }
                } else {
                    if (isset($result[$key][$item['id']])) {
                        $entity = &$result[$key][$item['id']];
                    }
                }
                // Если в ответ придёт не то кол-во сущностей, то мы конечно не упадём, но данные в сущности не обновим.
                if ($entity) {
                    $entity = Customer::create(array_merge($entity->getModified(), $item));
                }
            }
        }
    }

    /**
     * @return string
     */
    protected function getUrlList()
    {
        return 'private/api/v2/json/customers/list';
    }

    /**
     * @param array|null $response
     *
     * @return array|null
     *
     * @throws AmocrmApiException
     */
    protected function getEmbeddedItems($response)
    {
        if (is_null($response)) {
            return null;
        }

        if (!isset($response['response']['customers'])) {
            $this->throwWrongResponse();
        }

        return $response['response']['customers'];
    }
}