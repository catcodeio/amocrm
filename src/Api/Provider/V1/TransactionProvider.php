<?php

namespace Amocrm\Api\Provider\V1;

use Amocrm\Api\Model\Transaction;
use Amocrm\Api\Model\Transactions;
use Amocrm\Api\Provider\AbstractProvider;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;
use Amocrm\Exception\UpdateTokenFailedException;
use InvalidArgumentException;

class TransactionProvider extends AbstractProvider
{
    /**
     * @return Transaction
     */
    public function create(): Transaction
    {
        return Transaction::create();
    }

    /**
     * Сохраняет все транзакции, и которые нужно добавить, обновить или удалить.
     * Обновить можно только комментарий.
     *
     * Внимание! На выходе сущности могу перемешаться, порядок их может измениться.
     *
     * @param array $entities Массив, где под ключами add, update и delete соответсвующие массивы с сущностями.
     *                        В delete могжет быть просто массив id (в ответе уже будут сущности).
     *
     * @return array Массив с ключами add, update и delete соответсвующий входному массиву.
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws InvalidArgumentException
     */
    public function save(array $entities)
    {
        if (!isset($entities['add']) && !isset($entities['update']) && !isset($entities['delete'])) {
            throw new InvalidArgumentException('Во входном массиве должен быть один из ключей: add, update, delete.');
        }

        $current = [
            'add'    => [],
            'update' => [],
            'delete' => [],
        ];
        $result  = [
            'add'    => [],
            'update' => [],
            'delete' => [],
        ];

        foreach (['add', 'delete'] as $key) {
            if (!isset($entities[$key])) {
                continue;
            }

            $entities[$key] = $this->normalizeCollection($entities[$key]);

            // Для delete генерируем сущности если там ID, чтобы потом вернуть нормальные сущности.
            if ($key === 'delete') {
                foreach ($entities[$key] as &$id) {
                    if (!($id instanceof Transaction)) {
                        $id = $this->create()->setId($id);
                    }
                }
            }
            unset($id);
            /**
             * @var Transaction $entity
             */
            foreach ($entities[$key] as $entity) {
                // Для delete массив немного по-другому составляется.
                if ($key === 'delete') {
                    $id                = $entity->getId();
                    $current[$key][]   = $id;
                    $result[$key][$id] = $entity;
                } else {
                    $this->validateException($entity, ['customer_id', 'price']);
                    // Добавляем каждому request_id, если у него его нет.
                    if ($entity->getRequestId() === null) {
                        $entity->setRequestId(uniqid('transaction_', true));
                    }

                    $current[$key][]                       = $entity->getModifiedForApi();
                    $result[$key][$entity->getRequestId()] = $entity;
                }

                if (count($current['delete']) + count($current['add']) >= 200) {
                    $this->requestAddDelete($current, $result);
                }
            }
        }

        if (count($current['delete']) || count($current['add'])) {
            $this->requestAddDelete($current, $result);
        }

        if (isset($entities['update'])) {
            $entities['update'] = $this->normalizeCollection($entities['update']);
            /**
             * @var Transaction $entity
             */
            foreach ($entities['update'] as $entity) {
                // Для обновления доступно только поле comment, а поле id именуется по-другому.
                // Потому при обновлении массив формируем прямо тут вручную.
                $current['update'][]                = [
                    'transaction_id' => $entity->getId(),
                    'comment'        => $entity->getComment(),
                ];
                $result['update'][$entity->getId()] = $entity;

                if (count($current['update']) >= 200) {
                    $this->requestUpdate($current, $result);
                }
            }
        }

        if (count($current['update'])) {
            $this->requestUpdate($current, $result);
        }

        foreach (['add', 'update', 'delete'] as $key) {
            if (isset($result[$key])) {
                $result[$key] = Transactions::create($result[$key]);
            }
        }

        return $result;
    }

    /**
     * Добавляет все, но возвращает только первую сущность.
     *
     * @param Transactions|Transaction[]|Transaction $entities
     *
     * @return Transaction|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function addOne($entities): ?Transaction
    {
        $entities = $this->save(['add' => $entities]);

        if (!$entities['add']->count()) {
            return null;
        }

        return $entities['add']->first();
    }

    /**
     * Обновляет все, но возвращает только первую сущность.
     *
     * @param Transactions|Transaction[]|Transaction $entities
     *
     * @return Transaction|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function updateOne($entities): ?Transaction
    {
        $entities = $this->save(['update' => $entities]);

        if (!$entities['update']->count()) {
            return null;
        }

        return $entities['update']->first();
    }

    /**
     * Удаляет все, но возвращает только первую сущность.
     *
     * @param Transactions|Transaction[]|Transaction|int|int[] $entities
     *
     * @return Transaction|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function deleteOne($entities): ?Transaction
    {
        $entities = $this->save(['delete' => $entities]);

        if (!$entities['delete']->count()) {
            return null;
        }

        return $entities['delete']->first();
    }

    /**
     * Самый прямой и подробный метод запроса.
     *
     * @param int|null       $customerId
     * @param int|null       $limitRows
     * @param int|null       $limitOffset
     * @param array|int|null $id
     *
     * @return Transactions
     * @throws AmocrmApiException
     * @throws InvalidArgumentException
     */
    public function list(
        int $customerId = null,
        int $limitRows = null,
        int $limitOffset = null,
        $id = null
    ): Transactions
    {
        $params = [];

        if ($customerId) {
            $params['filter']['customer_id'] = $customerId;
        }

        if ($limitOffset) {
            $params['limit_offset'] = $limitOffset;
        }

        if ($limitRows) {
            $params['limit_rows'] = $limitRows;
        }

        if ($id) {
            $params['filter']['id'] = is_array($id) ? $id : [$id];
        }

        $response = $this->getClient()->get('private/api/v2/json/transactions/list', $params);

        if (is_null($response)) {
            return Transactions::create();
        }

        if (!isset($response['response']['transactions'])) {
            $this->throwWrongResponse();
        }

        return Transactions::create($response['response']['transactions']);
    }

    /**
     * Гибкий поиск сущностей с фильтрацией и без лимита по 500.
     *
     * @param int|null $customerId
     *
     * @return Transactions
     *
     * @throws AmocrmApiException
     */
    public function find(int $customerId = null): Transactions
    {
        // Максимально возможная выборка строк (ограничено amocrm), она же шаг.
        $limit    = 500;
        $end      = false;
        $offset   = 0;
        $entities = Transactions::create();

        do {
            $chunk = $this->list(
                $customerId,
                $limit,
                $offset
            );

            if ($chunk->count() > 0) {
                $entities->merge($chunk);

                if ($chunk->count() < $limit) {
                    $end = true;
                } else {
                    $offset += $limit;
                }
            } else {
                $end = true;
            }
        } while (!$end);

        return $entities;
    }

    /**
     * Получение сущности или множества сущностей по ID.
     *
     * @param int|int[] $ids
     *
     * @return Transactions|Transaction[]
     *
     * @throws AmocrmApiException
     */
    public function get($ids): Transactions
    {
        $ids = is_array($ids) ? $ids : [$ids];

        // Тут берётся ограничение длинны URL для GET-запроса. Обычно это ограничение сервера (nginx).
        // По умолчанию в nginx 8k (8192 символов), но берём немного меньше, исходим из того что на один
        // id уходит символов 15, и тогда 15 * 350 = 5250. Вроде помещаемся, будем бить их так.
        $limit    = 350;
        $entities = Transactions::create();

        foreach (array_chunk($ids, $limit) as $id) {
            $transactions = $this->list(
                null,
                null,
                null,
                $id
            );

            $entities->merge($transactions);
        }

        return $entities;
    }

    /**
     * Получение одной первой сущности по ID.
     *
     * @param int|int[] $ids
     *
     * @return Transaction|null
     *
     * @throws AmocrmApiException
     */
    public function getOne($ids): ?Transaction
    {
        $entities = $this->get($ids);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Функция для отправки запроса на добавление и удаление сущностей с
     * подготовкой т.к. она нужна в двух местах для разбиение большого
     * запроса на чанки.
     *
     * @param array $current
     * @param array $result
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    protected function requestAddDelete(&$current, &$result)
    {
        $request = ['request' => ['transactions' => []]];

        if (count($current['add'])) {
            $request['request']['transactions']['add'] = $current['add'];
            $current['add']                            = [];
        }

        if (count($current['delete'])) {
            $request['request']['transactions']['delete'] = $current['delete'];
            $current['delete']                            = [];
        }

        $response = $this->getClient()->post('private/api/v2/json/transactions/set', $request);

        foreach (['add', 'delete'] as $key) {
            if (isset($response['response']['transactions'][$key]['errors'])
                && count($response['response']['transactions'][$key]['errors']))
            {
                throw new DataIncorrectException(sprintf(
                    'В одной из сущностей ошибка: %s',
                    json_encode($response['response']['transactions'][$key]['errors'])
                ));
            }

            if (!isset($response['response']['transactions'][$key]['transactions'])
                || !count($response['response']['transactions'][$key]['transactions'])
            ) {
                continue;
            }

            foreach ($response['response']['transactions'][$key]['transactions'] as $item) {
                /**
                 * @var Transaction $entity
                 */
                $entity = null;

                if ($key === 'add') {
                    if (isset($result[$key][$item['request_id']])) {
                        $entity = &$result[$key][$item['request_id']];
                    }
                } else {
                    if (isset($result[$key][$item['id']])) {
                        $entity = &$result[$key][$item['id']];
                    }
                }
                // Если в ответ придёт не то кол-во сущностей, то мы конечно не упадём,
                // но данные в сущности не обновим, что уже нехорошо.
                if ($entity) {
                    $entity = Transaction::create(array_merge($entity->getModified(), $item));
                }
            }
        }
    }

    /**
     * Функция для отправки запроса на обновление сущностей с подготовкой
     * т.к. она нужна в двух местах для разбиение большого запроса на чанки.
     *
     * @param array $current
     * @param array $result
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    protected function requestUpdate(&$current, &$result)
    {
        $request = ['request' => ['comments' => ['update' => $current['update']]]];

        $response = $this->getClient()->post('private/api/v2/json/transactions/comment', $request);

        if (isset($response['response']['transactions']['comments']['errors'])
            && count($response['response']['transactions']['comments']['errors'])
        ) {
            throw new DataIncorrectException(sprintf(
                'В одной из сущностей ошибка: %s',
                json_encode($response['response']['transactions']['comments']['errors'])
            ));
        }

        if (!isset($response['response']['transactions']['comments']['transactions'])
            || !count($response['response']['transactions']['comments']['transactions'])
        ) {
            return;
        }

        foreach ($response['response']['transactions']['comments']['transactions'] as $item) {
            if (isset($result['comments']['transaction'][$item['id']])) {
                /** @var Transaction $entity */
                $entity = &$result['comments']['transaction'][$item['id']];
                $entity = Transaction::create(array_merge($entity->getModified(), $item));
            }
        }
    }
}