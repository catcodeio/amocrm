<?php

namespace Amocrm\Api\Provider\V1;

use Amocrm\Api\Model\AbstractEntity;
use Amocrm\Api\Model\Account\CustomField;
use Amocrm\Api\Model\CustomField\Field;
use Amocrm\Api\Model\CustomField\Fields;
use Amocrm\Api\Provider\AbstractProvider;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;
use Amocrm\Exception\UpdateTokenFailedException;

class FieldProvider extends AbstractProvider
{
    /**
     * @return Field
     */
    public function create(): Field
    {
        return Field::create();
    }

    /**
     * Генерация готового к удалению поля из существующего на основе данных из
     * аккаунта.
     *
     * @param CustomField $cf
     *
     * @return Field
     */
    public function createFromCustomField(CustomField $cf): Field
    {
        return Field::create()
            ->setId($cf->getId())
            ->setName($cf->getName())
            ->setElementType($cf->getElementType())
            ->setType($cf->getTypeId())
        ;
    }

    /**
     * Добавляет указанные доп. поля в аккаунт.
     *
     * @param Fields|Field[]|Field $entities
     *
     * @return Fields|Field[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function save($entities)
    {
        $entities   = $this->normalizeCollection($entities);
        $currentAdd = [];
        $resultAdd  = [];
        $result     = [];
        /** @var AbstractEntity $entity */
        foreach ($entities as $entity) {
            if (!$entity->getId()) {
                $currentAdd[] = $entity->getModifiedForApi();
                $resultAdd[]  = $entity;
            }

            if (count($currentAdd) >= 200) {
                $this->requestSave($currentAdd, $resultAdd, $result);
            }
        }

        if (count($currentAdd)) {
            $this->requestSave($currentAdd, $resultAdd, $result);
        }

        return Fields::create($result);
    }

    /**
     * Сохраняет все, но возвращает только первую сущность.
     *
     * @param Fields|Field[]|Field $entities
     *
     * @return Field|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function saveOne($entities): ?Field
    {
        $entities = $this->save($entities);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Удаляет указанные доп. поля из аккаунта.
     *
     * @param Fields|Field[]|Field $entities
     *
     * @return Fields|Field[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function delete($entities)
    {
        $entities      = $this->normalizeCollection($entities);
        $currentDelete = [];
        $resultDelete  = [];
        $result        = [];
        /** @var AbstractEntity $entity */
        foreach ($entities as $entity) {
            if ($entity->getId()) {
                $currentDelete[] = $entity->getModifiedForApi();
                $resultDelete[]  = $entity;
            }

            if (count($currentDelete) >= 200) {
                $this->requestDelete($currentDelete, $resultDelete, $result);
            }
        }

        if (count($currentDelete)) {
            $this->requestDelete($currentDelete, $resultDelete, $result);
        }

        return Fields::create($result);
    }

    /**
     * Удаляет все, но возвращает только первую сущность.
     *
     * @param Fields|Field[]|Field $entities
     *
     * @return Field|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function deleteOne($entities): ?Field
    {
        $entities = $this->delete($entities);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Функция для отправки запроса с подготовкой т.к. она нужна в двух местах для разбиение большого запроса на чанки.
     *
     * @param array $currentAdd
     * @param array $resultAdd
     * @param array $result
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    protected function requestSave(&$currentAdd, &$resultAdd, &$result)
    {
        $request = ['request' => ['fields' => []]];

        if ($currentAdd) {
            $request['request']['fields']['add'] = $currentAdd;
            $currentAdd = [];
        }

        $response = $this->getClient()->post('private/api/v2/json/fields/set', $request);

        if (isset($response['response']['fields']['add']) && count($response['response']['fields']['add'])) {
            foreach ($response['response']['fields']['add'] as $key => $item) {
                // Если в ответ придёт не то кол-во сущностей, то мы конечно не упадём, но ID никому не проставим.
                if (isset($resultAdd[$key])) {
                    /** @var Field[] $resultAdd */
                    $resultAdd[$key]->setId($item['id']);
                }
            }

            $result    = array_merge($result, $resultAdd);
            $resultAdd = [];
        }
    }

    /**
     * Функция для отправки запроса с подготовкой т.к. она нужна в двух местах для разбиение большого запроса на чанки.
     *
     * @param array $currentDelete
     * @param array $resultDelete
     * @param array $result
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    protected function requestDelete(&$currentDelete, &$resultDelete, &$result)
    {
        $request = ['request' => ['fields' => []]];

        if ($currentDelete) {
            $request['request']['fields']['delete'] = $currentDelete;
            $currentDelete = [];
        }

        $response = $this->getClient()->post('private/api/v2/json/fields/set', $request);

        if (isset($response['response']['fields']['delete']) && count($response['response']['fields']['delete'])) {
            $result       = array_merge($result, $resultDelete);
            $resultDelete = [];
        }
    }
}