<?php

namespace Amocrm\Api\Provider\V1;

use Amocrm\Api\Helper\ElementType;
use Amocrm\Api\Model\AbstractEntity;
use Amocrm\Api\Model\Link;
use Amocrm\Api\Model\Links;
use Amocrm\Api\Provider\AbstractProvider;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;
use Amocrm\Exception\UpdateTokenFailedException;

class LinkProvider extends AbstractProvider
{
    /**
     * @return Link
     */
    public function create(): Link
    {
        return Link::create();
    }

    /**
     * Добавляем все новые связи.
     *
     * @param Link[]|Link|Links $entities
     *
     * @return bool|array Если возвращается массив, то это массив ошибок и что-то пошло не так.
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function link($entities)
    {
        $entities = $this->normalizeCollection($entities);
        $links    = [];
        $errors   = [];
        /** @var AbstractEntity $entity */
        foreach ($entities as $entity) {
            $links[] = $entity->getModifiedForApi();

            if (count($links) >= 200) {
                $this->requestSave('link', $links, $errors);
            }
        }

        if (count($links)) {
            $this->requestSave('link', $links, $errors);
        }

        if (count($links)) {
            return $links;
        }

        return true;
    }

    /**
     * УДаление существующих связей.
     *
     * @param Link[]|Link|Links $entities
     *
     * @return bool|array Если возвращается массив, то это массив ошибок и что-то пошло не так.
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function unlink($entities)
    {
        $entities = $this->normalizeCollection($entities);
        $links    = [];
        $errors   = [];
        /** @var AbstractEntity $entity */
        foreach ($entities as $entity) {
            $links[] = $entity->getModifiedForApi();

            if (count($links) >= 200) {
                $this->requestSave('unlink', $links, $errors);
            }
        }

        if (count($links)) {
            $this->requestSave('unlink', $links, $errors);
        }

        if (count($links)) {
            return $links;
        }

        return true;
    }

    /**
     * @param int $fromId
     *
     * @return Links
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findFromLeadToContact(int $fromId)
    {
        return $this->list(ElementType::LEAD, $fromId, ElementType::CONTACT);
    }

    /**
     * @param int $fromId
     *
     * @return Links
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findFromLeadToCompany(int $fromId)
    {
        return $this->list(ElementType::LEAD, $fromId, ElementType::COMPANY);
    }

    /**
     * @param int $fromId
     *
     * @return Links
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findFromLeadToCustomer(int $fromId)
    {
        return $this->list(ElementType::LEAD, $fromId, ElementType::CUSTOMER);
    }

    /**
     * @param int $fromId
     *
     * @return Links
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findFromContactToLead(int $fromId)
    {
        return $this->list(ElementType::CONTACT, $fromId, ElementType::LEAD);
    }

    /**
     * @param int $fromId
     *
     * @return Links
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findFromContactToCompany(int $fromId)
    {
        return $this->list(ElementType::CONTACT, $fromId, ElementType::COMPANY);
    }

    /**
     * @param int $fromId
     *
     * @return Links
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findFromContactToCustomer(int $fromId)
    {
        return $this->list(ElementType::CONTACT, $fromId, ElementType::CUSTOMER);
    }

    /**
     * @param int $fromId
     *
     * @return Links
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findFromCompanyToLead(int $fromId)
    {
        return $this->list(ElementType::COMPANY, $fromId, ElementType::LEAD);
    }

    /**
     * @param int $fromId
     *
     * @return Links
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findFromCompanyToContact(int $fromId)
    {
        return $this->list(ElementType::COMPANY, $fromId, ElementType::CONTACT);
    }

    /**
     * @param int $fromId
     *
     * @return Links
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findFromCompanyToCustomer(int $fromId)
    {
        return $this->list(ElementType::COMPANY, $fromId, ElementType::CUSTOMER);
    }

    /**
     * @param int $fromId
     *
     * @return Links
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findFromCustomerToLead(int $fromId)
    {
        return $this->list(ElementType::CUSTOMER, $fromId, ElementType::LEAD);
    }

    /**
     * @param int $fromId
     *
     * @return Links
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findFromCustomerToContact(int $fromId)
    {
        return $this->list(ElementType::CUSTOMER, $fromId, ElementType::CONTACT);
    }

    /**
     * @param int $fromId
     *
     * @return Links
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findFromCustomerToCompany(int $fromId)
    {
        return $this->list(ElementType::CUSTOMER, $fromId, ElementType::COMPANY);
    }

    /**
     * Самый прямой и подробный метод запроса.
     *
     * @param string $from        Тип сущности, от которой надо искать связи (leads, contacts,
     *                            companies, customers). Принимает константы из ElementType.
     * @param int    $fromId
     * @param string $to          Тип сущности, к которой надо искать связи (leads, contacts,
     *                            companies, customers). Принимает константы из ElementType.
     * @param int    $toCatalogId Применятся только при получнии связей с каталогами.
     *
     * @return Links
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function list(
        string $from,
        int $fromId,
        string $to,
        int $toCatalogId = null
    ) {
        $response = $this->getClient()->get('private/api/v2/json/links/list', [
            'links' => [
                [
                    'from'          => ElementType::toPlural($from),
                    'from_id'       => $fromId,
                    'to'            => ElementType::toPlural($to),
                    'to_catalog_id' => $toCatalogId,
                ]
            ]
        ]);

        return Links::create($this->getEmbeddedItems($response));
    }

    /**
     * Функция для отправки запроса с подготовкой т.к. она нужна в двух местах для разбиение большого запроса на чанки.
     *
     * @param string $action Совершаемоей действие: link, unlink.
     * @param array  $links
     * @param array  $errors
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    protected function requestSave($action, &$links, &$errors)
    {
        $request = ['request' => ['links' => [
            $action => $links,
        ]]];

        $links = [];

        $response = $this->getClient()->post('private/api/v2/json/links/set', $request);

        if (is_null($response)) {
            $errors[] = 'Empty response (204)';
        }

        if (isset($response['response']['links'][$action]['errors'])
            && count($response['response']['links'][$action]['errors'])
        ) {
            $errors[] = $response['response']['links'][$action]['errors'];
        }
    }

    /**
     * @param array|null $response
     *
     * @return array|null
     *
     * @throws AmocrmApiException
     */
    protected function getEmbeddedItems($response)
    {
        if (is_null($response)) {
            return null;
        }

        if (!isset($response['response']['links'])) {
            $this->throwWrongResponse();
        }

        return $response['response']['links'];
    }
}