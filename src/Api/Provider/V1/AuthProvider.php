<?php

namespace Amocrm\Api\Provider\V1;

use Amocrm\Api\Provider\AbstractProvider;
use Amocrm\Exception\AmocrmApiException;

/**
 * Провайдер для работы с авторизацией в API.
 */
class AuthProvider extends AbstractProvider
{
    /**
     * @param string $login
     * @param string $token
     * @param string $clientUuid
     * @param string $clientSecret
     *
     * @throws AmocrmApiException
     *
     * @return bool
     */
    public function exchange(
        string $login,
        string $token,
        string $clientUuid,
        string $clientSecret
    )
    {
        try {
            $this->getClient()->post('oauth2/exchange_api_key', [
                'login'         => $login,
                'api_key'       => $token,
                'client_uuid'   => $clientUuid,
                'client_secret' => $clientSecret,
            ]);
        } catch (AmocrmApiException $e) {
            if ($e->getMessage() != 'Response is empty' && $e->getCode() != 200) {
                throw $e;
            }
        }

        return true;
    }
}