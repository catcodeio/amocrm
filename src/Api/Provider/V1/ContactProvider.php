<?php

namespace Amocrm\Api\Provider\V1;

use Amocrm\Api\Helper\ElementType;
use Amocrm\Api\Model\AbstractEntity;
use Amocrm\Api\Model\Contact;
use Amocrm\Api\Model\Contacts;
use Amocrm\Api\Model\ContactLink;
use Amocrm\Api\Model\ContactLinks;
use Amocrm\Api\Helper\Phone;
use Amocrm\Api\Provider\AbstractProvider;
use Amocrm\Exception\UpdateTokenFailedException;
use DateTime;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;

class ContactProvider extends AbstractProvider
{
    /**
     * @return Contact
     */
    public function create(): Contact
    {
        return Contact::create();
    }

    /**
     * Сохраняет все контакты, и которые нужно добавить и которые обновить.
     *
     * Внимание! На выходе контакты могу перемешаться, порядок их может измениться.
     *
     * @param Contacts|Contact[]|Contact $entities
     *
     * @return Contacts|Contact[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function save($entities)
    {
        $entities = $this->normalizeCollection($entities);

        $currentAdd    = [];
        $currentUpdate = [];
        $resultUpdate  = [];
        $resultAdd     = [];
        $result        = [];
        /** @var AbstractEntity $entity */
        foreach ($entities as $entity) {
            if ($entity->getId()) {
                $currentUpdate[] = $entity->getModifiedForApi();
                $resultUpdate[]  = $entity;
            } else {
                $currentAdd[] = $entity->getModifiedForApi();
                $resultAdd[]  = $entity;
            }

            if (count($currentAdd) + count($currentUpdate) >= 200) {
                $this->requestSave($currentAdd, $currentUpdate, $resultUpdate, $resultAdd, $result);
            }
        }

        if (count($currentAdd) || count($currentUpdate)) {
            $this->requestSave($currentAdd, $currentUpdate, $resultUpdate, $resultAdd, $result);
        }

        return Contacts::create($result);
    }

    /**
     * Сохраняет все, но возвращает только первую сущность.
     *
     * @param Contacts|Contact[]|Contact $entities
     *
     * @return Contact|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function saveOne($entities): ?Contact
    {
        $entities = $this->save($entities);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Гибкий поиск контактов с фильтрацией и без лимита по 500.
     *
     * @param string|null    $query
     * @param int|int[]|null $responsibleUserId
     * @param DateTime|null  $dateModified
     *
     * @return Contacts|Contact[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function find(
        string $query = null,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): Contacts
    {
        // Максимально возможная выборка строк (ограничено amocrm), она же шаг.
        $limit    = 500;
        $end      = false;
        $offset   = 0;
        $entities = Contacts::create();

        do {
            $chunk = $this->list(
                $query,
                $responsibleUserId,
                $dateModified,
                $limit,
                $offset
            );

            if ($chunk->count() > 0) {
                $entities->merge($chunk);

                if ($chunk->count() < $limit) {
                    $end = true;
                } else {
                    $offset += $limit;
                }
            } else {
                $end = true;
            }
        } while (!$end);

        return $entities;
    }

    /**
     * Получение контакта или множества контактов по ID.
     *
     * @param int[]|int $ids
     *
     * @return Contacts|Contact[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function get($ids): Contacts
    {
        $ids = is_array($ids) ? $ids : [$ids];

        // Тут берётся ограничение длинны URL для GET-запроса. Обычно это ограничение сервера (nginx).
        // По умолчанию в nginx 8k (8192 символов), но берём немного меньше, исходим из того что на один
        // id уходит символов 15, и тогда 15 * 350 = 5250. Вроде помещаемся, будем бить их так.
        $limit    = 350;
        $entities = Contacts::create();

        foreach (array_chunk($ids, $limit) as $id) {
            $chunk = $this->list(
                null,
                null,
                null,
                null,
                null,
                $id
            );

            $entities->merge($chunk);
        }

        return $entities;
    }

    /**
     * Получение одного первого контактка по ID.
     *
     * @param int|int[] $ids
     *
     * @return Contact|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function getOne($ids): ?Contact
    {
        $entities = $this->get($ids);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Получение контактов по сделкам.
     *
     * @param int[]|int $id
     *
     * @return Contacts|Contact[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findByLeads($id): Contacts
    {
        $contactIds = $this->links($id, 'lead')->getIdsOfContacts();

        return $this->get($contactIds);
    }

    /**
     * Получение одного первого контакта по сделкам.
     *
     * @param int[]|int $id
     *
     * @return Contact|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findOneByLeads($id): ?Contact
    {
        $entities = $this->findByLeads($id);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Получение контактов по телефону.
     *
     * @param string        $phone
     * @param int|null      $responsibleUserId
     * @param DateTime|null $dateModified
     *
     * @return Contacts
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findByPhone(
        string $phone,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): Contacts
    {
        $phoneClear = Phone::clear($phone);

        return $this
            ->find($phoneClear, $responsibleUserId, $dateModified)
            ->filter(function (Contact $contact) use ($phoneClear) {
                $cfPhone = $contact->getCustomFields()->filterOneByField('code', 'PHONE');

                if (!$cfPhone) {
                    return false;
                }

                $phones  = array_map(function ($phone) {
                    return Phone::clear($phone);
                }, $cfPhone->phone()->list());

                return in_array($phoneClear, $phones);
            });
    }

    /**
     * Получение одного первого контакта по телефону.
     *
     * @param string        $phone
     * @param int|null      $responsibleUserId
     * @param DateTime|null $dateModified
     *
     * @return Contact|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findOneByPhone(
        string $phone,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): ?Contact
    {
        $entities = $this->findByPhone($phone, $responsibleUserId, $dateModified);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Получение контактов по мылу.
     *
     * @param string        $email
     * @param int|null      $responsibleUserId
     * @param DateTime|null $dateModified
     *
     * @return Contacts
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findByEmail(
        string $email,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): Contacts
    {
        return $this
            ->find($email, $responsibleUserId, $dateModified)
            ->filter(function (Contact $contact) use ($email) {
                $cfEmail = $contact->getCustomFields()->filterOneByField('code', 'EMAIL');

                if (!$cfEmail) {
                    return false;
                }

                return in_array($email, $cfEmail->email()->list());
            });
    }

    /**
     * Получение одного первого контакта по мылу.
     *
     * @param string        $email
     * @param int|null      $responsibleUserId
     * @param DateTime|null $dateModified
     *
     * @return Contact|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findOneByEmail(
        string $email,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): ?Contact
    {
        $entities = $this->findByEmail($email, $responsibleUserId, $dateModified);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Получение контактов по значению обычного поля (например, текст).
     *
     * @param int           $fieldId
     * @param string        $query
     * @param int|null      $responsibleUserId
     * @param DateTime|null $dateModified
     *
     * @return Contacts
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findByField(
        int $fieldId,
        string $query,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): Contacts
    {
        return $this
            ->find($query, $responsibleUserId, $dateModified)
            ->filter(function (Contact $contact) use ($fieldId, $query) {
                $cf = $contact->getCustomField($fieldId);

                if (!$cf) {
                    return false;
                }

                return $cf->text()->get() == $query;
            });
    }

    /**
     * Получение одного первого контакта по значению обычного поля (например, текст).
     *
     * @param int           $fieldId
     * @param string        $query
     * @param int|null      $responsibleUserId
     * @param DateTime|null $dateModified
     *
     * @return Contact|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findOneByField(
        int $fieldId,
        string $query,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): ?Contact
    {
        $entities = $this->findByField($fieldId, $query, $responsibleUserId, $dateModified);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Самый прямой и подробный метод запроса контактов.
     *
     * @param string|null    $query
     * @param int|int[]|null $responsibleUserId
     * @param DateTime|null  $dateModified
     * @param int|null       $limitRows
     * @param int|null       $limitOffset
     * @param int|int[]|null $id
     * @param string|null    $type Возможны варианты contact (по умолчанию), company и all.
     *
     * @return Contacts|Contact[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function list(
        string $query = null,
        $responsibleUserId = null,
        DateTime $dateModified = null,
        int $limitRows = null,
        int $limitOffset = null,
        $id = null,
        string $type = null
    ): Contacts
    {
        $headers = [];

        if ($dateModified) {
            $headers = ['if-modified-since: ' . $dateModified->format('D, d M Y H:i:s')];
        }

        $response = $this->getClient()->get($this->getUrlList(), [
            'query'               => $query,
            'responsible_user_id' => $responsibleUserId,
            'limit_rows'          => $limitRows,
            'limit_offset'        => $limitOffset,
            'id'                  => $id,
            'type'                => $type,
        ], $headers);

        return Contacts::create($this->getEmbeddedItems($response));
    }

    /**
     * Самый прямой и подробный метод запроса связей между сделкой и контактами
     * без ограничения в 500.
     *
     * @param int|int[]     $id
     * @param string        $elementType Тип сущности, которого указанны id (contact, lead). Принимает константы из ElementType.
     * @param DateTime|null $dateModified
     *
     * @return ContactLinks|ContactLink[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function links(
        $id,
        string $elementType,
        DateTime $dateModified = null
    ): ContactLinks
    {
        $headers = [];

        if ($dateModified) {
            $headers = ['if-modified-since: ' . $dateModified->format('D, d M Y H:i:s')];
        }

        $id          = is_array($id) ? $id : [$id];
        $end         = false;
        $entities    = [];
        $keyName     = ($elementType == ElementType::LEAD ? 'deals_link' : 'contacts_link');
        $idsChunks   = array_chunk($id, 350); // Ограничение длинны GET-запроса.
        $countChunks = count($idsChunks) - 1;

        do {
            $response = $this->getClient()->get('private/api/v2/json/contacts/links', [
                $keyName => $idsChunks[$countChunks],
            ], $headers);

            $countChunks--;

            if (!is_null($response)) {
                if (!isset($response['response']['links'])) {
                    $this->throwWrongResponse();
                }

                if (count($response['response']['links']) > 0) {
                    $entities = array_merge($entities, $response['response']['links']);

                    if ($countChunks < 0) {
                        $end = true;
                    }
                } else {
                    $end = true;
                }
            } else {
                $end = true;
            }
        } while (!$end);

        return ContactLinks::create($entities);
    }

    /**
     * Функция для отправки запроса с подготовкой т.к. она нужна в двух местах для разбиение большого запроса на чанки.
     *
     * @param array $currentAdd
     * @param array $currentUpdate
     * @param array $resultUpdate
     * @param array $resultAdd
     * @param array $result
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    protected function requestSave(&$currentAdd, &$currentUpdate, &$resultUpdate, &$resultAdd, &$result)
    {
        $request = ['request' => ['contacts' => []]];

        if ($currentAdd) {
            $request['request']['contacts']['add'] = $currentAdd;
            $currentAdd = [];
        }

        if ($currentUpdate) {
            $request['request']['contacts']['update'] = $currentUpdate;
            $currentUpdate = [];
        }

        $response = $this->getClient()->post('private/api/v2/json/contacts/set', $request);

        if (isset($response['response']['contacts']['add']) && count($response['response']['contacts']['add'])) {
            foreach ($response['response']['contacts']['add'] as $key => $item) {
                // Если в ответ придёт не то кол-во сущностей, то мы конечно не упадём, но ID никому не проставим.
                if (isset($resultAdd[$key])) {
                    /** @var Contact[] $resultAdd */
                    $resultAdd[$key]->setId($item['id']);
                }
            }

            $result    = array_merge($result, $resultAdd);
            $resultAdd = [];
        }

        if (isset($response['response']['contacts']['update']) && count($response['response']['contacts']['update'])) {
            // Думается, что ID и так есть, а потому перебирать не будем.
            $result       = array_merge($result, $resultUpdate);
            $resultUpdate = [];
        }
    }

    /**
     * @return string
     */
    protected function getUrlList()
    {
        return 'private/api/v2/json/contacts/list';
    }

    /**
     * @param array|null $response
     *
     * @return array|null
     *
     * @throws AmocrmApiException
     */
    protected function getEmbeddedItems($response)
    {
        if (is_null($response)) {
            return null;
        }

        if (!isset($response['response']['contacts'])) {
            $this->throwWrongResponse();
        }

        return $response['response']['contacts'];
    }
}