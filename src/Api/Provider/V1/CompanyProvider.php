<?php

namespace Amocrm\Api\Provider\V1;

use Amocrm\Api\Model\AbstractEntity;
use Amocrm\Api\Model\Company;
use Amocrm\Api\Model\Companies;
use Amocrm\Api\Helper\Phone;
use Amocrm\Api\Provider\AbstractProvider;
use Amocrm\Exception\UpdateTokenFailedException;
use DateTime;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;

class CompanyProvider extends AbstractProvider
{
    /**
     * @return Company
     */
    public function create(): Company
    {
        return Company::create();
    }

    /**
     * Сохраняет все компании, и которые нужно добавить и которые обновить.
     *
     * Внимание! На выходе компании могу перемешаться, порядок их может измениться.
     *
     * @param Companies|Company[]|Company $entities
     *
     * @return Companies|Company[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function save($entities)
    {
        $entities = $this->normalizeCollection($entities);

        $currentAdd    = [];
        $currentUpdate = [];
        $resultUpdate  = [];
        $resultAdd     = [];
        $result        = [];
        /** @var AbstractEntity $entity */
        foreach ($entities as $entity) {
            if ($entity->getId()) {
                $currentUpdate[] = $entity->getModifiedForApi();
                $resultUpdate[]  = $entity;
            } else {
                $currentAdd[] = $entity->getModifiedForApi();
                $resultAdd[]  = $entity;
            }

            if (count($currentAdd) + count($currentUpdate) >= 200) {
                $this->requestSave($currentAdd, $currentUpdate, $resultUpdate, $resultAdd, $result);
            }
        }

        if (count($currentAdd) || count($currentUpdate)) {
            $this->requestSave($currentAdd, $currentUpdate, $resultUpdate, $resultAdd, $result);
        }

        return Companies::create($result);
    }

    /**
     * Сохраняет все, но возвращает только первую сущность.
     *
     * @param Companies|Company[]|Company $entities
     *
     * @return Company|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function saveOne($entities): ?Company
    {
        $entities = $this->save($entities);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Гибкий поиск контактов с фильтрацией и без лимита по 500.
     *
     * @param string|null    $query
     * @param int|int[]|null $responsibleUserId
     * @param DateTime|null  $dateModified
     *
     * @return Companies|Company[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function find(
        string $query = null,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): Companies
    {
        // Максимально возможная выборка строк (ограничено amocrm), она же шаг.
        $limit    = 500;
        $end      = false;
        $offset   = 0;
        $entities = Companies::create();

        do {
            $chunk = $this->list(
                $query,
                $responsibleUserId,
                $dateModified,
                $limit,
                $offset
            );

            if ($chunk->count() > 0) {
                $entities->merge($chunk);

                if ($chunk->count() < $limit) {
                    $end = true;
                } else {
                    $offset += $limit;
                }
            } else {
                $end = true;
            }
        } while (!$end);

        return $entities;
    }

    /**
     * Получение контакта или множества контактов по ID.
     *
     * @param int|int[] $ids
     *
     * @return Companies|Company[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function get($ids): Companies
    {
        $ids = is_array($ids) ? $ids : [$ids];

        // Тут берётся ограничение длинны URL для GET-запроса. Обычно это ограничение сервера (nginx).
        // По умолчанию в nginx 8k (8192 символов), но берём немного меньше, исходим из того что на один
        // id уходит символов 15, и тогда 15 * 350 = 5250. Вроде помещаемся, будем бить их так.
        $limit    = 350;
        $entities = Companies::create();

        foreach (array_chunk($ids, $limit) as $id) {
            $chunk = $this->list(
                null,
                null,
                null,
                null,
                null,
                $id
            );

            $entities->merge($chunk);
        }

        return $entities;
    }

    /**
     * Получение одной первой компании по ID.
     *
     * @param int|int[] $ids
     *
     * @return Company|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function getOne($ids): ?Company
    {
        $entities = $this->get($ids);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Получение компании по сделкам.
     *
     * @param int[]|int $id
     *
     * @return Companies|Company[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findByLeads($id): Companies
    {
        $leads      = $this->getAmocrmApi()->lead()->get($id);
        $companyIds = [];

        foreach ($leads as $lead) {
            $companyIds[] = $lead->getLinkedCompanyId();
        }

        return $this->get($companyIds);
    }

    /**
     * Получение одной первой компании по сделкам.
     *
     * @param int[]|int $id
     *
     * @return Company|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findOneByLeads($id): ?Company
    {
        $entities = $this->findByLeads($id);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Получение компании по контактам.
     *
     * @param int[]|int $id
     *
     * @return Companies|Company[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findByContacts($id): Companies
    {
        $contacts   = $this->getAmocrmApi()->contact()->get($id);
        $companyIds = [];

        foreach ($contacts as $contact) {
            $companyIds[] = $contact->getLinkedCompanyId();
        }

        return $this->get($companyIds);
    }

    /**
     * Получение одной первой компании по контактам.
     *
     * @param int[]|int $id
     *
     * @return Company|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findOneByContacts($id): ?Company
    {
        $entities = $this->findByContacts($id);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Получение компаний по телефону.
     *
     * @param string        $phone
     * @param int|null      $responsibleUserId
     * @param DateTime|null $dateModified
     *
     * @return Companies
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findByPhone(
        string $phone,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): Companies
    {
        $phoneClear = Phone::clear($phone);

        return $this
            ->find($phoneClear, $responsibleUserId, $dateModified)
            ->filter(function (Company $contact) use ($phoneClear) {
                $cfPhone = $contact->getCustomFields()->filterOneByField('code', 'PHONE');

                if (!$cfPhone) {
                    return false;
                }

                $phones  = array_map(function ($phone) {
                    return Phone::clear($phone);
                }, $cfPhone->phone()->list());

                return in_array($phoneClear, $phones);
            });
    }

    /**
     * Получение одной первой компании по телефону.
     *
     * @param string        $phone
     * @param int|null      $responsibleUserId
     * @param DateTime|null $dateModified
     *
     * @return Company|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findOneByPhone(string $phone,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): ?Company
    {
        $entities = $this->findByPhone($phone, $responsibleUserId, $dateModified);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Получение компаний по мылу.
     *
     * @param string        $email
     * @param int|null      $responsibleUserId
     * @param DateTime|null $dateModified
     *
     * @return Companies
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findByEmail(
        string $email,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): Companies
    {
        return $this
            ->find($email, $responsibleUserId, $dateModified)
            ->filter(function (Company $contact) use ($email) {
                $cfEmail = $contact->getCustomFields()->filterOneByField('code', 'EMAIL');

                if (!$cfEmail) {
                    return false;
                }

                return in_array($email, $cfEmail->email()->list());
            });
    }

    /**
     * Получение одной первой компании по мылу.
     *
     * @param string        $email
     * @param int|null      $responsibleUserId
     * @param DateTime|null $dateModified
     *
     * @return Company|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findOneByEmail(
        string $email,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): ?Company
    {
        $entities = $this->findByEmail($email, $responsibleUserId, $dateModified);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Получение компаний по значению обычного поля (например, текст).
     *
     * @param int           $fieldId
     * @param string        $query
     * @param int|null      $responsibleUserId
     * @param DateTime|null $dateModified
     *
     * @return Companies
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findByField(
        int $fieldId,
        string $query,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): Companies
    {
        return $this
            ->find($query, $responsibleUserId, $dateModified)
            ->filter(function (Company $company) use ($fieldId, $query) {
                $cf = $company->getCustomField($fieldId);

                if (!$cf) {
                    return false;
                }

                return $cf->text()->get() == $query;
            });
    }

    /**
     * Получение одной первой компании по значению обычного поля (например, текст).
     *
     * @param int           $fieldId
     * @param string        $query
     * @param int|null      $responsibleUserId
     * @param DateTime|null $dateModified
     *
     * @return Company|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findOneByField(
        int $fieldId,
        string $query,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): ?Company
    {
        $entities = $this->findByField($fieldId, $query, $responsibleUserId, $dateModified);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Самый прямой и подробный метод запроса.
     *
     * @param string|null    $query
     * @param int|int[]|null $responsibleUserId
     * @param DateTime|null  $dateModified
     * @param int|null       $limitRows
     * @param int|null       $limitOffset
     * @param int|int[]|null $id
     *
     * @return Companies|Company[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function list(
        string $query = null,
        $responsibleUserId = null,
        DateTime $dateModified = null,
        int $limitRows = null,
        int $limitOffset = null,
        $id = null
    ): Companies
    {
        $headers = [];

        if ($dateModified) {
            $headers = ['if-modified-since: ' . $dateModified->format('D, d M Y H:i:s')];
        }

        $response = $this->getClient()->get($this->getUrlList(), [
            'query'               => $query,
            'responsible_user_id' => $responsibleUserId,
            'limit_rows'          => $limitRows,
            'limit_offset'        => $limitOffset,
            'id'                  => $id,
        ], $headers);

        return Companies::create($this->getEmbeddedItems($response));
    }

    /**
     * Функция для отправки запроса с подготовкой т.к. она нужна в двух местах для разбиение большого запроса на чанки.
     *
     * @param array $currentAdd
     * @param array $currentUpdate
     * @param array $resultUpdate
     * @param array $resultAdd
     * @param array $result
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    protected function requestSave(&$currentAdd, &$currentUpdate, &$resultUpdate, &$resultAdd, &$result)
    {
        $request = ['request' => ['contacts' => []]];

        if ($currentAdd) {
            $request['request']['contacts']['add'] = $currentAdd;
            $currentAdd = [];
        }

        if ($currentUpdate) {
            $request['request']['contacts']['update'] = $currentUpdate;
            $currentUpdate = [];
        }

        $response = $this->getClient()->post('private/api/v2/json/company/set', $request);

        if (isset($response['response']['contacts']['add']) && count($response['response']['contacts']['add'])) {
            foreach ($response['response']['contacts']['add'] as $key => $item) {
                // Если в ответ придёт не то кол-во сущностей, то мы конечно не упадём,
                // но ID никому не проставим, что уже не хорошо.
                if (isset($resultAdd[$key])) {
                    /** @var Company[] $resultAdd */
                    $resultAdd[$key]->setId($item['id']);
                }
            }

            $result    = array_merge($result, $resultAdd);
            $resultAdd = [];
        }

        if (isset($response['response']['contacts']['update']) && count($response['response']['contacts']['update'])) {
            // Думается, что ID и так есть, а потому перебирать не будем.
            $result       = array_merge($result, $resultUpdate);
            $resultUpdate = [];
        }
    }

    /**
     * @return string
     */
    protected function getUrlList()
    {
        return 'private/api/v2/json/company/list';
    }

    /**
     * @param array|null $response
     *
     * @return array|null
     *
     * @throws AmocrmApiException
     */
    protected function getEmbeddedItems($response)
    {
        if (is_null($response)) {
            return null;
        }

        if (!isset($response['response']['contacts'])) {
            $this->throwWrongResponse();
        }

        return $response['response']['contacts'];
    }
}