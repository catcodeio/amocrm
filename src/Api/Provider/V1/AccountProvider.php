<?php

namespace Amocrm\Api\Provider\V1;

use Amocrm\Api\Model\Account;
use Amocrm\Api\Model\Account\CustomField;
use Amocrm\Api\Model\Account\Group;
use Amocrm\Api\Model\Account\NoteTypes;
use Amocrm\Api\Model\Account\Pipeline;
use Amocrm\Api\Model\Account\TaskTypes;
use Amocrm\Api\Model\Account\User;
use Amocrm\Api\Model\Account\Users;
use Amocrm\Api\Model\Account\Groups;
use Amocrm\Api\Model\Account\CustomFields;
use Amocrm\Api\Model\Account\Pipelines;
use Amocrm\Api\Provider\AbstractProvider;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;

/**
 * Внимание! Необходимо прописать ниже те же самые методы, что и в моделе Account,
 * чтобы подсказки работали. Другого выхода я не нашёл "унаследовать" методы.
 *
 * @method int          getId()
 * @method string       getName()
 * @method string       getSubdomain()
 * @method string       getTimezone()
 * @method string       getCurrency()
 * @method string       getLanguage()
 * @method string       getDateFormat()
 * @method string       getTimeFormat()
 * @method array        getShortDatePattern()
 * @method string       getCountry()
 * @method string       getUnsortedOn()
 * @method string       getTimezoneoffset()
 * @method Users        getUsers()
 * @method int          getCurrentUser()
 * @method Groups       getGroups()
 * @method NoteTypes    getNoteTypes()
 * @method TaskTypes    getTaskTypes()
 * @method CustomFields getCustomFields()
 * @method Pipelines    getPipelines()
 *
 * @method CustomField getCustomField(int $customFieldId)
 * @method Pipeline    getPipeline(int $pipelineId)
 * @method User        getUser(int $userId)
 * @method Group       getGroup(int $groupId)
 *
 * @method array        getInitial()
 */
class AccountProvider extends AbstractProvider
{
    /**
     * @var Account
     */
    private $account;

    /**
     * @param string      $message
     * @param string|null $header
     * @param string|null $type
     * @param string|null $link
     *
     * @return void
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function notify(string $message, string $header = '', string $type = 'error', string $link = null)
    {
        $this->getClient()->post('ajax/v1/inbox/add/', [
            'request' => [
                'date'    => time(),
                'type'    => $type,
                'message' => $message,
                'header'  => $header,
                'link'    => $link,
            ],
        ]);
    }

    /**
     * @param string $method
     * @param array  $arguments
     *
     * @return mixed
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function __call($method, $arguments)
    {
        if (!$this->account) {
            $this->account = Account::create($this->getAccountData());
        }

        return $this->account->{$method}(...$arguments);
    }

    /**
     * @return array
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    protected function getAccountData()
    {
        return $this->getClient()->get('private/api/v2/json/accounts/current')['response']['account'];
    }
}