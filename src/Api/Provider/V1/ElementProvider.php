<?php

namespace Amocrm\Api\Provider\V1;

use Amocrm\Api\Helper\Utils;
use Amocrm\Api\Model\AbstractModelCollection;
use Amocrm\Api\Model\Contact;
use Amocrm\Api\Model\Customer;
use Amocrm\Api\Model\Link;
use Amocrm\Api\Model\Transaction;
use Amocrm\Api\Provider\AbstractProvider;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;
use InvalidArgumentException;

class ElementProvider extends AbstractProvider
{
    /**
     * Сохраняет разом контактов, покупателей и связи между ними и другими
     * сущностями.
     *
     * Внимание! Метод не разбивает запрос на чанки, а посылает всё разом. По
     * многу отправть нельзя. Не больше 200 сущностей в сумме за раз!
     *
     * @param array $customers    Покупатели под ключами add, update, delete.
     * @param array $transactions Транзакции под ключами add, delete.
     * @param array $links        Ссылки между сущностями link, unlink.
     * @param array $contacts     Контакты под ключами add, update.
     *
     * @return array Возвращаем массив с ключами customers, contacts, transactions, links,
     *               где под подключами add, update, delete, link, unlink соответствующие сущности.
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws InvalidArgumentException
     */
    public function save($customers, $transactions = null, $links = null, $contacts = null)
    {
        if (!isset($customers['add']) && !isset($customers['update']) && !isset($customers['delete'])) {
            throw new InvalidArgumentException('Во входном массиве Покупателей нужно использовать только ключи add, update и delete.');
        }

        if ($transactions && ((!isset($transactions['add']) && !isset($transactions['delete'])) || isset($transactions['update']))) {
            throw new InvalidArgumentException('Во входном массиве Транзакций нужно использовать только ключи add и delete.');
        }

        if ($contacts && ((!isset($contacts['add']) && !isset($contacts['update'])) || isset($contacts['delete']))) {
            throw new InvalidArgumentException('Во входном массиве Контактов нужно использовать только ключи add и update.');
        }

        if ($links && (!isset($links['link']) && isset($links['unlink']))) {
            throw new InvalidArgumentException('Во входном массиве Ссылок нужно использовать только ключи link и unlink.');
        }

        $result  = [];

        if (isset($customers['add'])) {
            $customers['add']           = $this->normalizeCollection($customers['add']);
            $result['customers']['add'] = [];
        }

        if (isset($customers['update'])) {
            $customers['update']           = $this->normalizeCollection($customers['update']);
            $result['customers']['update'] = [];
        }

        if (isset($customers['delete'])) {
            $customers['delete']           = $this->normalizeCollection($customers['delete']);
            $result['customers']['delete'] = [];
        }

        if (isset($transactions['add'])) {
            $transactions['add']                 = $this->normalizeCollection($transactions['add']);
            $transactions['transactions']['add'] = [];
        }

        if (isset($transactions['delete'])) {
            $transactions['delete']           = $this->normalizeCollection($transactions['delete']);
            $result['transactions']['delete'] = [];
        }

        if (isset($contacts['add'])) {
            $contacts['add']           = $this->normalizeCollection($contacts['add']);
            $result['contacts']['add'] = [];
        }

        if (isset($contacts['update'])) {
            $contacts['update']           = $this->normalizeCollection($contacts['update']);
            $result['contacts']['update'] = [];
        }

        if (isset($links['link'])) {
            $links['link']           = $this->normalizeCollection($links['link']);
            $result['links']['link'] = [];
        }

        if (isset($links['unlink'])) {
            $links['unlink']           = $this->normalizeCollection($links['unlink']);
            $result['links']['unlink'] = [];
        }

        $request = ['request' => []];

        foreach (['add', 'update', 'delete'] as $action) {
            if (!isset($customers[$action])) {
                continue;
            }
            // Для delete генерируем сущности если там ID, чтобы потом вернуть нормальные сущности.
            if ($action === 'delete') {
                foreach ($customers[$action] as &$id) {
                    if (!($id instanceof Customer)) {
                        $id = $this->getAmocrmApi()->customer()->create()->setId($id);
                    }
                }
            }
            unset($id);
            /**
             * @var Customer $entity
             */
            foreach ($customers[$action] as $entity) {
                if ($action === 'delete') {
                    $id                                      = $entity->getId();
                    $request['request']['customers'][$action][] = $id;
                    $result['customers'][$action][$id]          = $entity;
                } elseif ($action === 'add') {
                    // Надо помнить, что в Переодических покупках поле next_date будт обязательным.
                    $this->validateException($entity, ['name']);
                    // Добавляем каждому request_id, если у него его нет.
                    if ($entity->getRequestId() === null) {
                        $entity->setRequestId(uniqid('sync_customer_', true));
                    }

                    $request['request']['customers'][$action][]            = $entity->getModifiedForApi();
                    $result['customers'][$action][$entity->getRequestId()] = $entity;
                } else {
                    $request['request']['customers'][$action][] = $entity->getModifiedForApi();
                    $result[$action][$entity->getId()]          = $entity;
                }
            }
        }

        if ($transactions) {
            foreach (['add', 'delete'] as $action) {
                if (!isset($transactions[$action])) {
                    continue;
                }
                // Для delete генерируем сущности если там ID, чтобы потом вернуть нормальные сущности.
                if ($action === 'delete') {
                    foreach ($transactions[$action] as &$id) {
                        if (!($id instanceof Transaction)) {
                            $id = $this->getAmocrmApi()->transaction()->create()->setId($id);
                        }
                    }
                }
                /**
                 * @var Transaction $entity
                 */
                foreach ($transactions[$action] as $entity) {
                    if ($action === 'delete') {
                        $id                                         = $entity->getId();
                        $request['request']['transactions'][$action][] = $id;
                        $result['transactions'][$action][$id]          = $entity;
                    } else {
                        $this->validateException($entity, [['customer_id', 'customer_request_id', 'request_id'], 'price']);
                        // Добавляем каждому request_id, если у него его нет.
                        if ($entity->getRequestId() === null) {
                            $entity->setRequestId(uniqid('sync_transaction_', true));
                        }

                        $request['request']['transactions'][$action][]            = $entity->getModifiedForApi();
                        $result['transactions'][$action][$entity->getRequestId()] = $entity;
                    }
                }
            }
        }

        if ($contacts) {
            foreach (['add', 'update'] as $action) {
                if (!isset($contacts[$action])) {
                    continue;
                }
                /**
                 * @var Contact $entity
                 */
                foreach ($contacts[$action] as $entity) {
                    if ($action === 'update') {
                        $request['request']['contacts'][$action][]     = $entity->getModifiedForApi();
                        $result['contacts'][$action][$entity->getId()] = $entity;
                    } else {
                        $this->validateException($entity, ['name']);
                        // Добавляем каждому request_id, если у него его нет.
                        if ($entity->getRequestId() === null) {
                            $entity->setRequestId(uniqid('sync_contact_', true));
                        }

                        $request['request']['contacts'][$action][]            = $entity->getModifiedForApi();
                        $result['contacts'][$action][$entity->getRequestId()] = $entity;
                    }
                }
            }
        }

        if ($links) {
            foreach (['link', 'unlink'] as $action) {
                if (!isset($links[$action])) {
                    continue;
                }
                /**
                 * @var Link $link
                 */
                foreach ($links[$action] as $entity) {
                    // Добавляем каждому request_id, если у него его нет.
                    if ($entity->getRequestId() === null) {
                        $entity->setRequestId(uniqid('sync_link_', true));
                    }
                    $request['request']['links'][$action][] = $entity->getModifiedForApi();
                }
            }
        }

        $response = $this->getClient()->post('private/api/v2/json/elements/sync', $request);

        foreach (['customers', 'contacts', 'transactions', 'links'] as $entityKey) {
            foreach (['add', 'update', 'delete', 'link', 'unlink'] as $action) {
                if (isset($response['response'][$entityKey][$action]['errors'])
                    && count($response['response'][$entityKey][$action]['errors'])
                ) {
                    throw new DataIncorrectException(sprintf(
                        'Ошибка в одной из сущностей: %s',
                        json_encode($response['response'][$entityKey][$action]['errors'])
                    ));
                }
            }
        }

        foreach (['add', 'update'] as $action) {
            if (!isset($response['response']['contacts'][$action])
                || !count($response['response']['contacts'][$action])
            ) {
                continue;
            }

            foreach ($response['response']['contacts'][$action] as $item) {
                /**
                 * @var Contact $entity
                 */
                $entity = null;

                if ($action === 'add') {
                    if (isset($result['contacts'][$action][$item['request_id']])) {
                        $entity = &$result['contacts'][$action][$item['request_id']];
                    }
                } else {
                    if (isset($result['contacts'][$action][$item['id']])) {
                        $entity = &$result['contacts'][$action][$item['id']];
                    }
                }

                if ($entity) {
                    $entity = Contact::create(array_merge($entity->getModified(), $item));

                    unset($entity);
                }
            }
        }

        foreach (['add', 'delete'] as $action) {
            if (!isset($response['response']['transactions'][$action]['transactions'])
                || !count($response['response']['transactions'][$action]['transactions'])
            ) {
                continue;
            }

            foreach ($response['response']['transactions'][$action]['transactions'] as $item) {
                /**
                 * @var Transaction $entity
                 */
                $entity = null;

                if ($action === 'add') {
                    if (isset($result['transactions'][$action][$item['request_id']])) {
                        $entity = &$result['transactions'][$action][$item['request_id']];
                    }
                } else {
                    if (isset($result['transactions'][$action][$item['id']])) {
                        $entity = &$result['transactions'][$action][$item['id']];
                    }
                }

                if ($entity) {
                    $entity = Transaction::create(array_merge($entity->getModified(), $item));

                    unset($entity);
                }
            }
        }

        foreach (['add', 'update', 'delete'] as $action) {
            if (!isset($response['response']['customers'][$action]['customers'])
                || !count($response['response']['customers'][$action]['customers'])
            ) {
                continue;
            }

            foreach ($response['response']['customers'][$action]['customers'] as $item) {
                /**
                 * @var Customer $entity
                 */
                $entity = null;

                if ($action === 'add') {
                    if (isset($result['customers'][$action][$item['request_id']])) {
                        $entity = &$result['customers'][$action][$item['request_id']];
                    }
                } else {
                    if (isset($result['customers'][$action][$item['id']])) {
                        $entity = &$result['customers'][$action][$item['id']];
                    }
                }

                if ($entity) {
                    $entity = Customer::create(array_merge($entity->getModified(), $item));

                    unset($entity);
                }
            }
        }

        foreach (['link', 'unlink'] as $action) {
            if (!isset($response['response']['links'][$action]['links'])
                || !count($response['response']['links'][$action]['links'])
            ) {
                continue;
            }
            // C прерыдущим состоянием сущности не объединяем т.к. в ответ не приходят [from|to]_request_id,
            // а значит ассоциировать никак. В будущем можно замутить сложную ассоциацию через остальные сущности
            // т.к. там есть и request_id и id, так что при необходимости замутить можно.
            foreach ($response['response']['links'][$action]['links'] as $item) {
                $result['links'][$action][] = Link::create($item);
            }
        }
        
        foreach (['contacts', 'transactions', 'customers', 'links'] as $entityKey) {
            foreach (['add', 'update', 'delete', 'link', 'unlink'] as $action) {
                if (!isset($result[$entityKey][$action])) {
                    continue;
                }

                $classEntity = '\\Amocrm\\Api\\Model\\' . Utils::toCamelCase($entityKey);
                /**
                 * @var AbstractModelCollection $classEntity
                 */
                $result[$entityKey][$action] = $classEntity::create($result[$entityKey][$action]);
            }
        }

        return $result;
    }
}