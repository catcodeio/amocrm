<?php

namespace Amocrm\Api\Provider\V1;

use Amocrm\Api\Model\AbstractEntity;
use Amocrm\Api\Model\Lead;
use Amocrm\Api\Model\Leads;
use Amocrm\Api\Provider\AbstractProvider;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;
use Amocrm\Exception\UpdateTokenFailedException;
use DateTime;

class LeadProvider extends AbstractProvider
{
    /**
     * @return Lead
     */
    public function create(): Lead
    {
        return Lead::create();
    }

    /**
     * Сохраняет все сделки, и которые нужно добавить и которые обновить.
     *
     * Внимание! На выходе сделки могу перемешаться, порядок их может измениться.
     *
     * @param Leads|Lead[]|Lead $entities
     *
     * @return Leads|Lead
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function save($entities)
    {
        $entities      = $this->normalizeCollection($entities);
        $currentAdd    = [];
        $currentUpdate = [];
        $resultUpdate  = [];
        $resultAdd     = [];
        $result        = [];
        /** @var AbstractEntity $entity */
        foreach ($entities as $entity) {
            if ($entity->getId()) {
                $currentUpdate[] = $entity->getModifiedForApi();
                $resultUpdate[]  = $entity;
            } else {
                $currentAdd[] = $entity->getModifiedForApi();
                $resultAdd[]  = $entity;
            }

            if (count($currentAdd) + count($currentUpdate) >= 200) {
                $this->requestSave($currentAdd, $currentUpdate, $resultUpdate, $resultAdd, $result);
            }
        }

        if (count($currentAdd) || count($currentUpdate)) {
            $this->requestSave($currentAdd, $currentUpdate, $resultUpdate, $resultAdd, $result);
        }

        return Leads::create($result);
    }

    /**
     * Сохраняет все, но возвращает только первую сущность.
     *
     * @param Leads|Lead[]|Lead $entities
     *
     * @return Lead|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function saveOne($entities): ?Lead
    {
        $entities = $this->save($entities);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Гибкий поиск сделок с фильтрацией и без лимита по 500.
     *
     * @param string|null    $query
     * @param int|int[]|null $status
     * @param int|int[]|null $responsibleUserId
     * @param DateTime|null  $dateModified
     *
     * @return Leads|Lead[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function find(
        string $query = null,
        $status = null,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): Leads
    {
        // Максимально возможная выборка строк (ограничено amocrm), она же шаг.
        $limit    = 500;
        $end      = false;
        $offset   = 0;
        $entities = Leads::create();

        do {
            $chunk = $this->list(
                $query,
                $status,
                $responsibleUserId,
                $dateModified,
                $limit,
                $offset
            );

            if ($chunk->count() > 0) {
                $entities->merge($chunk);

                if ($chunk->count() < $limit) {
                    $end = true;
                } else {
                    $offset += $limit;
                }
            } else {
                $end = true;
            }
        } while (!$end);

        return $entities;
    }

    /**
     * Получение сделки или множества сделок по ID.
     *
     * @param int|int[] $ids
     *
     * @return Leads|Lead[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function get($ids): Leads
    {
        $ids = is_array($ids) ? $ids : [$ids];

        // Тут берётся ограничение длинны URL для GET-запроса. Обычно это ограничение сервера (nginx).
        // По умолчанию в nginx 8k (8192 символов), но берём немного меньше, исходим из того что на один
        // id уходит символов 15, и тогда 15 * 350 = 5250. Вроде помещаемся, будем бить их так.
        $limit    = 350;
        $entities = Leads::create();

        foreach (array_chunk($ids, $limit) as $id) {
            $leads = $this->list(
                null,
                null,
                null,
                null,
                null,
                null,
                $id
            );

            $entities->merge($leads);
        }

        return $entities;
    }

    /**
     * Получение одной первой сделки по ID.
     *
     * @param int|int[] $ids
     *
     * @return Lead|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function getOne($ids): ?Lead
    {
        $entities = $this->get($ids);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Получение сделок по контактам.
     *
     * @param int[]|int $id
     *
     * @return Leads
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findByContacts($id): Leads
    {
        $leadIds = $this->getAmocrmApi()->contact()->links($id, 'contact')->getIdsOfLeads();

        return $this->get($leadIds);
    }

    /**
     * Получение одной первой сделки по контактам.
     *
     * @param int[]|int $id
     *
     * @return Lead|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findOneByContacts($id): ?Lead
    {
        $entities = $this->findByContacts($id);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Получение сделок по значению обычного поля (например, текст).
     *
     * @param int            $fieldId
     * @param string         $query
     * @param int|int[]|null $status
     * @param int|null       $responsibleUserId
     * @param DateTime|null  $dateModified
     *
     * @return Leads
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findByField(
        int $fieldId,
        string $query,
        $status = null,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): Leads
    {
        return $this
            ->find($query, $status, $responsibleUserId, $dateModified)
            ->filter(function (Lead $lead) use ($fieldId, $query) {
                $cf = $lead->getCustomField($fieldId);

                if (!$cf) {
                    return false;
                }

                return $cf->text()->get() == $query;
            });
    }

    /**
     * Получение одной первой сделки по значению обычного поля (например, текст).
     *
     * @param int            $fieldId
     * @param string         $query
     * @param int|int[]|null $status
     * @param int|null       $responsibleUserId
     * @param DateTime|null  $dateModified
     *
     * @return Lead|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function findOneByField(
        int $fieldId,
        string $query,
        $status = null,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): ?Lead
    {
        $entities = $this->findByField($fieldId, $query, $status, $responsibleUserId, $dateModified);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Самый прямой и подробный метод запроса.
     *
     * @param string|null    $query
     * @param int|int[]|null $status
     * @param int|int[]|null $responsibleUserId
     * @param DateTime|null  $dateModified
     * @param int|null       $limitRows
     * @param int|null       $limitOffset
     * @param int|int[]|null $id
     *
     * @return Leads|Lead[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function list(
        string $query = null,
        $status = null,
        $responsibleUserId = null,
        DateTime $dateModified = null,
        int $limitRows = null,
        int $limitOffset = null,
        $id = null
    ): Leads
    {
        $headers = [];

        if ($dateModified) {
            $headers = ['if-modified-since: ' . $dateModified->format('D, d M Y H:i:s')];
        }

        $response = $this->getClient()->get('private/api/v2/json/leads/list', [
            'query'               => $query,
            'status'              => $status,
            'responsible_user_id' => $responsibleUserId,
            'limit_rows'          => $limitRows,
            'limit_offset'        => $limitOffset,
            'id'                  => $id,
        ], $headers);

        return Leads::create($this->getEmbeddedItems($response));
    }

    /**
     * Функция для отправки запроса с подготовкой т.к. она нужна в двух местах для разбиение большого запроса на чанки.
     *
     * @param array $currentAdd
     * @param array $currentUpdate
     * @param array $resultUpdate
     * @param array $resultAdd
     * @param array $result
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    protected function requestSave(&$currentAdd, &$currentUpdate, &$resultUpdate, &$resultAdd, &$result)
    {
        $request = ['request' => ['leads' => []]];

        if ($currentAdd) {
            $request['request']['leads']['add'] = $currentAdd;
            $currentAdd = [];
        }

        if ($currentUpdate) {
            $request['request']['leads']['update'] = $currentUpdate;
            $currentUpdate = [];
        }

        $response = $this->getClient()->post('private/api/v2/json/leads/set', $request);

        if (isset($response['response']['leads']['add']) && count($response['response']['leads']['add'])) {
            foreach ($response['response']['leads']['add'] as $key => $item) {
                // Если в ответ придёт не то кол-во сущностей, то мы конечно не упадём, но ID никому не проставим.
                if (isset($resultAdd[$key])) {
                    /** @var Lead[] $resultAdd */
                    $resultAdd[$key]->setId($item['id']);
                }
            }

            $result    = array_merge($result, $resultAdd);
            $resultAdd = [];
        }

        if (isset($response['response']['leads']['update']) && count($response['response']['leads']['update'])) {
            // Думается, что ID и так есть, а потому перебирать не будем.
            $result       = array_merge($result, $resultUpdate);
            $resultUpdate = [];
        }
    }

    /**
     * @param array|null $response
     *
     * @return array|null
     *
     * @throws AmocrmApiException
     */
    protected function getEmbeddedItems($response)
    {
        if (is_null($response)) {
            return null;
        }

        if (!isset($response['response']['leads'])) {
            $this->throwWrongResponse();
        }

        return $response['response']['leads'];
    }
}