<?php

namespace Amocrm\Api\Provider\V1;

use Amocrm\Api\Model\AbstractEntity;
use Amocrm\Api\Model\Tasks;
use Amocrm\Api\Model\Task;
use Amocrm\Api\Provider\AbstractProvider;
use Amocrm\Exception\AccountUnavailableException;
use Amocrm\Exception\AmocrmApiException;
use Amocrm\Exception\CustomFieldIncorrectException;
use Amocrm\Exception\DataIncorrectException;
use Amocrm\Exception\ManagerUnavailableException;
use Amocrm\Exception\UpdateTokenFailedException;
use DateTime;

class TaskProvider extends AbstractProvider
{
    /**
     * @return Task
     */
    public function create(): Task
    {
        return Task::create();
    }

    /**
     * Сохраняет все задачи, и которые нужно добавить и которые обновить.
     *
     * Внимание! На выходе задачи могу перемешаться, порядок их может измениться.
     *
     * @param Tasks|Task[]|Task $entities
     *
     * @return Tasks|Task[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function save($entities)
    {
        $entities      = $this->normalizeCollection($entities);
        $currentAdd    = [];
        $currentUpdate = [];
        $resultUpdate  = [];
        $resultAdd     = [];
        $result        = [];
        /** @var AbstractEntity $entity */
        foreach ($entities as $entity) {
            if ($entity->getId()) {
                $currentUpdate[] = $this->getEntityData($entity);
                $resultUpdate[]  = $entity;
            } else {
                $currentAdd[] = $this->getEntityData($entity);
                $resultAdd[]  = $entity;
            }

            if (count($currentAdd) + count($currentUpdate) >= 200) {
                $this->requestSave($currentAdd, $currentUpdate, $resultUpdate, $resultAdd, $result);
            }
        }

        if (count($currentAdd) || count($currentUpdate)) {
            $this->requestSave($currentAdd, $currentUpdate, $resultUpdate, $resultAdd, $result);
        }

        return Tasks::create($result);
    }

    /**
     * Сохраняет все, но возвращает только первую сущность.
     *
     * @param Tasks|Task[]|Task $entities
     *
     * @return Task|null
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function saveOne($entities): ?Task
    {
        $entities = $this->save($entities);

        if (!$entities->count()) {
            return null;
        }

        return $entities->first();
    }

    /**
     * Гибкий поиск задач с фильтрацией и без лимита по 500.
     *
     * @param string|null    $type
     * @param int|null       $elementId
     * @param int|int[]|null $responsibleUserId
     * @param DateTime|null  $dateModified
     *
     * @return Tasks|Task[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function find(
        string $type = null,
        int $elementId = null,
        $responsibleUserId = null,
        DateTime $dateModified = null
    ): Tasks
    {
        // Максимально возможная выборка строк (ограничено amocrm), она же шаг.
        $limit    = 500;
        $end      = false;
        $offset   = 0;
        $entities = Tasks::create();

        do {
            $chunk = $this->list(
                $type,
                $elementId,
                $responsibleUserId,
                $dateModified,
                $limit,
                $offset
            );

            if ($chunk->count() > 0) {
                $entities->merge($chunk);

                if ($chunk->count() < $limit) {
                    $end = true;
                } else {
                    $offset += $limit;
                }
            } else {
                $end = true;
            }
        } while (!$end);

        return $entities;
    }

    /**
     * Получение задачи или множества задач по ID.
     *
     * @param int|int[] $ids
     *
     * @return Tasks|Task[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function get($ids): Tasks
    {
        $ids = is_array($ids) ? $ids : [$ids];

        // Тут берётся ограничение длинны URL для GET-запроса. Обычно это ограничение сервера (nginx).
        // По умолчанию в nginx 8k (8192 символов), но берём немного меньше, исходим из того что на один
        // id уходит символов 15, и тогда 15 * 350 = 5250. Вроде помещаемся, будем бить их так.
        $limit    = 350;
        $entities = Tasks::create();

        foreach (array_chunk($ids, $limit) as $id) {
            $chunk = $this->list(
                null,
                null,
                null,
                null,
                null,
                null,
                $id
            );

            $entities->merge($chunk);
        }

        return $entities;
    }

    /**
     * Самый прямой и подробный метод запроса.
     *
     * @param string|null    $type Тип сущности, чей ID указан: contact, lead и company. Если пусто. то ищет везде.
     * @param int|null       $elementId
     * @param int|int[]|null $responsibleUserId
     * @param DateTime|null  $dateModified
     * @param int|null       $limitRows
     * @param int|null       $limitOffset
     * @param int|int[]|null $id
     *
     * @return Tasks|Task[]
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     */
    public function list(
        string $type = null,
        int $elementId = null,
        $responsibleUserId = null,
        DateTime $dateModified = null,
        int $limitRows = null,
        int $limitOffset = null,
        $id = null
    ): Tasks
    {
        $headers = [];

        if ($dateModified) {
            $headers = ['if-modified-since: ' . $dateModified->format('D, d M Y H:i:s')];
        }

        $response = $this->getClient()->get($this->getUrlList(), [
            'type'                => $type,
            'element_id'          => $elementId,
            'responsible_user_id' => $responsibleUserId,
            'limit_rows'          => $limitRows,
            'limit_offset'        => $limitOffset,
            'id'                  => $id,
        ], $headers);

        return Tasks::create($this->getEmbeddedItems($response));
    }

    /**
     * Функция для отправки запроса с подготовкой т.к. она нужна в двух местах для разбиение большого запроса на чанки.
     *
     * @param array $currentAdd
     * @param array $currentUpdate
     * @param array $resultUpdate
     * @param array $resultAdd
     * @param array $result
     *
     * @throws AccountUnavailableException
     * @throws AmocrmApiException
     * @throws CustomFieldIncorrectException
     * @throws DataIncorrectException
     * @throws ManagerUnavailableException
     * @throws UpdateTokenFailedException
     */
    protected function requestSave(&$currentAdd, &$currentUpdate, &$resultUpdate, &$resultAdd, &$result)
    {
        $request = ['request' => ['tasks' => []]];

        if ($currentAdd) {
            $request['request']['tasks']['add'] = $currentAdd;
            $currentAdd = [];
        }

        if ($currentUpdate) {
            $request['request']['tasks']['update'] = $currentUpdate;
            $currentUpdate = [];
        }

        $response = $this->getClient()->post('private/api/v2/json/tasks/set', $request);

        if (isset($response['response']['tasks']['add']) && count($response['response']['tasks']['add'])) {
            foreach ($response['response']['tasks']['add'] as $key => $item) {
                // Если в ответ придёт не то кол-во сущностей, то мы конечно не упадём, но ID никому не проставим.
                if (isset($resultAdd[$key])) {
                    /** @var Task[] $resultAdd */
                    $resultAdd[$key]->setId($item['id']);
                }
            }

            $result    = array_merge($result, $resultAdd);
            $resultAdd = [];
        }

        if (isset($response['response']['tasks']['update']) && count($response['response']['tasks']['update'])) {
            // Думается, что ID и так есть, а потому перебирать не будем.
            $result       = array_merge($result, $resultUpdate);
            $resultUpdate = [];
        }
    }

    /**
     * @return string
     */
    protected function getUrlList()
    {
        return 'private/api/v2/json/tasks/list';
    }

    /**
     * @param array|null $response
     *
     * @return array|null
     *
     * @throws AmocrmApiException
     */
    protected function getEmbeddedItems($response)
    {
        if (is_null($response)) {
            return null;
        }

        if (!isset($response['response']['tasks'])) {
            $this->throwWrongResponse();
        }

        return $response['response']['tasks'];
    }

    /**
     * @param AbstractEntity $entity
     *
     * @return array
     */
    protected function getEntityData(AbstractEntity $entity)
    {
        return $entity->getModifiedForApiV1();
    }
}