<?php

use Amocrm\Api\Client\RestClientToken;
use Amocrm\Api\Provider\V1\CustomerProvider;
use Amocrm\Api\Model\Customer;
use Amocrm\Api\Model\CustomField\CustomField;
use PHPUnit\Framework\TestCase;

/**
 * Проверяет отправляемые в API данные на соответствие документации.
 */
class CustomerProviderRequestTest extends TestCase
{
    /**
     * @var RestClientToken
     */
    private $client;

    /**
     * @var CustomerProvider
     */
    private $provider;

    public function setUp()
    {
        $this->client       = new class('domain', 'login', 'token') extends RestClientToken
        {
            public $mockEndpoint;
            public $mockMethod;
            public $mockParams;
            public $mockHeaders;

            protected function requestCurl(string $endpoint, string $method = self::METHOD_GET, array $params = [], array $headers = [])
            {
                $this->mockEndpoint = $endpoint;
                $this->mockMethod   = $method;
                $this->mockParams   = $params;
                $this->mockHeaders  = $headers;

                return ['response' => ['customers' => []]];
            }
        };
        $this->provider = new CustomerProvider($this->client);
    }

    public function testCreate()
    {
        $this->assertInstanceOf(Customer::class, $this->provider->create());
    }

    public function testList()
    {
        $this->provider->list();

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/customers/list?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals('GET', $this->client->mockMethod);
        $this->assertEquals([], $this->client->mockParams);
        $this->assertEquals(['Content-Type: application/json'], $this->client->mockHeaders);
    }

    public function testListFull()
    {
        $dateFrom = new DateTime('01.02.2018');
        $dateTo = new DateTime('01.03.2018');
        $dateNextFrom = new DateTime('01.04.2018');
        $dateNextTo = new DateTime('01.05.2018');
        $cfs = [
            CustomField::create(['id' => 222])->email()->add('asdf@asdf.ff'),
            CustomField::create(['id' => 221])->text()->set('asdf'),
            CustomField::create(['id' => 223])->multiselect()->add('option'),
            CustomField::create(['id' => 224])->checkbox()->set(true),
        ];

        $cfsDate = [
            [
                'from' => CustomField::create(['id' => 220])->date()->set(new DateTime('01.01.2018')),
                'to' => CustomField::create(['id' => 220])->date()->set(new DateTime('14.12.2018')),
            ]
        ];

        $this->provider->list('create', $dateFrom, $dateTo, $dateNextFrom, $dateNextTo, $cfs, $cfsDate, 1234, 'no_tasks', 1, 2, 4);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/customers/list?USER_LOGIN=login&USER_HASH=token&filter%5Bdate%5D%5Btype%5D=create&filter%5Bdate%5D%5Bfrom%5D=01%2F02%2F2018&filter%5Bdate%5D%5Bto%5D=01%2F03%2F2018&filter%5Bnext_date%5D%5Bfrom%5D=01%2F04%2F2018&filter%5Bnext_date%5D%5Bto%5D=01%2F05%2F2018&filter%5Bcf%5D%5B222%5D=asdf%40asdf.ff&filter%5Bcf%5D%5B221%5D=asdf&filter%5Bcf%5D%5B223%5D%5B0%5D=option&filter%5Bcf%5D%5B224%5D=Y&filter%5Bcf%5D%5B220%5D%5Bfrom%5D=01%2F01%2F2018&filter%5Bcf%5D%5B220%5D%5Bto%5D=14%2F12%2F2018&filter%5Bmain_user%5D=1234&filter%5Btasks%5D%5B0%5D=no_tasks&filter%5Bid%5D%5B0%5D=4&limit_offset=2&limit_rows=1'
        );
        $this->assertEquals('GET', $this->client->mockMethod);
        $this->assertEquals([], $this->client->mockParams);
        $this->assertEquals([
            'Content-Type: application/json',
        ], $this->client->mockHeaders);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testExceptionSave()
    {
        $this->provider->save([]);
    }

    public function badListParams()
    {
        return [
            [[['asdf']]],
            [[['create']]],
            [[[null, null, null, null, null, null, null, null, 'asdf']]],
        ];
    }

    /**
     * @dataProvider badListParams
     * @expectedException \InvalidArgumentException
     *
     * @param $params
     */
    public function testExceptionList($params)
    {
        call_user_func_array([$this->provider, 'save'], $params);
    }

    public function testSetAdd()
    {
        $nextDate = new DateTime('20.01.2018');
        $customer = Customer::create()
            ->setNextPrice(12)
            ->setNextDate($nextDate)
            ->setName('Название')
            ->setCF(CustomField::create(['id' => 1234])->text()->set('asdf'));

        $this->provider->save(['add' => $customer]);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/customers/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['customers']['add'][0]));

        $data = $this->client->mockParams['request']['customers']['add'][0];

        $this->assertTrue(isset($data['next_price']));
        $this->assertEquals(12, $data['next_price']);
        $this->assertTrue(isset($data['name']));
        $this->assertEquals('Название', $data['name']);
        $this->assertTrue(isset($data['next_date']));
        $this->assertEquals($nextDate->format('U'), $data['next_date']);

        $this->assertEquals([
            [
                'id' => 1234,
                'values' => [['value' => 'asdf']],
            ],
        ], $data['custom_fields']);
    }

    public function testSetAddOne()
    {
        $nextDate = new DateTime('20.01.2018');
        $customer = Customer::create()
            ->setNextPrice(12)
            ->setNextDate($nextDate)
            ->setName('Название')
            ->setCF(CustomField::create(['id' => 1234])->text()->set('asdf'));

        $this->provider->addOne($customer);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/customers/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['customers']['add'][0]));

        $data = $this->client->mockParams['request']['customers']['add'][0];

        $this->assertTrue(isset($data['next_price']));
        $this->assertEquals(12, $data['next_price']);
        $this->assertTrue(isset($data['name']));
        $this->assertEquals('Название', $data['name']);
        $this->assertTrue(isset($data['next_date']));
        $this->assertEquals($nextDate->format('U'), $data['next_date']);

        $this->assertEquals([
            [
                'id' => 1234,
                'values' => [['value' => 'asdf']],
            ],
        ], $data['custom_fields']);
    }

    public function testSetUpdate()
    {
        $nextDate = new DateTime('20.01.2018');
        $customer = Customer::create([
            'id'    => 12,
            'name'  => 'Название',
            'next_price' => 100,
            'next_date' => 455555,
            'custom_fields' => [
                [
                    'id' => 1234,
                    'values' => [
                        [
                            'value' => 'zxcv'
                        ]
                    ],
                ],
                [
                    'id' => 2345,
                    'values' => [
                        [
                            'value' => 'xcvb'
                        ]
                    ],
                ],
            ],
        ]);
        $customer->setNextPrice(10);
        $customer->setNextDate($nextDate);
        $customer->getCustomField(1234)->text()->set('asdf');

        $this->provider->save(['update' => $customer]);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/customers/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['customers']['update'][0]));

        $data = $this->client->mockParams['request']['customers']['update'][0];

        $this->assertTrue(isset($data['id']));
        $this->assertTrue(isset($data['next_price']));
        $this->assertEquals(10, $data['next_price']);
        $this->assertFalse(isset($data['name']));
        $this->assertTrue(isset($data['next_date']));
        $this->assertEquals($nextDate->format('U'), $data['next_date']);

        $this->assertEquals([
            [
                'id' => 1234,
                'values' => [['value' => 'asdf']],
            ],
        ], $data['custom_fields']);
    }

    public function testSetUpdateOne()
    {
        $nextDate = new DateTime('20.01.2018');
        $customer = Customer::create([
            'id'    => 12,
            'name'  => 'Название',
            'next_price' => 100,
            'next_date' => 455555,
            'custom_fields' => [
                [
                    'id' => 1234,
                    'values' => [
                        [
                            'value' => 'zxcv'
                        ]
                    ],
                ],
                [
                    'id' => 2345,
                    'values' => [
                        [
                            'value' => 'xcvb'
                        ]
                    ],
                ],
            ],
        ]);
        $customer->setNextPrice(10);
        $customer->setNextDate($nextDate);
        $customer->getCustomField(1234)->text()->set('asdf');

        $this->provider->updateOne([$customer]);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/customers/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['customers']['update'][0]));

        $data = $this->client->mockParams['request']['customers']['update'][0];

        $this->assertTrue(isset($data['id']));
        $this->assertTrue(isset($data['next_price']));
        $this->assertEquals(10, $data['next_price']);
        $this->assertFalse(isset($data['name']));
        $this->assertTrue(isset($data['next_date']));
        $this->assertEquals($nextDate->format('U'), $data['next_date']);

        $this->assertEquals([
            [
                'id' => 1234,
                'values' => [['value' => 'asdf']],
            ],
        ], $data['custom_fields']);
    }

    public function testSetDelete()
    {
        $customer = Customer::create([
            'id'    => 12,
            'name'  => 'Название',
        ]);

        $this->provider->save(['delete' => [$customer]]);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/customers/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['customers']['delete']));

        $data = $this->client->mockParams['request']['customers']['delete'];

        $this->assertEquals(1, count($data));
        $this->assertEquals(12, $data[0]);
    }

    public function testSetDeleteOne()
    {
        $customer = Customer::create([
            'id'    => 12,
            'name'  => 'Название',
        ]);

        $this->provider->deleteOne($customer);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/customers/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['customers']['delete']));

        $data = $this->client->mockParams['request']['customers']['delete'];

        $this->assertEquals(1, count($data));
        $this->assertEquals(12, $data[0]);
    }

    public function testSetDeleteId()
    {
        $this->provider->save(['delete' => [123, 6453, 434]]);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/customers/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['customers']['delete']));

        $data = $this->client->mockParams['request']['customers']['delete'];

        $this->assertEquals(3, count($data));
        $this->assertEquals(123, $data[0]);
        $this->assertEquals(6453, $data[1]);
        $this->assertEquals(434, $data[2]);
    }
}