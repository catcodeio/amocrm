<?php

use Amocrm\Api\Client\RestClientToken;
use Amocrm\Api\Provider\V1\ContactProvider;
use Amocrm\Api\Model\Contact;
use Amocrm\Api\Model\CustomField\CustomField;
use PHPUnit\Framework\TestCase;

/**
 * Проверяет отправляемые в API данные на соответствие документации.
 */
class ContactProviderRequestTest extends TestCase
{
    /**
     * @var RestClientToken
     */
    private $client;

    /**
     * @var ContactProvider
     */
    private $provider;

    public function setUp()
    {
        $this->client       = new class('domain', 'login', 'token') extends RestClientToken
        {
            public $mockEndpoint;
            public $mockMethod;
            public $mockParams;
            public $mockHeaders;

            protected function requestCurl(string $endpoint, string $method = self::METHOD_GET, array $params = [], array $headers = [])
            {
                $this->mockEndpoint = $endpoint;
                $this->mockMethod   = $method;
                $this->mockParams   = $params;
                $this->mockHeaders  = $headers;

                return ['response' => ['contacts' => []]];
            }
        };
        $this->provider = new ContactProvider($this->client);
    }

    public function testCreate()
    {
        $this->assertInstanceOf(Contact::class, $this->provider->create());
    }

    public function testList()
    {
        $this->provider->list();

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/contacts/list?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals('GET', $this->client->mockMethod);
        $this->assertEquals([], $this->client->mockParams);
        $this->assertEquals(['Content-Type: application/json'], $this->client->mockHeaders);
    }

    public function testListFull()
    {
        $ifModifiedSince = new DateTime('now');

        $this->provider->list('query', 2345, $ifModifiedSince, 1, 2, 3456, 'all');

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/contacts/list?USER_LOGIN=login&USER_HASH=token&query=query&responsible_user_id=2345&limit_rows=1&limit_offset=2&id=3456&type=all'
        );
        $this->assertEquals('GET', $this->client->mockMethod);
        $this->assertEquals([], $this->client->mockParams);
        $this->assertEquals([
            'Content-Type: application/json',
            'if-modified-since: ' . $ifModifiedSince->format('D, d M Y H:i:s'),
        ], $this->client->mockHeaders);
    }

    public function testSetAdd()
    {
        $entity = Contact::create();
        $entity->setName('Название');
        $entity->setCustomField(CustomField::create(['id' => 1234])->phone()->add('79009009090'));

        $this->provider->save($entity);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/contacts/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['contacts']['add'][0]));

        $data = $this->client->mockParams['request']['contacts']['add'][0];

        $this->assertTrue(isset($data['last_modified']));
        $this->assertTrue(isset($data['name']));
        $this->assertEquals('Название', $data['name']);

        $this->assertEquals([
            [
                'id' => 1234,
                'values' => [
                    [
                        'value' => '79009009090',
                        'enum'  => 'MOB',
                    ]
                ],
            ],
        ], $data['custom_fields']);
    }

    public function testSetUpdate()
    {
        $entity = Contact::create([
            'id'            => 12,
            'name'          => 'Название',
            'custom_fields' => [
                [
                    'id'     => 1234,
                    'values' => [
                        [
                            'value' => '79009009090',
                            'enum'  => '43244',
                        ],
                    ],
                ],
                [
                    'id'     => 2345,
                    'values' => [
                        [
                            'value' => 'xcvb',
                        ],
                    ],
                ],
            ],
        ]);
        $entity->getCustomField(1234)->phone()->add('5550022');

        $this->provider->save($entity);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/contacts/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['contacts']['update'][0]));

        $data = $this->client->mockParams['request']['contacts']['update'][0];

        $this->assertTrue(isset($data['id']));
        $this->assertTrue(isset($data['last_modified']));
        $this->assertFalse(isset($data['name']));

        $this->assertEquals([
            [
                'id' => 1234,
                'values' => [
                    [
                        'value' => '79009009090',
                        'enum'  => '43244',
                    ],
                    [
                        'value' => '5550022',
                        'enum'  => '43244',
                    ]
                ],
            ],
        ], $data['custom_fields']);
    }
}