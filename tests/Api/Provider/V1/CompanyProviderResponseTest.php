<?php

use Amocrm\Api\Client\RestClientInterface;
use Amocrm\Api\Model\Company;
use Amocrm\Api\Model\Companies;
use Amocrm\Api\Provider\V1\CompanyProvider;
use PHPUnit\Framework\TestCase;

class CompanyProviderResponseTest extends TestCase
{
    /**
     * @var CompanyProvider
     */
    private $provider;

    /**
     * @var CompanyProvider
     */
    private $providerEmpty;

    public function setUp()
    {
        $this->provider = new CompanyProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'contacts'     => [
                        [
                            'id'   => 5207409,
                            'name' => 'new name2',
                        ],
                        [
                            'id'   => 2986625,
                            'name' => 'new name',
                        ],
                    ],
                    'server_time' => 1523111710,
                ]];
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'contacts' => [
                        'add' => [
                            [
                                'id'         => 3655494,
                                'request_id' => 0,
                            ],
                        ],
                    ],
                ]];
            }
        });

        $this->providerEmpty = new CompanyProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return null;
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'contacts' => [
                        'add' => [],
                    ]],
                ];
            }
        });
    }

    public function testGet()
    {
        $entities = $this->provider->get(5207409);
        $this->assertInstanceOf(Companies::class, $entities);
        $this->assertInstanceOf(Company::class, $entities->first());

        $this->assertInstanceOf(Companies::class, $this->provider->get([5207409, 2986625]));

        $entities = $this->providerEmpty->get(5207409);
        $this->assertInstanceOf(Companies::class, $entities);
        $this->assertEquals(0, $entities->count());

        $entities = $this->providerEmpty->get([5207409, 2986625]);
        $this->assertInstanceOf(Companies::class, $entities);
        $this->assertEquals(0, $entities->count());
    }

    public function testGetOne()
    {
        $this->assertInstanceOf(Company::class, $this->provider->getOne(5207409));
        $this->assertInstanceOf(Company::class, $this->provider->getOne([5207409, 2986625]));

        $this->assertNull($this->providerEmpty->getOne(5207409));
        $this->assertNull($this->providerEmpty->getOne([5207409, 2986625]));
    }

    public function testFind()
    {
        $this->assertInstanceOf(Companies::class, $this->provider->find('asdf', null, new DateTime()));
        $this->assertInstanceOf(Companies::class, $this->providerEmpty->find('asdf', null, new DateTime()));
    }

    public function testFindEmpty()
    {
        $this->assertInstanceOf(Companies::class, $this->provider->find());
        $this->assertInstanceOf(Companies::class, $this->providerEmpty->find());
    }

    public function testList()
    {
        $this->assertInstanceOf(Companies::class, $this->provider->list('asdf', null, new DateTime(), null, null, null));
        $this->assertInstanceOf(Companies::class, $this->providerEmpty->list('list', null, new DateTime(), null, null, null));
    }

    public function testSave()
    {
        $leadSaved = $this->provider->save([Company::create()]);
        $this->assertInstanceOf(Companies::class, $leadSaved);
        $this->assertEquals(1, $leadSaved->count());

        $this->assertInstanceOf(Companies::class, $this->provider->save(Company::create()));

        $this->assertEquals($this->providerEmpty->save([Company::create()])->count(), 0);
        $this->assertEquals($this->providerEmpty->save(Company::create())->count(), 0);
    }

    public function testSaveOne()
    {
        $this->assertInstanceOf(Company::class, $this->provider->saveOne([Company::create()]));
        $this->assertInstanceOf(Company::class, $this->provider->saveOne(Company::create()));

        $this->assertNull($this->providerEmpty->saveOne([Company::create()]));
        $this->assertNull($this->providerEmpty->saveOne(Company::create()));
    }
}