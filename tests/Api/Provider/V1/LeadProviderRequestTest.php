<?php

use Amocrm\Api\Client\RestClientToken;
use Amocrm\Api\Provider\V1\LeadProvider;
use Amocrm\Api\Model\Lead;
use Amocrm\Api\Model\CustomField\CustomField;
use PHPUnit\Framework\TestCase;

/**
 * Проверяет отправляемые в API данные на соответствие документации.
 */
class LeadProviderRequestTest extends TestCase
{
    /**
     * @var RestClientToken
     */
    private $client;

    /**
     * @var LeadProvider
     */
    private $provider;

    public function setUp()
    {
        $this->client       = new class('domain', 'login', 'token') extends RestClientToken
        {
            public $mockEndpoint;
            public $mockMethod;
            public $mockParams;
            public $mockHeaders;

            protected function requestCurl(string $endpoint, string $method = self::METHOD_GET, array $params = [], array $headers = [])
            {
                $this->mockEndpoint = $endpoint;
                $this->mockMethod   = $method;
                $this->mockParams   = $params;
                $this->mockHeaders  = $headers;

                return ['response' => ['leads' => []]];
            }
        };
        $this->provider = new LeadProvider($this->client);
    }

    public function testCreate()
    {
        $this->assertInstanceOf(Lead::class, $this->provider->create());
    }

    public function testList()
    {
        $this->provider->list();

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/leads/list?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals('GET', $this->client->mockMethod);
        $this->assertEquals([], $this->client->mockParams);
        $this->assertEquals(['Content-Type: application/json'], $this->client->mockHeaders);
    }

    public function testListFull()
    {
        $ifModifiedSince = new DateTime('now');

        $this->provider->list('query', 1234, 2345, $ifModifiedSince, 1, 2, 3456);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/leads/list?USER_LOGIN=login&USER_HASH=token&query=query&status=1234&responsible_user_id=2345&limit_rows=1&limit_offset=2&id=3456'
        );
        $this->assertEquals('GET', $this->client->mockMethod);
        $this->assertEquals([], $this->client->mockParams);
        $this->assertEquals([
            'Content-Type: application/json',
            'if-modified-since: ' . $ifModifiedSince->format('D, d M Y H:i:s'),
        ], $this->client->mockHeaders);
    }

    public function testSetAdd()
    {
        $lead = Lead::create();
        $lead->setPrice(12);
        $lead->setName('Название сделки');
        $lead->setCustomField(CustomField::create(['id' => 1234])->text()->set('asdf'));

        $this->provider->save($lead);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/leads/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['leads']['add'][0]));

        $data = $this->client->mockParams['request']['leads']['add'][0];

        $this->assertTrue(isset($data['last_modified']));
        $this->assertTrue(isset($data['price']));
        $this->assertEquals(12, $data['price']);
        $this->assertTrue(isset($data['name']));
        $this->assertEquals('Название сделки', $data['name']);

        $this->assertEquals([
            [
                'id' => 1234,
                'values' => [['value' => 'asdf']],
            ],
        ], $data['custom_fields']);
    }

    public function testSetUpdate()
    {
        $lead = Lead::create([
            'id'    => 12,
            'name'  => 'Название сделки',
            'price' => 100,
            'custom_fields' => [
                [
                    'id' => 1234,
                    'values' => [
                        [
                            'value' => 'zxcv'
                        ]
                    ],
                ],
                [
                    'id' => 2345,
                    'values' => [
                        [
                            'value' => 'xcvb'
                        ]
                    ],
                ],
            ]
        ]);
        $lead->setPrice(10);
        $lead->getCustomField(1234)->text()->set('asdf');

        $this->provider->save($lead);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/leads/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['leads']['update'][0]));

        $data = $this->client->mockParams['request']['leads']['update'][0];

        $this->assertTrue(isset($data['id']));
        $this->assertTrue(isset($data['last_modified']));
        $this->assertTrue(isset($data['price']));
        $this->assertEquals(10, $data['price']);
        $this->assertFalse(isset($data['name']));

        $this->assertEquals([
            [
                'id' => 1234,
                'values' => [['value' => 'asdf']],
            ],
        ], $data['custom_fields']);
    }
}