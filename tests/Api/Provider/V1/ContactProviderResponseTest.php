<?php

use Amocrm\Api\Model\Contact;
use Amocrm\Api\Model\Contacts;
use Amocrm\Api\Provider\V1\ContactProvider;
use PHPUnit\Framework\TestCase;

class ContactProviderResponseTest extends TestCase
{
    /**
     * @var ContactProvider
     */
    private $provider;

    /**
     * @var ContactProvider
     */
    private $providerEmpty;

    public function setUp()
    {
        $this->provider = new ContactProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'contacts'     => [
                        [
                            'id'                  => 5207409,
                            'name'                => 'new name2',
                            'last_modified'       => 1523314701,
                            'account_id'          => 19040293,
                            'date_create'         => 1523314701,
                            'created_user_id'     => 2291701,
                            'modified_user_id'    => 2291701,
                            'responsible_user_id' => 2291701,
                            'group_id'            => 0,
                            'closest_task'        => 0,
                            'linked_company_id'   => NULL,
                            'company_name'        => '',
                            'tags'                =>
                                [
                                ],
                            'type'                => 'contact',
                            'custom_fields'       =>
                                [
                                ],
                            'linked_leads_id'     =>
                                [
                                ],
                        ],
                        [
                            'id'                  => 2986625,
                            'name'                => 'new name',
                            'last_modified'       => 1523538510,
                            'account_id'          => 19040293,
                            'date_create'         => 1522415760,
                            'created_user_id'     => 2291701,
                            'modified_user_id'    => 2291701,
                            'responsible_user_id' => 2291701,
                            'group_id'            => 0,
                            'closest_task'        => 0,
                            'linked_company_id'   => '6251953',
                            'company_name'        => 'Новая компания',
                            'tags'                =>
                                [
                                    0 =>
                                        [
                                            'id'           => 75649,
                                            'name'         => 'два слова',
                                            'element_type' => 1,
                                        ],
                                    1 =>
                                        [
                                            'id'           => 75651,
                                            'name'         => 'одно',
                                            'element_type' => 1,
                                        ],
                                    2 =>
                                        [
                                            'id'           => 78905,
                                            'name'         => 'привет5',
                                            'element_type' => 1,
                                        ],
                                ],
                            'type'                => 'contact',
                            'custom_fields'       =>
                                [
                                    0 =>
                                        [
                                            'id'     => '123905',
                                            'name'   => 'Телефон',
                                            'code'   => 'PHONE',
                                            'values' =>
                                                [
                                                    0 =>
                                                        [
                                                            'value' => '79009090911',
                                                            'enum'  => '256775',
                                                        ],
                                                    1 =>
                                                        [
                                                            'value' => '11111233333',
                                                            'enum'  => '256775',
                                                        ],
                                                    2 =>
                                                        [
                                                            'value' => '54365434655',
                                                            'enum'  => '256779',
                                                        ],
                                                ],
                                        ],
                                    1 =>
                                        [
                                            'id'     => '123907',
                                            'name'   => 'Email',
                                            'code'   => 'EMAIL',
                                            'values' =>
                                                [
                                                    0 =>
                                                        [
                                                            'value' => 'test@mail.ru',
                                                            'enum'  => '256787',
                                                        ],
                                                ],
                                        ],
                                    2 =>
                                        [
                                            'id'     => '123911',
                                            'name'   => 'Мгн. сообщения',
                                            'code'   => 'IM',
                                            'values' =>
                                                [
                                                    0 =>
                                                        [
                                                            'value' => 'asdf',
                                                            'enum'  => '256793',
                                                        ],
                                                    1 =>
                                                        [
                                                            'value' => 'qqqqq',
                                                            'enum'  => '256801',
                                                        ],
                                                ],
                                        ],
                                    3 =>
                                        [
                                            'id'     => '252239',
                                            'name'   => 'Juridical person',
                                            'values' =>
                                                [
                                                    0 =>
                                                        [
                                                            'value' =>
                                                                [
                                                                    'name'                         => 'asdf',
                                                                    'entity_type'                  => NULL,
                                                                    'vat_id'                       => '123123',
                                                                    'tax_registration_reason_code' => NULL,
                                                                    'address'                      => NULL,
                                                                    'kpp'                          => '1123',
                                                                    'external_uid'                 => NULL,
                                                                ],
                                                        ],
                                                ],
                                        ],
                                ],
                            'linked_leads_id'     =>
                                [
                                    0 => '5207409',
                                ],
                        ],
                    ],
                    'server_time' => 1523111710,
                ]];
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'contacts' => [
                        'add' => [
                            [
                                'id'         => 3655494,
                                'request_id' => 0,
                            ],
                        ],
                    ],
                ]];
            }
        });

        $this->providerEmpty = new ContactProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return null;
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'contacts' => [
                        'add' => [],
                    ],
                ]];
            }
        });
    }

    public function testGet()
    {
        $entities = $this->provider->get(5207409);
        $this->assertInstanceOf(Contacts::class, $entities);
        $this->assertInstanceOf(Contact::class, $entities->first());

        $this->assertInstanceOf(Contacts::class, $this->provider->get([5207409, 2986625]));

        $entities = $this->providerEmpty->get(5207409);
        $this->assertInstanceOf(Contacts::class, $entities);
        $this->assertEquals(0, $entities->count());

        $entities = $this->providerEmpty->get([5207409, 2986625]);
        $this->assertInstanceOf(Contacts::class, $entities);
        $this->assertEquals(0, $entities->count());
    }

    public function testGetOne()
    {
        $this->assertInstanceOf(Contact::class, $this->provider->getOne(5207409));
        $this->assertInstanceOf(Contact::class, $this->provider->getOne([5207409, 2986625]));

        $this->assertNull($this->providerEmpty->getOne(5207409));
        $this->assertNull($this->providerEmpty->getOne([5207409, 2986625]));
    }

    public function testFind()
    {
        $this->assertInstanceOf(Contacts::class, $this->provider->find('asdf', null, new DateTime()));
        $this->assertInstanceOf(Contacts::class, $this->providerEmpty->find('asdf', null, new DateTime()));
    }

    public function testFindEmpty()
    {
        $this->assertInstanceOf(Contacts::class, $this->provider->find());
        $this->assertInstanceOf(Contacts::class, $this->providerEmpty->find());
    }

    public function testList()
    {
        $this->assertInstanceOf(Contacts::class, $this->provider->list('asdf', null, new DateTime(), null, null, null, null));
        $this->assertInstanceOf(Contacts::class, $this->providerEmpty->list('list', null, new DateTime(), null, null, null, null));
    }

    public function testSave()
    {
        $leadSaved = $this->provider->save([Contact::create()]);
        $this->assertInstanceOf(Contacts::class, $leadSaved);
        $this->assertEquals(1, $leadSaved->count());

        $this->assertInstanceOf(Contacts::class, $this->provider->save(Contact::create()));

        $this->assertEquals($this->providerEmpty->save([Contact::create()])->count(), 0);
        $this->assertEquals($this->providerEmpty->save(Contact::create())->count(), 0);
    }

    public function testSaveOne()
    {
        $this->assertInstanceOf(Contact::class, $this->provider->saveOne([Contact::create()]));
        $this->assertInstanceOf(Contact::class, $this->provider->saveOne(Contact::create()));

        $this->assertNull($this->providerEmpty->saveOne([Contact::create()]));
        $this->assertNull($this->providerEmpty->saveOne(Contact::create()));
    }
}