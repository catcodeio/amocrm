<?php

use Amocrm\Api\Client\RestClientInterface;
use Amocrm\Api\Model\Transaction;
use Amocrm\Api\Model\Transactions;
use Amocrm\Api\Provider\V1\TransactionProvider;
use PHPUnit\Framework\TestCase;

class TransactionProviderResponseTest extends TestCase
{
    /**
     * @var TransactionProvider
     */
    private $provider;

    /**
     * @var TransactionProvider
     */
    private $providerEmpty;

    /**
     * @var TransactionProvider
     */
    private $providerError;

    public function setUp()
    {
        $this->provider = new TransactionProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'transactions'     => [
                        "23869" => [
                            "id"          => 23869,
                            "date_create" => 1535332963,
                            "date_modify" => 1535332963,
                            "created_by"  => 2291701,
                            "modified_by" => 2291701,
                            "account_id"  => 19040293,
                            "customer_id" => 114555,
                            "deleted"     => false,
                            "price"       => 111,
                            "comment"     => "asdf",
                            "date"        => 1535332920,
                        ],
                        "23871" => [
                            "id"          => 23871,
                            "date_create" => 1535333134,
                            "date_modify" => 1535333134,
                            "created_by"  => 2291701,
                            "modified_by" => 2291701,
                            "account_id"  => 19040293,
                            "customer_id" => 114555,
                            "deleted"     => false,
                            "price"       => 0,
                            "comment"     => null,
                            "date"        => 1535333100,
                        ],
                    ],
                    'server_time' => 1523111710,
                ]];
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    "transactions" => [
                        "add" => [
                            "transactions" => [
                                [
                                    "id"          => 23949,
                                    "date_create" => 1535373036,
                                    "date_modify" => 1535373036,
                                    "created_by"  => 2291701,
                                    "modified_by" => 2291701,
                                    "account_id"  => 19040293,
                                    "customer_id" => 114555,
                                    "deleted"     => false,
                                    "price"       => 3500,
                                    "comment"     => "example",
                                    "date"        => 1483909200,
                                    "request_id"  => 0,
                                ]
                            ],
                            "errors" => [],
                        ]
                    ]
                ]];
            }
        });

        $this->providerEmpty = new TransactionProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return null;
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'transactions' => [
                        'add' => [
                        ],
                    ],
                ]];
            }
        });

        $this->providerError = new TransactionProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return null;
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'transactions' => [
                        'add' => [
                            'transactions' => [],
                            "errors" => [
                                [
                                    "id"         => 0,
                                    "code"       => 400,
                                    "message"    => "Price can't be empty",
                                    "request_id" => 0,
                                ]
                            ]
                        ],
                    ],
                ]];
            }
        });
    }

    public function testGet()
    {
        $entities = $this->provider->get(23869);
        $this->assertInstanceOf(Transactions::class, $entities);
        $this->assertInstanceOf(Transaction::class, $entities->first());

        $this->assertInstanceOf(Transactions::class, $this->provider->get([23869, 23871]));

        $entities = $this->providerEmpty->get(23869);
        $this->assertInstanceOf(Transactions::class, $entities);
        $this->assertEquals(0, $entities->count());

        $entities = $this->providerEmpty->get([23869, 23871]);
        $this->assertInstanceOf(Transactions::class, $entities);
        $this->assertEquals(0, $entities->count());
    }

    public function testGetOne()
    {
        $this->assertInstanceOf(Transaction::class, $this->provider->getOne(00001));
        $this->assertInstanceOf(Transaction::class, $this->provider->getOne([00001, 000002]));

        $this->assertNull($this->providerEmpty->getOne(000002));
        $this->assertNull($this->providerEmpty->getOne([000001, 000002]));
    }

    public function testFind()
    {
        $this->assertInstanceOf(Transactions::class, $this->provider->find(1));
        $this->assertInstanceOf(Transactions::class, $this->providerEmpty->find(1));
    }

    public function testList()
    {
        $this->assertInstanceOf(Transactions::class, $this->provider->list(1, 1, 2, 3));
        $this->assertInstanceOf(Transactions::class, $this->providerEmpty->list(1, 1, 2, 3));
    }

    public function testSave()
    {
        $transaction = Transaction::create()
            ->setCustomerId(11)
            ->setPrice(12);

        $transactionSaved = $this->provider->save(['add' => $transaction]);
        $this->assertTrue(is_array($transactionSaved));
        $this->assertInstanceOf(Transactions::class, $transactionSaved['add']);
        $this->assertEquals(1, $transactionSaved['add']->count());

        $this->assertInstanceOf(Transactions::class, $this->provider->save(['add' => $transaction])['add']);

        $this->assertEquals($this->providerEmpty->save(['add' => $transaction])['add']->count(), 1);
    }

    public function testOne()
    {
        $transaction = Transaction::create()
            ->setCustomerId(11)
            ->setPrice(12);

        $this->assertInstanceOf(Transaction::class, $this->provider->addOne($transaction));
        $this->assertInstanceOf(Transaction::class, $this->provider->addOne([$transaction]));

        $this->assertInstanceOf(Transaction::class, $this->providerEmpty->addOne($transaction));
        $this->assertInstanceOf(Transaction::class, $this->providerEmpty->addOne([$transaction]));

        $this->assertInstanceOf(Transaction::class, $this->provider->updateOne($transaction));
        $this->assertInstanceOf(Transaction::class, $this->provider->updateOne([$transaction]));

        $this->assertInstanceOf(Transaction::class, $this->providerEmpty->updateOne($transaction));
        $this->assertInstanceOf(Transaction::class, $this->providerEmpty->updateOne([$transaction]));

        $this->assertInstanceOf(Transaction::class, $this->provider->deleteOne($transaction));
        $this->assertInstanceOf(Transaction::class, $this->provider->deleteOne([$transaction]));

        $this->assertInstanceOf(Transaction::class, $this->providerEmpty->deleteOne($transaction));
        $this->assertInstanceOf(Transaction::class, $this->providerEmpty->deleteOne([$transaction]));
    }

    /**
     * @expectedException \Amocrm\Exception\DataIncorrectException
     */
    public function testExceptionAddOne()
    {
        $transaction = Transaction::create()
            ->setCustomerId(11)
            ->setPrice(12);

        $this->assertInstanceOf(Transaction::class, $this->providerError->addOne($transaction));
    }

    /**
     * @expectedException \Amocrm\Exception\DataIncorrectException
     */
    public function testExceptionAddOneArray()
    {
        $transaction = Transaction::create()
            ->setCustomerId(11)
            ->setPrice(12);

        $this->assertInstanceOf(Transaction::class, $this->providerError->addOne([$transaction]));
    }
}