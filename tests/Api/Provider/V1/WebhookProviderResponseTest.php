<?php

use Amocrm\Api\Client\RestClientInterface;
use Amocrm\Api\Model\Webhook;
use Amocrm\Api\Model\Webhooks;
use Amocrm\Api\Provider\V1\WebhookProvider;
use PHPUnit\Framework\TestCase;

class WebhookProviderResponseTestProviderResponseTest extends TestCase
{
    /**
     * @var WebhookProvider
     */
    private $provider;

    /**
     * @var WebhookProvider
     */
    private $providerEmpty;

    public function setUp()
    {
        $this->provider = new WebhookProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'webhooks'     => [
                        [
                            'id'      => '353255',
                            'url'     => 'https://site.ru/asdf',
                            'actions' => ['add_customer', 'update_contact'],
                        ],
                        [
                            'id'      => '123414',
                            'url'     => 'https://site.com/asdf',
                            'actions' => ['update_company', 'update_customer'],
                        ],
                        [
                            'id'      => '123114',
                            'url'     => 'https://boo.com/asdf',
                            'actions' => ['delete_lead', 'delete_contact'],
                        ],
                    ],
                    'server_time' => 1523111710,
                ]];
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'webhooks' => [
                        'subscribe' => [
                            [
                                'url'    => 'https://site.com/asdf',
                                'result' => true,
                            ],
                        ],
                    ],
                ]];
            }
        });

        $this->providerEmpty = new WebhookProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return null;
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'webhooks' => [
                        'subscribe' => [],
                    ],
                ]];
            }
        });
    }

    public function testGet()
    {
        $entities = $this->provider->get(1031821);
        $this->assertInstanceOf(Webhooks::class, $entities);
        $this->assertInstanceOf(Webhook::class, $entities->first());

        $this->assertInstanceOf(Webhooks::class, $this->provider->get([353255, 123414]));

        $entities = $this->providerEmpty->get(1031821);
        $this->assertInstanceOf(Webhooks::class, $entities);
        $this->assertEquals(0, $entities->count());

        $entities = $this->providerEmpty->get([1031821, 1111111]);
        $this->assertInstanceOf(Webhooks::class, $entities);
        $this->assertEquals(0, $entities->count());
    }

    public function testFind()
    {
        $this->assertInstanceOf(Webhooks::class, $this->provider->find(null, null));
        $this->assertInstanceOf(Webhooks::class, $this->providerEmpty->find(null, null));
    }

    public function testList()
    {
        $this->assertInstanceOf(Webhooks::class, $this->provider->list(null, null, null));
        $this->assertInstanceOf(Webhooks::class, $this->providerEmpty->list(null, null, null));
    }

    public function testSave()
    {
        $taskSaved = $this->provider->subscribe([Webhook::create()]);
        $this->assertInstanceOf(Webhooks::class, $taskSaved);
        $this->assertEquals(1, $taskSaved->count());

        $this->assertInstanceOf(Webhooks::class, $this->provider->subscribe(Webhook::create()));

        $this->assertEquals($this->providerEmpty->subscribe([Webhook::create()])->count(), 0);
        $this->assertEquals($this->providerEmpty->subscribe(Webhook::create())->count(), 0);
    }

    public function testSaveOne()
    {
        $this->assertInstanceOf(Webhook::class, $this->provider->subscribeOne([Webhook::create()]));
        $this->assertInstanceOf(Webhook::class, $this->provider->subscribeOne(Webhook::create()));

        $this->assertNull($this->providerEmpty->subscribeOne([Webhook::create()]));
        $this->assertNull($this->providerEmpty->subscribeOne(Webhook::create()));
    }
}