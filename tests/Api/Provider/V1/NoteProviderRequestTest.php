<?php

use Amocrm\Api\Client\RestClientToken;
use Amocrm\Api\Model\Account\NoteType;
use Amocrm\Api\Provider\V1\NoteProvider;
use Amocrm\Api\Model\Note;
use PHPUnit\Framework\TestCase;

/**
 * Проверяет отправляемые в API данные на соответствие документации.
 */
class NoteProviderRequestTest extends TestCase
{
    /**
     * @var RestClientToken
     */
    private $client;

    /**
     * @var NoteProvider
     */
    private $provider;

    public function setUp()
    {
        $this->client       = new class('domain', 'login', 'token') extends RestClientToken
        {
            public $mockEndpoint;
            public $mockMethod;
            public $mockParams;
            public $mockHeaders;

            protected function requestCurl(string $endpoint, string $method = self::METHOD_GET, array $params = [], array $headers = [])
            {
                $this->mockEndpoint = $endpoint;
                $this->mockMethod   = $method;
                $this->mockParams   = $params;
                $this->mockHeaders  = $headers;

                return ['response' => ['notes' => []]];
            }
        };
        $this->provider = new NoteProvider($this->client);
    }

    public function testCreate()
    {
        $this->assertInstanceOf(Note::class, $this->provider->create());
    }

    public function testList()
    {
        $this->provider->list('company');

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/notes/list?USER_LOGIN=login&USER_HASH=token&type=company'
        );
        $this->assertEquals('GET', $this->client->mockMethod);
        $this->assertEquals([], $this->client->mockParams);
        $this->assertEquals(['Content-Type: application/json'], $this->client->mockHeaders);
    }

    public function testListFull()
    {
        $ifModifiedSince = new DateTime('now');

        $this->provider->list('contact', 2345, $ifModifiedSince, 1, 2, 3456);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/notes/list?USER_LOGIN=login&USER_HASH=token&type=contact&element_id=2345&limit_rows=1&limit_offset=2&id=3456'
        );
        $this->assertEquals('GET', $this->client->mockMethod);
        $this->assertEquals([], $this->client->mockParams);
        $this->assertEquals([
            'Content-Type: application/json',
            'if-modified-since: ' . $ifModifiedSince->format('D, d M Y H:i:s'),
        ], $this->client->mockHeaders);
    }

    public function testSetAdd()
    {
        $entity = Note::create();
        $entity
            ->setText('Название')
            ->setLeadId(1234)
            ->setNoteType(NoteType::COMMON)
        ;

        $this->provider->save($entity);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/notes/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['notes']['add'][0]));

        $data = $this->client->mockParams['request']['notes']['add'][0];

        $this->assertTrue(isset($data['last_modified']));
        $this->assertEquals('Название', $data['text']);
        $this->assertEquals('1234', $data['element_id']);
        $this->assertEquals('2', $data['element_type']);
        $this->assertEquals('4', $data['note_type']);

        $this->provider->save($entity->setCustomerId(333));

        $data = $this->client->mockParams['request']['notes']['add'][0];

        $this->assertEquals('333', $data['element_id']);
        $this->assertEquals('12', $data['element_type']);

        $this->provider->save($entity->setContactId(111));

        $data = $this->client->mockParams['request']['notes']['add'][0];

        $this->assertEquals('111', $data['element_id']);
        $this->assertEquals('1', $data['element_type']);

        $this->provider->save($entity->setCompanyId(222));

        $data = $this->client->mockParams['request']['notes']['add'][0];

        $this->assertEquals('222', $data['element_id']);
        $this->assertEquals('3', $data['element_type']);
    }

    public function testSetUpdate()
    {
        $entity = Note::create([
            'id'                  => '23348893',
            'element_id'          => '1031821',
            'element_type'        => '2',
            'note_type'           => 4,
            'date_create'         => 1523662593,
            'created_user_id'     => '2291701',
            'last_modified'       => 1523662593,
            'text'                => 'Текст примечания',
            'responsible_user_id' => '2291701',
            'account_id'          => '19040293',
            'ATTACHEMENT'         => '',
            'group_id'            => 0,
            'editable'            => 'Y',
        ]);
        $entity
            ->setText('новый текст')
            ->setResponsibleUserId(111)
        ;

        $this->provider->save($entity);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/notes/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['notes']['update'][0]));

        $data = $this->client->mockParams['request']['notes']['update'][0];

        $this->assertTrue(isset($data['last_modified']));
        $this->assertEquals(23348893, $data['id']);
        $this->assertEquals('новый текст', $data['text']);
        $this->assertEquals(111, $data['responsible_user_id']);
    }
}