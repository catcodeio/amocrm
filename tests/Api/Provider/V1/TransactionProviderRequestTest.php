<?php

use Amocrm\Api\Client\RestClientToken;
use Amocrm\Api\Provider\V1\TransactionProvider;
use Amocrm\Api\Model\Transaction;
use PHPUnit\Framework\TestCase;

/**
 * Проверяет отправляемые в API данные на соответствие документации.
 */
class TransactionProviderRequestTest extends TestCase
{
    /**
     * @var RestClientToken
     */
    private $client;

    /**
     * @var TransactionProvider
     */
    private $provider;

    public function setUp()
    {
        $this->client       = new class('domain', 'login', 'token') extends RestClientToken
        {
            public $mockEndpoint;
            public $mockMethod;
            public $mockParams;
            public $mockHeaders;

            protected function requestCurl(string $endpoint, string $method = self::METHOD_GET, array $params = [], array $headers = [])
            {
                $this->mockEndpoint = $endpoint;
                $this->mockMethod   = $method;
                $this->mockParams   = $params;
                $this->mockHeaders  = $headers;

                return ['response' => ['transactions' => []]];
            }
        };
        $this->provider = new TransactionProvider($this->client);
    }

    public function testCreate()
    {
        $this->assertInstanceOf(Transaction::class, $this->provider->create());
    }

    public function testList()
    {
        $this->provider->list();

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/transactions/list?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals('GET', $this->client->mockMethod);
        $this->assertEquals([], $this->client->mockParams);
        $this->assertEquals(['Content-Type: application/json'], $this->client->mockHeaders);
    }

    public function testListFull()
    {
        $this->provider->list(1234, 1, 2, 4);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/transactions/list?USER_LOGIN=login&USER_HASH=token&filter%5Bcustomer_id%5D=1234&filter%5Bid%5D%5B0%5D=4&limit_offset=2&limit_rows=1'
        );
        $this->assertEquals('GET', $this->client->mockMethod);
        $this->assertEquals([], $this->client->mockParams);
        $this->assertEquals([
            'Content-Type: application/json',
        ], $this->client->mockHeaders);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testExceptionSave()
    {
        $this->provider->save([]);
    }

    /**
     * @expectedException \Amocrm\Exception\DataIncorrectException
     */
    public function testExceptionSaveCustomerId()
    {
        $transaction = Transaction::create()
            ->setPrice(12)
            ->setDate(new DateTime('20.01.2018'))
        ;

        $this->provider->save(['add' => $transaction]);
    }

    /**
     * @expectedException \Amocrm\Exception\DataIncorrectException
     */
    public function testExceptionSavePrice()
    {
        $transaction = Transaction::create()
            ->setCustomerId(12)
            ->setDate(new DateTime('20.01.2018'))
        ;

        $this->provider->save(['add' => $transaction]);
    }

    public function testSetAdd()
    {
        $transaction = Transaction::create()
            ->setCustomerId(11)
            ->setPrice(12)
            ->setComment('Название');

        $this->provider->save(['add' => $transaction]);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/transactions/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['transactions']['add'][0]));

        $data = $this->client->mockParams['request']['transactions']['add'][0];

        $this->assertTrue(isset($data['price']));
        $this->assertEquals(12, $data['price']);
        $this->assertTrue(isset($data['comment']));
        $this->assertEquals('Название', $data['comment']);
        $this->assertFalse(isset($data['date']));
    }

    public function testSetAddOne()
    {
        $nextDate = new DateTime('20.01.2018');
        $transaction = Transaction::create()
            ->setCustomerId(11)
            ->setPrice(12)
            ->setDate($nextDate)
            ->setComment('Название');

        $this->provider->addOne($transaction);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/transactions/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['transactions']['add'][0]));

        $data = $this->client->mockParams['request']['transactions']['add'][0];

        $this->assertTrue(isset($data['price']));
        $this->assertEquals(12, $data['price']);
        $this->assertTrue(isset($data['customer_id']));
        $this->assertEquals(11, $data['customer_id']);
        $this->assertTrue(isset($data['comment']));
        $this->assertEquals('Название', $data['comment']);
        $this->assertTrue(isset($data['date']));
        $this->assertEquals($nextDate->format('U'), $data['date']);
    }

    public function testSetUpdate()
    {
        $date = new DateTime('20.01.2018');
        $transaction = Transaction::create([
            'id'          => 12,
            'customer_id' => 11,
            'comment'     => 'Название',
            'price'       => 100,
        ]);
        $transaction->setPrice(10);
        $transaction->setDate($date);

        $this->provider->save(['update' => $transaction]);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/transactions/comment?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['comments']['update'][0]));

        $data = $this->client->mockParams['request']['comments']['update'][0];

        $this->assertTrue(isset($data['transaction_id']));
        $this->assertTrue(isset($data['comment']));
        $this->assertFalse(isset($data['customer_id']));
        $this->assertFalse(isset($data['price']));
        $this->assertFalse(isset($data['date']));
    }

    public function testSetUpdateOne()
    {
        $date = new DateTime('20.01.2018');
        $transaction = Transaction::create([
            'id'          => 12,
            'customer_id' => 11,
            'comment'     => 'Название',
            'price'       => 100,
        ]);
        $transaction->setPrice(10);
        $transaction->setDate($date);

        $this->provider->updateOne([$transaction]);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/transactions/comment?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['comments']['update'][0]));

        $data = $this->client->mockParams['request']['comments']['update'][0];

        $this->assertTrue(isset($data['transaction_id']));
        $this->assertTrue(isset($data['comment']));
        $this->assertFalse(isset($data['customer_id']));
        $this->assertFalse(isset($data['price']));
        $this->assertFalse(isset($data['date']));
    }

    public function testSetDelete()
    {
        $transaction = Transaction::create([
            'id'      => 12,
            'comment' => 'Название',
        ]);

        $this->provider->save(['delete' => [$transaction]]);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/transactions/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['transactions']['delete']));

        $data = $this->client->mockParams['request']['transactions']['delete'];

        $this->assertEquals(1, count($data));
        $this->assertEquals(12, $data[0]);
    }

    public function testSetDeleteOne()
    {
        $transaction = Transaction::create([
            'id'      => 12,
            'comment' => 'Название',
        ]);

        $this->provider->deleteOne($transaction);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/transactions/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['transactions']['delete']));

        $data = $this->client->mockParams['request']['transactions']['delete'];

        $this->assertEquals(1, count($data));
        $this->assertEquals(12, $data[0]);
    }

    public function testSetDeleteId()
    {
        $this->provider->save(['delete' => [123, 6453, 434]]);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/transactions/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['transactions']['delete']));

        $data = $this->client->mockParams['request']['transactions']['delete'];

        $this->assertEquals(3, count($data));
        $this->assertEquals(123, $data[0]);
        $this->assertEquals(6453, $data[1]);
        $this->assertEquals(434, $data[2]);
    }
}