<?php

use Amocrm\Api\Client\RestClientInterface;
use Amocrm\Api\Model\Lead;
use Amocrm\Api\Model\Leads;
use Amocrm\Api\Provider\V1\LeadProvider;
use PHPUnit\Framework\TestCase;

class LeadProviderResponseTest extends TestCase
{
    /**
     * @var LeadProvider
     */
    private $provider;

    /**
     * @var LeadProvider
     */
    private $providerEmpty;

    public function setUp()
    {
        $this->provider = new LeadProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'leads'     => [
                        [
                            'id'                  => '1031821',
                            'name'                => 'Выключен',
                            'date_create'         => 1522415760,
                            'created_user_id'     => '2291701',
                            'last_modified'       => 1523190638,
                            'account_id'          => '19040293',
                            'price'               => '0',
                            'responsible_user_id' => '2291701',
                            'linked_company_id'   => '',
                            'group_id'            => 0,
                            'pipeline_id'         => 1064215,
                            'date_close'          => 0,
                            'closest_task'        => 1522540804,
                            'loss_reason_id'      => 0,
                            'deleted'             => 0,
                            'tags'                => [],
                            'status_id'           => '19040299',
                            'custom_fields'       => [
                                [
                                    'id'     => '199323',
                                    'name'   => 'Тестовый список',
                                    'values' => [
                                        [
                                            'value' => 'Вариант 1',
                                            'enum'  => '406825',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '199327',
                                    'name'   => 'Тестовый переключатель',
                                    'values' => [
                                        [
                                            'value' => 'Вариант 2',
                                            'enum'  => '406841',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '199329',
                                    'name'   => 'Тестовый флаг',
                                    'values' => [
                                        [
                                            'value' => '1',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '199345',
                                    'name'   => 'Тестовый день рождения (обязательное)',
                                    'values' => [
                                        [
                                            'value' => '2018-04-25 00:00:00',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '125623',
                                    'name'   => 'Client ID',
                                    'values' => [
                                        [
                                            'value' => '222333444',
                                        ],
                                    ],
                                ],
                            ],
                            'main_contact_id'     => 2986625,
                        ],
                        [
                            'id'                  => '1111111',
                            'name'                => 'Название сделки',
                            'date_create'         => 1522415700,
                            'created_user_id'     => '2291701',
                            'last_modified'       => 1523190638,
                            'account_id'          => '19040293',
                            'price'               => '1000',
                            'responsible_user_id' => '2291701',
                            'linked_company_id'   => '',
                            'group_id'            => 0,
                            'pipeline_id'         => 1064215,
                            'date_close'          => 0,
                            'closest_task'        => 1522540804,
                            'loss_reason_id'      => 0,
                            'deleted'             => 0,
                            'tags'                => [
                                [
                                    'id'           => 58183,
                                    'name'         => 'тестовая',
                                    'element_type' => 2,
                                ]
                            ],
                            'status_id'           => '19040299',
                            'custom_fields'       => [
                                [
                                    'id'     => '199323',
                                    'name'   => 'Тестовый список',
                                    'values' => [
                                        [
                                            'value' => 'Вариант 1',
                                            'enum'  => '406825',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '199327',
                                    'name'   => 'Тестовый переключатель',
                                    'values' => [
                                        [
                                            'value' => 'Вариант 2',
                                            'enum'  => '406841',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '199329',
                                    'name'   => 'Тестовый флаг',
                                    'values' => [
                                        [
                                            'value' => '1',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '199345',
                                    'name'   => 'Тестовый день рождения (обязательное)',
                                    'values' => [
                                        [
                                            'value' => '2018-04-25 00:00:00',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '125623',
                                    'name'   => 'Client ID',
                                    'values' => [
                                        [
                                            'value' => '111222333',
                                        ],
                                    ],
                                ],
                            ],
                            'main_contact_id'     => 2986625,
                        ],
                    ],
                    'server_time' => 1523111710,
                ]];
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'leads' => [
                        'add' => [
                            [
                                'id'         => 3655494,
                                'request_id' => 0,
                            ],
                        ],
                    ],
                ]];
            }
        });

        $this->providerEmpty = new LeadProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return null;
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'leads' => [
                        'add' => [],
                    ],
                ]];
            }
        });
    }

    public function testGet()
    {
        $entities = $this->provider->get(1031821);
        $this->assertInstanceOf(Leads::class, $entities);
        $this->assertInstanceOf(Lead::class, $entities->first());

        $this->assertInstanceOf(Leads::class, $this->provider->get([1031821, 1111111]));

        $entities = $this->providerEmpty->get(1031821);
        $this->assertInstanceOf(Leads::class, $entities);
        $this->assertEquals(0, $entities->count());

        $entities = $this->providerEmpty->get([1031821, 1111111]);
        $this->assertInstanceOf(Leads::class, $entities);
        $this->assertEquals(0, $entities->count());
    }

    public function testGetOne()
    {
        $this->assertInstanceOf(Lead::class, $this->provider->getOne(5207409));
        $this->assertInstanceOf(Lead::class, $this->provider->getOne([5207409, 2986625]));

        $this->assertNull($this->providerEmpty->getOne(5207409));
        $this->assertNull($this->providerEmpty->getOne([5207409, 2986625]));
    }

    public function testFind()
    {
        $this->assertInstanceOf(Leads::class, $this->provider->find('asdf', null, null, new DateTime()));
        $this->assertInstanceOf(Leads::class, $this->providerEmpty->find('asdf', null, null, new DateTime()));
    }

    public function testFindEmpty()
    {
        $this->assertInstanceOf(Leads::class, $this->provider->find());
        $this->assertInstanceOf(Leads::class, $this->providerEmpty->find());
    }

    public function testList()
    {
        $this->assertInstanceOf(Leads::class, $this->provider->list('asdf', null, null, new DateTime(), null, null, null));
        $this->assertInstanceOf(Leads::class, $this->providerEmpty->list('list', null, null, new DateTime(), null, null, null));
    }

    public function testSave()
    {
        $leadSaved = $this->provider->save([Lead::create()]);
        $this->assertInstanceOf(Leads::class, $leadSaved);
        $this->assertEquals(1, $leadSaved->count());

        $this->assertInstanceOf(Leads::class, $this->provider->save(Lead::create()));

        $this->assertEquals($this->providerEmpty->save([Lead::create()])->count(), 0);
        $this->assertEquals($this->providerEmpty->save(Lead::create())->count(), 0);
    }

    public function testSaveOne()
    {
        $this->assertInstanceOf(Lead::class, $this->provider->saveOne([Lead::create()]));
        $this->assertInstanceOf(Lead::class, $this->provider->saveOne(Lead::create()));

        $this->assertNull($this->providerEmpty->saveOne([Lead::create()]));
        $this->assertNull($this->providerEmpty->saveOne(Lead::create()));
    }
}