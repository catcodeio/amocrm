<?php

use Amocrm\Api\Client\RestClientToken;
use Amocrm\Api\Helper\ElementType;
use Amocrm\Api\Model\Contact;
use Amocrm\Api\Model\Customer;
use Amocrm\Api\Model\Link;
use Amocrm\Api\Model\Transaction;
use Amocrm\Api\Provider\V1\ElementProvider;
use PHPUnit\Framework\TestCase;

/**
 * Проверяет отправляемые в API данные на соответствие документации.
 */
class ElementProviderRequestTest extends TestCase
{
    /**
     * @var RestClientToken
     */
    private $client;

    /**
     * @var ElementProvider
     */
    private $provider;

    public function setUp()
    {
        $this->client       = new class('domain', 'login', 'token') extends RestClientToken
        {
            public $mockEndpoint;
            public $mockMethod;
            public $mockParams;
            public $mockHeaders;

            protected function requestCurl(string $endpoint, string $method = self::METHOD_GET, array $params = [], array $headers = [])
            {
                $this->mockEndpoint = $endpoint;
                $this->mockMethod   = $method;
                $this->mockParams   = $params;
                $this->mockHeaders  = $headers;

                return ['response' => [
                    'customers' => [],
                    'transactions' => [],
                    'contacts' => [],
                    'links' => [],
                ]];
            }
        };
        $this->provider = new ElementProvider($this->client);
    }

    public function badSaveParams()
    {
        return ['response' => [
            [[[[]]]],
            [[[['add' => true], []]]],
            [[[['add' => true], ['add' => true], []]]],
            [[[['add' => true], ['add' => true], ['link' => true], []]]],
            [[[['add' => true], ['add' => true], ['link' => true], ['delete' => true]]]],
            [[[['add' => true], ['update' => true], ['link' => true], ['add' => true]]]],
            [[[['add' => true], ['add' => true], ['add' => true], ['add' => true]]]],
        ]];
    }

    /**
     * @dataProvider badSaveParams
     * @expectedException \InvalidArgumentException
     *
     * @param $params
     */
    public function testExceptionSave($params)
    {
        call_user_func_array([$this->provider, 'save'], $params);
    }

    public function badSaveEntityParams()
    {
        return [
            [[
                  ['add' => [
                      Customer::create()
                  ]],
                  [
                      'add' => Transaction::create()
                          ->setCustomerId(1)
                          ->setPrice(2)
                  ],
                  ['link' => Link::create()],
                  ['add' => [
                      Contact::create()
                          ->setName('Название')
                  ]]
            ]],
            [[
                  ['add' => [
                      Customer::create()
                          ->setNextPrice(10)
                          ->setName('Название')
                  ]],
                  [
                      'add' => Transaction::create()
                          ->setPrice(2)
                  ],
                  null,
                  ['add' => [
                      Contact::create()
                          ->setName('Название')
                  ]]
            ]],
            [[
                  ['add' => [
                      Customer::create()
                          ->setNextPrice(10)
                          ->setName('Название')
                  ]],
                  [
                      'add' => Transaction::create()
                          ->setCustomerId(1)
                  ],
                  ['link' => Link::create()],
                  ['add' => [
                      Contact::create()
                          ->setName('Название')
                  ]]
            ]],
            [[
                  ['add' => [
                      Customer::create()
                          ->setNextPrice(10)
                          ->setName('Название')
                  ]],
                  [
                      'add' => Transaction::create()
                          ->setCustomerId(1)
                          ->setPrice(2)
                  ],
                  ['link' => Link::create()],
                  ['add' => [
                      Contact::create()
                  ]]
            ]],
        ];
    }

    /**
     * @dataProvider badSaveEntityParams
     * @expectedException \Amocrm\Exception\DataIncorrectException
     *
     * @param $params
     */
    public function testExceptionSaveEntity($params)
    {
        call_user_func_array([$this->provider, 'save'], $params);
    }

    public function testSetAdd()
    {
        $this->provider->save(
            ['add' => [
                Customer::create()
                    ->setNextPrice(10)
                    ->setName('Название')
            ]],
            [
                'add' => Transaction::create()
                    ->setCustomerId(1)
                    ->setPrice(2)
            ],
            [
                'link' => Link::create()
                    ->setFrom(ElementType::CUSTOMER)
                    ->setFromId(2)
                    ->setTo(ElementType::CONTACT)
                    ->setToId(3),
                'unlink' => [
                    Link::create()
                        ->setFrom(ElementType::CUSTOMER)
                        ->setFromId(4)
                        ->setTo(ElementType::CONTACT)
                        ->setToId(5)
                ],
            ],
            ['add' => [
                Contact::create()
                    ->setName('Название')
            ]]
        );

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/elements/sync?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['customers']['add'][0]));
        $this->assertTrue(isset($this->client->mockParams['request']['transactions']['add'][0]));
        $this->assertTrue(isset($this->client->mockParams['request']['contacts']['add'][0]));
        $this->assertTrue(isset($this->client->mockParams['request']['links']['link'][0]));
        $this->assertTrue(isset($this->client->mockParams['request']['links']['unlink'][0]));

        $customer = $this->client->mockParams['request']['customers']['add'][0];

        $this->assertTrue(isset($customer['next_price']));
        $this->assertEquals(10, $customer['next_price']);
        $this->assertTrue(isset($customer['name']));
        $this->assertEquals('Название', $customer['name']);

        $transaction = $this->client->mockParams['request']['transactions']['add'][0];

        $this->assertTrue(isset($transaction['customer_id']));
        $this->assertEquals(1, $transaction['customer_id']);
        $this->assertTrue(isset($transaction['price']));
        $this->assertEquals(2, $transaction['price']);

        $contact = $this->client->mockParams['request']['contacts']['add'][0];

        $this->assertTrue(isset($contact['name']));
        $this->assertEquals('Название', $contact['name']);

        $link = $this->client->mockParams['request']['links']['link'][0];

        $this->assertTrue(isset($link['from']));
        $this->assertEquals('customers', $link['from']);
        $this->assertTrue(isset($link['from_id']));
        $this->assertEquals(2, $link['from_id']);
        $this->assertTrue(isset($link['to']));
        $this->assertEquals('contacts', $link['to']);
        $this->assertTrue(isset($link['to_id']));
        $this->assertEquals(3, $link['to_id']);

        $unlink = $this->client->mockParams['request']['links']['unlink'][0];

        $this->assertTrue(isset($unlink['from']));
        $this->assertEquals('customers', $unlink['from']);
        $this->assertTrue(isset($unlink['from_id']));
        $this->assertEquals(4, $unlink['from_id']);
        $this->assertTrue(isset($unlink['to']));
        $this->assertEquals('contacts', $unlink['to']);
        $this->assertTrue(isset($unlink['to_id']));
        $this->assertEquals(5, $unlink['to_id']);
    }
}