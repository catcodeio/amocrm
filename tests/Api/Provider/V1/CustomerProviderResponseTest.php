<?php

use Amocrm\Api\Client\RestClientInterface;
use Amocrm\Api\Model\Customer;
use Amocrm\Api\Model\Customers;
use Amocrm\Api\Provider\V1\CustomerProvider;
use PHPUnit\Framework\TestCase;

class CustomerProviderResponseTest extends TestCase
{
    /**
     * @var CustomerProvider
     */
    private $provider;

    /**
     * @var CustomerProvider
     */
    private $providerEmpty;

    /**
     * @var Customer
     */
    private $entity;

    public function setUp()
    {
        $this->provider = new CustomerProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'customers'     => [
                        [
                            'id'              => '1031821',
                            'name'            => 'Кастомер',
                            'date_create'     => 1522415760,
                            'date_modify'     => 1522415960,
                            'created_by'      => '2291701',
                            'modified_by'     => 1523190638,
                            'account_id'      => '19040293',
                            'next_price'      => '320',
                            'main_user_id'    => '2291701',
                            'periodicity'     => 30,
                            'next_date'       => 1523190638,
                            'main_contact_id' => 2986625,
                            'task_last_date'  => null,
                            'period_id'       => 1064215,
                            'deleted'         => false,
                            'tags'            => [
                                [
                                    'id'           => 58183,
                                    'name'         => 'тестовая',
                                    'element_type' => 2,
                                ],
                            ],
                            'custom_fields'   => [
                                [
                                    'id'     => '199323',
                                    'name'   => 'Тестовый список',
                                    'values' => [
                                        [
                                            'value' => 'Вариант 1',
                                            'enum'  => '406825',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '199327',
                                    'name'   => 'Тестовый переключатель',
                                    'values' => [
                                        [
                                            'value' => 'Вариант 2',
                                            'enum'  => '406841',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '199329',
                                    'name'   => 'Тестовый флаг',
                                    'values' => [
                                        [
                                            'value' => '1',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '199345',
                                    'name'   => 'Тестовый день рождения (обязательное)',
                                    'values' => [
                                        [
                                            'value' => '2018-04-25 00:00:00',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '125623',
                                    'name'   => 'Client ID',
                                    'values' => [
                                        [
                                            'value' => '222333444',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '199325',
                                    'name'   => 'Тестовый мультисписок',
                                    'values' => [
                                        [
                                            'value' => 'Вариант 1',
                                            'enum'  => '406831',
                                        ],
                                        [
                                            'value' => 'Вариант 2',
                                            'enum'  => '406833',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '199331',
                                    'name'   => 'Тестовый адрес (только API)',
                                    'values' => [
                                        [
                                            'value'   => 'Ломоносова',
                                            'subtype' => '1',
                                        ],
                                        [
                                            'value'   => '111',
                                            'subtype' => '2',
                                        ],
                                        [
                                            'value'   => 'СПб',
                                            'subtype' => '3',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '206417',
                                    'name'   => 'Тестовая ссылка',
                                    'values' => [
                                        [
                                            'value' => 'https://catcode.io',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '217963',
                                    'name'   => 'Тестовый текст',
                                    'values' => [
                                        [
                                            'value' => 'Много текста',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '217965',
                                    'name'   => 'Тестовое число',
                                    'values' => [
                                        [
                                            'value' => '567654345676543456787654.567876543',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '217981',
                                    'name'   => 'Тестовая дата',
                                    'values' => [
                                        [
                                            'value' => '2018-04-24 00:00:00',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '217983',
                                    'name'   => 'Тестовая текстовая область',
                                    'values' => [
                                        [
                                            'value' => 'Очень много текста',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '217997',
                                    'name'   => 'Тестовый короткий адрес',
                                    'values' => [
                                        [
                                            'value' => 'СПб Звеня 12',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            "id"              => 1111111,
                            "date_create"     => 1534873031,
                            "date_modify"     => 1534873031,
                            "created_by"      => 2291701,
                            "modified_by"     => 2291701,
                            "account_id"      => 19040293,
                            "main_user_id"    => 2291701,
                            "name"            => "Название покупателя",
                            "deleted"         => false,
                            "next_price"      => 0,
                            "periodicity"     => 0,
                            "next_date"       => 1533675600,
                            "task_last_date"  => null,
                            "status_id"       => 126352,
                            "ltv"             => null,
                            "purchases_count" => null,
                            "average_check"   => null,
                            "custom_fields"   => [],
                            "tags"            => [],
                            "main_contact_id" => false,
                            "period_id"       => "126352",
                        ],
                    ],
                    'server_time' => 1523111710,
                ]];
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'customers' => [
                        'add' => [
                            [
                                'id'         => 3655494,
                                'request_id' => 0,
                            ],
                        ],
                    ],
                ]];
            }
        });

        $this->providerEmpty = new CustomerProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return null;
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'customers' => [
                        'add' => [],
                    ]],
                ];
            }
        });

        $this->entity = Customer::create()
            ->setNextPrice(12)
            ->setName('Название');
    }

    public function testGet()
    {
        $entities = $this->provider->get(1031821);
        $this->assertInstanceOf(Customers::class, $entities);
        $this->assertInstanceOf(Customer::class, $entities->first());

        $this->assertInstanceOf(Customers::class, $this->provider->get([1031821, 1111111]));

        $entities = $this->providerEmpty->get(1031821);
        $this->assertInstanceOf(Customers::class, $entities);
        $this->assertEquals(0, $entities->count());

        $entities = $this->providerEmpty->get([1031821, 1111111]);
        $this->assertInstanceOf(Customers::class, $entities);
        $this->assertEquals(0, $entities->count());
    }

    public function testGetOne()
    {
        $this->assertInstanceOf(Customer::class, $this->provider->getOne(5207409));
        $this->assertInstanceOf(Customer::class, $this->provider->getOne([5207409, 2986625]));

        $this->assertNull($this->providerEmpty->getOne(5207409));
        $this->assertNull($this->providerEmpty->getOne([5207409, 2986625]));
    }

    public function testFind()
    {
        $this->assertInstanceOf(Customers::class, $this->provider->find('modify', new DateTime(), new DateTime(), new DateTime(), new DateTime(), [], [], 1, 'today'));
        $this->assertInstanceOf(Customers::class, $this->providerEmpty->find('modify', new DateTime(), new DateTime(), new DateTime(), new DateTime(), [], [], 1, 'today'));
    }

    public function testList()
    {
        $this->assertInstanceOf(Customers::class, $this->provider->list('modify', new DateTime(), new DateTime(), new DateTime(), new DateTime(), [], [], 1, 'today', 1, 2, 3));
        $this->assertInstanceOf(Customers::class, $this->providerEmpty->list('modify', new DateTime(), new DateTime(), new DateTime(), new DateTime(), [], [], 1, 'today', 1, 2, 3));
    }

    public function testSave()
    {
        $customerSaved = $this->provider->save(['add' => $this->entity]);
        $this->assertTrue(is_array($customerSaved));
        $this->assertInstanceOf(Customers::class, $customerSaved['add']);
        $this->assertEquals(1, $customerSaved['add']->count());

        $this->assertInstanceOf(Customers::class, $this->provider->save(['add' => $this->entity])['add']);

        $this->assertEquals($this->providerEmpty->save(['add' => $this->entity])['add']->count(), 1);
    }

    public function testOne()
    {
        $this->assertInstanceOf(Customer::class, $this->provider->addOne($this->entity));
        $this->assertInstanceOf(Customer::class, $this->provider->addOne([$this->entity]));

        $this->assertInstanceOf(Customer::class, $this->providerEmpty->addOne($this->entity));
        $this->assertInstanceOf(Customer::class, $this->providerEmpty->addOne([$this->entity]));

        $this->assertInstanceOf(Customer::class, $this->provider->updateOne($this->entity));
        $this->assertInstanceOf(Customer::class, $this->provider->updateOne([$this->entity]));

        $this->assertInstanceOf(Customer::class, $this->providerEmpty->updateOne($this->entity));
        $this->assertInstanceOf(Customer::class, $this->providerEmpty->updateOne([$this->entity]));

        $this->assertInstanceOf(Customer::class, $this->provider->deleteOne($this->entity));
        $this->assertInstanceOf(Customer::class, $this->provider->deleteOne([$this->entity]));

        $this->assertInstanceOf(Customer::class, $this->providerEmpty->deleteOne($this->entity));
        $this->assertInstanceOf(Customer::class, $this->providerEmpty->deleteOne([$this->entity]));
    }
}