<?php

use Amocrm\Api\Client\RestClientInterface;
use Psr\Log\LoggerInterface;

/**
 * Общий родитель для всех rest-клиентов, чтобы замокать ненужные методы.
 */
abstract class RestClientCommon implements RestClientInterface
{
    /**
     * @inheritdoc
     */
    public function setTimeout(int $timeout)
    {
        // TODO: Implement setTimeout() method.
    }

    /**
     * @inheritdoc
     */
    public function setProxy(string $proxy)
    {
        // TODO: Implement setProxy() method.
    }

    /**
     * @inheritdoc
     */
    public function setLocale(string $locale)
    {
        // TODO: Implement setLocale() method.
    }

    /**
     * @inheritdoc
     */
    public function setLogger(?LoggerInterface $logger)
    {
        // TODO: Implement setLogger() method.
    }

    /**
     * @inheritdoc
     */
    public function setUsleep(int $usleep)
    {
        // TODO: Implement setUsleep() method.
    }
}