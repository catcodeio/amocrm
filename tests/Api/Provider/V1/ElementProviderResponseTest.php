<?php

use Amocrm\Api\Helper\ElementType;
use Amocrm\Api\Model\Contact;
use Amocrm\Api\Model\Contacts;
use Amocrm\Api\Model\Customer;
use Amocrm\Api\Model\Customers;
use Amocrm\Api\Model\Link;
use Amocrm\Api\Model\Links;
use Amocrm\Api\Provider\V1\ElementProvider;
use PHPUnit\Framework\TestCase;

class ElementProviderResponseTest extends TestCase
{
    /**
     * @var ElementProvider
     */
    private $provider;

    /**
     * @var ElementProvider
     */
    private $providerError;

    public function setUp()
    {
        $this->provider = new ElementProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return null;
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    "contacts"    => [
                        "add" => [
                            [
                                "id"         => 17175661,
                                "request_id" => 0
                            ]
                        ]
                    ],
                    "customers"   => [
                        "add" => [
                            "customers" => [
                                [
                                    "id"              => 115109,
                                    "date_create"     => 1520215278,
                                    "date_modify"     => 1535548303,
                                    "created_by"      => 2291701,
                                    "modified_by"     => 2291701,
                                    "account_id"      => 19040293,
                                    "main_user_id"    => 2291701,
                                    "name"            => "Компания покупатель",
                                    "deleted"         => false,
                                    "next_price"      => null,
                                    "periodicity"     => 0,
                                    "next_date"       => 1535139458,
                                    "task_last_date"  => null,
                                    "status_id"       => 126352,
                                    "ltv"             => null,
                                    "purchases_count" => null,
                                    "average_check"   => null,
                                    "custom_fields"   => [],
                                    "tags"            => [
                                        [
                                            "id"   => 363507,
                                            "name" => "export_account"
                                        ]
                                    ],
                                    "main_contact_id" => false,
                                    "request_id"      => 16
                                ]
                            ],
                            "errors"    => []
                        ]
                    ],
                    "links"       => [
                        "link" => [
                            "links"  => [
                                [
                                    "from"    => "customers",
                                    "from_id" => 115109,
                                    "to"      => "contacts",
                                    "to_id"   => 17175661
                                ]
                            ],
                            "errors" => []
                        ]
                    ],
                    "server_time" => 1535548303,
                ]];
            }
        });

        $this->providerError = new ElementProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return null;
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    "contacts" => [
                            "add" => [
                            [
                                "error" => "Код ошибки 205. В случае повторного возникновения ошибки, обращайтесь в нашу техническую поддержку - support@amocrm.ru"
                            ]
                        ]
                    ],
                    "customers" => [
                            "add" => [
                                "customers" => [],
                            "errors" => [
                                [
                                    "id" => 16,
                                    "code" => 400,
                                    "message" => "Empty \"name\" field",
                                    "request_id" => 16
                                ]
                            ]
                        ],
                    ],
                    "links" => [
                            "link" => [
                                "links" => [],
                            "errors" => [
                                [
                                    "id" => 0,
                                    "code" => 400,
                                    "message" => "Error 284.",
                                    "request_id" => 0
                                ]
                            ]
                        ]
                    ],
                ]];
            }
        });
    }

    public function testSave()
    {
        $saved = $this->provider->save(
            ['add' => [
                Customer::create()
                    ->setNextPrice(10)
                    ->setName('Название')
            ]],
            null,
            [
                'link' => Link::create()
                    ->setFrom(ElementType::CUSTOMER)
                    ->setFromId(2)
                    ->setTo(ElementType::CONTACT)
                    ->setToId(3),
            ],
            ['add' => [
                Contact::create()
                    ->setName('Название')
            ]]
        );

        $this->assertTrue(is_array($saved));
        $this->assertTrue(isset($saved['customers']['add']));
        $this->assertTrue(isset($saved['links']['link']));
        $this->assertTrue(isset($saved['contacts']['add']));
        $this->assertFalse(isset($saved['transactions']));

        $this->assertInstanceOf(Customers::class, $saved['customers']['add']);
        $this->assertInstanceOf(Links::class, $saved['links']['link']);
        $this->assertInstanceOf(Contacts::class, $saved['contacts']['add']);

        $this->assertEquals(1, $saved['customers']['add']->count());
        $this->assertEquals(1, $saved['links']['link']->count());
        $this->assertEquals(1, $saved['contacts']['add']->count());
    }

    /**
     * @expectedException \Amocrm\Exception\DataIncorrectException
     */
    public function testSaveError()
    {
        $this->providerError->save(
            ['add' => [
                Customer::create()
                    ->setNextPrice(10)
            ]]
        );
    }
}