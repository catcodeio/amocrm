<?php

use Amocrm\Api\Client\RestClientInterface;
use Amocrm\Api\Model\Task;
use Amocrm\Api\Model\Tasks;
use Amocrm\Api\Provider\V1\TaskProvider;
use PHPUnit\Framework\TestCase;

class TaskProviderResponseTest extends TestCase
{
    /**
     * @var TaskProvider
     */
    private $provider;

    /**
     * @var TaskProvider
     */
    private $providerEmpty;

    public function setUp()
    {
        $this->provider = new TaskProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'tasks'     => [
                        [
                            'id'                  => '353255',
                            'element_id'          => '1031821',
                            'element_type'        => '2',
                            'task_type'           => 'CALL',
                            'date_create'         => 1522540805,
                            'created_user_id'     => '2291701',
                            'last_modified'       => 1522540805,
                            'text'                => 'Текст задачи',
                            'responsible_user_id' => '2291701',
                            'complete_till'       => 1522540804,
                            'status'              => '0',
                            'group_id'            => 0,
                            'account_id'          => '19040293',
                            'result'              => [],
                        ],
                        [
                            'id'                  => '349833',
                            'element_id'          => '1031821',
                            'element_type'        => '2',
                            'task_type'           => 'CALL',
                            'date_create'         => 1522502465,
                            'created_user_id'     => '2291701',
                            'last_modified'       => 1522542385,
                            'text'                => '',
                            'responsible_user_id' => '2291701',
                            'complete_till'       => 1522502465,
                            'status'              => '1',
                            'group_id'            => 0,
                            'account_id'          => '19040293',
                            'result'              => [],
                        ],
                        [
                            'id'                  => '1219851',
                            'element_id'          => '1031821',
                            'element_type'        => '2',
                            'task_type'           => 'CALL',
                            'date_create'         => 1523649717,
                            'created_user_id'     => '2291701',
                            'last_modified'       => 1523649734,
                            'text'                => 'asdfasdf',
                            'responsible_user_id' => '2291701',
                            'complete_till'       => 1523739540,
                            'status'              => '1',
                            'group_id'            => 0,
                            'account_id'          => '19040293',
                            'result'              => [
                                'id'   => 23331379,
                                'text' => 'resulttttttttt1111',
                            ],
                        ],
                    ],
                    'server_time' => 1523111710,
                ]];
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'tasks' => [
                        'add' => [
                            [
                                'id'         => 3655494,
                                'request_id' => 0,
                            ],
                        ],
                    ],
                ]];
            }
        });

        $this->providerEmpty = new TaskProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return null;
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'tasks' => [
                        'add' => [],
                    ],
                ]];
            }
        });
    }

    public function testGet()
    {
        $entities = $this->provider->get(1031821);
        $this->assertInstanceOf(Tasks::class, $entities);
        $this->assertInstanceOf(Task::class, $entities->first());

        $this->assertInstanceOf(Tasks::class, $this->provider->get([1031821, 1111111]));

        $entities = $this->providerEmpty->get(1031821);
        $this->assertInstanceOf(Tasks::class, $entities);
        $this->assertEquals(0, $entities->count());

        $entities = $this->providerEmpty->get([1031821, 1111111]);
        $this->assertInstanceOf(Tasks::class, $entities);
        $this->assertEquals(0, $entities->count());
    }

    public function testFind()
    {
        $this->assertInstanceOf(Tasks::class, $this->provider->find('contact', null, null, new DateTime()));
        $this->assertInstanceOf(Tasks::class, $this->providerEmpty->find('contact', null, null, new DateTime()));
    }

    public function testList()
    {
        $this->assertInstanceOf(Tasks::class, $this->provider->list('contact', null, null, new DateTime(), null, null, null));
        $this->assertInstanceOf(Tasks::class, $this->providerEmpty->list('contact', null, null, new DateTime(), null, null, null));
    }

    public function testSave()
    {
        $taskSaved = $this->provider->save([Task::create()]);
        $this->assertInstanceOf(Tasks::class, $taskSaved);
        $this->assertEquals(1, $taskSaved->count());

        $this->assertInstanceOf(Tasks::class, $this->provider->save(Task::create()));

        $this->assertEquals($this->providerEmpty->save([Task::create()])->count(), 0);
        $this->assertEquals($this->providerEmpty->save(Task::create())->count(), 0);
    }

    public function testSaveOne()
    {
        $this->assertInstanceOf(Task::class, $this->provider->saveOne([Task::create()]));
        $this->assertInstanceOf(Task::class, $this->provider->saveOne(Task::create()));

        $this->assertNull($this->providerEmpty->saveOne([Task::create()]));
        $this->assertNull($this->providerEmpty->saveOne(Task::create()));
    }
}