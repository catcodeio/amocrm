<?php

use Amocrm\Api\Client\RestClientInterface;
use Amocrm\Api\Helper\ElementType;
use Amocrm\Api\Model\CustomField\Field;
use Amocrm\Api\Model\CustomField\Fields;
use Amocrm\Api\Provider\V1\FieldProvider;
use PHPUnit\Framework\TestCase;

class FieldProviderResponseTest extends TestCase
{
    /**
     * @var FieldProvider
     */
    private $provider;

    /**
     * @var FieldProvider
     */
    private $providerEmpty;

    public function setUp()
    {
        $this->provider = new FieldProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'fields'     => [],
                ]];
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'fields' => [
                        'add' => [
                            [
                                'id'         => 3655494,
                                'request_id' => 0,
                            ],
                        ],
                        'delete' => [
                            [
                                'id'         => 3655494,
                                'request_id' => 0,
                            ],
                        ],
                    ],
                ]];
            }
        });

        $this->providerEmpty = new FieldProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return null;
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'fields' => [
                        'add' => [],
                        'delete' => [],
                    ],
                ]] ;
            }
        });
    }

    public function testSave()
    {
        $entity = Field::create()
            ->setName('Название')
            ->setType(Field::TEXT)
            ->setElementType(ElementType::CONTACT)
        ;

        $fieldSaved = $this->provider->save($entity);
        $this->assertInstanceOf(Fields::class, $fieldSaved);
        $this->assertEquals(1, $fieldSaved->count());

        $this->assertInstanceOf(Fields::class, $this->provider->save($entity));

        $this->assertEquals($this->providerEmpty->save([$entity])->count(), 0);
        $this->assertEquals($this->providerEmpty->save($entity)->count(), 0);
    }

    public function testSaveOne()
    {
        $entity = Field::create()
            ->setName('Название')
            ->setType(Field::TEXT)
            ->setElementType(ElementType::CONTACT)
        ;

        $this->assertInstanceOf(Field::class, $this->provider->saveOne([$entity]));
        $this->assertNull($this->provider->saveOne($entity));
        $entity->setId(null);
        $this->assertInstanceOf(Field::class, $this->provider->saveOne($entity));
        $entity->setId(null);
        $this->assertNull($this->providerEmpty->saveOne([$entity]));
        $this->assertNull($this->providerEmpty->saveOne($entity));
    }

    public function testDelete()
    {
        $entity = Field::create()
            ->setId(111)
            ->setOrigin(Field::generateOrigin(1, 1, 1))
        ;

        $fieldSaved = $this->provider->delete($entity);
        $this->assertInstanceOf(Fields::class, $fieldSaved);
        $this->assertEquals(1, $fieldSaved->count());

        $this->assertInstanceOf(Fields::class, $this->provider->delete($entity));

        $this->assertEquals($this->providerEmpty->delete([$entity])->count(), 0);
        $this->assertEquals($this->providerEmpty->delete($entity)->count(), 0);
    }

    public function testDeleteOne()
    {
        $entity = Field::create()
            ->setId(1111)
            ->setName('Название')
            ->setType(Field::TEXT)
            ->setElementType(ElementType::CONTACT)
        ;

        $this->assertInstanceOf(Field::class, $this->provider->deleteOne([$entity]));
        $this->assertInstanceOf(Field::class, $this->provider->deleteOne($entity));

        $this->assertNull($this->providerEmpty->deleteOne([$entity]));
        $this->assertNull($this->providerEmpty->deleteOne($entity));
    }
}