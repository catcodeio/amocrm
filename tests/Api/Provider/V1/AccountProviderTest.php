<?php

use Amocrm\Api\Client\RestClientInterface;
use Amocrm\Api\Model\Account\CustomField;
use Amocrm\Api\Model\Account\Group;
use Amocrm\Api\Model\Account\Pipeline;
use Amocrm\Api\Model\Account\User;
use Amocrm\Api\Model\Account\Users;
use Amocrm\Api\Provider\V1\AccountProvider;
use PHPUnit\Framework\TestCase;

class AccountProviderTest extends TestCase
{
    /**
     * @var AccountProvider
     */
    private $accountProvider;

    public function setUp()
    {
        $this->accountProvider = new AccountProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'account'     => [
                        'id'                      => '19040293',
                        'name'                    => 'CatCode',
                        'subdomain'               => 'catcode',
                        'currency'                => 'RUB',
                        'timezone'                => 'Europe/Moscow',
                        'language'                => 'ru',
                        'notifications_base_url'  => 'https://notifications.amocrm.ru',
                        'notifications_ws_url'    => 'wss://notifications.amocrm.ru',
                        'notifications_ws_url_v2' => 'wss://ws.amocrm.ru/catcode/v2/rtm?stand=v3',
                        'amojo_base_url'          => 'https://amojo.amocrm.ru',
                        'amojo_rights'            =>
                            [
                                'can_direct'       => true,
                                'can_group_create' => true,
                            ],
                        'current_user'            => 2291701,
                        'version'                 => 3,
                        'date_pattern'            => 'd.m.Y H:i',
                        'short_date_pattern'      =>
                            [
                                'date'      => 'd.m.Y',
                                'time'      => 'H:i',
                                'date_time' => 'd.m.Y H:i',
                            ],
                        'date_format'             => 'd.m.Y',
                        'time_format'             => 'H:i:s',
                        'country'                 => 'ru',
                        'unsorted_on'             => 'Y',
                        'mobile_feature_version'  => 0,
                        'customers_enabled'       => NULL,
                        'loss_reasons_enabled'    => true,
                        'limits'                  =>
                            [
                                'users_count'        => false,
                                'contacts_count'     => false,
                                'active_deals_count' => false,
                            ],
                        'users'                   =>
                            [
                                [
                                    'id'                    => '2291701',
                                    'mail_admin'            => 'A',
                                    'name'                  => 'Генадий Владимирович',
                                    'last_name'             => NULL,
                                    'login'                 => 'qwer@catcode.io',
                                    'photo_url'             => '/upload/main/4fa/photo_2018-03-15_14-45-56.jpg',
                                    'phone_number'          => '12344',
                                    'language'              => 'ru',
                                    'active'                => true,
                                    'is_admin'              => 'Y',
                                    'unsorted_access'       => 'Y',
                                    'catalogs_access'       => 'Y',
                                    'group_id'              => 0,
                                    'rights_lead_add'       => 'A',
                                    'rights_lead_view'      => 'A',
                                    'rights_lead_edit'      => 'A',
                                    'rights_lead_delete'    => 'A',
                                    'rights_lead_export'    => 'A',
                                    'rights_contact_add'    => 'A',
                                    'rights_contact_view'   => 'A',
                                    'rights_contact_edit'   => 'A',
                                    'rights_contact_delete' => 'A',
                                    'rights_contact_export' => 'A',
                                    'rights_company_add'    => 'A',
                                    'rights_company_view'   => 'A',
                                    'rights_company_edit'   => 'A',
                                    'rights_company_delete' => 'A',
                                    'rights_company_export' => 'A',
                                    'rights_task_edit'      => 'A',
                                    'rights_task_delete'    => 'A',
                                    'free_user'             => false,
                                ],
                                [
                                    'id'                    => '2267143',
                                    'mail_admin'            => 'A',
                                    'name'                  => 'CatCode',
                                    'last_name'             => NULL,
                                    'login'                 => 'support@catcode.io',
                                    'photo_url'             => '/upload/main/4fa/photo_2018-03-15_14-45-56.jpg',
                                    'phone_number'          => '',
                                    'language'              => 'ru',
                                    'active'                => true,
                                    'is_admin'              => 'Y',
                                    'unsorted_access'       => 'Y',
                                    'catalogs_access'       => 'Y',
                                    'group_id'              => 1,
                                    'rights_lead_add'       => 'A',
                                    'rights_lead_view'      => 'A',
                                    'rights_lead_edit'      => 'A',
                                    'rights_lead_delete'    => 'A',
                                    'rights_lead_export'    => 'A',
                                    'rights_contact_add'    => 'A',
                                    'rights_contact_view'   => 'A',
                                    'rights_contact_edit'   => 'A',
                                    'rights_contact_delete' => 'A',
                                    'rights_contact_export' => 'A',
                                    'rights_company_add'    => 'A',
                                    'rights_company_view'   => 'A',
                                    'rights_company_edit'   => 'A',
                                    'rights_company_delete' => 'A',
                                    'rights_company_export' => 'A',
                                    'rights_task_edit'      => 'A',
                                    'rights_task_delete'    => 'A',
                                    'free_user'             => false,
                                ],
                            ],
                        'groups'                  =>
                            [
                                [
                                    'id'   => 0,
                                    'name' => 'Отдел продаж',
                                ],
                                [
                                    'id'   => 1,
                                    'name' => 'Отдел перепродаж',
                                ],
                            ],
                        'leads_statuses'          =>
                            [
                                0 =>
                                    [
                                        'id'          => 19040299,
                                        'name'        => 'Первичный контакт',
                                        'pipeline_id' => 1064215,
                                        'sort'        => 10,
                                        'color'       => '#99ccff',
                                        'editable'    => 'Y',
                                    ],
                                1 =>
                                    [
                                        'id'          => 19040302,
                                        'name'        => 'Переговоры',
                                        'pipeline_id' => 1064215,
                                        'sort'        => 20,
                                        'color'       => '#ffff99',
                                        'editable'    => 'Y',
                                    ],
                                2 =>
                                    [
                                        'id'          => 19040305,
                                        'name'        => 'Принимают решение',
                                        'pipeline_id' => 1064215,
                                        'sort'        => 30,
                                        'color'       => '#ffcc66',
                                        'editable'    => 'Y',
                                    ],
                                3 =>
                                    [
                                        'id'          => 19040308,
                                        'name'        => 'Согласование договора',
                                        'pipeline_id' => 1064215,
                                        'sort'        => 40,
                                        'color'       => '#ffcccc',
                                        'editable'    => 'Y',
                                    ],
                                4 =>
                                    [
                                        'id'          => 142,
                                        'name'        => 'Успешно реализовано',
                                        'pipeline_id' => 1064215,
                                        'sort'        => 10000,
                                        'color'       => '#CCFF66',
                                        'editable'    => 'N',
                                    ],
                                5 =>
                                    [
                                        'id'          => 143,
                                        'name'        => 'Закрыто и не реализовано',
                                        'pipeline_id' => 1064215,
                                        'sort'        => 11000,
                                        'color'       => '#D5D8DB',
                                        'editable'    => 'N',
                                    ],
                            ],
                        'custom_fields'           =>
                            [
                                1937        =>
                                    [
                                        0 =>
                                            [
                                                'id'       => '198925',
                                                'name'     => 'Артикул',
                                                'code'     => 'SKU',
                                                'multiple' => 'N',
                                                'type_id'  => '1',
                                                'disabled' => '0',
                                                'sort'     => 1,
                                            ],
                                        1 =>
                                            [
                                                'id'       => '198927',
                                                'name'     => 'Количество',
                                                'code'     => 'QUANTITY',
                                                'multiple' => 'N',
                                                'type_id'  => '2',
                                                'disabled' => '0',
                                                'sort'     => 2,
                                            ],
                                        2 =>
                                            [
                                                'id'       => '198929',
                                                'name'     => 'Цена',
                                                'code'     => 'PRICE',
                                                'multiple' => 'N',
                                                'type_id'  => '2',
                                                'disabled' => '0',
                                                'sort'     => 3,
                                            ],
                                    ],
                                'contacts'  =>
                                    [
                                        0 =>
                                            [
                                                'id'       => '123903',
                                                'name'     => 'Должность',
                                                'code'     => 'POSITION',
                                                'multiple' => 'N',
                                                'type_id'  => '1',
                                                'disabled' => '0',
                                                'sort'     => 2,
                                            ],
                                        1 =>
                                            [
                                                'id'       => '123905',
                                                'name'     => 'Телефон',
                                                'code'     => 'PHONE',
                                                'multiple' => 'Y',
                                                'type_id'  => '8',
                                                'disabled' => '0',
                                                'sort'     => 4,
                                                'enums'    =>
                                                    [
                                                        256775 => 'WORK',
                                                        256777 => 'WORKDD',
                                                        256779 => 'MOB',
                                                        256781 => 'FAX',
                                                        256783 => 'HOME',
                                                        256785 => 'OTHER',
                                                    ],
                                            ],
                                        2 =>
                                            [
                                                'id'       => '123907',
                                                'name'     => 'Email',
                                                'code'     => 'EMAIL',
                                                'multiple' => 'Y',
                                                'type_id'  => '8',
                                                'disabled' => '0',
                                                'sort'     => 6,
                                                'enums'    =>
                                                    [
                                                        256787 => 'WORK',
                                                        256789 => 'PRIV',
                                                        256791 => 'OTHER',
                                                    ],
                                            ],
                                        3 =>
                                            [
                                                'id'       => '123911',
                                                'name'     => 'Мгн. сообщения',
                                                'code'     => 'IM',
                                                'multiple' => 'Y',
                                                'type_id'  => '8',
                                                'disabled' => '0',
                                                'sort'     => 10,
                                                'enums'    =>
                                                    [
                                                        256793 => 'SKYPE',
                                                        256795 => 'ICQ',
                                                        256797 => 'JABBER',
                                                        256799 => 'GTALK',
                                                        256801 => 'MSN',
                                                        256803 => 'OTHER',
                                                    ],
                                            ],
                                    ],
                                'leads'     => [
                                    [
                                        'id'       => '125623',
                                        'name'     => 'Client ID',
                                        'code'     => '',
                                        'multiple' => 'N',
                                        'type_id'  => '1',
                                        'disabled' => '0',
                                        'sort'     => 501,
                                    ],
                                    [
                                        'id'       => '199323',
                                        'name'     => 'Тестовый список',
                                        'code'     => '',
                                        'multiple' => 'N',
                                        'type_id'  => '4',
                                        'disabled' => '0',
                                        'sort'     => 502,
                                        'enums'    =>
                                            [
                                                406825 => 'Вариант 1',
                                                406827 => 'Вариант 2',
                                                406829 => 'Вариант 3',
                                            ],
                                    ],
                                    [
                                        'id'       => '199325',
                                        'name'     => 'Тестовый мультисписок',
                                        'code'     => '',
                                        'multiple' => 'N',
                                        'type_id'  => '5',
                                        'disabled' => '0',
                                        'sort'     => 503,
                                        'enums'    =>
                                            [
                                                406831 => 'Вариант 1',
                                                406833 => 'Вариант 2',
                                                406835 => 'Вариант 3',
                                                406837 => 'Вариант 4',
                                            ],
                                    ],
                                    [
                                        'id'       => '199327',
                                        'name'     => 'Тестовый переключатель',
                                        'code'     => '',
                                        'multiple' => 'N',
                                        'type_id'  => '10',
                                        'disabled' => '0',
                                        'sort'     => 504,
                                        'enums'    =>
                                            [
                                                406839 => 'Вариант 1',
                                                406841 => 'Вариант 2',
                                                406843 => 'Вариант 3',
                                            ],
                                    ],
                                    [
                                        'id'       => '199329',
                                        'name'     => 'Тестовый флаг',
                                        'code'     => '',
                                        'multiple' => 'N',
                                        'type_id'  => '3',
                                        'disabled' => '0',
                                        'sort'     => 505,
                                    ],
                                    [
                                        'id'       => '199331',
                                        'name'     => 'Тестовый адрес (только API)',
                                        'code'     => '',
                                        'multiple' => 'N',
                                        'type_id'  => '13',
                                        'disabled' => '0',
                                        'sort'     => 506,
                                        'subtypes' =>
                                            [
                                                1 =>
                                                    [
                                                        'id'    => 1,
                                                        'title' => 'Адрес',
                                                        'name'  => 'address_line_1',
                                                    ],
                                                2 =>
                                                    [
                                                        'id'    => 2,
                                                        'title' => 'Адрес - продолжение',
                                                        'name'  => 'address_line_2',
                                                    ],
                                                3 =>
                                                    [
                                                        'id'    => 3,
                                                        'title' => 'Город',
                                                        'name'  => 'city',
                                                    ],
                                                4 =>
                                                    [
                                                        'id'    => 4,
                                                        'title' => 'Регион',
                                                        'name'  => 'state',
                                                    ],
                                                5 =>
                                                    [
                                                        'id'    => 5,
                                                        'title' => 'Индекс',
                                                        'name'  => 'zip',
                                                    ],
                                                6 =>
                                                    [
                                                        'id'    => 6,
                                                        'title' => 'Страна',
                                                        'name'  => 'country',
                                                    ],
                                            ],
                                    ],
                                    [
                                        'id'       => '199345',
                                        'name'     => 'Тестовый день рождения (обязательное)',
                                        'code'     => '',
                                        'multiple' => 'N',
                                        'type_id'  => '14',
                                        'disabled' => '0',
                                        'sort'     => 507,
                                    ],
                                    [
                                        'id'       => '206417',
                                        'name'     => 'Тестовая ссылка',
                                        'code'     => '',
                                        'multiple' => 'N',
                                        'type_id'  => '7',
                                        'disabled' => '0',
                                        'sort'     => 508,
                                    ],
                                    [
                                        'id'       => '217963',
                                        'name'     => 'Тестовый текст',
                                        'code'     => '',
                                        'multiple' => 'N',
                                        'type_id'  => '1',
                                        'disabled' => '0',
                                        'sort'     => 509,
                                    ],
                                    [
                                        'id'       => '217965',
                                        'name'     => 'Тестовое число',
                                        'code'     => '',
                                        'multiple' => 'N',
                                        'type_id'  => '2',
                                        'disabled' => '0',
                                        'sort'     => 510,
                                    ],
                                    [
                                        'id'       => '217981',
                                        'name'     => 'Тестовая дата',
                                        'code'     => '',
                                        'multiple' => 'N',
                                        'type_id'  => '6',
                                        'disabled' => '0',
                                        'sort'     => 511,
                                    ],
                                    [
                                        'id'       => '217983',
                                        'name'     => 'Тестовая текстовая область',
                                        'code'     => '',
                                        'multiple' => 'N',
                                        'type_id'  => '9',
                                        'disabled' => '0',
                                        'sort'     => 512,
                                    ],
                                    [
                                        'id'       => '217997',
                                        'name'     => 'Тестовый короткий адрес',
                                        'code'     => '',
                                        'multiple' => 'N',
                                        'type_id'  => '11',
                                        'disabled' => '0',
                                        'sort'     => 513,
                                    ],
                                ],
                                'companies' =>
                                    [
                                        0 =>
                                            [
                                                'id'       => '123905',
                                                'name'     => 'Телефон',
                                                'code'     => 'PHONE',
                                                'multiple' => 'Y',
                                                'type_id'  => '8',
                                                'disabled' => '0',
                                                'sort'     => 4,
                                                'enums'    =>
                                                    [
                                                        256775 => 'WORK',
                                                        256777 => 'WORKDD',
                                                        256779 => 'MOB',
                                                        256781 => 'FAX',
                                                        256783 => 'HOME',
                                                        256785 => 'OTHER',
                                                    ],
                                            ],
                                        1 =>
                                            [
                                                'id'       => '123907',
                                                'name'     => 'Email',
                                                'code'     => 'EMAIL',
                                                'multiple' => 'Y',
                                                'type_id'  => '8',
                                                'disabled' => '0',
                                                'sort'     => 6,
                                                'enums'    =>
                                                    [
                                                        256787 => 'WORK',
                                                        256789 => 'PRIV',
                                                        256791 => 'OTHER',
                                                    ],
                                            ],
                                        2 =>
                                            [
                                                'id'       => '123909',
                                                'name'     => 'Web',
                                                'code'     => 'WEB',
                                                'multiple' => 'N',
                                                'type_id'  => '7',
                                                'disabled' => '0',
                                                'sort'     => 8,
                                            ],
                                        3 =>
                                            [
                                                'id'       => '123913',
                                                'name'     => 'Адрес',
                                                'code'     => 'ADDRESS',
                                                'multiple' => 'N',
                                                'type_id'  => '9',
                                                'disabled' => '0',
                                                'sort'     => 12,
                                            ],
                                    ],
                                'customers' =>
                                    [
                                    ],
                            ],
                        'note_types'              =>
                            [
                                0  =>
                                    [
                                        'id'       => 1,
                                        'name'     => '',
                                        'code'     => 'DEAL_CREATED',
                                        'editable' => 'N',
                                    ],
                                1  =>
                                    [
                                        'id'       => 2,
                                        'name'     => '',
                                        'code'     => 'CONTACT_CREATED',
                                        'editable' => 'N',
                                    ],
                                2  =>
                                    [
                                        'id'       => 3,
                                        'name'     => '',
                                        'code'     => 'DEAL_STATUS_CHANGED',
                                        'editable' => 'N',
                                    ],
                                3  =>
                                    [
                                        'id'       => 4,
                                        'name'     => '',
                                        'code'     => 'COMMON',
                                        'editable' => 'Y',
                                    ],
                                4  =>
                                    [
                                        'id'       => 5,
                                        'name'     => '',
                                        'code'     => 'ATTACHEMENT',
                                        'editable' => 'N',
                                    ],
                                5  =>
                                    [
                                        'id'       => 6,
                                        'name'     => '',
                                        'code'     => 'CALL',
                                        'editable' => 'N',
                                    ],
                                6  =>
                                    [
                                        'id'       => 7,
                                        'name'     => '',
                                        'code'     => 'MAIL_MESSAGE',
                                        'editable' => 'N',
                                    ],
                                7  =>
                                    [
                                        'id'       => 8,
                                        'name'     => '',
                                        'code'     => 'MAIL_MESSAGE_ATTACHMENT',
                                        'editable' => 'N',
                                    ],
                                8  =>
                                    [
                                        'id'       => 9,
                                        'name'     => '',
                                        'code'     => 'EXTERNAL_ATTACH',
                                        'editable' => 'N',
                                    ],
                                9  =>
                                    [
                                        'id'       => 10,
                                        'name'     => '',
                                        'code'     => 'CALL_IN',
                                        'editable' => 'N',
                                    ],
                                10 =>
                                    [
                                        'id'       => 11,
                                        'name'     => '',
                                        'code'     => 'CALL_OUT',
                                        'editable' => 'N',
                                    ],
                                11 =>
                                    [
                                        'id'       => 12,
                                        'name'     => '',
                                        'code'     => 'COMPANY_CREATED',
                                        'editable' => 'N',
                                    ],
                                12 =>
                                    [
                                        'id'       => 13,
                                        'name'     => '',
                                        'code'     => 'TASK_RESULT',
                                        'editable' => 'N',
                                    ],
                                13 =>
                                    [
                                        'id'       => 17,
                                        'name'     => '',
                                        'code'     => 'CHAT',
                                        'editable' => 'N',
                                    ],
                                14 =>
                                    [
                                        'id'       => 99,
                                        'name'     => '',
                                        'code'     => 'MAX_SYSTEM',
                                        'editable' => 'N',
                                    ],
                                15 =>
                                    [
                                        'id'       => 101,
                                        'name'     => 'Ссылка',
                                        'code'     => 'DROPBOX',
                                        'editable' => 'N',
                                    ],
                                16 =>
                                    [
                                        'id'       => 102,
                                        'name'     => 'Входящее',
                                        'code'     => 'SMS_IN',
                                        'editable' => 'N',
                                    ],
                                17 =>
                                    [
                                        'id'       => 103,
                                        'name'     => 'Исходящее',
                                        'code'     => 'SMS_OUT',
                                        'editable' => 'N',
                                    ],
                            ],
                        'task_types'              =>
                            [
                                0 =>
                                    [
                                        'id'   => 1,
                                        'name' => 'Связаться с клиентом',
                                        'code' => 'FOLLOW_UP',
                                    ],
                                1 =>
                                    [
                                        'id'   => 1,
                                        'name' => 'Звонок',
                                        'code' => 'CALL',
                                    ],
                                2 =>
                                    [
                                        'id'   => 2,
                                        'name' => 'Встреча',
                                        'code' => 'MEETING',
                                    ],
                                3 =>
                                    [
                                        'id'   => 3,
                                        'name' => 'Письмо',
                                        'code' => 'LETTER',
                                    ],
                            ],
                        'pipelines'               =>
                            [
                                1064215 =>
                                    [
                                        'id'       => 1064215,
                                        'value'    => 1064215,
                                        'label'    => 'Воронка',
                                        'name'     => 'Воронка',
                                        'sort'     => 1,
                                        'is_main'  => true,
                                        'statuses' =>
                                            [
                                                142      =>
                                                    [
                                                        'id'          => 142,
                                                        'name'        => 'Успешно реализовано',
                                                        'color'       => '#CCFF66',
                                                        'sort'        => 10000,
                                                        'editable'    => 'N',
                                                        'pipeline_id' => 1064215,
                                                    ],
                                                143      =>
                                                    [
                                                        'id'          => 143,
                                                        'name'        => 'Закрыто и не реализовано',
                                                        'color'       => '#D5D8DB',
                                                        'sort'        => 11000,
                                                        'editable'    => 'N',
                                                        'pipeline_id' => 1064215,
                                                    ],
                                                19040299 =>
                                                    [
                                                        'id'          => 19040299,
                                                        'name'        => 'Первичный контакт',
                                                        'pipeline_id' => 1064215,
                                                        'sort'        => 10,
                                                        'color'       => '#99ccff',
                                                        'editable'    => 'Y',
                                                    ],
                                                19040302 =>
                                                    [
                                                        'id'          => 19040302,
                                                        'name'        => 'Переговоры',
                                                        'pipeline_id' => 1064215,
                                                        'sort'        => 20,
                                                        'color'       => '#ffff99',
                                                        'editable'    => 'Y',
                                                    ],
                                                19040305 =>
                                                    [
                                                        'id'          => 19040305,
                                                        'name'        => 'Принимают решение',
                                                        'pipeline_id' => 1064215,
                                                        'sort'        => 30,
                                                        'color'       => '#ffcc66',
                                                        'editable'    => 'Y',
                                                    ],
                                                19040308 =>
                                                    [
                                                        'id'          => 19040308,
                                                        'name'        => 'Согласование договора',
                                                        'pipeline_id' => 1064215,
                                                        'sort'        => 40,
                                                        'color'       => '#ffcccc',
                                                        'editable'    => 'Y',
                                                    ],
                                            ],
                                        'leads'    => 7,
                                    ],
                                11111111 =>
                                    [
                                        'id'       => 11111111,
                                        'value'    => 11111111,
                                        'label'    => 'Привет',
                                        'name'     => 'Привет',
                                        'sort'     => 2,
                                        'is_main'  => false,
                                        'statuses' =>
                                            [
                                                142      =>
                                                    [
                                                        'id'          => 142,
                                                        'name'        => 'Успешно реализовано',
                                                        'color'       => '#CCFF66',
                                                        'sort'        => 10000,
                                                        'editable'    => 'N',
                                                        'pipeline_id' => 11111111,
                                                    ],
                                                143      =>
                                                    [
                                                        'id'          => 143,
                                                        'name'        => 'Закрыто и не реализовано',
                                                        'color'       => '#D5D8DB',
                                                        'sort'        => 11000,
                                                        'editable'    => 'N',
                                                        'pipeline_id' => 11111111,
                                                    ],
                                                11140299 =>
                                                    [
                                                        'id'          => 11140299,
                                                        'name'        => 'Первичный контакт',
                                                        'pipeline_id' => 11111111,
                                                        'sort'        => 10,
                                                        'color'       => '#99ccff',
                                                        'editable'    => 'Y',
                                                    ],
                                                11140302 =>
                                                    [
                                                        'id'          => 11140302,
                                                        'name'        => 'Переговоры',
                                                        'pipeline_id' => 11111111,
                                                        'sort'        => 20,
                                                        'color'       => '#ffff99',
                                                        'editable'    => 'Y',
                                                    ],
                                                11140305 =>
                                                    [
                                                        'id'          => 11140305,
                                                        'name'        => 'Принимают решение',
                                                        'pipeline_id' => 11111111,
                                                        'sort'        => 30,
                                                        'color'       => '#ffcc66',
                                                        'editable'    => 'Y',
                                                    ],
                                                11140308 =>
                                                    [
                                                        'id'          => 11140308,
                                                        'name'        => 'Согласование договора',
                                                        'pipeline_id' => 11111111,
                                                        'sort'        => 40,
                                                        'color'       => '#ffcccc',
                                                        'editable'    => 'Y',
                                                    ],
                                            ],
                                        'leads'    => 7,
                                    ],
                            ],
                        'timezoneoffset'          => '+03:00',
                    ],
                    'server_time' => 1523111710,
                ]];
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return [];
            }
        });
    }

    public function testProperty()
    {
        $this->assertEquals($this->accountProvider->getId(), 19040293);
        $this->assertEquals($this->accountProvider->getTimezone(), 'Europe/Moscow');
        $this->assertEquals($this->accountProvider->getCurrency(), 'RUB');
        $this->assertEquals($this->accountProvider->getTimezoneoffset(), '+03:00');
        $this->assertInstanceOf(Users::class, $this->accountProvider->getUsers());
    }

    public function testProxy()
    {
        $this->assertInstanceOf(User::class, $this->accountProvider->getUser(2291701));
        $this->assertInstanceOf(CustomField::class, $this->accountProvider->getCustomField(123903));
        $this->assertInstanceOf(Pipeline::class, $this->accountProvider->getPipeline(1064215));
        $this->assertInstanceOf(Group::class, $this->accountProvider->getGroup(1));

        $this->assertNull($this->accountProvider->getUser(99));
        $this->assertNull($this->accountProvider->getCustomField(99));
        $this->assertNull($this->accountProvider->getPipeline(99));
        $this->assertNull($this->accountProvider->getGroup(99));
    }
}