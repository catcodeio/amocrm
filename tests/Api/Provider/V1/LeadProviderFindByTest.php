<?php

use Amocrm\Api\AmocrmApi;
use Amocrm\Api\Model\Lead;
use Amocrm\Api\Model\Leads;
use PHPUnit\Framework\TestCase;

class LeadProviderFindByTest extends TestCase
{
    /**
     * @var AmocrmApi
     */
    private $amocrmApi;

    public function setUp()
    {
        $this->amocrmApi = new AmocrmApi(new class extends RestClientCommon
        {
            public $params;

            public function get(string $url, array $params = [], array $headers = [])
            {
                if (strpos($url, 'leads/list') !== false) {
                    $this->params = $params;

                    return ['response' => ['leads' => [
                        [
                            'id' => 1234,
                            'custom_fields' => [
                                [
                                    'id'     => '123905',
                                    'name'   => 'текст',
                                    'values' => [
                                        [
                                            'value' => 'qqqqq',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '11112',
                                    'name'   => 'текст',
                                    'values' => [
                                        [
                                            'value' => 'asdf',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'id' => 4321,
                            'custom_fields' => [
                                [
                                    'id'     => '123905',
                                    'name'   => 'текст',
                                    'values' => [
                                        [
                                            'value' => 'qqqqq',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '11112',
                                    'name'   => 'текст',
                                    'values' => [
                                        [
                                            'value' => '3333',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ]]];
                } elseif (strpos($url, 'contacts/links') !== false) {
                    return ['response' => [
                        'links'    => [
                            [
                                'contact_id'    => '111',
                                'lead_id'       => '333',
                                'last_modified' => 1523574064,
                            ],
                            [
                                'contact_id'    => '222',
                                'lead_id'       => '444',
                                'last_modified' => 1523574064,
                            ],
                        ],
                        'server_time' => 1523111710,
                    ]];
                }

                return [];
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return [];
            }
        }, AmocrmApi::VERSION_1);
    }

    public function testFindByContacts()
    {
        $leads = $this->amocrmApi->lead()->findByContacts([111, 222]);

        $this->assertInstanceOf(Leads::class, $leads);

        $this->assertEquals(['333', '444'], $this->amocrmApi->client()->params['id']);
    }

    public function testFindOneByContacts()
    {
        $this->assertInstanceOf(Lead::class, $this->amocrmApi->lead()->findOneByContacts([111, 222]));

        $this->assertEquals(['333', '444'], $this->amocrmApi->client()->params['id']);
    }

    public function testFindByField()
    {
        $leads = $this->amocrmApi->lead()->findByField(11112, '3333');

        $this->assertInstanceOf(Leads::class, $leads);
        $this->assertEquals(1, $leads->count());
        $this->assertInstanceOf(Lead::class, $leads->first());
        $this->assertEquals(4321, $leads->first()->getId());

        $leads = $this->amocrmApi->lead()->findByField(11112, '1111121212');
        $this->assertEquals(0, $leads->count());
    }

    public function testFindOneByField()
    {
        $lead = $this->amocrmApi->lead()->findOneByField(123905, 'qqqqq');

        $this->assertInstanceOf(Lead::class, $lead);
        $this->assertEquals(1234, $lead->getId());

        $this->assertNull($this->amocrmApi->lead()->findOneByField(11112, '1111121212'));
    }
}