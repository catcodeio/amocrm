<?php

use Amocrm\Api\Client\RestClientToken;
use Amocrm\Api\Model\Account\TaskType;
use Amocrm\Api\Provider\V1\TaskProvider;
use Amocrm\Api\Model\Task;
use PHPUnit\Framework\TestCase;

/**
 * Проверяет отправляемые в API данные на соответствие документации.
 */
class TaskProviderRequestTest extends TestCase
{
    /**
     * @var RestClientToken
     */
    private $client;

    /**
     * @var TaskProvider
     */
    private $provider;

    public function setUp()
    {
        $this->client       = new class('domain', 'login', 'token') extends RestClientToken
        {
            public $mockEndpoint;
            public $mockMethod;
            public $mockParams;
            public $mockHeaders;

            protected function requestCurl(string $endpoint, string $method = self::METHOD_GET, array $params = [], array $headers = [])
            {
                $this->mockEndpoint = $endpoint;
                $this->mockMethod   = $method;
                $this->mockParams   = $params;
                $this->mockHeaders  = $headers;

                return ['response' => ['tasks' => []]];
            }
        };
        $this->provider = new TaskProvider($this->client);
    }

    public function testCreate()
    {
        $this->assertInstanceOf(Task::class, $this->provider->create());
    }

    public function testList()
    {
        $this->provider->list();

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/tasks/list?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals('GET', $this->client->mockMethod);
        $this->assertEquals([], $this->client->mockParams);
        $this->assertEquals(['Content-Type: application/json'], $this->client->mockHeaders);
    }

    public function testListFull()
    {
        $ifModifiedSince = new DateTime('now');

        $this->provider->list('contact', 2345, 1111, $ifModifiedSince, 1, 2, 3456);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/tasks/list?USER_LOGIN=login&USER_HASH=token&type=contact&element_id=2345&responsible_user_id=1111&limit_rows=1&limit_offset=2&id=3456'
        );
        $this->assertEquals('GET', $this->client->mockMethod);
        $this->assertEquals([], $this->client->mockParams);
        $this->assertEquals([
            'Content-Type: application/json',
            'if-modified-since: ' . $ifModifiedSince->format('D, d M Y H:i:s'),
        ], $this->client->mockHeaders);
    }

    public function testSetAdd()
    {
        $completeTill = new DateTime('30.12.2013');

        $entity = Task::create()
            ->setText('Название')
            ->setLeadId(1234)
            ->setTaskType(TaskType::MEETING)
            ->setCompleteTill($completeTill)
        ;

        $this->provider->save($entity);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/tasks/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['tasks']['add'][0]));

        $data = $this->client->mockParams['request']['tasks']['add'][0];

        $this->assertTrue(isset($data['last_modified']));
        $this->assertEquals('Название', $data['text']);
        $this->assertEquals('1234', $data['element_id']);
        $this->assertEquals('2', $data['element_type']);
        $this->assertEquals($completeTill->format('U'), $data['complete_till']);
        $this->assertEquals(TaskType::MEETING, $data['task_type']);

        $this->provider->save($entity->setCustomerId(333));

        $data = $this->client->mockParams['request']['tasks']['add'][0];

        $this->assertEquals('333', $data['element_id']);
        $this->assertEquals('12', $data['element_type']);

        $this->provider->save($entity->setContactId(111));

        $data = $this->client->mockParams['request']['tasks']['add'][0];

        $this->assertEquals('111', $data['element_id']);
        $this->assertEquals('1', $data['element_type']);

        $this->provider->save($entity->setCompanyId(222));

        $data = $this->client->mockParams['request']['tasks']['add'][0];

        $this->assertEquals('222', $data['element_id']);
        $this->assertEquals('3', $data['element_type']);
    }

    public function testSetUpdate()
    {
        $entity = Task::create([
            'id'                  => '1219851',
            'element_id'          => '1031821',
            'element_type'        => '2',
            'task_type'           => '55555',
            'date_create'         => 1523649717,
            'created_user_id'     => '2291701',
            'last_modified'       => 1523649734,
            'text'                => 'текст задачи',
            'responsible_user_id' => '2291701',
            'complete_till'       => 1523739540,
            'status'              => '1',
            'group_id'            => 0,
            'account_id'          => '19040293',
            'result'              => [
                'id'   => 23331379,
                'text' => 'результат задачи',
            ],
        ]);
        $completeTill = new DateTime('now');
        $entity
            ->setTaskType(TaskType::FOLLOW_UP)
            ->setText('новый текст')
            ->setCompleteTill($completeTill)
        ;

        $this->provider->save($entity);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/tasks/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['tasks']['update'][0]));

        $data = $this->client->mockParams['request']['tasks']['update'][0];

        $this->assertTrue(isset($data['last_modified']));
        $this->assertEquals(TaskType::FOLLOW_UP, $data['task_type']);
        $this->assertEquals(1219851, $data['id']);
        $this->assertEquals('новый текст', $data['text']);
        $this->assertEquals($completeTill->format('U'), $data['complete_till']);
    }
}