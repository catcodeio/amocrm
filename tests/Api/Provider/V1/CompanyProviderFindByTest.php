<?php

use Amocrm\Api\AmocrmApi;
use Amocrm\Api\Model\Companies;
use Amocrm\Api\Model\Company;
use PHPUnit\Framework\TestCase;

// class AmocrmApiMockCompanyProviderFindByTest extends AmocrmApi
// {
//     public function client()
//     {
//         return $this->client;
//     }
//
//     public function __construct()
//     {
//         $this->client =
//     }
// }

class CompanyProviderFindByTest extends TestCase
{
    /**
     * @var AmocrmApi
     */
    private $amocrmApi;

    public function setUp()
    {
        $client = new class extends RestClientCommon
        {
            public $params;

            public function get(string $url, array $params = [], array $headers = [])
            {
                if (strpos($url, 'contacts') !== false) {
                    return ['response' => [
                        'contacts'    => [
                            [
                                'id'                => 1234,
                                'name'              => 'контакт 1',
                                'linked_company_id' => 111,
                            ],
                            [
                                'id'                => 1235,
                                'name'              => 'контакт 2',
                                'linked_company_id' => 222,
                            ],
                        ],
                        'server_time' => 1523111710,
                    ]];
                } elseif (strpos($url, 'leads') !== false) {
                    return ['response' => [
                        'leads'       => [
                            [
                                'id'                => 1234,
                                'name'              => 'сделка 1',
                                'linked_company_id' => 444,
                            ],
                            [
                                'id'                => 1235,
                                'name'              => 'сделка 2',
                                'linked_company_id' => 555,
                            ],
                        ],
                        'server_time' => 1523111710,
                    ]];
                } else {
                    $this->params = $params;

                    return ['response' => ['contacts' => [
                        [
                            'id'                => 1,
                            'name'              => 'компания 1 (находим по телефону)',
                            'custom_fields' => [
                                [
                                    'id'     => '123907',
                                    'name'   => 'текст',
                                    'values' => [
                                        [
                                            'value' => '79991112233',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '123905',
                                    'name'   => 'Телефон',
                                    'code'   => 'PHONE',
                                    'values' => [
                                        [
                                            'value' => '79009090911',
                                            'enum'  => '256775',
                                        ],
                                        [
                                            'value' => '79991112233',
                                            'enum'  => '256779',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'id'                => 2,
                            'name'              => 'компания 2 (находим по мылу)',
                            'custom_fields' => [
                                [
                                    'id'     => '123907',
                                    'name'   => 'Email',
                                    'code'   => 'EMAIL',
                                    'values' => [
                                        [
                                            'value' => 'test@mail.ru',
                                            'enum'  => '256787',
                                        ],
                                        [
                                            'value' => 'test11@mail.ru',
                                            'enum'  => '256787',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '123905',
                                    'name'   => 'текст',
                                    'values' => [
                                        [
                                            'value' => 'test11@mail.ru',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '11112',
                                    'name'   => 'текст',
                                    'values' => [
                                        [
                                            'value' => 'asdf',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ]]];
                }
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return [];
            }
        };

        $this->amocrmApi = new AmocrmApi($client, AmocrmApi::VERSION_1);
    }

    public function testFindByContacts()
    {
        $companies = $this->amocrmApi->company()->findByContacts([1234, 1235]);

        $this->assertInstanceOf(Companies::class, $companies);

        $this->assertEquals(['111', '222'], $this->amocrmApi->client()->params['id']);
    }

    public function testFindOneByContacts()
    {
        $companies = $this->amocrmApi->company()->findOneByContacts([1234, 1235]);

        $this->assertInstanceOf(Company::class, $companies);

        $this->assertEquals(['111', '222'], $this->amocrmApi->client()->params['id']);
    }

    public function testFindByLeads()
    {
        $companies = $this->amocrmApi->company()->findByLeads([1234, 1235]);

        $this->assertInstanceOf(Companies::class, $companies);

        $this->assertEquals(['444', '555'], $this->amocrmApi->client()->params['id']);
    }

    public function testFindOneByLeads()
    {
        $entities = $this->amocrmApi->company()->findOneByLeads([1234, 1235]);

        $this->assertInstanceOf(Company::class, $entities);

        $this->assertEquals(['444', '555'], $this->amocrmApi->client()->params['id']);
    }

    public function testFindByPhone()
    {
        $companies = $this->amocrmApi->company()->findByPhone('79991112233');

        $this->assertInstanceOf(Companies::class, $companies);
        $this->assertEquals(1, $companies->count());
        $this->assertInstanceOf(Company::class, $companies->first());
        $this->assertEquals(1, $companies->first()->getId());

        $companies = $this->amocrmApi->company()->findByPhone('1111121212');
        $this->assertEquals(0, $companies->count());
    }

    public function testFindOneByPhone()
    {
        $this->assertInstanceOf(Company::class, $this->amocrmApi->company()->findOneByPhone('79991112233'));

        $this->assertNull($this->amocrmApi->company()->findOneByPhone('1111121212'));
    }

    public function testFindByField()
    {
        $companies = $this->amocrmApi->company()->findByField(11112, 'asdf');

        $this->assertInstanceOf(Companies::class, $companies);
        $this->assertEquals(1, $companies->count());
        $this->assertInstanceOf(Company::class, $companies->first());
        $this->assertEquals(2, $companies->first()->getId());

        $companies = $this->amocrmApi->company()->findByField(11112, '1111121212');
        $this->assertEquals(0, $companies->count());
    }

    public function testFindOneByField()
    {
        $this->assertInstanceOf(Company::class, $this->amocrmApi->company()->findOneByField(11112, 'asdf'));

        $this->assertNull($this->amocrmApi->company()->findOneByField(11112, '1111121212'));
    }

    public function testFindByEmail()
    {
        $companies = $this->amocrmApi->company()->findByEmail('test11@mail.ru');

        $this->assertInstanceOf(Companies::class, $companies);
        $this->assertEquals(1, $companies->count());
        $this->assertInstanceOf(Company::class, $companies->first());
        $this->assertEquals(2, $companies->first()->getId());

        $companies = $this->amocrmApi->company()->findByPhone('tsss@mail.ru');
        $this->assertEquals(0, $companies->count());
    }

    public function testFindOneByEmail()
    {
        $this->assertInstanceOf(Company::class, $this->amocrmApi->company()->findOneByEmail('test11@mail.ru'));

        $this->assertNull($this->amocrmApi->company()->findOneByEmail('tsss@mail.ru'));
    }
}