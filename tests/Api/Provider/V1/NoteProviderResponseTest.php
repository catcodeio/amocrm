<?php

use Amocrm\Api\Client\RestClientInterface;
use Amocrm\Api\Model\Note;
use Amocrm\Api\Model\Notes;
use Amocrm\Api\Provider\V1\NoteProvider;
use PHPUnit\Framework\TestCase;

class NoteProviderResponseTest extends TestCase
{
    /**
     * @var NoteProvider
     */
    private $provider;

    /**
     * @var NoteProvider
     */
    private $providerEmpty;

    public function setUp()
    {
        $this->provider = new NoteProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'notes'     => [
                        [
                            'id'                  => '7667733',
                            'element_id'          => '1031821',
                            'element_type'        => '2',
                            'note_type'           => 1,
                            'date_create'         => 1522415760,
                            'created_user_id'     => '2291701',
                            'last_modified'       => 1522415760,
                            'text'                => 'Добавлен новый объект',
                            'responsible_user_id' => '2291701',
                            'account_id'          => '19040293',
                            'ATTACHEMENT'         => '',
                            'group_id'            => 0,
                            'editable'            => 'N',
                        ],
                        [
                            'id'                  => '23348893',
                            'element_id'          => '1031821',
                            'element_type'        => '2',
                            'note_type'           => 4,
                            'date_create'         => 1523662593,
                            'created_user_id'     => '2291701',
                            'last_modified'       => 1523662593,
                            'text'                => 'Текст примечания',
                            'responsible_user_id' => '2291701',
                            'account_id'          => '19040293',
                            'ATTACHEMENT'         => '',
                            'group_id'            => 0,
                            'editable'            => 'Y',
                        ],
                        [
                            'id'                  => '23348897',
                            'element_id'          => '1031821',
                            'element_type'        => '2',
                            'note_type'           => 3,
                            'date_create'         => 1523662613,
                            'created_user_id'     => '2291701',
                            'last_modified'       => 1523662613,
                            'text'                => '{"STATUS_NEW":"19040305","STATUS_OLD":"19040299","TEXT":"Lead stage changed","PIPELINE_ID_OLD":1064215,"PIPELINE_ID_NEW":1064215,"LOSS_REASON_ID":0}',
                            'responsible_user_id' => '2291701',
                            'account_id'          => '19040293',
                            'ATTACHEMENT'         => '',
                            'group_id'            => 0,
                            'editable'            => 'N',
                        ],
                    ],
                    'server_time' => 1523111710,
                ]];
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'notes' => [
                        'add' => [
                            [
                                'id'         => 3655494,
                                'request_id' => 0,
                            ],
                        ],
                    ],
                ]];
            }
        });

        $this->providerEmpty = new NoteProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return null;
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['response' => [
                    'notes' => [
                        'add' => [],
                    ],
                ]];
            }
        });
    }

    public function testGet()
    {
        $entities = $this->provider->get(1031821, 'lead');
        $this->assertInstanceOf(Notes::class, $entities);
        $this->assertInstanceOf(Note::class, $entities->first());

        $this->assertInstanceOf(Notes::class, $this->provider->get([1031821, 1111111], 'lead'));

        $entities = $this->providerEmpty->get(1031821, 'lead');
        $this->assertInstanceOf(Notes::class, $entities);
        $this->assertEquals(0, $entities->count());

        $entities = $this->providerEmpty->get([1031821, 1111111], 'lead');
        $this->assertInstanceOf(Notes::class, $entities);
        $this->assertEquals(0, $entities->count());
    }

    public function testFind()
    {
        $this->assertInstanceOf(Notes::class, $this->provider->find('contact', null, new DateTime()));
        $this->assertInstanceOf(Notes::class, $this->providerEmpty->find('contact', null, new DateTime()));
    }

    public function testList()
    {
        $this->assertInstanceOf(Notes::class, $this->provider->list('contact', null, new DateTime(), null, null, null));
        $this->assertInstanceOf(Notes::class, $this->providerEmpty->list('contact', null, new DateTime(), null, null, null));
    }

    public function testSave()
    {
        $noteSaved = $this->provider->save([Note::create()]);
        $this->assertInstanceOf(Notes::class, $noteSaved);
        $this->assertEquals(1, $noteSaved->count());

        $this->assertInstanceOf(Notes::class, $this->provider->save(Note::create()));

        $this->assertEquals($this->providerEmpty->save([Note::create()])->count(), 0);
        $this->assertEquals($this->providerEmpty->save(Note::create())->count(), 0);
    }

    public function testSaveOne()
    {
        $this->assertInstanceOf(Note::class, $this->provider->saveOne([Note::create()]));
        $this->assertInstanceOf(Note::class, $this->provider->saveOne(Note::create()));

        $this->assertNull($this->providerEmpty->saveOne([Note::create()]));
        $this->assertNull($this->providerEmpty->saveOne(Note::create()));
    }
}