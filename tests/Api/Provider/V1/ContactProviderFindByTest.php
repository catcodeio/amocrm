<?php

use Amocrm\Api\AmocrmApi;
use Amocrm\Api\Client\RestClientInterface;
use Amocrm\Api\Model\Contacts;
use Amocrm\Api\Model\Contact;
use PHPUnit\Framework\TestCase;

class ContactProviderFindByTest extends TestCase
{
    /**
     * @var AmocrmApi
     */
    private $amocrmApi;

    public function setUp()
    {
        $this->amocrmApi = new AmocrmApi(new class extends RestClientCommon
        {
            public $params;

            public function get(string $url, array $params = [], array $headers = [])
            {
                if (strpos($url, 'contacts/list') !== false) {
                    $this->params = $params;

                    return ['response' => ['contacts' => [
                        [
                            'id'                => 1,
                            'name'              => 'компания 1 (находим по телефону)',
                            'custom_fields' => [
                                [
                                    'id'     => '123907',
                                    'name'   => 'текст',
                                    'values' => [
                                        [
                                            'value' => '79991112233',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '123905',
                                    'name'   => 'Телефон',
                                    'code'   => 'PHONE',
                                    'values' => [
                                        [
                                            'value' => '79009090911',
                                            'enum'  => '256775',
                                        ],
                                        [
                                            'value' => '79991112233',
                                            'enum'  => '256779',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'id'                => 2,
                            'name'              => 'компания 2 (находим по мылу)',
                            'custom_fields' => [
                                [
                                    'id'     => '123907',
                                    'name'   => 'Email',
                                    'code'   => 'EMAIL',
                                    'values' => [
                                        [
                                            'value' => 'test@mail.ru',
                                            'enum'  => '256787',
                                        ],
                                        [
                                            'value' => 'test11@mail.ru',
                                            'enum'  => '256787',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '123905',
                                    'name'   => 'текст',
                                    'values' => [
                                        [
                                            'value' => 'test11@mail.ru',
                                        ],
                                    ],
                                ],
                                [
                                    'id'     => '11112',
                                    'name'   => 'текст',
                                    'values' => [
                                        [
                                            'value' => 'asdf',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ]]];
                } elseif (strpos($url, 'contacts/links') !== false) {
                    return ['response' => [
                        'links'    => [
                            [
                                'contact_id'    => '111',
                                'lead_id'       => '333',
                                'last_modified' => 1523574064,
                            ],
                            [
                                'contact_id'    => '222',
                                'lead_id'       => '444',
                                'last_modified' => 1523574064,
                            ],
                        ],
                        'server_time' => 1523111710,
                    ]];
                }

                return [];
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return [];
            }
        }, AmocrmApi::VERSION_1);
    }

    public function testFindByLeads()
    {
        $contacts = $this->amocrmApi->contact()->findByLeads([333, 444]);

        $this->assertInstanceOf(Contacts::class, $contacts);

        $this->assertEquals(['111', '222'], $this->amocrmApi->client()->params['id']);
    }

    public function testFindOneByLeads()
    {
        $entities = $this->amocrmApi->contact()->findOneByLeads([333, 444]);

        $this->assertInstanceOf(Contact::class, $entities);

        $this->assertEquals(['111', '222'], $this->amocrmApi->client()->params['id']);
    }

    public function testFindByPhone()
    {
        $companies = $this->amocrmApi->contact()->findByPhone('79991112233');

        $this->assertInstanceOf(Contacts::class, $companies);
        $this->assertEquals(1, $companies->count());
        $this->assertInstanceOf(Contact::class, $companies->first());
        $this->assertEquals(1, $companies->first()->getId());

        $companies = $this->amocrmApi->contact()->findByPhone('1111121212');
        $this->assertEquals(0, $companies->count());
    }

    public function testFindOneByPhone()
    {
        $this->assertInstanceOf(Contact::class, $this->amocrmApi->contact()->findOneByPhone('79991112233'));

        $this->assertNull($this->amocrmApi->contact()->findOneByPhone('1111121212'));
    }

    public function testFindByField()
    {
        $contacts = $this->amocrmApi->contact()->findByField(11112, 'asdf');

        $this->assertInstanceOf(Contacts::class, $contacts);
        $this->assertEquals(1, $contacts->count());
        $this->assertInstanceOf(Contact::class, $contacts->first());
        $this->assertEquals(2, $contacts->first()->getId());

        $contacts = $this->amocrmApi->contact()->findByField(11112, '1111121212');
        $this->assertEquals(0, $contacts->count());
    }

    public function testFindOneByField()
    {
        $this->assertInstanceOf(Contact::class, $this->amocrmApi->contact()->findOneByField(11112, 'asdf'));

        $this->assertNull($this->amocrmApi->contact()->findOneByField(11112, '1111121212'));
    }

    public function testFindByEmail()
    {
        $companies = $this->amocrmApi->contact()->findByEmail('test11@mail.ru');

        $this->assertInstanceOf(Contacts::class, $companies);
        $this->assertEquals(1, $companies->count());
        $this->assertInstanceOf(Contact::class, $companies->first());
        $this->assertEquals(2, $companies->first()->getId());

        $companies = $this->amocrmApi->contact()->findByPhone('tsss@mail.ru');
        $this->assertEquals(0, $companies->count());
    }

    public function testFindOneByEmail()
    {
        $this->assertInstanceOf(Contact::class, $this->amocrmApi->contact()->findOneByEmail('test11@mail.ru'));

        $this->assertNull($this->amocrmApi->contact()->findOneByEmail('tsss@mail.ru'));
    }
}