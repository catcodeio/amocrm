<?php

use Amocrm\Api\Client\RestClientToken;
use Amocrm\Api\Provider\V1\WebhookProvider;
use Amocrm\Api\Model\Webhook;
use PHPUnit\Framework\TestCase;

/**
 * Проверяет отправляемые в API данные на соответствие документации.
 */
class WebhookProviderRequestTest extends TestCase
{
    /**
     * @var RestClientToken
     */
    private $client;

    /**
     * @var WebhookProvider
     */
    private $provider;

    public function setUp()
    {
        $this->client       = new class('domain', 'login', 'token') extends RestClientToken
        {
            public $mockEndpoint;
            public $mockMethod;
            public $mockParams;
            public $mockHeaders;

            protected function requestCurl(string $endpoint, string $method = self::METHOD_GET, array $params = [], array $headers = [])
            {
                $this->mockEndpoint = $endpoint;
                $this->mockMethod   = $method;
                $this->mockParams   = $params;
                $this->mockHeaders  = $headers;

                return ['response' => ['webhooks' => []]];
            }
        };
        $this->provider = new WebhookProvider($this->client);
    }

    public function testCreate()
    {
        $this->assertInstanceOf(Webhook::class, $this->provider->create());
    }

    public function testList()
    {
        $this->provider->list();

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/webhooks/list?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals('GET', $this->client->mockMethod);
        $this->assertEquals([], $this->client->mockParams);
        $this->assertEquals(['Content-Type: application/json'], $this->client->mockHeaders);
    }

    public function testListFull()
    {
        $ifModifiedSince = new DateTime('now');

        $this->provider->list('https://asdf.as', ['responsible_contact', 'restore_company'], 1111);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/webhooks/list?USER_LOGIN=login&USER_HASH=token&url=https%3A%2F%2Fasdf.as&events%5B0%5D=responsible_contact&events%5B1%5D=restore_company&id=1111'
        );
        $this->assertEquals('GET', $this->client->mockMethod);
        $this->assertEquals([], $this->client->mockParams);
        $this->assertEquals([
            'Content-Type: application/json',
        ], $this->client->mockHeaders);
    }

    public function testSubscribe()
    {
        $entity = Webhook::create()
            ->setUrl('https://site.ru')
            ->setEvents([
                Webhook::ADD_COMPANY,
                Webhook::UPDATE_COMPANY,
            ])
        ;

        $this->provider->subscribe($entity);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/webhooks/subscribe?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['webhooks']['subscribe'][0]));

        $data = $this->client->mockParams['request']['webhooks']['subscribe'][0];

        $this->assertEquals('https://site.ru', $data['url']);
        $this->assertEquals([
            'add_company',
            'update_company',
        ], $data['events']);
    }

    public function testUnsubscribe()
    {
        $entity = Webhook::create()
            ->setUrl('https://site.ru')
            ->setEvents([
                Webhook::ADD_COMPANY,
                Webhook::UPDATE_COMPANY,
            ])
        ;

        $this->provider->unsubscribe($entity);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/webhooks/unsubscribe?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['webhooks']['unsubscribe'][0]));

        $data = $this->client->mockParams['request']['webhooks']['unsubscribe'][0];

        $this->assertEquals('https://site.ru', $data['url']);
        $this->assertEquals([
            'add_company',
            'update_company',
        ], $data['events']);
    }
}