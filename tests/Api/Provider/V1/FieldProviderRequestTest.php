<?php

use Amocrm\Api\Client\RestClientToken;
use Amocrm\Api\Helper\ElementType;
use Amocrm\Api\Model\Account\CustomField;
use Amocrm\Api\Model\CustomField\Field;
use Amocrm\Api\Provider\V1\FieldProvider;
use PHPUnit\Framework\TestCase;

/**
 * Проверяет отправляемые в API данные на соответствие документации.
 */
class FieldProviderRequestTest extends TestCase
{
    /**
     * @var RestClientToken
     */
    private $client;

    /**
     * @var FieldProvider
     */
    private $provider;

    public function setUp()
    {
        $this->client       = new class('domain', 'login', 'token') extends RestClientToken
        {
            public $mockEndpoint;
            public $mockMethod;
            public $mockParams;
            public $mockHeaders;

            protected function requestCurl(string $endpoint, string $method = self::METHOD_GET, array $params = [], array $headers = [])
            {
                $this->mockEndpoint = $endpoint;
                $this->mockMethod   = $method;
                $this->mockParams   = $params;
                $this->mockHeaders  = $headers;

                return ['response' => ['fields' => []]];
            }
        };
        $this->provider = new FieldProvider($this->client);
    }

    public function testCreate()
    {
        $this->assertInstanceOf(Field::class, $this->provider->create());
    }

    public function testAdd()
    {
        $entity = Field::create()
            ->setName('Название')
            ->setType(Field::TEXT)
            ->setElementType(ElementType::LEAD)
        ;

        $this->provider->save($entity);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/fields/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['fields']['add'][0]));

        $this->assertEquals([
            'name'         => 'Название',
            'type'         => '1',
            'element_type' => '2',
            'origin'       => Field::generateOrigin('Название', Field::TEXT, ElementType::LEAD),
        ], $this->client->mockParams['request']['fields']['add'][0]);
    }

    public function testDelete()
    {
        $origin = Field::generateOrigin('Название', Field::TEXT, ElementType::LEAD);

        $entity = $this
            ->provider
            ->create()
            ->setId(111)
            ->setOrigin($origin)
        ;

        $this->provider->delete($entity);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/fields/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['fields']['delete'][0]));

        $this->assertEquals([
            'id' => '111',
            'origin' => $origin,
        ], $this->client->mockParams['request']['fields']['delete'][0]);
    }

    public function testDeleteFromCustomField()
    {
        $origin = Field::generateOrigin('Название', Field::TEXT, ElementType::CONTACT);

        $entity = $this->provider->createFromCustomField(CustomField::create([
            'id'           => 1234,
            'name'         => 'Название',
            'type_id'      => 1,
            'element_type' => 1,
        ]));

        $this->provider->delete($entity);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/fields/set?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json']);

        $this->assertTrue(isset($this->client->mockParams['request']['fields']['delete'][0]));

        $this->assertEquals([
            'id'           => '1234',
            'name'         => 'Название',
            'origin'       => $origin,
            'element_type' => 1,
            'type'         => 1,
        ], $this->client->mockParams['request']['fields']['delete'][0]);
    }
}