<?php

use Amocrm\Api\Client\RestClientToken;
use Amocrm\Api\Provider\V2\WidgetProvider;
use Amocrm\Api\Model\Widget;
use PHPUnit\Framework\TestCase;

class WidgetProviderRequestTest extends TestCase
{
    /**
     * @var RestClientToken
     */
    private $clientSave;

    /**
     * @var RestClientToken
     */
    private $clientList;

    /**
     * @var WidgetProvider
     */
    private $providerSave;

    /**
     * @var WidgetProvider
     */
    private $providerList;

    public function setUp()
    {
        $this->clientSave = new class('domain', 'login', 'token') extends RestClientToken
        {
            public $mockEndpoint;
            public $mockMethod;
            public $mockParams;
            public $mockHeaders;

            protected function requestCurl(string $endpoint, string $method = self::METHOD_GET, array $params = [], array $headers = [])
            {
                $this->mockEndpoint = $endpoint;
                $this->mockMethod   = $method;
                $this->mockParams   = $params;
                $this->mockHeaders  = $headers;

                return ['_embedded' => ['widgets' => []]];
            }
        };

        $this->providerSave = new WidgetProvider($this->clientSave);

        $this->clientList = new class('domain', 'login', 'token') extends RestClientToken
        {
            public $mockEndpoint;
            public $mockMethod;
            public $mockParams;
            public $mockHeaders;

            protected function requestCurl(string $endpoint, string $method = self::METHOD_GET, array $params = [], array $headers = [])
            {
                $this->mockEndpoint = $endpoint;
                $this->mockMethod   = $method;
                $this->mockParams   = $params;
                $this->mockHeaders  = $headers;

                return ['_embedded' => ['items' => []]];
            }
        };

        $this->providerList = new WidgetProvider($this->clientList);
    }

    public function testCreate()
    {
        $this->assertInstanceOf(Widget::class, $this->providerSave->create());
    }

    public function testList()
    {
        $this->providerList->list();

        $this->assertEquals(
            $this->clientList->mockEndpoint,
            'https://domain/api/v2/widgets?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals('GET', $this->clientList->mockMethod);
        $this->assertEquals([], $this->clientList->mockParams);
        $this->assertEquals(['Content-Type: application/json'], $this->clientList->mockHeaders);
    }

    public function testListFull()
    {
        $this->providerList->list('id1', 'code1');

        $this->assertEquals(
            $this->clientList->mockEndpoint,
            'https://domain/api/v2/widgets?USER_LOGIN=login&USER_HASH=token&widget_id=id1&widget_code=code1'
        );
        $this->assertEquals('GET', $this->clientList->mockMethod);
        $this->assertEquals([], $this->clientList->mockParams);
        $this->assertEquals(['Content-Type: application/json'], $this->clientList->mockHeaders);
    }

    public function testOneByCode()
    {
        $this->providerList->getOneByCode('code1');

        $this->assertEquals(
            $this->clientList->mockEndpoint,
            'https://domain/api/v2/widgets?USER_LOGIN=login&USER_HASH=token&widget_code=code1'
        );
        $this->assertEquals('GET', $this->clientList->mockMethod);
        $this->assertEquals([], $this->clientList->mockParams);
        $this->assertEquals(['Content-Type: application/json'], $this->clientList->mockHeaders);
    }

    public function testByCode()
    {
        $this->providerList->getByCode('code1');

        $this->assertEquals(
            $this->clientList->mockEndpoint,
            'https://domain/api/v2/widgets?USER_LOGIN=login&USER_HASH=token&widget_code=code1'
        );
        $this->assertEquals('GET', $this->clientList->mockMethod);
        $this->assertEquals([], $this->clientList->mockParams);
        $this->assertEquals(['Content-Type: application/json'], $this->clientList->mockHeaders);
    }

    public function testById()
    {
        $this->providerList->getById('id1');

        $this->assertEquals(
            $this->clientList->mockEndpoint,
            'https://domain/api/v2/widgets?USER_LOGIN=login&USER_HASH=token&widget_id=id1'
        );
        $this->assertEquals('GET', $this->clientList->mockMethod);
        $this->assertEquals([], $this->clientList->mockParams);
        $this->assertEquals(['Content-Type: application/json'], $this->clientList->mockHeaders);
    }

    public function testOneById()
    {
        $this->providerList->getOneById('id1');

        $this->assertEquals(
            $this->clientList->mockEndpoint,
            'https://domain/api/v2/widgets?USER_LOGIN=login&USER_HASH=token&widget_id=id1'
        );
        $this->assertEquals('GET', $this->clientList->mockMethod);
        $this->assertEquals([], $this->clientList->mockParams);
        $this->assertEquals(['Content-Type: application/json'], $this->clientList->mockHeaders);
    }

    public function testInstall()
    {
        $entity = Widget::create()
            ->setWidgetCode('code1')
            ->setSettings([
                'field1' => 'value1',
            ])
        ;

        $this->providerSave->install($entity);

        $this->assertEquals(
            $this->clientSave->mockEndpoint,
            'https://domain/api/v2/widgets?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals('POST', $this->clientSave->mockMethod);

        $this->assertTrue(isset($this->clientSave->mockParams['widgets']['install'][0]['widget_code']));
        $this->assertTrue(isset($this->clientSave->mockParams['widgets']['install'][0]['settings']['field1']));

        $this->assertEquals('code1', $this->clientSave->mockParams['widgets']['install'][0]['widget_code']);
        $this->assertEquals('value1', $this->clientSave->mockParams['widgets']['install'][0]['settings']['field1']);

        $this->assertEquals(['Content-Type: application/json'], $this->clientSave->mockHeaders);
    }

    public function testUninstall()
    {
        $entity = Widget::create()
            ->setWidgetCode('code1')
            ->setSettings([
                'field1' => 'value1',
            ])
        ;

        $this->providerSave->uninstall($entity);

        $this->assertEquals(
            $this->clientSave->mockEndpoint,
            'https://domain/api/v2/widgets?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals('POST', $this->clientSave->mockMethod);

        $this->assertTrue(isset($this->clientSave->mockParams['widgets']['uninstall'][0]['widget_code']));
        $this->assertTrue(isset($this->clientSave->mockParams['widgets']['uninstall'][0]['settings']['field1']));

        $this->assertEquals('code1', $this->clientSave->mockParams['widgets']['uninstall'][0]['widget_code']);
        $this->assertEquals('value1', $this->clientSave->mockParams['widgets']['uninstall'][0]['settings']['field1']);

        $this->assertEquals(['Content-Type: application/json'], $this->clientSave->mockHeaders);
    }
}