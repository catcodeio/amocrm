<?php

use Amocrm\Api\Model\Widget;
use Amocrm\Api\Model\Widgets;
use Amocrm\Api\Provider\V2\WidgetProvider;
use PHPUnit\Framework\TestCase;

class WidgetProviderResponseTest extends TestCase
{
    /**
     * @var WidgetProvider
     */
    private $provider;

    /**
     * @var WidgetProvider
     */
    private $providerEmpty;

    public function setUp()
    {
        $this->provider = new WidgetProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return ['_embedded' => [
                    'items' => [
                        [
                            'widget_id'         => 742,
                            'widget_code'       => 'amo_dropbox',
                            'settings_template' => [
                                'conf' => [
                                    'name'     => 'settings.custom',
                                    'type'     => 'custom',
                                    'required' => false,
                                ],
                            ],
                            'settings'          => [],
                            'active'            => 0,
                            'path'              => '/widgets/amo_dropbox',
                        ],
                        [
                            'widget_id'         => 495514,
                            'widget_code'       => 'diai06w9nzwjrpilnvzckh5ilztddmgbfvsrduy6',
                            'settings_template' => [
                                'api_key' => [
                                    'name'     => 'settings.api_key',
                                    'type'     => 'text',
                                    'required' => true,
                                ],
                                'oferta'  => [
                                    'name'     => 'settings.oferta',
                                    'type'     => 'custom',
                                    'required' => true,
                                ],
                            ],
                            'settings'          => [],
                            'active'            => 0,
                            'path'              => '/widgets/diai06w9nzwjrpilnvzckh5ilztddmgbfvsrduy6',
                        ],
                    ],
                ]];
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['_embedded' => [
                    'widgets' => [
                        [

                            'id'   => 742,
                            'code' => 'amo_dropbox',
                        ],
                    ],
                ]];
            }
        });

        $this->providerEmpty = new WidgetProvider(new class extends RestClientCommon
        {
            public function get(string $url, array $params = [], array $headers = [])
            {
                return null;
            }

            public function post(string $url, array $params = [], array $headers = [])
            {
                return ['_embedded' => [
                    'errors' => [
                        'install' => 'Not found in widgets list',
                    ],
                    'widgets' => [],
                ]];
            }
        });
    }

    public function testGet()
    {
        $entities = $this->provider->getById(742);
        $this->assertInstanceOf(Widgets::class, $entities);
        $this->assertInstanceOf(Widget::class, $entities->first());

        $this->assertInstanceOf(Widgets::class, $this->provider->getByCode('amo_dropbox'));
        $this->assertInstanceOf(Widget::class, $this->provider->getOneById(742));
        $this->assertInstanceOf(Widget::class, $this->provider->getOneByCode('amo_dropbox'));

        $entities = $this->providerEmpty->getById(742);
        $this->assertInstanceOf(Widgets::class, $entities);
        $this->assertEquals(0, $entities->count());

        $entities = $this->providerEmpty->getByCode('amo_dropbox');
        $this->assertInstanceOf(Widgets::class, $entities);
        $this->assertEquals(0, $entities->count());

        $this->assertNull($this->providerEmpty->getOneById(742));
        $this->assertNull($this->providerEmpty->getOneByCode('amo_dropbox'));
    }

    public function testList()
    {
        $this->assertInstanceOf(Widgets::class, $this->provider->list(null, null));
        $this->assertInstanceOf(Widgets::class, $this->providerEmpty->list(null, null));
    }

    /**
     * @expectedException \Amocrm\Exception\DataIncorrectException
     */
    public function testSaveDataIncorrectException()
    {
        $this->provider->install(Widget::create());
    }

    public function testSave()
    {
        $widgetWithId   = Widget::create()->setWidgetId(742);
        $widgetWithCode = Widget::create()->setWidgetCode('amo_dropbox');

        $entities = $this->provider->install([$widgetWithId]);
        $this->assertInstanceOf(Widgets::class, $entities);
        $this->assertEquals(1, $entities->count());

        $this->assertInstanceOf(Widgets::class, $this->provider->uninstall($widgetWithCode));

        $this->assertEquals($this->providerEmpty->install([$widgetWithId])->count(), 0);
        $this->assertEquals($this->providerEmpty->uninstall($widgetWithCode)->count(), 0);
    }
}