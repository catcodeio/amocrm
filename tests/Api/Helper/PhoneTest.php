<?php

use PHPUnit\Framework\TestCase;
use Amocrm\Api\Helper\Phone;

class PhoneTest extends TestCase
{
    /**
     * @dataProvider phonesWithoutCodeProvider
     *
     * @param $phone
     * @param $phoneClear
     */
    public function testPhonesWithoutCode($phone, $phoneClear)
    {
        $this->assertEquals($phoneClear, Phone::clear($phone));
    }

    /**
     * @dataProvider phonesWithCodeProvider
     *
     * @param $phone
     * @param $phoneClear
     */
    public function testPhonesWithCode($phone, $phoneClear)
    {
        $this->assertEquals($phoneClear, Phone::clear($phone, true));
    }

    public function phonesWithoutCodeProvider()
    {
        return [
            ['89051234578', '9051234578'],
            ['+7 905 123-45-78', '9051234578'],
            ['+33 (123) 213 23 3', '3123213233'],
            ['123 4567', '1234567'],
            ['1234567', '1234567'],
            ['2059720744', '2059720744'],
            ['asdf', 'asdf'],
            ['+7 (91_) ___-__-__', '791'],
            ['19099009090', '9099009090'],
            ['(229) 430-1808', '2294301808'],
            ['+108 (229) 430-1808', '2294301808'],
            ['+18 (229) 430-1808', '2294301808'],
            ['+1 (229) 430-1808', '2294301808'],
            ['+108 (229) 430-18-08', '2294301808'],

        ];
    }

    public function phonesWithCodeProvider()
    {
        return [
            ['89051234578', '79051234578'],
            ['+7 905 123-45-78', '79051234578'],
            ['+33 (123) 213 23 3', '33123213233'],
            ['123 4567', '1234567'],
            ['1234567', '1234567'],
            ['2059720744', '2059720744'],
            ['asdf', 'asdf'],
            ['+7 (91_) ___-__-__', '791'],
            ['19099009090', '19099009090'],
            ['(229) 430-1808', '2294301808'],
            ['+108 (229) 430-1808', '1082294301808'],
            ['+18 (229) 430-1808', '182294301808'],
            ['+1 (229) 430-1808', '12294301808'],
            ['+108 (229) 430-18-08', '1082294301808'],
        ];
    }
}