<?php

use PHPUnit\Framework\TestCase;
use Amocrm\Api\Helper\ElementType;

class ElementTypeTest extends TestCase
{
    public function testConstant()
    {
        $this->assertEquals('contact', ElementType::CONTACT);
        $this->assertEquals('lead', ElementType::LEAD);
        $this->assertEquals('company', ElementType::COMPANY);
        $this->assertEquals('task', ElementType::TASK);
        $this->assertEquals('customer', ElementType::CUSTOMER);
        $this->assertEquals('element', ElementType::ELEMENT);
    }

    /**
     * @dataProvider pluralProvider
     *
     * @param $type
     * @param $newType
     */
    public function testToPlural($type, $newType)
    {
        $this->assertEquals($newType, ElementType::toPlural($type));
    }

    /**
     * @dataProvider pluralProvider
     *
     * @param $type
     * @param $newType
     */
    public function testFromPlural($type, $newType)
    {
        $this->assertEquals($type, ElementType::fromPlural($newType));
    }

    public function pluralProvider()
    {
        return [
            [ElementType::CONTACT, 'contacts'],
            [ElementType::LEAD, 'leads'],
            [ElementType::COMPANY, 'companies'],
            [ElementType::TASK, 'tasks'],
            [ElementType::CUSTOMER, 'customers'],
            [ElementType::ELEMENT, 'catalog_elements'],
            ['asdf', 'asdf'],
            [1, 1],
            [9999, 9999],
        ];
    }

    /**
     * @dataProvider numberProvider
     *
     * @param $type
     * @param $newType
     */
    public function testToNumber($type, $newType)
    {
        $this->assertEquals($newType, ElementType::toNumber($type));
    }

    /**
     * @dataProvider numberProvider
     *
     * @param $type
     * @param $newType
     */
    public function testFromNumber($type, $newType)
    {
        $this->assertEquals($type, ElementType::fromNumber($newType));
    }

    public function numberProvider()
    {
        return [
            [ElementType::CONTACT, 1],
            [ElementType::LEAD, 2],
            [ElementType::COMPANY, 3],
            [ElementType::TASK, 4],
            [ElementType::CUSTOMER, 12],
            [ElementType::ELEMENT, 5],
            ['asdf', 'asdf'],
            [9999, 9999],
        ];
    }

    public function testToNumberWithoutConstant()
    {
        $this->assertEquals(1, ElementType::toNumber(1));
    }

    /**
     * @dataProvider pluralToNumberProvider
     *
     * @param $type
     * @param $newType
     */
    public function testPluralToNumber($type, $newType)
    {
        $this->assertEquals($newType, ElementType::pluralToNumber($type));
    }

    public function pluralToNumberProvider()
    {
        return [
            ['contacts', 1],
            ['leads', 2],
            ['companies', 3],
            ['tasks', 4],
            ['customers', 12],
            ['catalog_elements', 5],
            ['asdf', 'asdf'],
            [1, 1],
            [9999, 9999],
        ];
    }
}