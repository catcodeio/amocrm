<?php

use PHPUnit\Framework\TestCase;
use Amocrm\Api\Helper\Country;

class CountryTest extends TestCase
{
    /**
     * @dataProvider countriesProvider
     *
     * @param string $result
     * @param string $iata
     * @param string $locale
     */
    public function testCountries($result, $iata, $locale)
    {
        $this->assertEquals($result, Country::getName($iata, $locale));
    }

    /**
     * @dataProvider countriesWithoutLocaleProvider
     *
     * @param string $result
     * @param string $iata
     */
    public function testCountriesWithoutLocale($result, $iata)
    {
        $this->assertEquals($result, Country::getName($iata));
    }

    public function countriesProvider()
    {
        return [
            ['Южно-Африканская Республика', 'za', 'ru'],
            ['Южно-Африканская Республика', 'ZA', 'RU'],
            ['Afghanistan', 'af', 'en'],
            ['Ливан', 'lb', 'aa'],
            [null, 'asdf', 'asdf'],
        ];
    }

    public function countriesWithoutLocaleProvider()
    {
        return [
            ['Алжир', 'DZ'],
            ['Новая Каледония', 'nc'],
            ['Токелау', 'tk'],
            ['Россия', 'ru'],
            [null, 'фыва'],
        ];
    }
}