<?php

use Amocrm\Api\Client\RestClientToken;
use PHPUnit\Framework\TestCase;

class RestClientTokenTest extends TestCase
{
    /**
     * @var RestClientToken
     */
    private $client;

    public function setUp()
    {
        $this->client = new class('domain', 'login', 'token') extends RestClientToken
        {
            public $mockEndpoint;
            public $mockMethod;
            public $mockParams;
            public $mockHeaders;

            protected function requestCurl(string $endpoint, string $method = self::METHOD_GET, array $params = [], array $headers = [])
            {
                $this->mockEndpoint = $endpoint;
                $this->mockMethod   = $method;
                $this->mockParams   = $params;
                $this->mockHeaders  = $headers;

                return [];
            }
        };
    }

    public function testRequestGet()
    {
        $this->client->get('private/api/v2/json/test/url', ['foo' => 2], ['bar: goo']);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/test/url?USER_LOGIN=login&USER_HASH=token&foo=2'
        );
        $this->assertEquals($this->client->mockMethod, 'GET');
        $this->assertEquals($this->client->mockParams, []);
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json', 'bar: goo']);
    }

    public function testRequestPost()
    {
        $this->client->post('private/api/v2/json/test/url', ['foo' => 2], ['bar: goo']);

        $this->assertEquals(
            $this->client->mockEndpoint,
            'https://domain/private/api/v2/json/test/url?USER_LOGIN=login&USER_HASH=token'
        );
        $this->assertEquals($this->client->mockMethod, 'POST');
        $this->assertEquals($this->client->mockParams, ['foo' => 2]);
        $this->assertEquals($this->client->mockHeaders, ['Content-Type: application/json', 'bar: goo']);
    }

    public function testRequestLang()
    {
        $this->client->post('test/url');

        $this->assertEquals($this->client->mockEndpoint, 'https://domain/test/url?USER_LOGIN=login&USER_HASH=token');

        $this->client->setLocale('ru');
        $this->client->post('test/url');

        $this->assertEquals($this->client->mockEndpoint, 'https://domain/test/url?USER_LOGIN=login&USER_HASH=token&lang=ru');

        $this->client->setLocale('en');
        $this->client->post('test/url');

        $this->assertEquals($this->client->mockEndpoint, 'https://domain/test/url?USER_LOGIN=login&USER_HASH=token&lang=en');
    }
}