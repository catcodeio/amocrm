<?php

use Amocrm\Api\AmocrmApi;
use Amocrm\Api\Client\RestClientToken;
use Amocrm\Api\Provider\V1\AccountProvider;
use Amocrm\Api\Provider\V1\CompanyProvider;
use Amocrm\Api\Provider\V1\ContactProvider;
use Amocrm\Api\Provider\V1\CustomerProvider;
use Amocrm\Api\Provider\V1\ElementProvider;
use Amocrm\Api\Provider\V1\LeadProvider;
use Amocrm\Api\Provider\V1\TaskProvider;
use Amocrm\Api\Provider\V1\NoteProvider;
use Amocrm\Api\Provider\V1\FieldProvider;
use Amocrm\Api\Provider\V1\TransactionProvider;
use Amocrm\Api\Provider\V1\WebhookProvider;
use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\CustomField\CustomField;

class AmocrmApiTest extends TestCase
{
    /**
     * @var AmocrmApi
     */
    private $amocrmApi;

    public function setUp()
    {
        $this->amocrmApi = new AmocrmApi(new RestClientToken('asdf', 'asdf', 'asdf'), AmocrmApi::VERSION_1);
    }

    public function testCacheAccountProvider()
    {
        $accountCached = $this->amocrmApi->account();

        $this->assertInstanceOf(AccountProvider::class, $accountCached);
        $this->assertEquals($accountCached, $this->amocrmApi->account());
        $this->assertFalse($accountCached === $this->amocrmApi->account(true));
    }

    public function testCustomField()
    {
        $cf = $this->amocrmApi->cf(5);

        $this->assertInstanceOf(CustomField::class, $cf);
        $this->assertEquals(5, $cf->getId());
    }

    public function testLeadProvider()
    {
        $this->assertInstanceOf(LeadProvider::class, $this->amocrmApi->lead());
    }

    public function testContactProvider()
    {
        $this->assertInstanceOf(ContactProvider::class, $this->amocrmApi->contact());
    }

    public function testCompanyProvider()
    {
        $this->assertInstanceOf(CompanyProvider::class, $this->amocrmApi->company());
    }

    public function testTaskProvider()
    {
        $this->assertInstanceOf(TaskProvider::class, $this->amocrmApi->task());
    }

    public function testNoteProvider()
    {
        $this->assertInstanceOf(NoteProvider::class, $this->amocrmApi->note());
    }

    public function testFieldProvider()
    {
        $this->assertInstanceOf(FieldProvider::class, $this->amocrmApi->field());
    }

    public function testWebhookProvider()
    {
        $this->assertInstanceOf(WebhookProvider::class, $this->amocrmApi->webhook());
    }

    public function testCustomerProvider()
    {
        $this->assertInstanceOf(CustomerProvider::class, $this->amocrmApi->customer());
    }

    public function testTransactionProvider()
    {
        $this->assertInstanceOf(TransactionProvider::class, $this->amocrmApi->transaction());
    }

    public function testElementProvider()
    {
        $this->assertInstanceOf(ElementProvider::class, $this->amocrmApi->element());
    }
}