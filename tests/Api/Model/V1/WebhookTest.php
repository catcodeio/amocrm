<?php

use Amocrm\Api\Helper\ElementType;
use Amocrm\Api\Model\Account\TaskType;
use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Webhook;

class WebhookTest extends TestCase
{
    /**
     * @var Webhook
     */
    private $webhook;

    public function setUp()
    {
        $this->webhook = Webhook::create([
            'id'       => '1219851',
            'url'      => 'https://asdf.ff',
            'actions'  => ['responsible_lead', 'add_lead'],
            'result'   => true,
            'disabled' => false,
        ]);
    }

    public function testConstants()
    {
        $this->assertEquals('responsible_lead', Webhook::RESPONSIBLE_LEAD);
        $this->assertEquals('restore_company', Webhook::RESTORE_COMPANY);
        $this->assertEquals('add_task', Webhook::ADD_TASK);
        $this->assertEquals('update_customer', Webhook::UPDATE_CUSTOMER);
        $this->assertEquals('delete_company', Webhook::DELETE_COMPANY);
        $this->assertEquals('status_lead', Webhook::STATUS_LEAD);
        $this->assertEquals('note_company', Webhook::NOTE_COMPANY);
    }

    public function testPlainProperty()
    {
        $this->assertEquals($this->webhook->getId(), 1219851);
        $this->assertEquals($this->webhook->getUrl(), 'https://asdf.ff');
        $this->assertEquals($this->webhook->getEvents(), ['responsible_lead', 'add_lead']);
        $this->assertEquals($this->webhook->getResult(), true);
        $this->assertEquals($this->webhook->getDisabled(), false);

        $this->webhook->setId(111);
        $this->webhook->setUrl('https://new.ru');
        $this->webhook->setEvents([Webhook::NOTE_COMPANY]);

        $this->assertEquals($this->webhook->getId(), 111);
        $this->assertEquals($this->webhook->getUrl(), 'https://new.ru');
        $this->assertEquals($this->webhook->getEvents(), ['note_company']);
    }

    public function testModify()
    {
        $this->assertEquals($this->webhook->getModifiedForApi(), [
            'url'    => 'https://asdf.ff',
            'events' => ['responsible_lead', 'add_lead'],
        ]);

        $this->webhook->setUrl('https://new.ru');

        $this->assertEquals($this->webhook->getModifiedForApi(), [
            'url'    => 'https://new.ru',
            'events' => ['responsible_lead', 'add_lead'],
        ]);
    }
}