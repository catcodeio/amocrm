<?php

use Amocrm\Api\Model\ContactLinks;
use Amocrm\Api\Model\ContactLink;
use PHPUnit\Framework\TestCase;

class ContactLinksTest extends TestCase
{
    /**
     * @var ContactLinks
     */
    private $contactLinks;

    public function setUp()
    {
        $this->contactLinks = ContactLinks::create(
            [
                [
                    'contact_id'    => '2986625',
                    'lead_id'       => '1031821',
                    'last_modified' => 1523574064,
                ],
                [
                    'contact_id'    => '2986625',
                    'lead_id'       => '1704227',
                    'last_modified' => 1523574064,
                ],
                [
                    'contact_id'    => '2986625',
                    'lead_id'       => '1031619',
                    'last_modified' => 1523574064,
                ],
            ]
        );
    }

    public function testIterates()
    {
        $this->assertInstanceOf(ContactLinks::class, $this->contactLinks);
        $this->assertEquals($this->contactLinks->count(), 3);
        $this->assertEquals($this->contactLinks->key(), 0);

        $this->contactLinks->next();

        $this->assertEquals($this->contactLinks->key(), 1);

        foreach ($this->contactLinks as $contactLink) {
            $this->assertInstanceOf(ContactLink::class, $contactLink);
        }

        $this->assertEquals($this->contactLinks->key(), 1);

        $this->contactLinks->rewind();

        $this->assertEquals($this->contactLinks->key(), 0);

        $lead = $this->contactLinks->current();

        $this->assertInstanceOf(ContactLink::class, $lead);
    }

    public function testGettingIds()
    {
        $this->assertEquals($this->contactLinks->getIdsOfContacts(), [
            2986625, 2986625, 2986625,
        ]);

        $this->assertEquals($this->contactLinks->getIdsOfLeads(), [
            1031821, 1704227, 1031619,
        ]);
    }
}