<?php

use Amocrm\Api\Helper\ElementType;
use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\CustomField\Field;

class FieldTest extends TestCase
{
    /**
     * @var Field
     */
    private $entity;

    public function setUp()
    {
        $this->entity = Field::create([
            'id'           => '1219851',
            'name'         => 'Доп. поле',
            'element_type' => '1',
            'type'         => '2',
            'origin'       => 'asdf',
            'disabled'     => '0',
        ]);
    }

    public function testConstants()
    {
        $this->assertEquals(1, Field::TEXT);
        $this->assertEquals(2, Field::NUMERIC);
        $this->assertEquals(3, Field::CHECKBOX);
        $this->assertEquals(4, Field::SELECT);
        $this->assertEquals(5, Field::MULTISELECT);
        $this->assertEquals(6, Field::DATE);
        $this->assertEquals(7, Field::LINK);
        $this->assertEquals(8, Field::MULTITEXT);
        $this->assertEquals(9, Field::TEXTAREA);
        $this->assertEquals(10, Field::RADIO_BUTTON);
        $this->assertEquals(11, Field::ADDRESS_SHORT);
        $this->assertEquals(13, Field::ADDRESS);
        $this->assertEquals(14, Field::BIRTHDAY);
        $this->assertEquals(15, Field::ENTITY);
    }

    public function testPlainProperty()
    {
        $this->assertEquals($this->entity->getId(), 1219851);
        $this->assertEquals($this->entity->getName(), 'Доп. поле');
        $this->assertEquals($this->entity->getElementType(), ElementType::CONTACT);
        $this->assertEquals($this->entity->getType(), 2);
        $this->assertEquals($this->entity->getOrigin(), 'asdf');
        $this->assertFalse($this->entity->isDisabled());

        $this->entity->setName(111);
        $this->entity->setElementType(222);
        $this->entity->setType(Field::TEXT);
        $this->entity->setOrigin('новый текст');
        $this->entity->setDisabled(true);

        $this->assertEquals($this->entity->getName(), 111);
        $this->assertEquals($this->entity->getElementType(), 222);
        $this->assertEquals($this->entity->getType(), 1);
        $this->assertEquals($this->entity->getOrigin(), 'новый текст');
        $this->assertTrue($this->entity->isDisabled());
    }

    public function testOrigin()
    {
        $this->entity
            ->setOrigin(null)
            ->setName(1)
            ->setType(Field::CHECKBOX)
            ->setElementType(ElementType::COMPANY)
        ;

        $this->assertNull($this->entity->getOrigin());

        $data   = $this->entity->getModifiedForApi();
        $origin = $this->entity::generateOrigin(1, Field::CHECKBOX, ElementType::COMPANY);

        $this->assertEquals($data['origin'], $origin);
    }

    /**
     * @expectedException RuntimeException
     */
    public function testExceptionOriginName()
    {
        $this->entity
            ->setOrigin(null)
            ->setName(null)
        ;

        $this->entity->getModifiedForApi();
    }

    /**
     * @expectedException RuntimeException
     */
    public function testExceptionOriginType()
    {
        $this->entity
            ->setOrigin(null)
            ->setType(null)
        ;

        $this->entity->getModifiedForApi();
    }

    /**
     * @expectedException RuntimeException
     */
    public function testExceptionOriginElementType()
    {
        $this->entity
            ->setOrigin(null)
            ->setElementType(null)
        ;

        $this->entity->getModifiedForApi();
    }
}