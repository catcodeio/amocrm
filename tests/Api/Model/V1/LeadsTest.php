<?php

use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Lead;
use Amocrm\Api\Model\Leads;

class LeadsTest extends TestCase
{
    /**
     * @var Leads
     */
    private $entities;

    public function setUp()
    {
        $this->entities = Leads::create([
            [
                'id'                  => '1031821',
                'name'                => 'Выключен',
                'date_create'         => 1522415760,
                'created_user_id'     => '2291701',
                'last_modified'       => 1523190638,
                'account_id'          => '19040293',
                'price'               => '0',
                'responsible_user_id' => '2291701',
                'linked_company_id'   => '',
                'group_id'            => 0,
                'pipeline_id'         => 1064215,
                'date_close'          => 0,
                'closest_task'        => 1522540804,
                'loss_reason_id'      => 0,
                'deleted'             => 0,
                'tags'                => [],
                'status_id'           => '19040299',
                'custom_fields'       => [
                    [
                        'id'     => '199323',
                        'name'   => 'Тестовый список',
                        'values' => [
                            [
                                'value' => 'Вариант 1',
                                'enum'  => '406825',
                            ],
                        ],
                    ],
                    [
                        'id'     => '199327',
                        'name'   => 'Тестовый переключатель',
                        'values' => [
                            [
                                'value' => 'Вариант 2',
                                'enum'  => '406841',
                            ],
                        ],
                    ],
                    [
                        'id'     => '199329',
                        'name'   => 'Тестовый флаг',
                        'values' => [
                            [
                                'value' => '1',
                            ],
                        ],
                    ],
                    [
                        'id'     => '199345',
                        'name'   => 'Тестовый день рождения (обязательное)',
                        'values' => [
                            [
                                'value' => '2018-04-25 00:00:00',
                            ],
                        ],
                    ],
                    [
                        'id'     => '125623',
                        'name'   => 'Client ID',
                        'values' => [
                            [
                                'value' => '222333444',
                            ],
                        ],
                    ],
                ],
                'main_contact_id'     => 2986625,
            ],
            [
                'id'                  => '1111111',
                'name'                => 'Название сделки',
                'date_create'         => 1522415700,
                'created_user_id'     => '2291701',
                'last_modified'       => 1523190638,
                'account_id'          => '19040293',
                'price'               => '1000',
                'responsible_user_id' => '2291701',
                'linked_company_id'   => '',
                'group_id'            => 0,
                'pipeline_id'         => 1064211,
                'date_close'          => 0,
                'closest_task'        => 1522540804,
                'loss_reason_id'      => 0,
                'deleted'             => 0,
                'tags'                => [
                    [
                        'id'           => 58183,
                        'name'         => 'тестовая',
                        'element_type' => 2,
                    ]
                ],
                'status_id'           => '12341234',
                'custom_fields'       => [
                    [
                        'id'     => '199323',
                        'name'   => 'Тестовый список',
                        'values' => [
                            [
                                'value' => 'Вариант 1',
                                'enum'  => '406825',
                            ],
                        ],
                    ],
                    [
                        'id'     => '199327',
                        'name'   => 'Тестовый переключатель',
                        'values' => [
                            [
                                'value' => 'Вариант 2',
                                'enum'  => '406841',
                            ],
                        ],
                    ],
                    [
                        'id'     => '199329',
                        'name'   => 'Тестовый флаг',
                        'values' => [
                            [
                                'value' => '1',
                            ],
                        ],
                    ],
                    [
                        'id'     => '199345',
                        'name'   => 'Тестовый день рождения (обязательное)',
                        'values' => [
                            [
                                'value' => '2018-04-25 00:00:00',
                            ],
                        ],
                    ],
                    [
                        'id'     => '125623',
                        'name'   => 'Client ID',
                        'values' => [
                            [
                                'value' => '111222333',
                            ],
                        ],
                    ],
                ],
                'main_contact_id'     => 2986625,
            ],
        ]);
    }

    public function testIterates()
    {
        $this->assertInstanceOf(Leads::class, $this->entities);
        $this->assertEquals($this->entities->count(), 2);
        $this->assertEquals([1031821, 1111111], $this->entities->getIds());

        $this->assertEquals($this->entities->key(), 0);

        $this->entities->next();

        $this->assertEquals($this->entities->key(), 1);

        foreach ($this->entities as $lead) {
            $this->assertInstanceOf(Lead::class, $lead);
        }

        $this->assertEquals($this->entities->key(), 1);

        $this->entities->rewind();

        $this->assertEquals($this->entities->key(), 0);

        $lead = $this->entities->current();

        $this->assertInstanceOf(Lead::class, $lead);
    }

    public function testFilterByStatus()
    {
        $filtered = $this->entities->filterByStatusIds(19040299);
        
        $this->assertEquals($filtered->count(), 1);
        $this->assertEquals($filtered->first()->getId(), 1031821);
    }

    public function testFilterByPipeline()
    {
        $filtered = $this->entities->filterByPipelineIds([1064211]);

        $this->assertEquals($filtered->count(), 1);
        $this->assertEquals($filtered->first()->getId(), 1111111);
    }

    public function testIsModify()
    {
        $this->assertFalse($this->entities->isModified());

        $this->entities->map(function (Lead $lead) {
            return $lead->setPrice(11);
        });

        $this->assertTrue($this->entities->isModified());
    }
}