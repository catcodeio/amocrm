<?php

use Amocrm\Api\Helper\ElementType;
use Amocrm\Api\Model\Account\NoteType;
use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Note;

class NoteTest extends TestCase
{
    /**
     * @var Note
     */
    private $entity;

    public function setUp()
    {
        $this->entity = Note::create([
            'id'                  => '23348911',
            'element_id'          => '1031821',
            'element_type'        => '2',
            'note_type'           => 5,
            'date_create'         => 1523662708,
            'created_user_id'     => '2291701',
            'last_modified'       => 1523662708,
            'text'                => 'asdf',
            'responsible_user_id' => '2291701',
            'account_id'          => '19040293',
            'ATTACHEMENT'         => 'qwer.png',
            'group_id'            => 0,
            'editable'            => 'N',
        ]);
    }

    public function testConstants()
    {
        $this->assertEquals(1, NoteType::DEAL_CREATED);
        $this->assertEquals(2, NoteType::CONTACT_CREATED);
        $this->assertEquals(3, NoteType::DEAL_STATUS_CHANGED);
        $this->assertEquals(4, NoteType::COMMON);
        $this->assertEquals(5, NoteType::ATTACHMENT);
        $this->assertEquals(6, NoteType::CALL);
        $this->assertEquals(7, NoteType::MAIL_MESSAGE);
        $this->assertEquals(8, NoteType::MAIL_MESSAGE_ATTACHMENT);
        $this->assertEquals(9, NoteType::EXTERNAL_ATTACH);
        $this->assertEquals(10, NoteType::CALL_IN);
        $this->assertEquals(11, NoteType::CALL_OUT);
        $this->assertEquals(12, NoteType::COMPANY_CREATED);
        $this->assertEquals(13, NoteType::TASK_RESULT);
        $this->assertEquals(17, NoteType::CHAT);
        $this->assertEquals(25, NoteType::SYSTEM);
        $this->assertEquals(99, NoteType::MAX_SYSTEM);
        $this->assertEquals(101, NoteType::DROPBOX);
        $this->assertEquals(102, NoteType::SMS_IN);
        $this->assertEquals(103, NoteType::SMS_OUT);
    }

    public function testPlainProperty()
    {
        $this->assertEquals($this->entity->getId(), 23348911);
        $this->assertEquals($this->entity->getElementId(), 1031821);
        $this->assertEquals($this->entity->getElementType(), ElementType::LEAD);
        $this->assertEquals($this->entity->getNoteType(), NoteType::ATTACHMENT);
        $this->assertEquals($this->entity->getCreatedUserId(), 2291701);
        $this->assertEquals($this->entity->getResponsibleUserId(), 2291701);
        $this->assertEquals($this->entity->getText(), 'asdf');
        $this->assertEquals($this->entity->getAttachment(), 'qwer.png');
        $this->assertEquals($this->entity->getGroupId(), 0);
        $this->assertEquals($this->entity->getAccountId(), 19040293);
        $this->assertFalse($this->entity->isEditable());

        $this->entity->setNoteType(NoteType::COMMON);
        $this->entity->setResponsibleUserId(222);
        $this->entity->setCompanyId(333);
        $this->entity->setText('новый текст');

        $this->assertEquals($this->entity->getNoteType(), 4);
        $this->assertEquals($this->entity->getResponsibleUserId(), 222);
        $this->assertEquals($this->entity->getElementId(), 333);
        $this->assertEquals($this->entity->getElementType(), ElementType::COMPANY);
        $this->assertEquals($this->entity->getText(), 'новый текст');
    }

    public function testDateTime()
    {
        $dateCreate = $this->entity->getDateCreate();
        $this->assertInstanceOf(DateTime::class, $dateCreate);
        $this->assertEquals($dateCreate->format('U'), 1523662708);

        $lastModified = $this->entity->getLastModified();
        $this->assertInstanceOf(DateTime::class, $lastModified);
        $this->assertEquals($lastModified->format('U'), 1523662708);
    }
}