<?php

use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Customer;
use Amocrm\Api\Model\Customers;

class CustomersTest extends TestCase
{
    /**
     * @var Customers
     */
    private $entities;

    public function setUp()
    {
        $this->entities = Customers::create([
            [
                "id"              => 113945,
                "date_create"     => 1534873031,
                "date_modify"     => 1534873031,
                "created_by"      => 2291701,
                "modified_by"     => 2291701,
                "account_id"      => 19040293,
                "main_user_id"    => 2291701,
                "name"            => "Название покупателя",
                "deleted"         => false,
                "next_price"      => 0,
                "periodicity"     => 0,
                "next_date"       => 1533675600,
                "task_last_date"  => null,
                "status_id"       => 126352,
                "ltv"             => null,
                "purchases_count" => null,
                "average_check"   => null,
                "tags"            => [],
                "main_contact_id" => false,
                "period_id"       => "126352",
                'custom_fields'       => [
                    [
                        'id'     => '199323',
                        'name'   => 'Тестовый список',
                        'values' => [
                            [
                                'value' => 'Вариант 1',
                                'enum'  => '406825',
                            ],
                        ],
                    ],
                    [
                        'id'     => '199327',
                        'name'   => 'Тестовый переключатель',
                        'values' => [
                            [
                                'value' => 'Вариант 2',
                                'enum'  => '406841',
                            ],
                        ],
                    ],
                    [
                        'id'     => '199329',
                        'name'   => 'Тестовый флаг',
                        'values' => [
                            [
                                'value' => '1',
                            ],
                        ],
                    ],
                    [
                        'id'     => '199345',
                        'name'   => 'Тестовый день рождения (обязательное)',
                        'values' => [
                            [
                                'value' => '2018-04-25 00:00:00',
                            ],
                        ],
                    ],
                    [
                        'id'     => '125623',
                        'name'   => 'Client ID',
                        'values' => [
                            [
                                'value' => '222333444',
                            ],
                        ],
                    ],
                ],
            ],
            [
                "id"              => 113944,
                "date_create"     => 1534873031,
                "date_modify"     => 1534873031,
                "created_by"      => 2291701,
                "modified_by"     => 2291701,
                "account_id"      => 19040293,
                "main_user_id"    => 2291701,
                "name"            => "Название покупателя",
                "deleted"         => false,
                "next_price"      => 0,
                "periodicity"     => 0,
                "next_date"       => 1533675600,
                "task_last_date"  => null,
                "status_id"       => 126352,
                "ltv"             => null,
                "purchases_count" => null,
                "average_check"   => null,
                "main_contact_id" => false,
                "period_id"       => "126352",
                'tags'                => [
                    [
                        'id'           => 58183,
                        'name'         => 'тестовая',
                        'element_type' => 2,
                    ]
                ],
                'custom_fields'       => [
                    [
                        'id'     => '199323',
                        'name'   => 'Тестовый список',
                        'values' => [
                            [
                                'value' => 'Вариант 1',
                                'enum'  => '406825',
                            ],
                        ],
                    ],
                    [
                        'id'     => '199327',
                        'name'   => 'Тестовый переключатель',
                        'values' => [
                            [
                                'value' => 'Вариант 2',
                                'enum'  => '406841',
                            ],
                        ],
                    ],
                    [
                        'id'     => '199329',
                        'name'   => 'Тестовый флаг',
                        'values' => [
                            [
                                'value' => '1',
                            ],
                        ],
                    ],
                    [
                        'id'     => '199345',
                        'name'   => 'Тестовый день рождения (обязательное)',
                        'values' => [
                            [
                                'value' => '2018-04-25 00:00:00',
                            ],
                        ],
                    ],
                    [
                        'id'     => '125623',
                        'name'   => 'Client ID',
                        'values' => [
                            [
                                'value' => '111222333',
                            ],
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function testIterates()
    {
        $this->assertInstanceOf(Customers::class, $this->entities);
        $this->assertEquals($this->entities->count(), 2);
        $this->assertEquals([113945, 113944], $this->entities->getIds());

        $this->assertEquals($this->entities->key(), 0);

        $this->entities->next();

        $this->assertEquals($this->entities->key(), 1);

        foreach ($this->entities as $lead) {
            $this->assertInstanceOf(Customer::class, $lead);
        }

        $this->assertEquals($this->entities->key(), 1);

        $this->entities->rewind();

        $this->assertEquals($this->entities->key(), 0);

        $lead = $this->entities->current();

        $this->assertInstanceOf(Customer::class, $lead);
    }

    public function testFilterByStatus()
    {
        $filtered = $this->entities->filterByField('id', 113945);
        
        $this->assertEquals($filtered->count(), 1);
        $this->assertEquals($filtered->first()->getId(), 113945);
    }
}