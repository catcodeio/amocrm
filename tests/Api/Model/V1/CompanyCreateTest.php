<?php

use Amocrm\Api\Model\Company;
use PHPUnit\Framework\TestCase;

class CompanyCreateTest extends TestCase
{
    /**
     * @var Company
     */
    private $company;

    public function setUp()
    {
        $this->company = Company::create();
    }

    public function testIterates()
    {
        $this->assertInstanceOf(Company::class, $this->company);
        $this->assertEquals($this->company->getLinkedLeadsId(), null);

        $this->company->addLeadId(222);

        $this->assertEquals($this->company->getLinkedLeadsId(), [222]);

        $this->company->addLeadId(444);
        $this->company->addLeadId(555);

        $this->assertEquals($this->company->getLinkedLeadsId(), [222, 444, 555]);

        $this->company->removeLeadId(444);

        $this->assertEquals($this->company->getLinkedLeadsId(), [222, 555]);

        $this->company->removeLeadId(222);
        $this->company->removeLeadId(555);

        $this->assertEquals($this->company->getLinkedLeadsId(), []);
    }
}