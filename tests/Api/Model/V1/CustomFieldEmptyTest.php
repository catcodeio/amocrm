<?php

use Amocrm\Api\Model\CustomField\CustomField;
use PHPUnit\Framework\TestCase;

class CustomFieldEmptyTest extends TestCase
{
    /**
     * @dataProvider provideValuesFill
     *
     * @param array $data
     */
    public function testIsNotEmpty($data)
    {
        $this->assertFalse(CustomField::create($data)->isEmpty());
    }

    /**
     * @dataProvider provideValuesEmpty
     *
     * @param array $data
     */
    public function testIsEmpty($data)
    {
        $this->assertTrue(CustomField::create($data)->isEmpty());
    }

    public function provideValuesEmpty()
    {
        return [
            [[
                'id'     => '12847',
                'name'   => 'Мгн. сообщения',
                'values' => [[
                    'value' => '',
                ]],
            ]],
            [[
                'id'     => '12847',
                'name'   => 'Мгн. сообщения',
                'values' => [[
                    'value' => null,
                ]],
            ]],
            [[
                'id'     => '12847',
                'name'   => 'Мгн. сообщения',
                'values' => [[
                    'value' => false,
                ]],
            ]],
            [null],
            [[]],
        ];
    }

    public function provideValuesFill()
    {
        return [
            [[
                'id'     => '12847',
                'name'   => 'Мгн. сообщения',
                'values' => [[
                    'value' => 'val',
                ]],
            ]],
            [[
                'id'     => '12847',
                'name'   => 'Мгн. сообщения',
                'values' => [[
                    'value' => '1234',
                ]],
            ]],
            [[
                'id'     => '12847',
                'name'   => 'Мгн. сообщения',
                'values' => [[
                    'value' => '0',
                ]],
            ]],
            [[
                'id'     => '12847',
                'name'   => 'Мгн. сообщения',
                'values' => [[
                    'value' => 0,
                ]],
            ]],
            [[
                'id'     => '12847',
                'name'   => 'Мгн. сообщения',
                'values' => [[
                    'value' => 10,
                ]],
            ]],
            [[
                'id'     => '12847',
                'name'   => 'Мгн. сообщения',
                'values' => [[
                    'value' => true,
                ]],
            ]],
        ];
    }
}