<?php

use Amocrm\Api\Helper\ElementType;
use Amocrm\Api\Model\Account\TaskType;
use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Task;

class TaskTest extends TestCase
{
    /**
     * @var Task
     */
    private $task;

    public function setUp()
    {
        $this->task = Task::create([
            'id'                  => '1219851',
            'element_id'          => '1031821',
            'element_type'        => '2',
            'task_type'           => 'CALL',
            'date_create'         => 1523649717,
            'created_user_id'     => '2291701',
            'last_modified'       => 1523649734,
            'text'                => 'текст задачи',
            'responsible_user_id' => '2291701',
            'complete_till'       => 1523739540,
            'status'              => '1',
            'group_id'            => 0,
            'account_id'          => '19040293',
            'result'              => [
                'id'   => 23331379,
                'text' => 'результат задачи',
            ],
        ]);
    }

    public function testConstants()
    {
        $this->assertEquals('FOLLOW_UP', TaskType::FOLLOW_UP);
        $this->assertEquals('CALL', TaskType::CALL);
        $this->assertEquals('LETTER', TaskType::LETTER);
        $this->assertEquals('MEETING', TaskType::MEETING);
    }

    public function testPlainProperty()
    {
        $this->assertEquals($this->task->getId(), 1219851);
        $this->assertEquals($this->task->getElementId(), 1031821);
        $this->assertEquals($this->task->getElementType(), ElementType::LEAD);
        $this->assertEquals($this->task->getTaskType(), TaskType::CALL);
        $this->assertEquals($this->task->getCreatedUserId(), 2291701);
        $this->assertEquals($this->task->getResponsibleUserId(), 2291701);
        $this->assertEquals($this->task->getText(), 'текст задачи');
        $this->assertEquals($this->task->getStatus(), 1);
        $this->assertEquals($this->task->getGroupId(), 0);
        $this->assertEquals($this->task->getAccountId(), 19040293);
        $this->assertTrue(is_array($this->task->getResult()));
        $this->assertEquals($this->task->getResultText(), 'результат задачи');
        $this->assertTrue($this->task->isClosed());

        $this->task->setTaskType(111);
        $this->task->setResponsibleUserId(222);
        $this->task->setContactId(333);
        $this->task->setText('новый текст');

        $this->assertEquals($this->task->getTaskType(), 111);
        $this->assertEquals($this->task->getResponsibleUserId(), 222);
        $this->assertEquals($this->task->getElementId(), 333);
        $this->assertEquals($this->task->getElementType(), ElementType::CONTACT);
        $this->assertEquals($this->task->getText(), 'новый текст');
    }

    public function testDateTime()
    {
        $dateCreate = $this->task->getDateCreate();
        $this->assertInstanceOf(DateTime::class, $dateCreate);
        $this->assertEquals($dateCreate->format('U'), 1523649717);

        $lastModified = $this->task->getLastModified();
        $this->assertInstanceOf(DateTime::class, $lastModified);
        $this->assertEquals($lastModified->format('U'), 1523649734);

        $completeTill = $this->task->getCompleteTill();
        $this->assertInstanceOf(DateTime::class, $completeTill);
        $this->assertEquals($completeTill->format('U'), 1523739540);
    }
}