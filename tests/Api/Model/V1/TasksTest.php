<?php

use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Task;
use Amocrm\Api\Model\Tasks;

class TasksTest extends TestCase
{
    /**
     * @var Tasks
     */
    private $tasks;

    public function setUp()
    {
        $this->tasks = Tasks::create([
            [
                'id'                  => '353255',
                'element_id'          => '1031821',
                'element_type'        => '2',
                'task_type'           => 'CALL',
                'date_create'         => 1522540805,
                'created_user_id'     => '2291701',
                'last_modified'       => 1522540805,
                'text'                => 'Текст задачи',
                'responsible_user_id' => '2291701',
                'complete_till'       => 1522540804,
                'status'              => '0',
                'group_id'            => 0,
                'account_id'          => '19040293',
                'result'              => [],
            ],
            [
                'id'                  => '349833',
                'element_id'          => '1031821',
                'element_type'        => '2',
                'task_type'           => 'CALL',
                'date_create'         => 1522502465,
                'created_user_id'     => '2291701',
                'last_modified'       => 1522542385,
                'text'                => '',
                'responsible_user_id' => '2291701',
                'complete_till'       => 1522502465,
                'status'              => '1',
                'group_id'            => 0,
                'account_id'          => '19040293',
                'result'              => [],
            ],
            [
                'id'                  => '1219851',
                'element_id'          => '1031821',
                'element_type'        => '2',
                'task_type'           => 'CALL',
                'date_create'         => 1523649717,
                'created_user_id'     => '2291701',
                'last_modified'       => 1523649734,
                'text'                => 'asdfasdf',
                'responsible_user_id' => '2291701',
                'complete_till'       => 1523739540,
                'status'              => '1',
                'group_id'            => 0,
                'account_id'          => '19040293',
                'result'              => [
                    'id'   => 23331379,
                    'text' => 'resulttttttttt1111',
                ],
            ],
        ]);
    }

    public function testIterates()
    {
        $this->assertInstanceOf(Tasks::class, $this->tasks);
        $this->assertEquals($this->tasks->count(), 3);
        $this->assertEquals([353255, 349833, 1219851], $this->tasks->getIds());

        $this->assertEquals($this->tasks->key(), 0);

        $this->tasks->next();

        $this->assertEquals($this->tasks->key(), 1);

        foreach ($this->tasks as $lead) {
            $this->assertInstanceOf(Task::class, $lead);
        }

        $this->assertEquals($this->tasks->key(), 1);

        $this->tasks->rewind();

        $this->assertEquals($this->tasks->key(), 0);

        $lead = $this->tasks->current();

        $this->assertInstanceOf(Task::class, $lead);
    }
}