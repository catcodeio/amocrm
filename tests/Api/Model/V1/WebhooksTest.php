<?php

use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Webhook;
use Amocrm\Api\Model\Webhooks;

class WebhooksTest extends TestCase
{
    /**
     * @var Webhooks
     */
    private $webhooks;

    public function setUp()
    {
        $this->webhooks = Webhooks::create([
            [
                'id'      => '1',
                'url'     => 'https://asdf.ff',
                'actions' => ['responsible_lead', 'add_lead'],
            ],
            [
                'id'      => '2',
                'url'     => 'https://asdf.ff/asdf',
                'actions' => ['update_lead'],
            ],
            [
                'id'      => '3',
                'url'     => 'https://site.dd/dfff?asdf=ff1',
                'actions' => ['delete_company', 'update_company', 'update_contact'],
            ],
        ]);
    }

    public function testIterates()
    {
        $this->assertInstanceOf(Webhooks::class, $this->webhooks);
        $this->assertEquals($this->webhooks->count(), 3);
        $this->assertEquals([1, 2, 3], $this->webhooks->getIds());

        $this->assertEquals($this->webhooks->key(), 0);

        $this->webhooks->next();

        $this->assertEquals($this->webhooks->key(), 1);

        foreach ($this->webhooks as $entity) {
            $this->assertInstanceOf(Webhook::class, $entity);
        }

        $this->assertEquals($this->webhooks->key(), 1);

        $this->webhooks->rewind();

        $this->assertEquals($this->webhooks->key(), 0);

        $entity = $this->webhooks->current();

        $this->assertInstanceOf(Webhook::class, $entity);
    }
}