<?php

use Amocrm\Api\Model\CustomField\CustomField;
use Amocrm\Api\Model\CustomField\CustomFields;
use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Contact;

class ContactTest extends TestCase
{
    /**
     * @var Contact
     */
    private $contact;

    public function setUp()
    {
        $this->contact = Contact::create(
            [
                'id'                  => 2986625,
                'name'                => 'new name',
                'last_modified'       => 1523490191,
                'account_id'          => 19040293,
                'date_create'         => 1522415760,
                'created_user_id'     => 2291701,
                'modified_user_id'    => 2291701,
                'responsible_user_id' => 2291701,
                'group_id'            => 0,
                'closest_task'        => 0,
                'linked_company_id'   => '6251953',
                'company_name'        => 'Новая компания',
                'tags'                => [
                    [
                        'id'           => 75649,
                        'name'         => 'два слова',
                        'element_type' => 1,
                    ],
                    [
                        'id'           => 75651,
                        'name'         => 'одно',
                        'element_type' => 1,
                    ],
                ],
                'type'                => 'contact',
                'custom_fields'       =>
                    [
                        0 =>
                            [
                                'id'     => '123907',
                                'name'   => 'Email',
                                'code'   => 'EMAIL',
                                'values' => [
                                    [
                                        'value' => 'test@mail.ru',
                                        'enum'  => '256787',
                                    ],
                                ],
                            ],
                        1 =>
                            [
                                'id'     => '123905',
                                'name'   => 'Телефон',
                                'code'   => 'PHONE',
                                'values' =>
                                    [
                                        0 =>
                                            [
                                                'value' => '79009090911',
                                                'enum'  => '256775',
                                            ],
                                        1 =>
                                            [
                                                'value' => '54365434655',
                                                'enum'  => '256779',
                                            ],
                                    ],
                            ],
                        2 =>
                            [
                                'id'     => '123903',
                                'name'   => 'Должность',
                                'code'   => 'POSITION',
                                'values' =>
                                    [
                                        0 =>
                                            [
                                                'value' => 'Position1',
                                            ],
                                    ],
                            ],
                        3 =>
                            [
                                'id'     => '123911',
                                'name'   => 'Мгн. сообщения',
                                'code'   => 'IM',
                                'values' =>
                                    [
                                        0 =>
                                            [
                                                'value' => 'asdf',
                                                'enum'  => '256793',
                                            ],
                                        1 =>
                                            [
                                                'value' => 'qqqqq',
                                                'enum'  => '256801',
                                            ],
                                    ],
                            ],
                        4 =>
                            [
                                'id'     => '252239',
                                'name'   => 'Juridical person',
                                'values' => [
                                    [
                                        'value' => [
                                            'name'                         => 'asdf',
                                            'entity_type'                  => NULL,
                                            'vat_id'                       => '123123',
                                            'tax_registration_reason_code' => NULL,
                                            'address'                      => NULL,
                                            'kpp'                          => '1123',
                                            'external_uid'                 => NULL,
                                        ],
                                    ],
                                ],
                            ],
                    ],
                'linked_leads_id'     => ['1031821'],
            ]
        );
    }

    public function testPlainProperty()
    {
        $this->assertEquals($this->contact->getId(), 2986625);
        $this->assertEquals($this->contact->getName(), 'new name');
        $this->assertEquals($this->contact->getModifiedUserId(), 2291701);
        $this->assertEquals($this->contact->getCreatedUserId(), 2291701);
        $this->assertEquals($this->contact->getResponsibleUserId(), 2291701);
        $this->assertEquals($this->contact->getLinkedCompanyId(), 6251953);
        $this->assertEquals($this->contact->getLinkedLeadsId(), ['1031821']);

        $this->contact->setName('Новое название');
        $this->contact->setResponsibleUserId(111);
        $this->contact->setLinkedCompanyId(444);

        $this->assertEquals($this->contact->getName(), 'Новое название');
        $this->assertEquals($this->contact->getResponsibleUserId(), 111);
        $this->assertEquals($this->contact->getLinkedCompanyId(), 444);
    }

    public function testDateTime()
    {
        $dateCreate = $this->contact->getDateCreate();
        $this->assertInstanceOf(DateTime::class, $dateCreate);
        $this->assertEquals($dateCreate->format('U'), 1522415760);

        $lastModified = $this->contact->getLastModified();
        $this->assertInstanceOf(DateTime::class, $lastModified);
        $this->assertEquals($lastModified->format('U'), 1523490191);

        $this->assertNull($this->contact->getClosestTask());
    }

    public function testLinkedLeads()
    {
        $this->assertEquals($this->contact->getLinkedLeadsId(), [1031821]);

        $this->contact
            ->addLeadId(11111)
            ->addLeadId(22222)
        ;

        $this->assertEquals($this->contact->getLinkedLeadsId(), [1031821, 11111, 22222]);


        $this->contact
            ->removeLeadId(1031821)
            ->addLeadId(22222)
            ->addLeadId(55555)
            ->removeLeadId(11111)
        ;

        $this->assertEquals($this->contact->getLinkedLeadsId(), [22222, 55555]);
    }

    public function testCustomFields()
    {
        $customFields = $this->contact->getCustomFields();

        $this->assertInstanceOf(CustomFields::class, $customFields);
        $this->assertEquals($customFields->count(), 5);
        $customField = $customFields->first();
        $this->assertInstanceOf(CustomField::class, $customField);

        $cf = $this->contact->getCustomField(123907)->email();
        $this->assertEquals($cf->list(), ['test@mail.ru']);
        $this->assertEquals($cf->getByEnum(256787), 'test@mail.ru');
        $cf->add('qwer@qwer.rr');
        $cf->add('qwer@qwer.rr');
        $this->assertEquals($cf->list(), ['test@mail.ru', 'qwer@qwer.rr']);
        $cf->remove('test@mail.ru');
        $this->assertEquals($cf->list(), ['qwer@qwer.rr']);
        $this->assertEquals([
            'id'     => '123907',
            'values' => [
                [
                    'value' => 'qwer@qwer.rr',
                    'enum'  => '256787',
                ],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->contact->getCustomField(123905)->phone();
        $this->assertEquals($cf->list(), ['79009090911', '54365434655']);
        $cf->add('89019009191');
        $cf->add('+7 (901) 900-91-91');
        $this->assertEquals($cf->list(), ['79009090911', '54365434655', '79019009191']);
        $cf->remove('79009090911');
        $this->assertEquals($cf->list(), ['54365434655', '79019009191']);
        $this->assertEquals($cf->getByEnum(256779), '54365434655');
        $this->assertEquals([
            'id'     => '123905',
            'values' => [
                [
                    'value' => '54365434655',
                    'enum'  => '256779',
                ],
                [
                    'value' => '79019009191',
                    'enum'  => '256775',
                ],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->contact->getCustomField(123911)->im();
        $this->assertEquals($cf->list(), ['asdf', 'qqqqq']);
        $cf->add('www');
        $this->assertEquals($cf->list(), ['asdf', 'qqqqq', 'www']);
        $cf->remove('qqqqq');
        $this->assertEquals($cf->list(), ['asdf', 'www']);
        $this->assertEquals($cf->getByEnum(256793), 'asdf');
        $this->assertEquals([
            'id'     => '123911',
            'values' => [
                [
                    'value' => 'asdf',
                    'enum'  => '256793',
                ],
                [
                    'value' => 'www',
                    'enum'  => '256793',
                ],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->contact->getCustomField(252239)->entity();
        $this->assertEquals($cf->getName(), 'asdf');
        $this->assertNull($cf->getEntityType());
        $this->assertEquals($cf->getVatId(), '123123');
        $this->assertNull($cf->getTaxRegistrationReasonCode());
        $this->assertNull($cf->getAddress());
        $this->assertEquals($cf->getKpp(), '1123');
        $this->assertNull($cf->getExternalUid());
        $cf
            ->setName('New')
            ->setKpp('New1')
            ->setAddress('address')
            ->setTaxRegistrationReasonCode('code')
        ;
        $this->assertEquals($cf->getName(), 'New');
        $this->assertEquals($cf->getTaxRegistrationReasonCode(), 'code');
        $this->assertEquals($cf->getAddress(), 'address');
        $this->assertEquals($cf->getKpp(), 'New1');
        $this->assertNull($cf->getEntityType());
        $this->assertEquals($cf->getVatId(), '123123');
        $this->assertNull($cf->getExternalUid());
        $this->assertEquals([
            'id'     => '252239',
            'values' => [
                [
                    'value' => [
                        'name'                         => 'New',
                        'entity_type'                  => null,
                        'vat_id'                       => '123123',
                        'tax_registration_reason_code' => 'code',
                        'address'                      => 'address',
                        'kpp'                          => 'New1',
                        'external_uid'                 => null,
                    ],
                ],
            ],
        ], $cf->getModifiedForApi());
    }
}