<?php

use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Note;
use Amocrm\Api\Model\Notes;

class NotesTest extends TestCase
{
    /**
     * @var Notes
     */
    private $entities;

    public function setUp()
    {
        $this->entities = Notes::create([
            [
                'id'                  => '7667733',
                'element_id'          => '1031821',
                'element_type'        => '2',
                'note_type'           => 1,
                'date_create'         => 1522415760,
                'created_user_id'     => '2291701',
                'last_modified'       => 1522415760,
                'text'                => 'Добавлен новый объект',
                'responsible_user_id' => '2291701',
                'account_id'          => '19040293',
                'ATTACHEMENT'         => '',
                'group_id'            => 0,
                'editable'            => 'N',
            ],
            [
                'id'                  => '23348893',
                'element_id'          => '1031821',
                'element_type'        => '2',
                'note_type'           => 4,
                'date_create'         => 1523662593,
                'created_user_id'     => '2291701',
                'last_modified'       => 1523662593,
                'text'                => 'Текст примечания',
                'responsible_user_id' => '2291701',
                'account_id'          => '19040293',
                'ATTACHEMENT'         => '',
                'group_id'            => 0,
                'editable'            => 'Y',
            ],
            [
                'id'                  => '23348897',
                'element_id'          => '1031821',
                'element_type'        => '2',
                'note_type'           => 3,
                'date_create'         => 1523662613,
                'created_user_id'     => '2291701',
                'last_modified'       => 1523662613,
                'text'                => '{"STATUS_NEW":"19040305","STATUS_OLD":"19040299","TEXT":"Lead stage changed","PIPELINE_ID_OLD":1064215,"PIPELINE_ID_NEW":1064215,"LOSS_REASON_ID":0}',
                'responsible_user_id' => '2291701',
                'account_id'          => '19040293',
                'ATTACHEMENT'         => '',
                'group_id'            => 0,
                'editable'            => 'N',
            ],
            [
                'id'                  => '23348911',
                'element_id'          => '1031821',
                'element_type'        => '2',
                'note_type'           => 5,
                'date_create'         => 1523662708,
                'created_user_id'     => '2291701',
                'last_modified'       => 1523662708,
                'text'                => '{"TEXT":"\\u041b\\u043e\\u0433\\u043e-\\u0441-\\u0442\\u0435\\u043a\\u0441\\u0442\\u043e\\u043c1.png","HTML":"\\u041b\\u043e\\u0433\\u043e-\\u0441-\\u0442\\u0435\\u043a\\u0441\\u0442\\u043e\\u043c1.png<\\/a>"}',
                'responsible_user_id' => '2291701',
                'account_id'          => '19040293',
                'ATTACHEMENT'         => 'rXuc_Logo-s-tekstom1.png',
                'group_id'            => 0,
                'editable'            => 'N',
            ],
        ]);
    }

    public function testIterates()
    {
        $this->assertInstanceOf(Notes::class, $this->entities);
        $this->assertEquals($this->entities->count(), 4);
        $this->assertEquals([7667733, 23348893, 23348897, 23348911], $this->entities->getIds());

        $this->assertEquals($this->entities->key(), 0);

        $this->entities->next();

        $this->assertEquals($this->entities->key(), 1);

        foreach ($this->entities as $entity) {
            $this->assertInstanceOf(Note::class, $entity);
        }

        $this->assertEquals($this->entities->key(), 1);

        $this->entities->rewind();

        $this->assertEquals($this->entities->key(), 0);

        $entity = $this->entities->current();

        $this->assertInstanceOf(Note::class, $entity);
    }
}