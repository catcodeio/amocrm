<?php

use Amocrm\Api\Model\CustomField\CustomField;
use Amocrm\Api\Model\CustomField\CustomFields;
use Amocrm\Api\Model\CustomField\Type\Entity;
use Amocrm\Api\Model\CustomField\Type\Link;
use Amocrm\Api\Model\CustomField\Type\Organization;
use Amocrm\Api\Model\CustomField\Type\Phone;
use Amocrm\Api\Model\CustomField\Type\Text;
use Amocrm\Api\Model\CustomField\Type\Im;
use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Lead;
use Amocrm\Api\Model\Tag\Tags;
use Amocrm\Api\Model\Tag\Tag;

class LeadTest extends TestCase
{
    /**
     * @var Lead
     */
    private $lead;

    public function setUp()
    {
        $this->lead = Lead::create([
            'id'                  => '1031821',
            'name'                => 'Выключен',
            'date_create'         => 1522415760,
            'created_user_id'     => '2291701',
            'last_modified'       => 1523190638,
            'account_id'          => '19040293',
            'price'               => '0',
            'responsible_user_id' => '2291701',
            'linked_company_id'   => '5575745',
            'group_id'            => 0,
            'pipeline_id'         => 1064215,
            'date_close'          => 0,
            'closest_task'        => 1522540804,
            'loss_reason_id'      => 0,
            'deleted'             => 0,
            'tags'                => [
                [
                    'id'           => 58183,
                    'name'         => 'тестовая',
                    'element_type' => 2,
                ],
            ],
            'status_id'           => '19040299',
            'custom_fields'       => [
                [
                    'id'     => '199323',
                    'name'   => 'Тестовый список',
                    'values' => [
                        [
                            'value' => 'Вариант 1',
                            'enum'  => '406825',
                        ],
                    ],
                ],
                [
                    'id'     => '199327',
                    'name'   => 'Тестовый переключатель',
                    'values' => [
                        [
                            'value' => 'Вариант 2',
                            'enum'  => '406841',
                        ],
                    ],
                ],
                [
                    'id'     => '199329',
                    'name'   => 'Тестовый флаг',
                    'values' => [
                        [
                            'value' => '1',
                        ],
                    ],
                ],
                [
                    'id'     => '199345',
                    'name'   => 'Тестовый день рождения (обязательное)',
                    'values' => [
                        [
                            'value' => '2018-04-25 00:00:00',
                        ],
                    ],
                ],
                [
                    'id'     => '125623',
                    'name'   => 'Client ID',
                    'values' => [
                        [
                            'value' => '222333444',
                        ],
                    ],
                ],
                [
                    'id'     => '199325',
                    'name'   => 'Тестовый мультисписок',
                    'values' => [
                        [
                            'value' => 'Вариант 1',
                            'enum'  => '406831',
                        ],
                        [
                            'value' => 'Вариант 2',
                            'enum'  => '406833',
                        ],
                    ],
                ],
                [
                    'id'     => '199331',
                    'name'   => 'Тестовый адрес (только API)',
                    'values' => [
                        [
                            'value'   => 'Ломоносова',
                            'subtype' => 'address_line_1',
                        ],
                        [
                            'value'   => '111',
                            'subtype' => 'address_line_2',
                        ],
                        [
                            'value'   => 'СПб',
                            'subtype' => 'city',
                        ],
                    ],
                ],

                [
                    'id'     => '206417',
                    'name'   => 'Тестовая ссылка',
                    'values' => [
                        [
                            'value' => 'https://catcode.io',
                        ],
                    ],
                ],
                [
                    'id'     => '217963',
                    'name'   => 'Тестовый текст',
                    'values' => [
                        [
                            'value' => 'Много текста',
                        ],
                    ],
                ],
                [
                    'id'     => '217965',
                    'name'   => 'Тестовое число',
                    'values' => [
                        [
                            'value' => '567654345676543456787654.567876543',
                        ],
                    ],
                ],
                [
                    'id'     => '217981',
                    'name'   => 'Тестовая дата',
                    'values' => [
                        [
                            'value' => '2018-04-24 00:00:00',
                        ],
                    ],
                ],
                [
                    'id'     => '217983',
                    'name'   => 'Тестовая текстовая область',
                    'values' => [
                        [
                            'value' => 'Очень много текста',
                        ],
                    ],
                ],
                [
                    'id'     => '217997',
                    'name'   => 'Тестовый короткий адрес',
                    'values' => [
                        [
                            'value' => 'СПб Звеня 12',
                        ],
                    ],
                ],
            ],
            'main_contact_id'     => 2986625,
        ]);
    }

    public function testPlainProperty()
    {
        $this->assertEquals($this->lead->getId(), 1031821);
        $this->assertEquals($this->lead->getName(), 'Выключен');
        $this->assertEquals($this->lead->getPrice(), 0);
        $this->assertEquals($this->lead->getResponsibleUserId(), 2291701);
        $this->assertEquals($this->lead->getLinkedCompanyId(), 5575745);
        $this->assertEquals($this->lead->getStatusId(), 19040299);
        $this->assertEquals($this->lead->isDeleted(), false);

        $this->lead->setName('Новое название');
        $this->lead->setPrice(100);
        $this->lead->setResponsibleUserId(111);
        $this->lead->setLinkedCompanyId(444);
        $this->lead->setStatusId(555);

        $this->assertEquals($this->lead->getName(), 'Новое название');
        $this->assertEquals($this->lead->getPrice(), 100);
        $this->assertEquals($this->lead->getResponsibleUserId(), 111);
        $this->assertEquals($this->lead->getLinkedCompanyId(), 444);
        $this->assertEquals($this->lead->getStatusId(), 555);
    }

    public function testDateTime()
    {
        $dateCreate = $this->lead->getDateCreate();
        $this->assertInstanceOf(DateTime::class, $dateCreate);
        $this->assertEquals($dateCreate->format('U'), 1522415760);

        $lastModified = $this->lead->getLastModified();
        $this->assertInstanceOf(DateTime::class, $lastModified);
        $this->assertEquals($lastModified->format('U'), 1523190638);

        $closestTask = $this->lead->getClosestTask();
        $this->assertInstanceOf(DateTime::class, $closestTask);
        $this->assertEquals($closestTask->format('U'), 1522540804);

        $dateClose = $this->lead->getDateClose();
        $this->assertNull($dateClose);
    }

    public function testTags()
    {
        $tags = $this->lead->getTags();
        $this->assertInstanceOf(Tags::class, $tags);
        $this->assertEquals($tags->count(), 1);
        $tag = $tags->first();
        $this->assertInstanceOf(Tag::class, $tag);
        $this->assertEquals($tag->getId(), 58183);
        $this->assertEquals($tag->getName(), 'тестовая');

        $tag = $this->lead->getTag('тестовая');
        $this->assertInstanceOf(Tag::class, $tag);
        $this->assertEquals($tag->getId(), 58183);

        $this->lead->addTag('новый тег');

        $tagsArray = $this->lead->getModifiedForApi();
        $this->assertTrue(isset($tagsArray['tags']));
        $this->assertEquals('тестовая, новый тег', $tagsArray['tags']);

        $this->lead->removeTag('тестовая');

        $tag = $this->lead->getTags()->first();
        $this->assertInstanceOf(Tag::class, $tag);
        $this->assertEquals($tag->getId(), null);
        $this->assertEquals($tag->getName(), 'новый тег');

        $tagsArray = $this->lead->getModifiedForApi();
        $this->assertTrue(isset($tagsArray['tags']));
        $this->assertEquals('новый тег', $tagsArray['tags']);

        $this->lead->addTag(['qwer', 'asdf']);
        $this->assertEquals('новый тег, qwer, asdf', $this->lead->getModifiedForApi()['tags']);

        $this->lead->removeTag(['новый тег', 'asdf']);
        $this->assertEquals('qwer', $this->lead->getModifiedForApi()['tags']);
    }

    public function testCustomFields()
    {
        $customFields = $this->lead->getCustomFields();

        $this->assertInstanceOf(CustomFields::class, $customFields);
        $this->assertEquals($customFields->count(), 13);
        $customField = $customFields->first();
        $this->assertInstanceOf(CustomField::class, $customField);

        $cf = $this->lead->getCustomField(199329)->checkbox();
        $this->assertEquals($cf->get(), true);
        $cf->set(false);
        $this->assertEquals($cf->get(), false);
        $this->assertEquals([
            'id' => '199329',
            'values' => [['value' => 0]],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCF(199329)->checkbox();
        $this->assertEquals($cf->get(), false);
        $cf->set(true);
        $this->assertEquals($cf->get(), true);
        $this->assertEquals([
            'id' => '199329',
            'values' => [['value' => '1']],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(199323)->select();
        $this->assertEquals($cf->get(), 'Вариант 1');
        $cf->set('Вариант 2');
        $this->assertEquals($cf->get(), 'Вариант 2');
        $this->assertEquals([
            'id' => '199323',
            'values' => [
                ['value' => 'Вариант 2'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(199327)->radioButton();
        $this->assertEquals($cf->get(), 'Вариант 2');
        $cf->set('Вариант 3');
        $this->assertEquals($cf->get(), 'Вариант 3');
        $this->assertEquals([
            'id' => '199327',
            'values' => [
                ['value' => 'Вариант 3'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(199345)->birthday();
        $this->assertInstanceOf(DateTime::class, $cf->get());
        $this->assertEquals($cf->get()->format('Y-m-d H:i:s'), '2018-04-25 00:00:00');
        $cf->set(new DateTime('30.01.2999'));
        $this->assertEquals($cf->get()->format('Y-m-d H:i:s'), '2999-01-30 00:00:00');
        $this->assertEquals([
            'id' => '199345',
            'values' => [
                ['value' => '30.01.2999'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(199325)->multiselect();
        $this->assertTrue(is_array($cf->get()));
        $this->assertEquals(count($cf->get()), 2);
        $this->assertTrue(is_array($cf->list()));
        $this->assertEquals(count($cf->list()), 2);
        $this->assertEquals($cf->list(), ['Вариант 1', 'Вариант 2']);
        $cf->remove('Вариант 3');
        $this->assertEquals(count($cf->get()), 2);
        $cf->remove('Вариант 1');
        $this->assertEquals(count($cf->get()), 1);
        $this->assertEquals($cf->list(), ['Вариант 2']);
        $cf->add('Вариант 3');
        $this->assertEquals(count($cf->get()), 2);
        $this->assertEquals($cf->list(), ['Вариант 2', 'Вариант 3']);
        $this->assertEquals([
            'id' => '199325',
            'values' => [
                '406833',
                'Вариант 3',
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(199331)->address();
        $this->assertEquals('Ломоносова', $cf->getAddressLine1());
        $this->assertEquals('111', $cf->getAddressLine2());
        $this->assertEquals('СПб', $cf->getCity());
        $this->assertEquals(null, $cf->getState());
        $this->assertEquals(null, $cf->getZip());
        $this->assertEquals(null, $cf->getCountry());
        $cf
            ->setAddressLine1('asdf')
            ->setAddressLine2('qwer')
            ->setCity('zzzz')
            ->setState('qqqq')
            ->setZip('wwww')
            ->setCountry('RU')
        ;
        $this->assertEquals('asdf', $cf->getAddressLine1());
        $this->assertEquals('qwer', $cf->getAddressLine2());
        $this->assertEquals('zzzz', $cf->getCity());
        $this->assertEquals('qqqq', $cf->getState());
        $this->assertEquals('wwww', $cf->getZip());
        $this->assertEquals('RU', $cf->getCountry());
        $this->assertEquals([
            'id' => '199331',
            'values' => [
                ['value' => 'asdf', 'subtype' => 'address_line_1'],
                ['value' => 'qwer', 'subtype' => 'address_line_2'],
                ['value' => 'zzzz', 'subtype' => 'city'],
                ['value' => 'qqqq', 'subtype' => 'state'],
                ['value' => 'wwww', 'subtype' => 'zip'],
                ['value' => 'RU', 'subtype' => 'country'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(206417)->link();
        $this->assertEquals('https://catcode.io', $cf->get());
        $cf->set('asdf');
        $this->assertEquals('asdf', $cf->get());
        $this->assertEquals([
            'id' => '206417',
            'values' => [
                ['value' => 'asdf'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(217963)->text();
        $this->assertEquals('Много текста', $cf->get());
        $cf->set('asdf');
        $this->assertEquals('asdf', $cf->get());
        $this->assertEquals([
            'id' => 217963,
            'values' => [
                ['value' => 'asdf'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(217965)->numeric();
        $this->assertEquals(567654345676543456787654.567876543, $cf->get());
        $cf->set(2.66778987);
        $this->assertEquals('2.66778987', $cf->get());
        $this->assertEquals([
            'id' => 217965,
            'values' => [
                ['value' => '2.66778987'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(217981)->date();
        $this->assertInstanceOf(DateTime::class, $cf->get());
        $this->assertEquals($cf->get()->format('Y-m-d H:i:s'), '2018-04-24 00:00:00');
        $cf->set(new DateTime('30.01.1999'));
        $this->assertEquals($cf->get()->format('Y-m-d H:i:s'), '1999-01-30 00:00:00');
        $this->assertEquals([
            'id' => 217981,
            'values' => [
                ['value' => '30.01.1999'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(217983)->textarea();
        $this->assertEquals('Очень много текста', $cf->get());
        $cf->set('asdf1');
        $this->assertEquals('asdf1', $cf->get());
        $this->assertEquals([
            'id' => 217983,
            'values' => [
                ['value' => 'asdf1'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(217997)->addressShort();
        $this->assertEquals('СПб Звеня 12', $cf->get());
        $cf->set('asdf2');
        $this->assertEquals('asdf2', $cf->get());
        $this->assertEquals([
            'id' => 217997,
            'values' => [
                ['value' => 'asdf2'],
            ],
        ], $cf->getModifiedForApi());
    }

    public function testCustomFieldsCreate()
    {
        $cf = $this->lead->getCF(111)->checkbox();
        $this->assertEquals($cf->get(), null);
        $cf->set(true);
        $this->assertEquals($cf->get(), true);
        $this->assertEquals([
            'id' => '111',
            'values' => [['value' => '1']],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(222)->select();
        $this->assertEquals($cf->get(), null);
        $cf->set('Вариант 2');
        $this->assertEquals($cf->get(), 'Вариант 2');
        $this->assertEquals([
            'id' => '222',
            'values' => [
                ['value' => 'Вариант 2'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(333)->radioButton();
        $this->assertEquals($cf->get(), null);
        $cf->set('Вариант 3');
        $this->assertEquals($cf->get(), 'Вариант 3');
        $this->assertEquals([
            'id' => '333',
            'values' => [['value' => 'Вариант 3']],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(444)->birthday();
        $this->assertEquals($cf->get(), null);
        $cf->set(new DateTime('30.01.2999'));
        $this->assertInstanceOf(DateTime::class, $cf->get());
        $this->assertEquals($cf->get()->format('Y-m-d H:i:s'), '2999-01-30 00:00:00');
        $this->assertEquals([
            'id' => '444',
            'values' => [['value' => '30.01.2999']],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(555)->multiselect();
        $this->assertEquals($cf->get(), null);
        $cf->add('Вариант 3');
        $this->assertEquals(count($cf->get()), 1);
        $cf->add('Вариант 1');
        $this->assertEquals(count($cf->get()), 2);
        $cf->remove('Вариант 3');
        $this->assertEquals(count($cf->get()), 1);
        $this->assertEquals([
            'id' => '555',
            'values' => [
                'Вариант 1',
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(666)->address();
        $this->assertEquals($cf->get(), null);
        $cf
            ->setAddressLine1('asdf')
            ->setAddressLine2('qwer')
            ->setCity('zzzz')
            ->setState('qqqq')
            ->setCountry('RU')
        ;
        $this->assertEquals('asdf', $cf->getAddressLine1());
        $this->assertEquals('qwer', $cf->getAddressLine2());
        $this->assertEquals('zzzz', $cf->getCity());
        $this->assertEquals('qqqq', $cf->getState());
        $this->assertEquals(null, $cf->getZip());
        $this->assertEquals('RU', $cf->getCountry());
        $cf->setAddressLine2(null);
        $cf->setState('');
        $this->assertEquals(null, $cf->getAddressLine2());
        $this->assertEquals('', $cf->getState());
        $this->assertEquals([
            'id' => 666,
            'values' => [
                ['value' => 'asdf', 'subtype' => 'address_line_1'],
                ['value' => 'zzzz', 'subtype' => 'city'],
                ['value' => '', 'subtype' => 'state'],
                ['value' => 'RU', 'subtype' => 'country'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(777)->link();
        $this->assertEquals($cf->get(), null);
        $cf->set('asdf');
        $this->assertEquals('asdf', $cf->get());
        $this->assertEquals([
            'id' => '777',
            'values' => [['value' => 'asdf']],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(888)->text();
        $this->assertEquals($cf->get(), null);
        $cf->set('asdf');
        $this->assertEquals('asdf', $cf->get());
        $this->assertEquals([
            'id' => 888,
            'values' => [['value' => 'asdf']],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(999)->numeric();
        $this->assertEquals($cf->get(), null);
        $cf->set(2.66778987);
        $this->assertEquals('2.66778987', $cf->get());
        $this->assertEquals([
            'id' => 999,
            'values' => [['value' => '2.66778987']],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(123)->date();
        $this->assertEquals($cf->get(), null);
        $cf->set(new DateTime('30.01.1999'));
        $this->assertInstanceOf(DateTime::class, $cf->get());
        $this->assertEquals($cf->get()->format('Y-m-d H:i:s'), '1999-01-30 00:00:00');
        $this->assertEquals([
            'id' => 123,
            'values' => [['value' => '30.01.1999']],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(321)->textarea();
        $this->assertEquals($cf->get(), null);
        $cf->set('asdf1');
        $this->assertEquals('asdf1', $cf->get());
        $this->assertEquals([
            'id' => 321,
            'values' => [['value' => 'asdf1']],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(456)->addressShort();
        $this->assertEquals($cf->get(), null);
        $cf->set('asdf2');
        $this->assertEquals('asdf2', $cf->get());
        $this->assertEquals([
            'id' => 456,
            'values' => [['value' => 'asdf2']],
        ], $cf->getModifiedForApi());

        $cf = $this->lead->getCustomField(111)->organization();
        $this->assertEquals($cf->get(), null);
        $cf->setName('asdf2');
        $cf->setZip('asdf');
        $cf->setExternalUid('asdf1');
        $this->assertEquals('asdf2', $cf->getName());
        $this->assertEquals('asdf', $cf->getZip());
        $this->assertEquals('asdf1', $cf->getExternalUid());
        $this->assertEquals([
            'id' => 111,
            'values' => [[
                'value' => [
                    'name' => 'asdf2',
                    'zip' => 'asdf',
                    'external_uid' => 'asdf1',
                ],
            ]],
        ], $cf->getModifiedForApi());
    }

    public function testCustomFieldsIsClear()
    {
        $cf = $this->lead->getCustomField(217997);
        $this->assertEquals('СПб Звеня 12', $cf->addressShort()->get());
        $this->assertEquals([
            'id'     => 217997,
            'values' => [
                ['value' => 'СПб Звеня 12'],
            ],
        ], $cf->getModifiedForApi());
        $this->assertFalse($cf->isClear());

        $cf->clear();

        $this->assertEquals([
            'id'     => 217997,
            'values' => null,
        ], $cf->getModifiedForApi());
        $this->assertTrue($cf->isClear());
    }

    public function testCustomFieldsClearCF()
    {
        $this->lead->clearCF(217983);
        $this->assertTrue($this->lead->getCF(217983)->isClear());

        $data = $this->lead->getModifiedForApi();

        $this->assertTrue(isset($data['custom_fields']));

        $data = current($data['custom_fields']);

        $this->assertTrue(array_key_exists('values', $data));
        $this->assertNull($data['values']);
    }

    public function testCustomFieldsRewrite()
    {
        $this->assertEquals('Очень много текста', $this->lead->getCF(217983)->textarea()->get());

        $cf = $this->lead->getCF(217983)->textarea()->set('Перезапись');

        $this->lead->setCF($cf);

        $this->assertEquals('Перезапись', $this->lead->getCF(217983)->textarea()->get());

        $data = $this->lead->getModifiedForApi();

        $this->assertTrue(isset($data['custom_fields']));

        $data = current($data['custom_fields']);

        $this->assertTrue(array_key_exists('values', $data));
        $this->assertEquals('Перезапись', $data['values'][0]['value']);
    }

    public function testCustomFieldsCreateRewrite()
    {
        $this->assertEquals('Очень много текста', $this->lead->getCF(217983)->textarea()->get());

        $cf = CustomField::create(['id' => 217983])->textarea()->set('Перезапись');

        $this->lead->setCF($cf);

        $this->assertEquals('Перезапись', $this->lead->getCF(217983)->textarea()->get());

        $data = $this->lead->getModifiedForApi();

        $this->assertTrue(isset($data['custom_fields']));

        $data = current($data['custom_fields']);

        $this->assertTrue(array_key_exists('values', $data));
        $this->assertEquals('Перезапись', $data['values'][0]['value']);
    }

    public function testCustomFieldsGetEmpty()
    {
        $this->assertNull($this->lead->getCF(111122)->textarea()->get());
        $this->assertNull($this->lead->getCF(111122)->date()->get());
        $this->assertNull($this->lead->getCF(111122)->address()->get());
        $this->assertNull($this->lead->getCF(111122)->select()->get());
        $this->assertNull($this->lead->getCF(111122)->multiselect()->get());
        $this->assertNull($this->lead->getCF(111122)->phone()->get());
        $this->assertNull($this->lead->getCF(111122)->entity()->get());
        $this->assertNull($this->lead->getCF(111122)->organization()->get());

        $this->assertTrue($this->lead->getCF(111122)->isEmpty());
        $this->assertFalse($this->lead->getCF(217983)->isEmpty());
    }

    public function testCustomFieldsAddToFull()
    {
        $this->assertNotNull($this->lead->getCF(199325)->multiselect()->get());
        $this->assertFalse($this->lead->getCF(199325)->isEmpty());

        $this->lead->setCF($this->lead->getCF(199325)->multiselect()->add('item'));

        $this->assertEquals([
            [
                'value' => 'Вариант 1',
                'enum'  => '406831',
            ],
            [
                'value' => 'Вариант 2',
                'enum'  => '406833',
            ],
            [
                'value' => 'item',
            ],
        ], $this->lead->getCF(199325)->multiselect()->get());
        $this->assertFalse($this->lead->getCF(199325)->isEmpty());
    }

    public function testCustomFieldsAddToEmpty()
    {
        $this->assertNull($this->lead->getCF(111122)->phone()->get());
        $this->assertTrue($this->lead->getCF(111122)->isEmpty());

        $this->lead->setCF($this->lead->getCF(111122)->phone()->add('666'));

        $this->assertEquals([['value' => '666', 'enum' => 'MOB']], $this->lead->getCF(111122)->phone()->get());
        $this->assertFalse($this->lead->getCF(111122)->isEmpty());
    }

    public function testEmptyAddress()
    {
        $this->assertNull($this->lead->getCF(99999)->address()->getAddressLine1());
        $this->assertNull($this->lead->getCF(99999)->address()->getAddressLine2());
        $this->assertNull($this->lead->getCF(99999)->address()->getCity());
        $this->assertNull($this->lead->getCF(99999)->address()->getCountry());
        $this->assertNull($this->lead->getCF(99999)->address()->getState());
        $this->assertNull($this->lead->getCF(99999)->address()->getZip());
    }

    public function testCopy()
    {
        $lead = clone $this->lead;
        $this->assertInstanceOf(Lead::class, $lead);
        $this->assertNotEquals($this->lead, $lead);
        $this->assertNotEquals($lead->getId(), $this->lead->getId());
        $this->assertNull($lead->getId());
        $this->assertEquals($lead->getName(), $this->lead->getName());

        $this->assertEquals(1, $this->lead->getTags()->count());
        $this->assertInstanceOf(Tags::class, $lead->getTags());
        $this->assertEquals($lead->getTags()->count(), 1);
        $this->assertEquals($lead->getTags()->first()->getName(), 'тестовая');

        $leadArray = $lead->getModifiedForApi();
        $this->assertEquals($this->lead->getName(), $leadArray['name']);

        $customFields = $lead->getCustomFields();

        $this->assertInstanceOf(CustomFields::class, $customFields);
        $this->assertEquals($customFields->count(), 13);
        $customField = $customFields->first();
        $this->assertInstanceOf(CustomField::class, $customField);

        $cf = $lead->getCustomField(199329)->checkbox();
        $this->assertEquals($cf->get(), true);
        $this->assertEquals([
            'id' => '199329',
            'values' => [['value' => '1']],
        ], $cf->getModifiedForApi());

        $cf = $lead->getCustomField(199323)->select();
        $this->assertEquals($cf->get(), 'Вариант 1');
        $this->assertEquals([
            'id' => '199323',
            'values' => [
                [
                    'value' => 'Вариант 1',
                    'enum' => '406825',
                ],
            ],
        ], $cf->getModifiedForApi());

        $cf = $lead->getCustomField(199327)->radioButton();
        $this->assertEquals($cf->get(), 'Вариант 2');
        $this->assertEquals([
            'id' => '199327',
            'values' => [
                [
                    'value' => 'Вариант 2',
                    'enum' => '406841',
                ],
            ],
        ], $cf->getModifiedForApi());

        $lastModified = (new DateTime())->modify('-1 day');
        $lead->setLastModified($lastModified);

        $this->assertEquals([
            'id'                  => null,
            'name'                => 'Выключен',
            'date_create'         => 1522415760,
            'created_user_id'     => '2291701',
            'last_modified'       => $lastModified->format('U'),
            'account_id'          => '19040293',
            'price'               => '0',
            'responsible_user_id' => '2291701',
            'linked_company_id'   => '5575745',
            'group_id'            => 0,
            'pipeline_id'         => 1064215,
            'date_close'          => 0,
            'closest_task'        => 1522540804,
            'loss_reason_id'      => 0,
            'deleted'             => 0,
            'tags'                => 'тестовая',
            'status_id'           => '19040299',
            'custom_fields'       => [
                [
                    'id'     => '199323',
                    'values' => [
                        [
                            'value' => 'Вариант 1',
                            'enum'  => '406825',
                        ],
                    ],
                ],
                [
                    'id'     => '199327',
                    'values' => [
                        [
                            'value' => 'Вариант 2',
                            'enum'  => '406841',
                        ],
                    ],
                ],
                [
                    'id'     => '199329',
                    'values' => [
                        [
                            'value' => '1',
                        ],
                    ],
                ],
                [
                    'id'     => '199345',
                    'name'   => 'Тестовый день рождения (обязательное)',
                    'values' => [
                        [
                            'value' => '2018-04-25 00:00:00',
                        ],
                    ],
                ],
                [
                    'id'     => '125623',
                    'name'   => 'Client ID',
                    'values' => [
                        [
                            'value' => '222333444',
                        ],
                    ],
                ],
                [
                    'id'     => '199325',
                    'name'   => 'Тестовый мультисписок',
                    'values' => [
                        [
                            'value' => 'Вариант 1',
                            'enum'  => '406831',
                        ],
                        [
                            'value' => 'Вариант 2',
                            'enum'  => '406833',
                        ],
                    ],
                ],
                [
                    'id'     => '199331',
                    'name'   => 'Тестовый адрес (только API)',
                    'values' => [
                        [
                            'value'   => 'Ломоносова',
                            'subtype' => 'address_line_1',
                        ],
                        [
                            'value'   => '111',
                            'subtype' => 'address_line_2',
                        ],
                        [
                            'value'   => 'СПб',
                            'subtype' => 'city',
                        ],
                    ],
                ],

                [
                    'id'     => '206417',
                    'name'   => 'Тестовая ссылка',
                    'values' => [
                        [
                            'value' => 'https://catcode.io',
                        ],
                    ],
                ],
                [
                    'id'     => '217963',
                    'name'   => 'Тестовый текст',
                    'values' => [
                        [
                            'value' => 'Много текста',
                        ],
                    ],
                ],
                [
                    'id'     => '217965',
                    'name'   => 'Тестовое число',
                    'values' => [
                        [
                            'value' => '567654345676543456787654.567876543',
                        ],
                    ],
                ],
                [
                    'id'     => '217981',
                    'name'   => 'Тестовая дата',
                    'values' => [
                        [
                            'value' => '2018-04-24 00:00:00',
                        ],
                    ],
                ],
                [
                    'id'     => '217983',
                    'name'   => 'Тестовая текстовая область',
                    'values' => [
                        [
                            'value' => 'Очень много текста',
                        ],
                    ],
                ],
                [
                    'id'     => '217997',
                    'name'   => 'Тестовый короткий адрес',
                    'values' => [
                        [
                            'value' => 'СПб Звеня 12',
                        ],
                    ],
                ],
            ],
            'main_contact_id'     => 2986625,
        ], $lead->getModifiedForApi());
    }

    public function testNotModify()
    {
        $modifiedForApi = $this->lead->getModifiedForApi();

        $this->assertTrue(isset($modifiedForApi['id']));
        $this->assertEquals(1031821, $modifiedForApi['id']);
        $this->assertTrue(isset($modifiedForApi['last_modified']));
    }

    public function testModifyId()
    {
        $this->lead->setId(1234);

        $modifiedForApi = $this->lead->getModifiedForApi();

        $this->assertTrue(isset($modifiedForApi['id']));
        $this->assertEquals(1234, $modifiedForApi['id']);
        $this->assertTrue(isset($modifiedForApi['last_modified']));
    }

    public function testIsModify()
    {
        $this->assertFalse($this->lead->isModified());
        $this->lead->setId(222);
        $this->assertTrue($this->lead->isModified());
        $this->lead->setPrice(333);
        $this->assertTrue($this->lead->isModified());

        $leadNew = Lead::create();

        $this->assertFalse($leadNew->isModified());
        $leadNew->setCF($leadNew->getCF(1234)->checkbox()->set(true));
        $this->assertTrue($leadNew->isModified());
    }

    public function testTypes()
    {
        $this->assertEquals(Organization::getType(), 17);
        $this->assertEquals(Entity::getType(), 15);
        $this->assertEquals(Text::getType(), 1);
        $this->assertEquals(Link::getType(), 7);
        $this->assertEquals(Im::getType(), 8);
        $this->assertEquals(Phone::getType(), 8);
    }
}