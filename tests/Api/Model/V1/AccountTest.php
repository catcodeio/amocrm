<?php

use Amocrm\Api\Helper\ElementType;
use Amocrm\Api\Model\Account\User;
use Amocrm\Api\Model\Account\Users;
use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Account;
use Amocrm\Api\Model\Account\Groups;
use Amocrm\Api\Model\Account\Group;
use Amocrm\Api\Model\Account\NoteTypes;
use Amocrm\Api\Model\Account\NoteType;
use Amocrm\Api\Model\Account\TaskTypes;
use Amocrm\Api\Model\Account\TaskType;
use Amocrm\Api\Model\Account\Pipelines;
use Amocrm\Api\Model\Account\Pipeline;
use Amocrm\Api\Model\Account\Pipeline\Statuses;
use Amocrm\Api\Model\Account\Pipeline\Status;
use Amocrm\Api\Model\Account\CustomFields;
use Amocrm\Api\Model\Account\CustomField;
use Amocrm\Api\Model\Account\CustomField\Subtypes;
use Amocrm\Api\Model\Account\CustomField\Subtype;

class AccountTest extends TestCase
{
    /**
     * @var Account
     */
    private $account;

    public function setUp()
    {
        $this->account = Account::create([
            'id'                      => '19040293',
            'name'                    => 'CatCode',
            'subdomain'               => 'catcode',
            'currency'                => 'RUB',
            'timezone'                => 'Europe/Moscow',
            'language'                => 'ru',
            'notifications_base_url'  => 'https://notifications.amocrm.ru',
            'notifications_ws_url'    => 'wss://notifications.amocrm.ru',
            'notifications_ws_url_v2' => 'wss://ws.amocrm.ru/catcode/v2/rtm?stand=v3',
            'amojo_base_url'          => 'https://amojo.amocrm.ru',
            'amojo_rights'            =>
                [
                    'can_direct'       => true,
                    'can_group_create' => true,
                ],
            'current_user'            => 2291701,
            'version'                 => 3,
            'date_pattern'            => 'd.m.Y H:i',
            'short_date_pattern'      =>
                [
                    'date'      => 'd.m.Y',
                    'time'      => 'H:i',
                    'date_time' => 'd.m.Y H:i',
                ],
            'date_format'             => 'd.m.Y',
            'time_format'             => 'H:i:s',
            'country'                 => 'ru',
            'unsorted_on'             => 'Y',
            'mobile_feature_version'  => 0,
            'customers_enabled'       => NULL,
            'loss_reasons_enabled'    => true,
            'limits'                  =>
                [
                    'users_count'        => false,
                    'contacts_count'     => false,
                    'active_deals_count' => false,
                ],
            'users'                   =>
                [
                    [
                        'id'                    => '2291701',
                        'mail_admin'            => 'A',
                        'name'                  => 'Генадий Владимирович',
                        'last_name'             => NULL,
                        'login'                 => 'qwer@catcode.io',
                        'photo_url'             => '/upload/main/4fa/photo_2018-03-15_14-45-56.jpg',
                        'phone_number'          => '12344',
                        'language'              => 'ru',
                        'active'                => true,
                        'is_admin'              => 'Y',
                        'unsorted_access'       => 'Y',
                        'catalogs_access'       => 'Y',
                        'group_id'              => 0,
                        'rights_lead_add'       => 'A',
                        'rights_lead_view'      => 'A',
                        'rights_lead_edit'      => 'A',
                        'rights_lead_delete'    => 'A',
                        'rights_lead_export'    => 'A',
                        'rights_contact_add'    => 'A',
                        'rights_contact_view'   => 'A',
                        'rights_contact_edit'   => 'A',
                        'rights_contact_delete' => 'A',
                        'rights_contact_export' => 'A',
                        'rights_company_add'    => 'A',
                        'rights_company_view'   => 'A',
                        'rights_company_edit'   => 'A',
                        'rights_company_delete' => 'A',
                        'rights_company_export' => 'A',
                        'rights_task_edit'      => 'A',
                        'rights_task_delete'    => 'A',
                        'free_user'             => false,
                    ],
                    [
                        'id'                    => '2267143',
                        'mail_admin'            => 'A',
                        'name'                  => 'CatCode',
                        'last_name'             => NULL,
                        'login'                 => 'support@catcode.io',
                        'photo_url'             => '/upload/main/4fa/photo_2018-03-15_14-45-56.jpg',
                        'phone_number'          => '',
                        'language'              => 'ru',
                        'active'                => true,
                        'is_admin'              => 'Y',
                        'unsorted_access'       => 'Y',
                        'catalogs_access'       => 'Y',
                        'group_id'              => 1,
                        'rights_lead_add'       => 'A',
                        'rights_lead_view'      => 'A',
                        'rights_lead_edit'      => 'A',
                        'rights_lead_delete'    => 'A',
                        'rights_lead_export'    => 'A',
                        'rights_contact_add'    => 'A',
                        'rights_contact_view'   => 'A',
                        'rights_contact_edit'   => 'A',
                        'rights_contact_delete' => 'A',
                        'rights_contact_export' => 'A',
                        'rights_company_add'    => 'A',
                        'rights_company_view'   => 'A',
                        'rights_company_edit'   => 'A',
                        'rights_company_delete' => 'A',
                        'rights_company_export' => 'A',
                        'rights_task_edit'      => 'A',
                        'rights_task_delete'    => 'A',
                        'free_user'             => false,
                    ],
                ],
            'groups'                  =>
                [
                    [
                        'id'   => 0,
                        'name' => 'Отдел продаж',
                    ],
                    [
                        'id'   => 1,
                        'name' => 'Отдел перепродаж',
                    ],
                ],
            'leads_statuses'          =>
                [
                    0 =>
                        [
                            'id'          => 19040299,
                            'name'        => 'Первичный контакт',
                            'pipeline_id' => 1064215,
                            'sort'        => 10,
                            'color'       => '#99ccff',
                            'editable'    => 'Y',
                        ],
                    1 =>
                        [
                            'id'          => 19040302,
                            'name'        => 'Переговоры',
                            'pipeline_id' => 1064215,
                            'sort'        => 20,
                            'color'       => '#ffff99',
                            'editable'    => 'Y',
                        ],
                    2 =>
                        [
                            'id'          => 19040305,
                            'name'        => 'Принимают решение',
                            'pipeline_id' => 1064215,
                            'sort'        => 30,
                            'color'       => '#ffcc66',
                            'editable'    => 'Y',
                        ],
                    3 =>
                        [
                            'id'          => 19040308,
                            'name'        => 'Согласование договора',
                            'pipeline_id' => 1064215,
                            'sort'        => 40,
                            'color'       => '#ffcccc',
                            'editable'    => 'Y',
                        ],
                    4 =>
                        [
                            'id'          => 142,
                            'name'        => 'Успешно реализовано',
                            'pipeline_id' => 1064215,
                            'sort'        => 10000,
                            'color'       => '#CCFF66',
                            'editable'    => 'N',
                        ],
                    5 =>
                        [
                            'id'          => 143,
                            'name'        => 'Закрыто и не реализовано',
                            'pipeline_id' => 1064215,
                            'sort'        => 11000,
                            'color'       => '#D5D8DB',
                            'editable'    => 'N',
                        ],
                ],
            'custom_fields'           =>
                [
                    1937        =>
                        [
                            0 =>
                                [
                                    'id'       => '198925',
                                    'name'     => 'Артикул',
                                    'code'     => 'SKU',
                                    'multiple' => 'N',
                                    'type_id'  => '1',
                                    'disabled' => '0',
                                    'sort'     => 1,
                                ],
                            1 =>
                                [
                                    'id'       => '198927',
                                    'name'     => 'Количество',
                                    'code'     => 'QUANTITY',
                                    'multiple' => 'N',
                                    'type_id'  => '2',
                                    'disabled' => '0',
                                    'sort'     => 2,
                                ],
                            2 =>
                                [
                                    'id'       => '198929',
                                    'name'     => 'Цена',
                                    'code'     => 'PRICE',
                                    'multiple' => 'N',
                                    'type_id'  => '2',
                                    'disabled' => '0',
                                    'sort'     => 3,
                                ],
                        ],
                    'contacts'  =>
                        [
                            [
                                'id'       => '123903',
                                'name'     => 'Должность',
                                'code'     => 'POSITION',
                                'multiple' => 'N',
                                'type_id'  => '1',
                                'disabled' => '0',
                                'sort'     => 2,
                            ],
                            [
                                'id'       => '123905',
                                'name'     => 'Телефон',
                                'code'     => 'PHONE',
                                'multiple' => 'Y',
                                'type_id'  => '8',
                                'disabled' => '0',
                                'sort'     => 4,
                                'enums'    =>
                                    [
                                        256775 => 'WORK',
                                        256777 => 'WORKDD',
                                        256779 => 'MOB',
                                        256781 => 'FAX',
                                        256783 => 'HOME',
                                        256785 => 'OTHER',
                                    ],
                            ],
                            [
                                'id'       => '123907',
                                'name'     => 'Email',
                                'code'     => 'EMAIL',
                                'multiple' => 'Y',
                                'type_id'  => '8',
                                'disabled' => '0',
                                'sort'     => 6,
                                'enums'    =>
                                    [
                                        256787 => 'WORK',
                                        256789 => 'PRIV',
                                        256791 => 'OTHER',
                                    ],
                            ],
                            [
                                'id'       => '123911',
                                'name'     => 'Мгн. сообщения',
                                'code'     => 'IM',
                                'multiple' => 'Y',
                                'type_id'  => '8',
                                'disabled' => '0',
                                'sort'     => 10,
                                'enums'    =>
                                    [
                                        256793 => 'SKYPE',
                                        256795 => 'ICQ',
                                        256797 => 'JABBER',
                                        256799 => 'GTALK',
                                        256801 => 'MSN',
                                        256803 => 'OTHER',
                                    ],
                            ],
                            [
                                'id'           => '252239',
                                'name'         => 'Juridical person',
                                'code'         => '',
                                'multiple'     => 'Y',
                                'type_id'      => '15',
                                'disabled'     => '0',
                                'sort'         => 505,
                                'is_required'  => false,
                                'is_deletable' => true,
                                'is_visible'   => true,
                            ],
                        ],
                    'leads'     => [
                        [
                            'id'       => '125623',
                            'name'     => 'Client ID',
                            'code'     => '',
                            'multiple' => 'N',
                            'type_id'  => '1',
                            'disabled' => '0',
                            'sort'     => 501,
                        ],
                        [
                            'id'       => '199323',
                            'name'     => 'Тестовый список',
                            'code'     => '',
                            'multiple' => 'N',
                            'type_id'  => '4',
                            'disabled' => '0',
                            'sort'     => 502,
                            'enums'    =>
                                [
                                    406825 => 'Вариант 1',
                                    406827 => 'Вариант 2',
                                    406829 => 'Вариант 3',
                                ],
                        ],
                        [
                            'id'       => '199325',
                            'name'     => 'Тестовый мультисписок',
                            'code'     => '',
                            'multiple' => 'N',
                            'type_id'  => '5',
                            'disabled' => '0',
                            'sort'     => 503,
                            'enums'    =>
                                [
                                    406831 => 'Вариант 1',
                                    406833 => 'Вариант 2',
                                    406835 => 'Вариант 3',
                                    406837 => 'Вариант 4',
                                ],
                        ],
                        [
                            'id'       => '199327',
                            'name'     => 'Тестовый переключатель',
                            'code'     => '',
                            'multiple' => 'N',
                            'type_id'  => '10',
                            'disabled' => '0',
                            'sort'     => 504,
                            'enums'    =>
                                [
                                    406839 => 'Вариант 1',
                                    406841 => 'Вариант 2',
                                    406843 => 'Вариант 3',
                                ],
                        ],
                        [
                            'id'       => '199329',
                            'name'     => 'Тестовый флаг',
                            'code'     => '',
                            'multiple' => 'N',
                            'type_id'  => '3',
                            'disabled' => '0',
                            'sort'     => 505,
                        ],
                        [
                            'id'       => '199331',
                            'name'     => 'Тестовый адрес (только API)',
                            'code'     => '',
                            'multiple' => 'N',
                            'type_id'  => '13',
                            'disabled' => '0',
                            'sort'     => 506,
                            'subtypes' =>
                                [
                                    1 =>
                                        [
                                            'id'    => 1,
                                            'title' => 'Адрес',
                                            'name'  => 'address_line_1',
                                        ],
                                    2 =>
                                        [
                                            'id'    => 2,
                                            'title' => 'Адрес - продолжение',
                                            'name'  => 'address_line_2',
                                        ],
                                    3 =>
                                        [
                                            'id'    => 3,
                                            'title' => 'Город',
                                            'name'  => 'city',
                                        ],
                                    4 =>
                                        [
                                            'id'    => 4,
                                            'title' => 'Регион',
                                            'name'  => 'state',
                                        ],
                                    5 =>
                                        [
                                            'id'    => 5,
                                            'title' => 'Индекс',
                                            'name'  => 'zip',
                                        ],
                                    6 =>
                                        [
                                            'id'    => 6,
                                            'title' => 'Страна',
                                            'name'  => 'country',
                                        ],
                                ],
                        ],
                        [
                            'id'       => '199345',
                            'name'     => 'Тестовый день рождения (обязательное)',
                            'code'     => '',
                            'multiple' => 'N',
                            'type_id'  => '14',
                            'disabled' => '0',
                            'sort'     => 507,
                        ],
                        [
                            'id'       => '206417',
                            'name'     => 'Тестовая ссылка',
                            'code'     => '',
                            'multiple' => 'N',
                            'type_id'  => '7',
                            'disabled' => '0',
                            'sort'     => 508,
                        ],
                        [
                            'id'       => '217963',
                            'name'     => 'Тестовый текст',
                            'code'     => '',
                            'multiple' => 'N',
                            'type_id'  => '1',
                            'disabled' => '0',
                            'sort'     => 509,
                        ],
                        [
                            'id'       => '217965',
                            'name'     => 'Тестовое число',
                            'code'     => '',
                            'multiple' => 'N',
                            'type_id'  => '2',
                            'disabled' => '0',
                            'sort'     => 510,
                        ],
                        [
                            'id'       => '217981',
                            'name'     => 'Тестовая дата',
                            'code'     => '',
                            'multiple' => 'N',
                            'type_id'  => '6',
                            'disabled' => '0',
                            'sort'     => 511,
                        ],
                        [
                            'id'       => '217983',
                            'name'     => 'Тестовая текстовая область',
                            'code'     => '',
                            'multiple' => 'N',
                            'type_id'  => '9',
                            'disabled' => '0',
                            'sort'     => 512,
                        ],
                        [
                            'id'       => '217997',
                            'name'     => 'Тестовый короткий адрес',
                            'code'     => '',
                            'multiple' => 'N',
                            'type_id'  => '11',
                            'disabled' => '0',
                            'sort'     => 513,
                        ],
                    ],
                    'companies' =>
                        [
                            0 =>
                                [
                                    'id'       => '123905',
                                    'name'     => 'Телефон',
                                    'code'     => 'PHONE',
                                    'multiple' => 'Y',
                                    'type_id'  => '8',
                                    'disabled' => '0',
                                    'sort'     => 4,
                                    'enums'    =>
                                        [
                                            256775 => 'WORK',
                                            256777 => 'WORKDD',
                                            256779 => 'MOB',
                                            256781 => 'FAX',
                                            256783 => 'HOME',
                                            256785 => 'OTHER',
                                        ],
                                ],
                            1 =>
                                [
                                    'id'       => '123907',
                                    'name'     => 'Email',
                                    'code'     => 'EMAIL',
                                    'multiple' => 'Y',
                                    'type_id'  => '8',
                                    'disabled' => '0',
                                    'sort'     => 6,
                                    'enums'    =>
                                        [
                                            256787 => 'WORK',
                                            256789 => 'PRIV',
                                            256791 => 'OTHER',
                                        ],
                                ],
                            2 =>
                                [
                                    'id'       => '123909',
                                    'name'     => 'Web',
                                    'code'     => 'WEB',
                                    'multiple' => 'N',
                                    'type_id'  => '7',
                                    'disabled' => '0',
                                    'sort'     => 8,
                                ],
                            3 =>
                                [
                                    'id'       => '123913',
                                    'name'     => 'Адрес',
                                    'code'     => 'ADDRESS',
                                    'multiple' => 'N',
                                    'type_id'  => '9',
                                    'disabled' => '0',
                                    'sort'     => 12,
                                ],
                        ],
                    'customers' =>
                        [
                        ],
                ],
            'note_types'              =>
                [
                    0  =>
                        [
                            'id'       => 1,
                            'name'     => '',
                            'code'     => 'DEAL_CREATED',
                            'editable' => 'N',
                        ],
                    1  =>
                        [
                            'id'       => 2,
                            'name'     => '',
                            'code'     => 'CONTACT_CREATED',
                            'editable' => 'N',
                        ],
                    2  =>
                        [
                            'id'       => 3,
                            'name'     => '',
                            'code'     => 'DEAL_STATUS_CHANGED',
                            'editable' => 'N',
                        ],
                    3  =>
                        [
                            'id'       => 4,
                            'name'     => '',
                            'code'     => 'COMMON',
                            'editable' => 'Y',
                        ],
                    4  =>
                        [
                            'id'       => 5,
                            'name'     => '',
                            'code'     => 'ATTACHEMENT',
                            'editable' => 'N',
                        ],
                    5  =>
                        [
                            'id'       => 6,
                            'name'     => '',
                            'code'     => 'CALL',
                            'editable' => 'N',
                        ],
                    6  =>
                        [
                            'id'       => 7,
                            'name'     => '',
                            'code'     => 'MAIL_MESSAGE',
                            'editable' => 'N',
                        ],
                    7  =>
                        [
                            'id'       => 8,
                            'name'     => '',
                            'code'     => 'MAIL_MESSAGE_ATTACHMENT',
                            'editable' => 'N',
                        ],
                    8  =>
                        [
                            'id'       => 9,
                            'name'     => '',
                            'code'     => 'EXTERNAL_ATTACH',
                            'editable' => 'N',
                        ],
                    9  =>
                        [
                            'id'       => 10,
                            'name'     => '',
                            'code'     => 'CALL_IN',
                            'editable' => 'N',
                        ],
                    10 =>
                        [
                            'id'       => 11,
                            'name'     => '',
                            'code'     => 'CALL_OUT',
                            'editable' => 'N',
                        ],
                    11 =>
                        [
                            'id'       => 12,
                            'name'     => '',
                            'code'     => 'COMPANY_CREATED',
                            'editable' => 'N',
                        ],
                    12 =>
                        [
                            'id'       => 13,
                            'name'     => '',
                            'code'     => 'TASK_RESULT',
                            'editable' => 'N',
                        ],
                    13 =>
                        [
                            'id'       => 17,
                            'name'     => '',
                            'code'     => 'CHAT',
                            'editable' => 'N',
                        ],
                    14 =>
                        [
                            'id'       => 99,
                            'name'     => '',
                            'code'     => 'MAX_SYSTEM',
                            'editable' => 'N',
                        ],
                    15 =>
                        [
                            'id'       => 101,
                            'name'     => 'Ссылка',
                            'code'     => 'DROPBOX',
                            'editable' => 'N',
                        ],
                    16 =>
                        [
                            'id'       => 102,
                            'name'     => 'Входящее',
                            'code'     => 'SMS_IN',
                            'editable' => 'N',
                        ],
                    17 =>
                        [
                            'id'       => 103,
                            'name'     => 'Исходящее',
                            'code'     => 'SMS_OUT',
                            'editable' => 'N',
                        ],
                ],
            'task_types'              =>
                [
                    [
                        'id'   => 1,
                        'name' => 'Связаться с клиентом',
                        'code' => 'FOLLOW_UP',
                    ],
                    [
                        'id'   => 1,
                        'name' => 'Звонок',
                        'code' => 'CALL',
                    ],
                    [
                        'id'   => 2,
                        'name' => 'Встреча',
                        'code' => 'MEETING',
                    ],
                    [
                        'id'   => 3,
                        'name' => 'Письмо',
                        'code' => 'LETTER',
                    ],
                    [
                        'id'   => '907192',
                        'name' => 'Кастомный тип',
                        'code' => '',
                    ],
                ],
            'pipelines'               =>
                [
                    1064215 =>
                        [
                            'id'       => 1064215,
                            'value'    => 1064215,
                            'label'    => 'Воронка',
                            'name'     => 'Воронка',
                            'sort'     => 1,
                            'is_main'  => true,
                            'statuses' =>
                                [
                                    142      =>
                                        [
                                            'id'          => 142,
                                            'name'        => 'Успешно реализовано',
                                            'color'       => '#CCFF66',
                                            'sort'        => 10000,
                                            'editable'    => 'N',
                                            'pipeline_id' => 1064215,
                                        ],
                                    143      =>
                                        [
                                            'id'          => 143,
                                            'name'        => 'Закрыто и не реализовано',
                                            'color'       => '#D5D8DB',
                                            'sort'        => 11000,
                                            'editable'    => 'N',
                                            'pipeline_id' => 1064215,
                                        ],
                                    19040299 =>
                                        [
                                            'id'          => 19040299,
                                            'name'        => 'Первичный контакт',
                                            'pipeline_id' => 1064215,
                                            'sort'        => 10,
                                            'color'       => '#99ccff',
                                            'editable'    => 'Y',
                                        ],
                                    19040302 =>
                                        [
                                            'id'          => 19040302,
                                            'name'        => 'Переговоры',
                                            'pipeline_id' => 1064215,
                                            'sort'        => 20,
                                            'color'       => '#ffff99',
                                            'editable'    => 'Y',
                                        ],
                                    19040305 =>
                                        [
                                            'id'          => 19040305,
                                            'name'        => 'Принимают решение',
                                            'pipeline_id' => 1064215,
                                            'sort'        => 30,
                                            'color'       => '#ffcc66',
                                            'editable'    => 'Y',
                                        ],
                                    19040308 =>
                                        [
                                            'id'          => 19040308,
                                            'name'        => 'Согласование договора',
                                            'pipeline_id' => 1064215,
                                            'sort'        => 40,
                                            'color'       => '#ffcccc',
                                            'editable'    => 'Y',
                                        ],
                                ],
                            'leads'    => 7,
                        ],
                    11111111 =>
                        [
                            'id'       => 11111111,
                            'value'    => 11111111,
                            'label'    => 'Привет',
                            'name'     => 'Привет',
                            'sort'     => 2,
                            'is_main'  => false,
                            'statuses' =>
                                [
                                    142      =>
                                        [
                                            'id'          => 142,
                                            'name'        => 'Успешно реализовано',
                                            'color'       => '#CCFF66',
                                            'sort'        => 10000,
                                            'editable'    => 'N',
                                            'pipeline_id' => 11111111,
                                        ],
                                    143      =>
                                        [
                                            'id'          => 143,
                                            'name'        => 'Закрыто и не реализовано',
                                            'color'       => '#D5D8DB',
                                            'sort'        => 11000,
                                            'editable'    => 'N',
                                            'pipeline_id' => 11111111,
                                        ],
                                    11140299 =>
                                        [
                                            'id'          => 11140299,
                                            'name'        => 'Первичный контакт',
                                            'pipeline_id' => 11111111,
                                            'sort'        => 10,
                                            'color'       => '#99ccff',
                                            'editable'    => 'Y',
                                        ],
                                    11140302 =>
                                        [
                                            'id'          => 11140302,
                                            'name'        => 'Переговоры',
                                            'pipeline_id' => 11111111,
                                            'sort'        => 20,
                                            'color'       => '#ffff99',
                                            'editable'    => 'Y',
                                        ],
                                    11140305 =>
                                        [
                                            'id'          => 11140305,
                                            'name'        => 'Принимают решение',
                                            'pipeline_id' => 11111111,
                                            'sort'        => 30,
                                            'color'       => '#ffcc66',
                                            'editable'    => 'Y',
                                        ],
                                    11140308 =>
                                        [
                                            'id'          => 11140308,
                                            'name'        => 'Согласование договора',
                                            'pipeline_id' => 11111111,
                                            'sort'        => 40,
                                            'color'       => '#ffcccc',
                                            'editable'    => 'Y',
                                        ],
                                ],
                            'leads'    => 7,
                        ],
                ],
            'timezoneoffset'          => '+03:00',
        ]);
    }

    public function testPlainProperty()
    {
        $this->assertEquals($this->account->getId(), 19040293);
        $this->assertEquals($this->account->getName(), 'CatCode');
        $this->assertEquals($this->account->getSubdomain(), 'catcode');
        $this->assertEquals($this->account->getTimezone(), 'Europe/Moscow');
        $this->assertEquals($this->account->getCurrency(), 'RUB');
        $this->assertEquals($this->account->getLanguage(), 'ru');
        $this->assertEquals($this->account->getDateFormat(), 'd.m.Y');
        $this->assertEquals($this->account->getShortDatePattern()['date'], 'd.m.Y');
        $this->assertEquals($this->account->getTimeFormat(), 'H:i:s');
        $this->assertEquals($this->account->getCountry(), 'ru');
        $this->assertEquals($this->account->getUnsortedOn(), 'Y');
        $this->assertEquals($this->account->getTimezoneoffset(), '+03:00');
    }

    public function testUsers()
    {
        $users = $this->account->getUsers();

        $this->assertInstanceOf(Users::class, $users);
        $this->assertEquals($users->count(), 2);
        $this->assertEquals($users->key(), 0);

        $users->next();

        $this->assertEquals($users->key(), 1);

        foreach ($users as $user) {
            $this->assertInstanceOf(User::class, $user);
        }

        $this->assertEquals($users->key(), 1);

        $users->rewind();

        $this->assertEquals($users->key(), 0);

        $user = $users->current();

        $this->assertEquals($user->getId(), 2291701);
        $this->assertEquals($user->getFullName(), 'Генадий Владимирович');
        $this->assertEquals($user->getPhoneNumber(), '12344');
        $this->assertEquals($user->getIsAdmin(), 'Y');
        $this->assertEquals($user->isAdmin(), true);

        $userFiltered = $users->getByGroupId(1);
        $this->assertEquals($userFiltered->count(), 1);

        $userFiltered = $users->getOneById(2291701);
        $this->assertEquals($userFiltered->getFullName(), 'Генадий Владимирович');

        $userFiltered = $users->getOneByEmail('support@catcode.io');
        $this->assertEquals($userFiltered->getFullName(), 'CatCode');
    }

    public function testGroups()
    {
        $groups = $this->account->getGroups();

        $this->assertInstanceOf(Groups::class, $groups);
        $this->assertEquals($groups->count(), 2);

        foreach ($groups as $group) {
            $this->assertInstanceOf(Group::class, $group);
        }

        $group = $groups->first();

        $this->assertEquals($group->getId(), 0);
        $this->assertEquals($group->getName(), 'Отдел продаж');

        $groupIds = $groups->getIdsByNames(['Отдел перепродаж']);
        $userFiltered = $this->account->getUsers()->getByGroupId($groupIds);
        $this->assertEquals($userFiltered->count(), 1);
        $this->assertEquals($userFiltered->first()->getId(), 2267143);
    }

    public function testCustomFields()
    {
        $customFields = $this->account->getCustomFields();

        $this->assertInstanceOf(CustomFields::class, $customFields);
        $this->assertTrue($customFields->count() > 0);

        foreach ($customFields as $customField) {
            $this->assertInstanceOf(CustomField::class, $customField);
        }

        $customField = $customFields->first();
        $this->assertInstanceOf(CustomField::class, $customField);

        $customField = $customFields->getOneById(199331);
        $this->assertInstanceOf(CustomField::class, $customField);
        $this->assertEquals($customField->getId(), 199331);
        $this->assertEquals($customField->getTypeId(), 13);

        $customField = $customFields->getOneByName('Тестовый флаг');
        $this->assertInstanceOf(CustomField::class, $customField);
        $this->assertEquals($customField->getId(), 199329);

        $customFields = $this->account->getCustomFields()->getByElementType(ElementType::COMPANY);
        $this->assertEquals($customFields->count(), 4);
        $customField = $customFields->first();
        $this->assertInstanceOf(CustomField::class, $customField);

        $customFields = $this->account->getCustomFields()->getByElementType(ElementType::LEAD);
        $this->assertEquals($customFields->count(), 13);

        $customFields = $this->account->getCustomFields()->getByElementType(ElementType::CONTACT);
        $this->assertEquals($customFields->count(), 5);

        $customFields = $this->account->getCustomFields()->getByElementType(ElementType::CUSTOMER);
        $this->assertEquals($customFields->count(), 0);

        $customFields = $this->account->getCustomFields()->getByElementType(1937);
        $this->assertEquals($customFields->count(), 3);

        $customField = $this->account->getCustomField(199331);
        $this->assertInstanceOf(CustomField::class, $customField);
    }

    public function testSubtypes()
    {
        $customFields = $this->account->getCustomFields();
        $customField  = $customFields->getOneByTypeId(13);
        $subtypes     = $customField->getSubtypes();

        $this->assertInstanceOf(Subtypes::class, $subtypes);

        foreach ($subtypes as $subtype) {
            $this->assertInstanceOf(Subtype::class, $subtype);
        }

        $subtype = $subtypes->first();

        $this->assertEquals($subtype->getId(), 1);
        $this->assertEquals($subtype->getTitle(), 'Адрес');
        $this->assertEquals($subtype->getName(), 'address_line_1');
    }

    public function testNoteTypes()
    {
        $noteTypes = $this->account->getNoteTypes();

        $this->assertInstanceOf(NoteTypes::class, $noteTypes);
        $this->assertTrue($noteTypes->count() > 0);

        foreach ($noteTypes as $noteType) {
            $this->assertInstanceOf(NoteType::class, $noteType);
        }

        $noteType = $noteTypes->first();
        $this->assertInstanceOf(NoteType::class, $noteType);

        $noteType = $noteTypes->getOneByCode('COMMON');
        $this->assertInstanceOf(NoteType::class, $noteType);
        $this->assertEquals($noteType->getId(), 4);
        $this->assertEquals($noteType->isEditable(), true);
    }

    public function testTaskTypes()
    {
        $taskTypes = $this->account->getTaskTypes();

        $this->assertInstanceOf(TaskTypes::class, $taskTypes);
        $this->assertTrue($taskTypes->count() > 0);

        foreach ($taskTypes as $taskType) {
            $this->assertInstanceOf(TaskType::class, $taskType);
        }

        $taskType = $taskTypes->first();
        $this->assertInstanceOf(TaskType::class, $taskType);

        $taskType = $taskTypes->getOneByCode('call');
        $this->assertInstanceOf(TaskType::class, $taskType);
        $this->assertEquals($taskType->getId(), 1);
        $this->assertEquals($taskType->getName(), 'Звонок');
    }

    public function testPipelines()
    {
        $pipelines = $this->account->getPipelines();

        $this->assertInstanceOf(Pipelines::class, $pipelines);
        $this->assertTrue($pipelines->count() > 0);

        foreach ($pipelines as $pipeline) {
            $this->assertInstanceOf(Pipeline::class, $pipeline);
        }

        $pipeline = $pipelines->first();
        $this->assertInstanceOf(Pipeline::class, $pipeline);

        $pipeline = $pipelines->getOneByName('Воронка');
        $this->assertInstanceOf(Pipeline::class, $pipeline);
        $this->assertEquals($pipeline->getId(), 1064215);
        $this->assertEquals($pipeline->isMain(), true);

        $pipeline = $pipelines->getOneById(1064215);
        $this->assertEquals($pipeline->getId(), 1064215);

        $pipeline = $this->account->getPipeline(1064215);
        $this->assertInstanceOf(Pipeline::class, $pipeline);
        $this->assertEquals($pipeline->getId(), 1064215);
    }

    public function testStatuses()
    {
        $pipelines = $this->account->getPipelines();
        $this->assertEquals([1064215, 11111111], $pipelines->getIds());

        $pipeline  = $pipelines->first();
        $statuses  = $pipeline->getStatuses();

        $this->assertInstanceOf(Statuses::class, $statuses);
        $this->assertTrue($statuses->count() > 0);

        foreach ($statuses as $status) {
            $this->assertInstanceOf(Status::class, $status);
        }

        $status = $statuses->first();
        $this->assertInstanceOf(Status::class, $status);

        $status = $statuses->getOneById(19040299);
        $this->assertInstanceOf(Status::class, $status);
        $this->assertEquals($status->getId(), 19040299);
        $this->assertEquals($status->isEditable(), true);

        $status = $statuses->getOneByName('Успешно реализовано');
        $this->assertEquals($status->getId(), 142);

        $status = $this->account->getPipeline(1064215)->getStatus(143);
        $this->assertInstanceOf(Status::class, $status);
        $this->assertEquals($status->getId(), 143);
        $this->assertEquals($status->getName(), 'Закрыто и не реализовано');

        $statuses = $this->account->getPipelines()->getStatusesActive();
        $this->assertInstanceOf(Statuses::class, $statuses);
        $this->assertEquals($statuses->count(), 8);

        $statuses = $this->account->getPipeline(1064215)->getStatusesActive();
        $this->assertInstanceOf(Statuses::class, $statuses);
        $this->assertEquals($statuses->count(), 4);

        $statuses = $this->account->getPipeline(1064215)->getStatuses()->getActive();
        $this->assertInstanceOf(Statuses::class, $statuses);
        $this->assertEquals($statuses->count(), 4);

        $this->assertEquals([19040299, 19040302, 19040305, 19040308], $statuses->getIds());
    }
}