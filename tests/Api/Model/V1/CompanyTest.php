<?php

use Amocrm\Api\Model\CustomField\CustomField;
use Amocrm\Api\Model\CustomField\CustomFields;
use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Company;

class CompanyTest extends TestCase
{
    /**
     * @var Company
     */
    private $company;

    public function setUp()
    {
        $this->company = Company::create(
            [
                'id'                  => 5575745,
                'name'                => 'Компания 1',
                'last_modified'       => 1523542810,
                'account_id'          => 19040293,
                'date_create'         => 1523355172,
                'created_user_id'     => 2291701,
                'modified_user_id'    => 2291701,
                'responsible_user_id' => 2291701,
                'group_id'            => 0,
                'closest_task'        => 0,
                'linked_company_id'   => '0',
                'company_name'        => '',
                'tags'                =>
                    [
                    ],
                'type'                => 'company',
                'custom_fields'       =>
                    [
                        0 =>
                            [
                                'id'     => '123905',
                                'name'   => 'Phone',
                                'code'   => 'PHONE',
                                'values' =>
                                    [
                                        0 =>
                                            [
                                                'value' => '911',
                                                'enum'  => '256775',
                                            ],
                                    ],
                            ],
                        1 =>
                            [
                                'id'     => '123907',
                                'name'   => 'Email',
                                'code'   => 'EMAIL',
                                'values' =>
                                    [
                                        0 =>
                                            [
                                                'value' => 'help_me@hell.org',
                                                'enum'  => '256787',
                                            ],
                                    ],
                            ],
                        2 =>
                            [
                                'id'     => '123909',
                                'name'   => 'Web',
                                'code'   => 'WEB',
                                'values' =>
                                    [
                                        0 =>
                                            [
                                                'value' => 'http://site.com',
                                            ],
                                    ],
                            ],
                        3 =>
                            [
                                'id'     => '252255',
                                'name'   => 'Юр. лицо',
                                'values' =>
                                    [
                                        0 =>
                                            [
                                                'value' =>
                                                    [
                                                        'name'                         => 'Серьёзная компания',
                                                        'entity_type'                  => NULL,
                                                        'vat_id'                       => '1234',
                                                        'tax_registration_reason_code' => NULL,
                                                        'address'                      => NULL,
                                                        'kpp'                          => '4321',
                                                        'external_uid'                 => NULL,
                                                    ],
                                            ],
                                    ],
                            ],
                    ],
                'linked_leads_id'     =>
                    [
                        0 => '1031821',
                    ],
            ]
        );
    }

    public function testPlainProperty()
    {
        $this->assertEquals($this->company->getId(), 5575745);
        $this->assertEquals($this->company->getName(), 'Компания 1');
        $this->assertEquals($this->company->getModifiedUserId(), 2291701);
        $this->assertEquals($this->company->getCreatedUserId(), 2291701);
        $this->assertEquals($this->company->getResponsibleUserId(), 2291701);
        $this->assertEquals($this->company->getLinkedLeadsId(), ['1031821']);

        $this->company->setName('Новое название');
        $this->company->setResponsibleUserId(111);

        $this->assertEquals($this->company->getName(), 'Новое название');
        $this->assertEquals($this->company->getResponsibleUserId(), 111);
    }

    public function testDateTime()
    {
        $dateCreate = $this->company->getDateCreate();
        $this->assertInstanceOf(DateTime::class, $dateCreate);
        $this->assertEquals($dateCreate->format('U'), 1523355172);

        $lastModified = $this->company->getLastModified();
        $this->assertInstanceOf(DateTime::class, $lastModified);
        $this->assertEquals($lastModified->format('U'), 1523542810);

        $this->assertNull($this->company->getClosestTask());
    }

    public function testCustomFields()
    {
        $customFields = $this->company->getCustomFields();

        $this->assertInstanceOf(CustomFields::class, $customFields);
        $this->assertEquals($customFields->count(), 4);
        $customField = $customFields->first();
        $this->assertInstanceOf(CustomField::class, $customField);

        $cf = $this->company->getCustomField(123907)->email();
        $this->assertEquals($cf->list(), ['help_me@hell.org']);
        $this->assertEquals([
            'id'     => '123907',
            'values' => [
                [
                    'value' => 'help_me@hell.org',
                    'enum'  => '256787',
                ],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->company->getCustomField(123905)->phone();
        $this->assertEquals($cf->list(), ['911']);
        $cf->add('55555555555');
        $this->assertEquals($cf->list(), ['911', '55555555555']);
        $cf->remove('911');
        $this->assertEquals($cf->list(), ['55555555555']);
        $this->assertEquals([
            'id'     => '123905',
            'values' => [
                [
                    'value' => '55555555555',
                    'enum'  => '256775',
                ],
            ],
        ], $cf->getModifiedForApi());
    }
}