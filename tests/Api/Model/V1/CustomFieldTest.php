<?php

use Amocrm\Api\Model\CustomField\CustomField;
use PHPUnit\Framework\TestCase;

class CustomFieldTest extends TestCase
{
    public function testText()
    {
        $cf = CustomField::create(['id' => 1])
            ->text()
            ->set(null);

        $this->assertNull($cf->get());

        $cf->set('asdf');

        $this->assertEquals('asdf', $cf->get());
    }

    public function testPhone()
    {
        $cf = CustomField::create(['id' => 1])
            ->phone();

        $this->assertEquals([
            'id' => 1,
            'values' => []
        ], $cf->getModifiedForApi());

        $cf->add(null);

        $this->assertNull($cf->get());
        $this->assertEquals([], $cf->list());
        $this->assertEquals([
            'id' => 1,
            'values' => []
        ], $cf->getModifiedForApi());

        $cf->add('23567');

        $this->assertEquals(['23567'], $cf->list());

        $this->assertEquals([
            'id' => 1,
            'values' => [[
                'value' => '23567',
                'enum' => 'MOB',
            ]],
        ], $cf->getModifiedForApi());
    }
}