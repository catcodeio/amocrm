<?php

use Amocrm\Api\Model\Companies;
use Amocrm\Api\Model\Company;
use PHPUnit\Framework\TestCase;

class CompaniesTest extends TestCase
{
    /**
     * @var Companies
     */
    private $companies;

    public function setUp()
    {
        $this->companies = Companies::create(
            [
                [
                    'id'                  => 6251953,
                    'name'                => 'Новая компания',
                    'last_modified'       => 1523490191,
                    'account_id'          => 19040293,
                    'date_create'         => 1523490191,
                    'created_user_id'     => 2291701,
                    'modified_user_id'    => 2291701,
                    'responsible_user_id' => 2291701,
                    'group_id'            => 0,
                    'closest_task'        => 0,
                    'linked_company_id'   => NULL,
                    'company_name'        => '',
                    'tags'                =>
                        [
                        ],
                    'type'                => 'company',
                    'custom_fields'       =>
                        [
                        ],
                    'linked_leads_id'     =>
                        [
                        ],
                ],
                [
                    'id'                  => 5575745,
                    'name'                => 'Компания 1',
                    'last_modified'       => 1523542810,
                    'account_id'          => 19040293,
                    'date_create'         => 1523355172,
                    'created_user_id'     => 2291701,
                    'modified_user_id'    => 2291701,
                    'responsible_user_id' => 2291701,
                    'group_id'            => 0,
                    'closest_task'        => 0,
                    'linked_company_id'   => '0',
                    'company_name'        => '',
                    'tags'                =>
                        [
                        ],
                    'type'                => 'company',
                    'custom_fields'       =>
                        [
                            0 =>
                                [
                                    'id'     => '123905',
                                    'name'   => 'Phone',
                                    'code'   => 'PHONE',
                                    'values' =>
                                        [
                                            0 =>
                                                [
                                                    'value' => '911',
                                                    'enum'  => '256775',
                                                ],
                                        ],
                                ],
                            1 =>
                                [
                                    'id'     => '123907',
                                    'name'   => 'Email',
                                    'code'   => 'EMAIL',
                                    'values' =>
                                        [
                                            0 =>
                                                [
                                                    'value' => 'help_me@hell.org',
                                                    'enum'  => '256787',
                                                ],
                                        ],
                                ],
                            2 =>
                                [
                                    'id'     => '123909',
                                    'name'   => 'Web',
                                    'code'   => 'WEB',
                                    'values' =>
                                        [
                                            0 =>
                                                [
                                                    'value' => 'http://site.com',
                                                ],
                                        ],
                                ],
                            3 =>
                                [
                                    'id'     => '252255',
                                    'name'   => 'Юр. лицо',
                                    'values' =>
                                        [
                                            0 =>
                                                [
                                                    'value' =>
                                                        [
                                                            'name'                         => 'Серьёзная компания',
                                                            'entity_type'                  => NULL,
                                                            'vat_id'                       => '1234',
                                                            'tax_registration_reason_code' => NULL,
                                                            'address'                      => NULL,
                                                            'kpp'                          => '4321',
                                                            'external_uid'                 => NULL,
                                                        ],
                                                ],
                                        ],
                                ],
                        ],
                    'linked_leads_id'     =>
                        [
                            0 => '1031821',
                        ],
                ],
            ]
        );
    }

    public function testIterates()
    {
        $this->assertInstanceOf(Companies::class, $this->companies);
        $this->assertEquals($this->companies->count(), 2);
        $this->assertEquals([6251953, 5575745], $this->companies->getIds());

        $this->assertEquals($this->companies->key(), 0);

        $this->companies->next();

        $this->assertEquals($this->companies->key(), 1);

        foreach ($this->companies as $company) {
            $this->assertInstanceOf(Company::class, $company);
        }

        $this->assertEquals($this->companies->key(), 1);

        $this->companies->rewind();

        $this->assertEquals($this->companies->key(), 0);

        $lead = $this->companies->current();

        $this->assertInstanceOf(Company::class, $lead);
    }
}