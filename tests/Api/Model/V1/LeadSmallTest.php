<?php

use Amocrm\Api\Model\CustomField\Type\TypeInterface;
use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Lead;
use Amocrm\Api\Model\Tag\Tags;
use Amocrm\Api\Model\Tag\Tag;
use Amocrm\Api\Model\CustomField\CustomFields;
use Amocrm\Api\Model\CustomField\CustomField;

/**
 * Тесторование сделки с минимальным заполнением. Добавленно только название
 * сделки. Остально amoCRM выставляет по умолчанию.
 */
class LeadSmallTest extends TestCase
{
    /**
     * @var Lead
     */
    private $lead;

    public function setUp()
    {
        $this->lead = Lead::create([
            'id'                  => '1704177',
            'name'                => 'Название сделки',
            'date_create'         => 1523192711,
            'created_user_id'     => '2291701',
            'last_modified'       => 1523192711,
            'account_id'          => '19040293',
            'price'               => '',
            'responsible_user_id' => '2291701',
            'linked_company_id'   => '',
            'group_id'            => 0,
            'pipeline_id'         => 1064215,
            'date_close'          => 0,
            'closest_task'        => 0,
            'loss_reason_id'      => 0,
            'deleted'             => 0,
            'tags'                => [],
            'status_id'           => '19040299',
            'custom_fields'       => [],
            'main_contact_id'     => false,
        ]);
    }

    public function testPlainProperty()
    {
        $this->assertEquals($this->lead->getId(), 1704177);
        $this->assertEquals($this->lead->getName(), 'Название сделки');
        $this->assertEquals($this->lead->getPrice(), 0);
        $this->assertEquals($this->lead->getResponsibleUserId(), 2291701);
        $this->assertEquals($this->lead->getLinkedCompanyId(), null);
        $this->assertEquals($this->lead->getStatusId(), 19040299);

        $this->lead->setName('Новое название');
        $this->lead->setPrice(100);
        $this->lead->setResponsibleUserId(111);
        $this->lead->setLinkedCompanyId(444);
        $this->lead->setStatusId(555);

        $this->assertEquals($this->lead->getName(), 'Новое название');
        $this->assertEquals($this->lead->getPrice(), 100);
        $this->assertEquals($this->lead->getResponsibleUserId(), 111);
        $this->assertEquals($this->lead->getLinkedCompanyId(), 444);
        $this->assertEquals($this->lead->getStatusId(), 555);
    }

    public function testDateTime()
    {
        $dateCreate = $this->lead->getDateCreate();
        $this->assertInstanceOf(DateTime::class, $dateCreate);
        $this->assertEquals($dateCreate->format('U'), 1523192711);

        $lastModified = $this->lead->getLastModified();
        $this->assertInstanceOf(DateTime::class, $lastModified);
        $this->assertEquals($lastModified->format('U'), 1523192711);
    }

    public function testTags()
    {
        $tags = $this->lead->getTags();
        $this->assertInstanceOf(Tags::class, $tags);
        $this->assertEquals($tags->count(), 0);
        $this->assertEquals(false, $tags->first());
        $this->assertEquals(false, $this->lead->getTag('тестовая'));

        $this->lead->addTag('новый тег');
        $this->lead->removeTag('тестовая');

        $tag = $this->lead->getTags()->first();
        $this->assertInstanceOf(Tag::class, $tag);
        $this->assertEquals($tag->getId(), null);
        $this->assertEquals($tag->getName(), 'новый тег');
        $this->assertEquals($tags->count(), 1);
    }

    public function testCustomFields()
    {
        $customFields = $this->lead->getCustomFields();

        $this->assertInstanceOf(CustomFields::class, $customFields);
        $this->assertEquals($customFields->count(), 0);
        $this->assertEquals(false, $customFields->first());
        $this->assertInstanceOf(CustomField::class, $this->lead->getCF(199329));
        $this->assertTrue($this->lead->getCF(199329)->isEmpty());

        $cf = CustomField::create(['id' => 199329]);
        $cf->checkbox()->set(true);
        $this->assertInstanceOf(CustomField::class, $cf);
        $this->lead->setCustomField($cf->checkbox());

        $cf = $this->lead->getCustomfield(199329)->checkbox();
        $this->assertEquals($cf->get(), true);
        $this->assertEquals([
            'id' => '199329',
            'values' => [
                ['value' => '1'],
            ],
        ], $cf->getModifiedForApi());
        $cf->set(false);
        $this->assertEquals($cf->get(), false);
        $this->assertEquals([
            'id' => '199329',
            'values' => [['value' => 0]],
        ], $cf->getModifiedForApi());

        $cf = (CustomField::create(['id' => 217981]))->date();
        $cf->set(new DateTime('30.08.2000'));
        $this->assertInstanceOf(TypeInterface::class, $cf);
        $this->lead->setCustomField($cf);

        $cf = $this->lead->getCustomfield(217981)->date();
        $this->assertInstanceOf(DateTime::class, $cf->get());
        $this->assertEquals($cf->get()->format('d.m.Y'), '30.08.2000');
        $this->assertEquals([
            'id' => '217981',
            'values' => [
                ['value' => '30.08.2000'],
            ],
        ], $cf->getModifiedForApi());

        $this->lead->setCF($cf->set(new DateTime('10.08.2000')));
        $cf = $this->lead->getCustomfield(217981)->date();
        $this->assertInstanceOf(DateTime::class, $cf->get());
        $this->assertEquals($cf->get()->format('d.m.Y'), '10.08.2000');
        $this->assertEquals([
            'id' => '217981',
            'values' => [
                ['value' => '10.08.2000'],
            ],
        ], $cf->getModifiedForApi());
    }
}