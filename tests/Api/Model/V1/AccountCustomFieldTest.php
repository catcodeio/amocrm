<?php

use Amocrm\Api\Model\Account\CustomField;
use Amocrm\Api\Model\CustomField\Type\Phone;
use Amocrm\Api\Model\CustomField\Type\Email;
use Amocrm\Api\Model\CustomField\Type\Im;
use PHPUnit\Framework\TestCase;

class AccountCustomFieldTest extends TestCase
{
    public function testPhonesEnum()
    {
        $cf = CustomField::create([
            'id'           => '12841',
            'name'         => 'Телефон',
            'code'         => 'PHONE',
            'multiple'     => 'Y',
            'type_id'      => '8',
            'disabled'     => '0',
            'sort'         => 4,
            'is_required'  => false,
            'is_deletable' => true,
            'is_visible'   => true,
            'enums'        => [
                '27709' => 'WORK',
                '27711' => 'WORKDD',
                '27713' => 'MOB',
                '27715' => 'FAX',
                '27717' => 'HOME',
                '27719' => 'OTHER',
            ],
        ]);

        $this->assertEquals(27709, $cf->getEnumId(Phone::ENUM_WORK));
        $this->assertEquals(27711, $cf->getEnumId(Phone::ENUM_WORKDD));
        $this->assertEquals(27713, $cf->getEnumId(Phone::ENUM_MOB));
        $this->assertEquals(27715, $cf->getEnumId(Phone::ENUM_FAX));
        $this->assertEquals(27717, $cf->getEnumId(Phone::ENUM_HOME));
        $this->assertEquals(27719, $cf->getEnumId(Phone::ENUM_OTHER));

        $this->assertNull($cf->getEnumId('asdf'));
    }

    public function testEmailsEnum()
    {
        $cf = CustomField::create([
            'id'           => '12843',
            'name'         => 'Email',
            'code'         => 'EMAIL',
            'multiple'     => 'Y',
            'type_id'      => '8',
            'disabled'     => '0',
            'sort'         => 6,
            'is_required'  => false,
            'is_deletable' => true,
            'is_visible'   => true,
            'enums'        => [
                '27721' => 'WORK',
                '27723' => 'PRIV',
                '27725' => 'OTHER',
            ],
        ]);

        $this->assertEquals(27721, $cf->getEnumId(Email::ENUM_WORK));
        $this->assertEquals(27723, $cf->getEnumId(Email::ENUM_PRIV));
        $this->assertEquals(27725, $cf->getEnumId(Email::ENUM_OTHER));
    }

    public function testImsEnum()
    {
        $cf = CustomField::create([
            'id'           => '12847',
            'name'         => 'Мгн. сообщения',
            'code'         => 'IM',
            'multiple'     => 'Y',
            'type_id'      => '8',
            'disabled'     => '0',
            'sort'         => 10,
            'is_required'  => false,
            'is_deletable' => true,
            'is_visible'   => true,
            'enums'        => [
                '27727' => 'SKYPE',
                '27729' => 'ICQ',
                '27731' => 'JABBER',
                '27733' => 'GTALK',
                '27735' => 'MSN',
                '27737' => 'OTHER',
            ],
        ]);

        $this->assertEquals(27727, $cf->getEnumId(Im::ENUM_SKYPE));
        $this->assertEquals(27729, $cf->getEnumId(Im::ENUM_ICQ));
        $this->assertEquals(27731, $cf->getEnumId(Im::ENUM_JABBER));
        $this->assertEquals(27733, $cf->getEnumId(Im::ENUM_GTALK));
        $this->assertEquals(27735, $cf->getEnumId(Im::ENUM_MSN));
        $this->assertEquals(27737, $cf->getEnumId(Im::ENUM_OTHER));
    }
}