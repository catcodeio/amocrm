<?php

use Amocrm\Api\Model\Contacts;
use Amocrm\Api\Model\Contact;
use PHPUnit\Framework\TestCase;

class ContactsTest extends TestCase
{
    /**
     * @var Contacts
     */
    private $contacts;

    public function setUp()
    {
        $this->contacts = Contacts::create(
            [
                [
                    'id'                  => 5207409,
                    'name'                => 'new name2',
                    'last_modified'       => 1523314701,
                    'account_id'          => 19040293,
                    'date_create'         => 1523314701,
                    'created_user_id'     => 2291701,
                    'modified_user_id'    => 2291701,
                    'responsible_user_id' => 2291701,
                    'group_id'            => 0,
                    'closest_task'        => 0,
                    'linked_company_id'   => NULL,
                    'company_name'        => '',
                    'tags'                =>
                        [
                        ],
                    'type'                => 'contact',
                    'custom_fields'       =>
                        [
                        ],
                    'linked_leads_id'     =>
                        [
                        ],
                ],
                [
                    'id'                  => 2986625,
                    'name'                => 'new name',
                    'last_modified'       => 1523538510,
                    'account_id'          => 19040293,
                    'date_create'         => 1522415760,
                    'created_user_id'     => 2291701,
                    'modified_user_id'    => 2291701,
                    'responsible_user_id' => 2291701,
                    'group_id'            => 0,
                    'closest_task'        => 0,
                    'linked_company_id'   => '6251953',
                    'company_name'        => 'Новая компания',
                    'tags'                =>
                        [
                            0 =>
                                [
                                    'id'           => 75649,
                                    'name'         => 'два слова',
                                    'element_type' => 1,
                                ],
                            1 =>
                                [
                                    'id'           => 75651,
                                    'name'         => 'одно',
                                    'element_type' => 1,
                                ],
                            2 =>
                                [
                                    'id'           => 78905,
                                    'name'         => 'привет5',
                                    'element_type' => 1,
                                ],
                        ],
                    'type'                => 'contact',
                    'custom_fields'       =>
                        [
                            0 =>
                                [
                                    'id'     => '123905',
                                    'name'   => 'Телефон',
                                    'code'   => 'PHONE',
                                    'values' =>
                                        [
                                            0 =>
                                                [
                                                    'value' => '79009090911',
                                                    'enum'  => '256775',
                                                ],
                                            1 =>
                                                [
                                                    'value' => '11111233333',
                                                    'enum'  => '256775',
                                                ],
                                            2 =>
                                                [
                                                    'value' => '54365434655',
                                                    'enum'  => '256779',
                                                ],
                                        ],
                                ],
                            1 =>
                                [
                                    'id'     => '123907',
                                    'name'   => 'Email',
                                    'code'   => 'EMAIL',
                                    'values' =>
                                        [
                                            0 =>
                                                [
                                                    'value' => 'test@mail.ru',
                                                    'enum'  => '256787',
                                                ],
                                        ],
                                ],
                            2 =>
                                [
                                    'id'     => '123911',
                                    'name'   => 'Мгн. сообщения',
                                    'code'   => 'IM',
                                    'values' =>
                                        [
                                            0 =>
                                                [
                                                    'value' => 'asdf',
                                                    'enum'  => '256793',
                                                ],
                                            1 =>
                                                [
                                                    'value' => 'qqqqq',
                                                    'enum'  => '256801',
                                                ],
                                        ],
                                ],
                            3 =>
                                [
                                    'id'     => '252239',
                                    'name'   => 'Juridical person',
                                    'values' =>
                                        [
                                            0 =>
                                                [
                                                    'value' =>
                                                        [
                                                            'name'                         => 'asdf',
                                                            'entity_type'                  => NULL,
                                                            'vat_id'                       => '123123',
                                                            'tax_registration_reason_code' => NULL,
                                                            'address'                      => NULL,
                                                            'kpp'                          => '1123',
                                                            'external_uid'                 => NULL,
                                                        ],
                                                ],
                                        ],
                                ],
                        ],
                    'linked_leads_id'     =>
                        [
                            0 => '1031821',
                        ],
                ],
                [
                    'id'                  => 6498085,
                    'name'                => 'Новая сделка',
                    'last_modified'       => 1523538510,
                    'account_id'          => 19040293,
                    'date_create'         => 1523538510,
                    'created_user_id'     => 2291701,
                    'modified_user_id'    => 2291701,
                    'responsible_user_id' => 2291701,
                    'group_id'            => 0,
                    'closest_task'        => 0,
                    'linked_company_id'   => NULL,
                    'company_name'        => '',
                    'tags'                =>
                        [
                            0 =>
                                [
                                    'id'           => 78903,
                                    'name'         => 'Проверочный тег',
                                    'element_type' => 1,
                                ],
                        ],
                    'type'                => 'contact',
                    'custom_fields'       =>
                        [
                            0 =>
                                [
                                    'id'     => '123903',
                                    'name'   => 'Должность',
                                    'code'   => 'POSITION',
                                    'values' =>
                                        [
                                            0 =>
                                                [
                                                    'value' => 'Важный чел',
                                                ],
                                        ],
                                ],
                            1 =>
                                [
                                    'id'     => '123905',
                                    'name'   => 'Телефон',
                                    'code'   => 'PHONE',
                                    'values' =>
                                        [
                                            0 =>
                                                [
                                                    'value' => '522522',
                                                    'enum'  => '256779',
                                                ],
                                        ],
                                ],
                        ],
                    'linked_leads_id'     =>
                        [
                        ],
                ],
            ]
        );
    }

    public function testIterates()
    {
        $this->assertInstanceOf(Contacts::class, $this->contacts);
        $this->assertEquals($this->contacts->count(), 3);
        $this->assertEquals([5207409, 2986625, 6498085], $this->contacts->getIds());

        $this->assertEquals($this->contacts->key(), 0);

        $this->contacts->next();

        $this->assertEquals($this->contacts->key(), 1);

        foreach ($this->contacts as $lead) {
            $this->assertInstanceOf(Contact::class, $lead);
        }

        $this->assertEquals($this->contacts->key(), 1);

        $this->contacts->rewind();

        $this->assertEquals($this->contacts->key(), 0);

        $lead = $this->contacts->current();

        $this->assertInstanceOf(Contact::class, $lead);
    }
}