<?php

use Amocrm\Api\Model\CustomField\Type\TypeInterface;
use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Customer;
use Amocrm\Api\Model\Tag\Tags;
use Amocrm\Api\Model\Tag\Tag;
use Amocrm\Api\Model\CustomField\CustomFields;
use Amocrm\Api\Model\CustomField\CustomField;

/**
 * Тесторование сущности с минимальным заполнением. Добавлено только название и
 * дата следующей покупки.
 */
class CustomerSmallTest extends TestCase
{
    /**
     * @var Customer
     */
    private $customer;

    public function setUp()
    {
        $this->customer = Customer::create([
            "id"              => 113945,
            "date_create"     => 1534873031,
            "date_modify"     => 1534873031,
            "created_by"      => 2291701,
            "modified_by"     => 2291701,
            "account_id"      => 19040293,
            "main_user_id"    => 2291701,
            "name"            => "Название покупателя",
            "deleted"         => false,
            "next_price"      => 0,
            "periodicity"     => 0,
            "next_date"       => 1533675600,
            "task_last_date"  => null,
            "status_id"       => 126352,
            "ltv"             => null,
            "purchases_count" => null,
            "average_check"   => null,
            "custom_fields"   => [],
            "tags"            => [],
            "main_contact_id" => false,
            "period_id"       => "126352",
        ]);
    }

    public function testPlainProperty()
    {
        $this->assertEquals($this->customer->getId(), 113945);
        $this->assertEquals($this->customer->getName(), 'Название покупателя');
        $this->assertEquals($this->customer->getNextPrice(), 0);
        $this->assertEquals($this->customer->getMainUserId(), 2291701);
        $this->assertEquals($this->customer->getPeriodId(), 126352);

        $this->customer->setName('Новое название');
        $this->customer->setNextPrice(100);
        $this->customer->setMainUserId(111);
        $this->customer->setPeriodId(444);

        $this->assertEquals($this->customer->getName(), 'Новое название');
        $this->assertEquals($this->customer->getNextPrice(), 100);
        $this->assertEquals($this->customer->getMainUserId(), 111);
        $this->assertEquals($this->customer->getPeriodId(), 444);
    }

    public function testDateTime()
    {
        $dateCreate = $this->customer->getDateCreate();
        $this->assertInstanceOf(DateTime::class, $dateCreate);
        $this->assertEquals($dateCreate->format('U'), 1534873031);

        $dateModify = $this->customer->getDateModify();
        $this->assertInstanceOf(DateTime::class, $dateModify);
        $this->assertEquals($dateModify->format('U'), 1534873031);

        $nextDate = $this->customer->getNextDate();
        $this->assertInstanceOf(DateTime::class, $nextDate);
        $this->assertEquals($nextDate->format('U'), 1533675600);

        $taskLastDate = $this->customer->getTaskLastDate();
        $this->assertNull($taskLastDate);
    }

    public function testTags()
    {
        $tags = $this->customer->getTags();
        $this->assertInstanceOf(Tags::class, $tags);
        $this->assertEquals($tags->count(), 0);
        $this->assertEquals(false, $tags->first());
        $this->assertEquals(false, $this->customer->getTag('тестовая'));

        $this->customer->addTag('новый тег');
        $this->customer->removeTag('тестовая');

        $tag = $this->customer->getTags()->first();
        $this->assertInstanceOf(Tag::class, $tag);
        $this->assertEquals($tag->getId(), null);
        $this->assertEquals($tag->getName(), 'новый тег');
        $this->assertEquals($tags->count(), 1);
    }

    public function testCustomFields()
    {
        $customFields = $this->customer->getCustomFields();

        $this->assertInstanceOf(CustomFields::class, $customFields);
        $this->assertEquals($customFields->count(), 0);
        $this->assertEquals(false, $customFields->first());
        $this->assertInstanceOf(CustomField::class, $this->customer->getCF(199329));
        $this->assertTrue($this->customer->getCF(199329)->isEmpty());

        $cf = CustomField::create(['id' => 199329]);
        $cf->checkbox()->set(true);
        $this->assertInstanceOf(CustomField::class, $cf);
        $this->customer->setCustomField($cf->checkbox());

        $cf = $this->customer->getCustomfield(199329)->checkbox();
        $this->assertEquals($cf->get(), true);
        $this->assertEquals([
            'id' => '199329',
            'values' => [['value' => 1]],
        ], $cf->getModifiedForApi());
        $cf->set(false);
        $this->assertEquals($cf->get(), false);
        $this->assertEquals([
            'id' => '199329',
            'values' => [['value' => 0]],
        ], $cf->getModifiedForApi());

        $cf = (CustomField::create(['id' => 217981]))->date();
        $cf->set(new DateTime('30.08.2000'));
        $this->assertInstanceOf(TypeInterface::class, $cf);
        $this->customer->setCustomField($cf);

        $cf = $this->customer->getCustomfield(217981)->date();
        $this->assertInstanceOf(DateTime::class, $cf->get());
        $this->assertEquals($cf->get()->format('d.m.Y'), '30.08.2000');
        $this->assertEquals([
            'id' => '217981',
            'values' => [
                ['value' => '30.08.2000'],
            ],
        ], $cf->getModifiedForApi());

        $this->customer->setCF($cf->set(new DateTime('10.08.2000')));
        $cf = $this->customer->getCustomfield(217981)->date();
        $this->assertInstanceOf(DateTime::class, $cf->get());
        $this->assertEquals($cf->get()->format('d.m.Y'), '10.08.2000');
        $this->assertEquals([
            'id' => '217981',
            'values' => [
                ['value' => '10.08.2000'],
            ],
        ], $cf->getModifiedForApi());
    }
}