<?php

use Amocrm\Api\Model\CustomField\CustomField;
use Amocrm\Api\Model\CustomField\CustomFields;
use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Transaction;
use Amocrm\Api\Model\Tag\Tags;
use Amocrm\Api\Model\Tag\Tag;

class TransactionTest extends TestCase
{
    /**
     * @var Transaction
     */
    private $transaction;

    public function setUp()
    {
        $this->transaction = Transaction::create([
            'id'          => 1031821,
            'date_create' => 1522415760,
            'date_modify' => 1522415960,
            'created_by'  => 2291701,
            'modified_by' => 1523190638,
            'account_id'  => 19040293,
            'customer_id' => 40293,
            'comment'     => 'Комментарий',
            'deleted'     => false,
            'price'       => '320',
            'date'        => 1523190638,
        ]);
    }

    public function testPlainProperty()
    {
        $this->assertEquals($this->transaction->getId(), 1031821);
        $this->assertEquals($this->transaction->getComment(), 'Комментарий');
        $this->assertEquals($this->transaction->getPrice(), 320);
        $this->assertEquals($this->transaction->getCreatedBy(), 2291701);
        $this->assertEquals($this->transaction->getCustomerId(), 40293);
        $this->assertEquals($this->transaction->getAccountId(), 19040293);
        $this->assertEquals($this->transaction->isDeleted(), false);
        $this->assertEquals($this->transaction->getModifiedBy(), 1523190638);

        $this->transaction->setComment('Новое');
        $this->transaction->setPrice(100);

        $this->assertEquals($this->transaction->getComment(), 'Новое');
        $this->assertEquals($this->transaction->getPrice(), 100);
    }

    public function testDateTime()
    {
        $dateCreate = $this->transaction->getDateCreate();
        $this->assertInstanceOf(DateTime::class, $dateCreate);
        $this->assertEquals($dateCreate->format('U'), 1522415760);

        $dateModify = $this->transaction->getDateModify();
        $this->assertInstanceOf(DateTime::class, $dateModify);
        $this->assertEquals($dateModify->format('U'), 1522415960);

        $date = $this->transaction->getDate();
        $this->assertInstanceOf(DateTime::class, $date);
        $this->assertEquals($date->format('U'), 1523190638);

        $newDate = new DateTime();
        $this->transaction->setDate($newDate);

        $date = $this->transaction->getDate();
        $this->assertInstanceOf(DateTime::class, $date);
        $this->assertEquals($date->format('U'), $newDate->format('U'));
    }

    public function testCopy()
    {
        $transaction = clone $this->transaction;
        $this->assertInstanceOf(Transaction::class, $transaction);
        $this->assertNotEquals($this->transaction, $transaction);
        $this->assertNotEquals($transaction->getId(), $this->transaction->getId());
        $this->assertNull($transaction->getId());
        $this->assertEquals($transaction->getComment(), $this->transaction->getComment());

        $transactionArray = $transaction->getModifiedForApi();
        $this->assertEquals($this->transaction->getComment(), $transactionArray['comment']);

        $dateModify = new DateTime();
        $transaction->setDateModify($dateModify);

        $this->assertEquals([
            'id'              => null,
            'date_create'     => 1522415760,
            'date_modify'     => $dateModify->format('U'),
            'created_by'      => 2291701,
            'modified_by'     => 1523190638,
            'account_id'      => 19040293,
            'customer_id'     => 40293,
            'comment'         => 'Комментарий',
            'deleted'         => false,
            'price'           => '320',
            'date'            => 1523190638,
        ], $transaction->getModifiedForApi());
    }
}