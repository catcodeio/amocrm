<?php

use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Company;
use Amocrm\Api\Model\CustomField\CustomFields;
use Amocrm\Api\Model\CustomField\CustomField;

/**
 * Тесторование компании с минимальным заполнением. Добавленно только название.
 * Остально amoCRM выставляет по умолчанию.
 */
class CompanySmallTest extends TestCase
{
    /**
     * @var Company
     */
    private $company;

    public function setUp()
    {
        $this->company = Company::create([
            'id'                  => 6251953,
            'name'                => 'Новая компания',
            'last_modified'       => 1523490191,
            'account_id'          => 19040293,
            'date_create'         => 1523490191,
            'created_user_id'     => 2291701,
            'modified_user_id'    => 2291701,
            'responsible_user_id' => 2291701,
            'group_id'            => 0,
            'closest_task'        => 0,
            'linked_company_id'   => NULL,
            'company_name'        => '',
            'tags'                => [],
            'type'                => 'company',
            'custom_fields'       => [],
            'linked_leads_id'     => [],
        ]);
    }

    public function testPlainProperty()
    {
        $this->assertEquals($this->company->getId(), 6251953);
        $this->assertEquals($this->company->getName(), 'Новая компания');
        $this->assertEquals($this->company->getResponsibleUserId(), 2291701);
        $this->assertEquals($this->company->getLinkedLeadsId(), []);

        $this->company->setName('Новое название');
        $this->company->setResponsibleUserId(111);

        $this->assertEquals($this->company->getName(), 'Новое название');
        $this->assertEquals($this->company->getResponsibleUserId(), 111);
    }

    public function testDateTime()
    {
        $dateCreate = $this->company->getDateCreate();
        $this->assertInstanceOf(DateTime::class, $dateCreate);
        $this->assertEquals($dateCreate->format('U'), 1523490191);

        $lastModified = $this->company->getLastModified();
        $this->assertInstanceOf(DateTime::class, $lastModified);
        $this->assertEquals($lastModified->format('U'), 1523490191);

        $this->assertNull($this->company->getClosestTask());
    }

    public function testCustomFields()
    {
        $customFields = $this->company->getCustomFields();

        $this->assertInstanceOf(CustomFields::class, $customFields);
        $this->assertEquals($customFields->count(), 0);
        $this->assertFalse($customFields->first());

        $this->assertInstanceOf(CustomField::class, $this->company->getCustomField(123907));
        $this->assertTrue($this->company->getCustomField(123907)->isEmpty());

        $cf = CustomField::create(['id' => 123907])->email();
        $this->assertEquals($cf->list(), []);
        $cf->add('qwer@qwer.rr');
        $this->assertEquals($cf->list(), ['qwer@qwer.rr']);
        $this->assertEquals([
            'id'     => '123907',
            'values' => [
                [
                    'value' => 'qwer@qwer.rr',
                    'enum'  => 'WORK',
                ],
            ],
        ], $cf->getModifiedForApi());

        $this->assertInstanceOf(CustomField::class, $this->company->getCustomField(123905));
        $this->assertTrue($this->company->getCustomField(123905)->isEmpty());

        $cf = CustomField::create(['id' => 123905])->phone();
        $this->assertEquals($cf->list(), []);
        $cf->add('55555555555');
        $this->assertEquals($cf->list(), ['55555555555']);
        $this->assertEquals([
            'id'     => '123905',
            'values' => [
                [
                    'value' => '55555555555',
                    'enum'  => 'MOB',
                ],
            ],
        ], $cf->getModifiedForApi());

        $this->assertInstanceOf(CustomField::class, $this->company->getCustomField(123911));
        $this->assertTrue($this->company->getCustomField(123911)->isEmpty());

        $cf = CustomField::create(['id' => 123911])->im();
        $this->assertEquals($cf->list(), []);
        $cf->add('www');
        $this->assertEquals($cf->list(), ['www']);
        $this->assertEquals([
            'id'     => '123911',
            'values' => [
                [
                    'value' => 'www',
                    'enum'  => 'OTHER',
                ],
            ],
        ], $cf->getModifiedForApi());

        $this->assertInstanceOf(CustomField::class, $this->company->getCustomField(252239));
        $this->assertTrue($this->company->getCustomField(252239)->isEmpty());

        $cf = CustomField::create(['id' => 252239])->entity();
        $this->assertNull($cf->getName());
        $this->assertNull($cf->getEntityType());
        $this->assertNull($cf->getVatId());
        $this->assertNull($cf->getTaxRegistrationReasonCode());
        $this->assertNull($cf->getAddress());
        $this->assertNull($cf->getKpp());
        $this->assertNull($cf->getExternalUid());
        $cf
            ->setName('New')
            ->setKpp('New1')
            ->setAddress('address')
            ->setTaxRegistrationReasonCode('code')
        ;
        $this->assertEquals($cf->getName(), 'New');
        $this->assertEquals($cf->getTaxRegistrationReasonCode(), 'code');
        $this->assertEquals($cf->getAddress(), 'address');
        $this->assertEquals($cf->getKpp(), 'New1');
        $this->assertNull($cf->getEntityType());
        $this->assertNull($cf->getVatId());
        $this->assertNull($cf->getExternalUid());
        $this->assertEquals([
            'id'     => '252239',
            'values' => [
                [
                    'value' => [
                        'name'                         => 'New',
                        'tax_registration_reason_code' => 'code',
                        'address'                      => 'address',
                        'kpp'                          => 'New1',
                    ],
                ],
            ],
        ], $cf->getModifiedForApi());
    }
}