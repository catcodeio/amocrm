<?php

use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Transaction;
use Amocrm\Api\Model\Transactions;

class TransactionsTest extends TestCase
{
    /**
     * @var Transactions
     */
    private $entities;

    public function setUp()
    {
        $this->entities = Transactions::create([
            "23869" => [
                "id"          => 23869,
                "date_create" => 1535332963,
                "date_modify" => 1535332963,
                "created_by"  => 2291701,
                "modified_by" => 2291701,
                "account_id"  => 19040293,
                "customer_id" => 114555,
                "deleted"     => false,
                "price"       => 111,
                "comment"     => "asdfasdf",
                "date"        => 1535332920,
            ],
            "23871" => [
                "id"          => 23871,
                "date_create" => 1535333134,
                "date_modify" => 1535333134,
                "created_by"  => 2291701,
                "modified_by" => 2291701,
                "account_id"  => 19040293,
                "customer_id" => 114555,
                "deleted"     => false,
                "price"       => 0,
                "comment"     => null,
                "date"        => 1535333100,
            ],
        ]);
    }

    public function testIterates()
    {
        $this->assertInstanceOf(Transactions::class, $this->entities);
        $this->assertEquals($this->entities->count(), 2);
        $this->assertEquals([23869, 23871], $this->entities->getIds());

        $this->assertEquals($this->entities->key(), 23869);

        $this->entities->next();

        $this->assertEquals($this->entities->key(), 23871);

        foreach ($this->entities as $lead) {
            $this->assertInstanceOf(Transaction::class, $lead);
        }

        $this->assertEquals($this->entities->key(), 23871);

        $this->entities->rewind();

        $this->assertEquals($this->entities->key(), 23869);

        $lead = $this->entities->current();

        $this->assertInstanceOf(Transaction::class, $lead);
    }

    public function testFilterByStatus()
    {
        $filtered = $this->entities->filterByField('id', 23871);
        
        $this->assertEquals($filtered->count(), 1);
        $this->assertEquals($filtered->first()->getId(), 23871);
    }
}