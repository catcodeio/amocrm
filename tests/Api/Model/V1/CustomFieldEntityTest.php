<?php

use Amocrm\Api\Model\CustomField\CustomField;
use PHPUnit\Framework\TestCase;

class CustomFieldEntityTest extends TestCase
{
    public function testAddress()
    {
        $cf = CustomField::create([
            'id'     => '199331',
            'name'   => 'Тестовый адрес (только API)',
            'values' => [
                [
                    'value'   => 'Ломоносова',
                    'subtype' => 'address_line_1',
                ],
                [
                    'value'   => '111',
                    'subtype' => 'address_line_2',
                ],
                [
                    'value'   => 'СПб',
                    'subtype' => 'city',
                ],
                [
                    'value'   => 'Ленинградская область',
                    'subtype' => 'state',
                ],
                [
                    'value'   => '123123',
                    'subtype' => 'zip',
                ],
                [
                    'value'   => 'RU',
                    'subtype' => 'country',
                ]
            ],
        ])->address();

        $this->assertEquals('Ломоносова', $cf->getAddressLine1());
        $this->assertEquals('Ломоносова', $cf->getBySubtype('address_line_1'));
        $this->assertEquals('111', $cf->getAddressLine2());
        $this->assertEquals('111', $cf->getBySubtype('address_line_2'));
        $this->assertEquals('СПб', $cf->getCity());
        $this->assertEquals('СПб', $cf->getBySubtype('city'));
        $this->assertEquals('Ленинградская область', $cf->getState());
        $this->assertEquals('Ленинградская область', $cf->getBySubtype('state'));
        $this->assertEquals('123123', $cf->getZip());
        $this->assertEquals('123123', $cf->getBySubtype('zip'));
        $this->assertEquals('RU', $cf->getCountry());
        $this->assertEquals('RU', $cf->getBySubtype('country'));
        $this->assertEquals('Россия', $cf->getCountryName('ru'));
        $this->assertEquals('Russia', $cf->getCountryName('en'));
        $this->assertEquals('Россия', $cf->getCountryName('asdf'));
        $this->assertEquals('Россия', $cf->getBySubtype('country', 'ru'));
        $this->assertEquals('Russia', $cf->getBySubtype('country', 'en'));
        $this->assertEquals('Россия', $cf->getBySubtype('country', 'asdf'));

        $this->assertNull($cf->getBySubtype('asdf'));
    }

    public function testOrganization()
    {
        $cf = CustomField::create([
            'id'     => '621343',
            'name'   => 'Организация',
            'values' => [[
                'value' => [
                    'name'                         => 'ООО \'Получи по щам\'',
                    'entity_type'                  => 1,
                    'vat_id'                       => 2,
                    'tax_registration_reason_code' => 3,
                    'address'                      => 4,
                    'kpp'                          => 5,
                    'external_uid'                 => 6,
                    'line1'                        => 7,
                    'line2'                        => 8,
                    'city'                         => 9,
                    'state'                        => 10,
                    'zip'                          => 11,
                    'country'                      => 'US',
                ],
            ],
            [
                'value' => [
                    'name'                         => 'AAAAAA',
                    'entity_type'                  => null,
                    'vat_id'                       => null,
                    'tax_registration_reason_code' => null,
                    'address'                      => null,
                    'kpp'                          => null,
                    'external_uid'                 => null,
                    'line1'                        => null,
                    'line2'                        => null,
                    'city'                         => null,
                    'state'                        => null,
                    'zip'                          => null,
                    'country'                      => null,
                ],
            ]]
        ])->organization();

        $this->assertEquals(17, $cf::getType());

        $this->assertEquals('ООО \'Получи по щам\'', $cf->getName());
        $this->assertEquals(1, $cf->getEntityType());
        $this->assertEquals(2, $cf->getVatId());
        $this->assertEquals(3, $cf->getTaxRegistrationReasonCode());
        $this->assertEquals(4, $cf->getAddress());
        $this->assertEquals(5, $cf->getKpp());
        $this->assertEquals(6, $cf->getExternalUid());
        $this->assertEquals(7, $cf->getLine1());
        $this->assertEquals(8, $cf->getLine2());
        $this->assertEquals(9, $cf->getCity());
        $this->assertEquals(10, $cf->getState());
        $this->assertEquals(11, $cf->getZip());
        $this->assertEquals('US', $cf->getCountry());
        $this->assertEquals('Соединённые Штаты Америки', $cf->getCountryName('ru'));
        $this->assertEquals('United States of America', $cf->getCountryName('en'));
        $this->assertEquals('Соединённые Штаты Америки', $cf->getCountryName('qwer'));

        $this->assertEquals(0, $cf->key());
        $this->assertEquals(2, $cf->count());
        $this->assertEquals(true, $cf->valid());
        $this->assertEquals(true, $cf->next());
        $this->assertEquals(1, $cf->key());

        $this->assertEquals('AAAAAA', $cf->getName());

        $this->assertEquals(false, $cf->next());
        $this->assertEquals(false, $cf->valid());
        $this->assertEquals(2, $cf->key());

        $this->assertEquals(null, $cf->getName());

        $cf->add('New');

        $this->assertEquals(3, $cf->count());
        $this->assertEquals(true, $cf->valid());
        $this->assertEquals(2, $cf->key());

        $this->assertEquals('New', $cf->getName());
    }

    public function testEntity()
    {
        $cf = CustomField::create([
            'id'     => '621343',
            'name'   => 'Юр. лицо',
            'values' => [[
                 'value' => [
                     'name'                         => 'ООО \'Получи по щам\'',
                     'entity_type'                  => 1,
                     'vat_id'                       => 2,
                     'tax_registration_reason_code' => 3,
                     'address'                      => 4,
                     'kpp'                          => 5,
                     'external_uid'                 => 6,
                 ],
            ],
            [
                 'value' => [
                     'name'                         => 'AAAAAA',
                     'entity_type'                  => null,
                     'vat_id'                       => null,
                     'tax_registration_reason_code' => null,
                     'address'                      => null,
                     'kpp'                          => null,
                     'external_uid'                 => null,
                 ],
            ]]
        ])->entity();

        $this->assertEquals(15, $cf::getType());

        $this->assertEquals('ООО \'Получи по щам\'', $cf->getName());
        $this->assertEquals(1, $cf->getEntityType());
        $this->assertEquals(2, $cf->getVatId());
        $this->assertEquals(3, $cf->getTaxRegistrationReasonCode());
        $this->assertEquals(4, $cf->getAddress());
        $this->assertEquals(5, $cf->getKpp());
        $this->assertEquals(6, $cf->getExternalUid());

        $this->assertEquals(0, $cf->key());
        $this->assertEquals(2, $cf->count());
        $this->assertEquals(true, $cf->valid());
        $this->assertEquals(true, $cf->next());
        $this->assertEquals(1, $cf->key());

        $this->assertEquals('AAAAAA', $cf->getName());

        $this->assertEquals(false, $cf->next());
        $this->assertEquals(false, $cf->valid());
        $this->assertEquals(2, $cf->key());

        $this->assertEquals(null, $cf->getName());

        $cf->add('New');

        $this->assertEquals(3, $cf->count());
        $this->assertEquals(true, $cf->valid());
        $this->assertEquals(2, $cf->key());

        $this->assertEquals('New', $cf->getName());
    }
}