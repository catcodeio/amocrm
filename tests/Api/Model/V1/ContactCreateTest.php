<?php

use Amocrm\Api\Model\Contact;
use PHPUnit\Framework\TestCase;

class ContactCreateTest extends TestCase
{
    /**
     * @var Contact
     */
    private $contact;

    public function setUp()
    {
        $this->contact = Contact::create();
    }

    public function testIterates()
    {
        $this->assertInstanceOf(Contact::class, $this->contact);
        $this->assertEquals($this->contact->getLinkedLeadsId(), null);
        $this->assertEquals($this->contact->getLinkedCompanyId(), null);

        $this->contact->addLeadId(222);
        $this->contact->setLinkedCompanyId(333);

        $this->assertEquals($this->contact->getLinkedLeadsId(), [222]);
        $this->assertEquals($this->contact->getLinkedCompanyId(), 333);

        $this->contact->addLeadId(444);
        $this->contact->addLeadId(555);

        $this->assertEquals($this->contact->getLinkedLeadsId(), [222, 444, 555]);

        $this->contact->removeLeadId(444);

        $this->assertEquals($this->contact->getLinkedLeadsId(), [222, 555]);

        $this->contact->removeLeadId(222);
        $this->contact->removeLeadId(555);

        $this->assertEquals($this->contact->getLinkedLeadsId(), []);
    }
}