<?php

use Amocrm\Api\Model\CustomField\CustomField;
use Amocrm\Api\Model\CustomField\CustomFields;
use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\Customer;
use Amocrm\Api\Model\Tag\Tags;
use Amocrm\Api\Model\Tag\Tag;

class CustomerTest extends TestCase
{
    /**
     * @var Customer
     */
    private $customer;

    public function setUp()
    {
        $this->customer = Customer::create([
            'id'              => '1031821',
            'name'            => 'Кастомер',
            'date_create'     => 1522415760,
            'date_modify'     => 1522415960,
            'created_by'      => '2291701',
            'modified_by'     => 1523190638,
            'account_id'      => '19040293',
            'next_price'      => '320',
            'main_user_id'    => '2291701',
            'periodicity'     => 30,
            'next_date'       => 1523190638,
            'main_contact_id' => 2986625,
            'task_last_date'  => null,
            'period_id'       => 1064215,
            'deleted'         => false,
            'tags'            => [
                [
                    'id'           => 58183,
                    'name'         => 'тестовая',
                    'element_type' => 2,
                ],
            ],
            'custom_fields'   => [
                [
                    'id'     => '199323',
                    'name'   => 'Тестовый список',
                    'values' => [
                        [
                            'value' => 'Вариант 1',
                            'enum'  => '406825',
                        ],
                    ],
                ],
                [
                    'id'     => '199327',
                    'name'   => 'Тестовый переключатель',
                    'values' => [
                        [
                            'value' => 'Вариант 2',
                            'enum'  => '406841',
                        ],
                    ],
                ],
                [
                    'id'     => '199329',
                    'name'   => 'Тестовый флаг',
                    'values' => [
                        [
                            'value' => '1',
                        ],
                    ],
                ],
                [
                    'id'     => '199345',
                    'name'   => 'Тестовый день рождения (обязательное)',
                    'values' => [
                        [
                            'value' => '2018-04-25 00:00:00',
                        ],
                    ],
                ],
                [
                    'id'     => '125623',
                    'name'   => 'Client ID',
                    'values' => [
                        [
                            'value' => '222333444',
                        ],
                    ],
                ],
                [
                    'id'     => '199325',
                    'name'   => 'Тестовый мультисписок',
                    'values' => [
                        [
                            'value' => 'Вариант 1',
                            'enum'  => '406831',
                        ],
                        [
                            'value' => 'Вариант 2',
                            'enum'  => '406833',
                        ],
                    ],
                ],
                [
                    'id'     => '199331',
                    'name'   => 'Тестовый адрес (только API)',
                    'values' => [
                        [
                            'value'   => 'Ломоносова',
                            'subtype' => 'address_line_1',
                        ],
                        [
                            'value'   => '111',
                            'subtype' => 'address_line_2',
                        ],
                        [
                            'value'   => 'СПб',
                            'subtype' => 'city',
                        ],
                    ],
                ],
                [
                    'id'     => '206417',
                    'name'   => 'Тестовая ссылка',
                    'values' => [
                        [
                            'value' => 'https://catcode.io',
                        ],
                    ],
                ],
                [
                    'id'     => '217963',
                    'name'   => 'Тестовый текст',
                    'values' => [
                        [
                            'value' => 'Много текста',
                        ],
                    ],
                ],
                [
                    'id'     => '217965',
                    'name'   => 'Тестовое число',
                    'values' => [
                        [
                            'value' => '567654345676543456787654.567876543',
                        ],
                    ],
                ],
                [
                    'id'     => '217981',
                    'name'   => 'Тестовая дата',
                    'values' => [
                        [
                            'value' => '2018-04-24 00:00:00',
                        ],
                    ],
                ],
                [
                    'id'     => '217983',
                    'name'   => 'Тестовая текстовая область',
                    'values' => [
                        [
                            'value' => 'Очень много текста',
                        ],
                    ],
                ],
                [
                    'id'     => '217997',
                    'name'   => 'Тестовый короткий адрес',
                    'values' => [
                        [
                            'value' => 'СПб Звеня 12',
                        ],
                    ],
                ],
            ],
        ]);
    }

    public function testPlainProperty()
    {
        $this->assertEquals($this->customer->getId(), 1031821);
        $this->assertEquals($this->customer->getName(), 'Кастомер');
        $this->assertEquals($this->customer->getNextPrice(), 320);
        $this->assertEquals($this->customer->getMainUserId(), 2291701);
        $this->assertEquals($this->customer->getPeriodicity(), 30);
        $this->assertEquals($this->customer->getPeriodId(), 1064215);
        $this->assertEquals($this->customer->isDeleted(), false);
        $this->assertEquals($this->customer->getModifiedBy(), 1523190638);

        $this->customer->setName('Новое название');
        $this->customer->setNextPrice(100);
        $this->customer->setMainUserId(111);
        $this->customer->setPeriodicity(444);
        $this->customer->setPeriodId(555);
        $this->customer->setModifiedBy(134);

        $this->assertEquals($this->customer->getName(), 'Новое название');
        $this->assertEquals($this->customer->getNextPrice(), 100);
        $this->assertEquals($this->customer->getMainUserId(), 111);
        $this->assertEquals($this->customer->getPeriodicity(), 444);
        $this->assertEquals($this->customer->getPeriodId(), 555);
        $this->assertEquals($this->customer->getModifiedBy(), 134);
    }

    public function testDateTime()
    {
        $dateCreate = $this->customer->getDateCreate();
        $this->assertInstanceOf(DateTime::class, $dateCreate);
        $this->assertEquals($dateCreate->format('U'), 1522415760);

        $dateModify = $this->customer->getDateModify();
        $this->assertInstanceOf(DateTime::class, $dateModify);
        $this->assertEquals($dateModify->format('U'), 1522415960);

        $nextDate = $this->customer->getNextDate();
        $this->assertInstanceOf(DateTime::class, $nextDate);
        $this->assertEquals($nextDate->format('U'), 1523190638);

        $taskLastDate = $this->customer->getTaskLastDate();
        $this->assertNull($taskLastDate);
    }

    public function testTags()
    {
        $tags = $this->customer->getTags();
        $this->assertInstanceOf(Tags::class, $tags);
        $this->assertEquals($tags->count(), 1);
        $tag = $tags->first();
        $this->assertInstanceOf(Tag::class, $tag);
        $this->assertEquals($tag->getId(), 58183);
        $this->assertEquals($tag->getName(), 'тестовая');

        $tag = $this->customer->getTag('тестовая');
        $this->assertInstanceOf(Tag::class, $tag);
        $this->assertEquals($tag->getId(), 58183);

        $this->customer->addTag('новый тег');

        $tagsArray = $this->customer->getModifiedForApi();
        $this->assertTrue(isset($tagsArray['tags']));
        $this->assertEquals('тестовая, новый тег', $tagsArray['tags']);

        $this->customer->removeTag('тестовая');

        $tag = $this->customer->getTags()->first();
        $this->assertInstanceOf(Tag::class, $tag);
        $this->assertEquals($tag->getId(), null);
        $this->assertEquals($tag->getName(), 'новый тег');

        $tagsArray = $this->customer->getModifiedForApi();
        $this->assertTrue(isset($tagsArray['tags']));
        $this->assertEquals('новый тег', $tagsArray['tags']);

        $this->customer->addTag(['qwer', 'asdf']);
        $this->assertEquals('новый тег, qwer, asdf', $this->customer->getModifiedForApi()['tags']);

        $this->customer->removeTag(['новый тег', 'asdf']);
        $this->assertEquals('qwer', $this->customer->getModifiedForApi()['tags']);
    }

    public function testCustomFields()
    {
        $customFields = $this->customer->getCustomFields();

        $this->assertInstanceOf(CustomFields::class, $customFields);
        $this->assertEquals($customFields->count(), 13);
        $customField = $customFields->first();
        $this->assertInstanceOf(CustomField::class, $customField);

        $cf = $this->customer->getCustomField(199329)->checkbox();
        $this->assertEquals($cf->get(), true);
        $cf->set(false);
        $this->assertEquals($cf->get(), false);
        $this->assertEquals([
            'id' => '199329',
            'values' => [['value' => 0]],
        ], $cf->getModifiedForApi());

        $cf = $this->customer->getCF(199329)->checkbox();
        $this->assertEquals($cf->get(), false);
        $cf->set(true);
        $this->assertEquals($cf->get(), true);
        $this->assertEquals([
            'id' => '199329',
            'values' => [['value' => '1']],
        ], $cf->getModifiedForApi());

        $cf = $this->customer->getCustomField(199323)->select();
        $this->assertEquals($cf->get(), 'Вариант 1');
        $cf->set('Вариант 2');
        $this->assertEquals($cf->get(), 'Вариант 2');
        $this->assertEquals([
            'id' => '199323',
            'values' => [
                ['value' => 'Вариант 2'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->customer->getCustomField(199327)->radioButton();
        $this->assertEquals($cf->get(), 'Вариант 2');
        $cf->set('Вариант 3');
        $this->assertEquals($cf->get(), 'Вариант 3');
        $this->assertEquals([
            'id' => '199327',
            'values' => [
                ['value' => 'Вариант 3'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->customer->getCustomField(199345)->birthday();
        $this->assertInstanceOf(DateTime::class, $cf->get());
        $this->assertEquals($cf->get()->format('Y-m-d H:i:s'), '2018-04-25 00:00:00');
        $cf->set(new DateTime('30.01.2999'));
        $this->assertEquals($cf->get()->format('Y-m-d H:i:s'), '2999-01-30 00:00:00');
        $this->assertEquals([
            'id' => '199345',
            'values' => [
                ['value' => '30.01.2999'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->customer->getCustomField(199325)->multiselect();
        $this->assertTrue(is_array($cf->get()));
        $this->assertEquals(count($cf->get()), 2);
        $cf->remove('Вариант 3');
        $this->assertEquals(count($cf->get()), 2);
        $cf->remove('Вариант 1');
        $this->assertEquals(count($cf->get()), 1);
        $cf->add('Вариант 3');
        $this->assertEquals(count($cf->get()), 2);
        $this->assertEquals([
            'id' => '199325',
            'values' => [
                '406833',
                'Вариант 3',
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->customer->getCustomField(199331)->address();
        $this->assertEquals('Ломоносова', $cf->getAddressLine1());
        $this->assertEquals('111', $cf->getAddressLine2());
        $this->assertEquals('СПб', $cf->getCity());
        $this->assertEquals(null, $cf->getState());
        $this->assertEquals(null, $cf->getZip());
        $this->assertEquals(null, $cf->getCountry());
        $cf
            ->setAddressLine1('asdf')
            ->setAddressLine2('qwer')
            ->setCity('zzzz')
            ->setState('qqqq')
            ->setZip('wwww')
            ->setCountry('RU')
        ;
        $this->assertEquals('asdf', $cf->getAddressLine1());
        $this->assertEquals('qwer', $cf->getAddressLine2());
        $this->assertEquals('zzzz', $cf->getCity());
        $this->assertEquals('qqqq', $cf->getState());
        $this->assertEquals('wwww', $cf->getZip());
        $this->assertEquals('RU', $cf->getCountry());
        $this->assertEquals([
            'id' => '199331',
            'values' => [
                ['value' => 'asdf', 'subtype' => 'address_line_1'],
                ['value' => 'qwer', 'subtype' => 'address_line_2'],
                ['value' => 'zzzz', 'subtype' => 'city'],
                ['value' => 'qqqq', 'subtype' => 'state'],
                ['value' => 'wwww', 'subtype' => 'zip'],
                ['value' => 'RU', 'subtype' => 'country'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->customer->getCustomField(206417)->link();
        $this->assertEquals('https://catcode.io', $cf->get());
        $cf->set('asdf');
        $this->assertEquals('asdf', $cf->get());
        $this->assertEquals([
            'id' => '206417',
            'values' => [
                ['value' => 'asdf'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->customer->getCustomField(217963)->text();
        $this->assertEquals('Много текста', $cf->get());
        $cf->set('asdf');
        $this->assertEquals('asdf', $cf->get());
        $this->assertEquals([
            'id' => 217963,
            'values' => [
                ['value' => 'asdf'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->customer->getCustomField(217965)->numeric();
        $this->assertEquals(567654345676543456787654.567876543, $cf->get());
        $cf->set(2.66778987);
        $this->assertEquals('2.66778987', $cf->get());
        $this->assertEquals([
            'id' => 217965,
            'values' => [
                ['value' => '2.66778987'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->customer->getCustomField(217981)->date();
        $this->assertInstanceOf(DateTime::class, $cf->get());
        $this->assertEquals($cf->get()->format('Y-m-d H:i:s'), '2018-04-24 00:00:00');
        $cf->set(new DateTime('30.01.1999'));
        $this->assertEquals($cf->get()->format('Y-m-d H:i:s'), '1999-01-30 00:00:00');
        $this->assertEquals([
            'id' => 217981,
            'values' => [
                ['value' => '30.01.1999'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->customer->getCustomField(217983)->textarea();
        $this->assertEquals('Очень много текста', $cf->get());
        $cf->set('asdf1');
        $this->assertEquals('asdf1', $cf->get());
        $this->assertEquals([
            'id' => 217983,
            'values' => [
                ['value' => 'asdf1'],
            ],
        ], $cf->getModifiedForApi());

        $cf = $this->customer->getCustomField(217997)->addressShort();
        $this->assertEquals('СПб Звеня 12', $cf->get());
        $cf->set('asdf2');
        $this->assertEquals('asdf2', $cf->get());
        $this->assertEquals([
            'id' => 217997,
            'values' => [
                ['value' => 'asdf2'],
            ],
        ], $cf->getModifiedForApi());
    }

    public function testCopy()
    {
        $customer = clone $this->customer;
        $this->assertInstanceOf(Customer::class, $customer);
        $this->assertNotEquals($this->customer, $customer);
        $this->assertNotEquals($customer->getId(), $this->customer->getId());
        $this->assertNull($customer->getId());
        $this->assertEquals($customer->getName(), $this->customer->getName());

        $this->assertEquals(1, $this->customer->getTags()->count());
        $this->assertInstanceOf(Tags::class, $customer->getTags());
        $this->assertEquals($customer->getTags()->count(), 1);
        $this->assertEquals($customer->getTags()->first()->getName(), 'тестовая');

        $customerArray = $customer->getModifiedForApi();
        $this->assertEquals($this->customer->getName(), $customerArray['name']);

        $customFields = $customer->getCustomFields();

        $this->assertInstanceOf(CustomFields::class, $customFields);
        $this->assertEquals($customFields->count(), 13);
        $customField = $customFields->first();
        $this->assertInstanceOf(CustomField::class, $customField);

        $cf = $customer->getCustomField(199329)->checkbox();
        $this->assertEquals($cf->get(), true);
        $this->assertEquals([
            'id' => '199329',
            'values' => [['value' => '1']],
        ], $cf->getModifiedForApi());

        $cf = $customer->getCustomField(199323)->select();
        $this->assertEquals($cf->get(), 'Вариант 1');
        $this->assertEquals([
            'id' => '199323',
            'values' => [
                [
                    'value' => 'Вариант 1',
                    'enum' => '406825',
                ],
            ],
        ], $cf->getModifiedForApi());

        $cf = $customer->getCustomField(199327)->radioButton();
        $this->assertEquals($cf->get(), 'Вариант 2');
        $this->assertEquals([
            'id' => '199327',
            'values' => [
                [
                    'value' => 'Вариант 2',
                    'enum' => '406841',
                ],
            ],
        ], $cf->getModifiedForApi());

        $dateModify = new DateTime();
        $customer->setDateModify($dateModify);

        $this->assertEquals([
            'id'              => null,
            'name'            => 'Кастомер',
            'date_create'     => 1522415760,
            'date_modify'     => $dateModify->format('U'),
            'created_by'      => '2291701',
            'modified_by'     => 1523190638,
            'account_id'      => '19040293',
            'next_price'      => '320',
            'main_user_id'    => '2291701',
            'periodicity'     => 30,
            'next_date'       => 1523190638,
            'task_last_date'  => null,
            'period_id'       => 1064215,
            'deleted'         => false,
            'tags'            => 'тестовая',
            'main_contact_id' => 2986625,
            'custom_fields'   => [
                [
                    'id'     => '199323',
                    'values' => [
                        [
                            'value' => 'Вариант 1',
                            'enum'  => '406825',
                        ],
                    ],
                ],
                [
                    'id'     => '199327',
                    'values' => [
                        [
                            'value' => 'Вариант 2',
                            'enum'  => '406841',
                        ],
                    ],
                ],
                [
                    'id'     => '199329',
                    'values' => [
                        [
                            'value' => '1',
                        ],
                    ],
                ],
                [
                    'id'     => '199345',
                    'name'   => 'Тестовый день рождения (обязательное)',
                    'values' => [
                        [
                            'value' => '2018-04-25 00:00:00',
                        ],
                    ],
                ],
                [
                    'id'     => '125623',
                    'name'   => 'Client ID',
                    'values' => [
                        [
                            'value' => '222333444',
                        ],
                    ],
                ],
                [
                    'id'     => '199325',
                    'name'   => 'Тестовый мультисписок',
                    'values' => [
                        [
                            'value' => 'Вариант 1',
                            'enum'  => '406831',
                        ],
                        [
                            'value' => 'Вариант 2',
                            'enum'  => '406833',
                        ],
                    ],
                ],
                [
                    'id'     => '199331',
                    'name'   => 'Тестовый адрес (только API)',
                    'values' => [
                        [
                            'value'   => 'Ломоносова',
                            'subtype' => 'address_line_1',
                        ],
                        [
                            'value'   => '111',
                            'subtype' => 'address_line_2',
                        ],
                        [
                            'value'   => 'СПб',
                            'subtype' => 'city',
                        ],
                    ],
                ],

                [
                    'id'     => '206417',
                    'name'   => 'Тестовая ссылка',
                    'values' => [
                        [
                            'value' => 'https://catcode.io',
                        ],
                    ],
                ],
                [
                    'id'     => '217963',
                    'name'   => 'Тестовый текст',
                    'values' => [
                        [
                            'value' => 'Много текста',
                        ],
                    ],
                ],
                [
                    'id'     => '217965',
                    'name'   => 'Тестовое число',
                    'values' => [
                        [
                            'value' => '567654345676543456787654.567876543',
                        ],
                    ],
                ],
                [
                    'id'     => '217981',
                    'name'   => 'Тестовая дата',
                    'values' => [
                        [
                            'value' => '2018-04-24 00:00:00',
                        ],
                    ],
                ],
                [
                    'id'     => '217983',
                    'name'   => 'Тестовая текстовая область',
                    'values' => [
                        [
                            'value' => 'Очень много текста',
                        ],
                    ],
                ],
                [
                    'id'     => '217997',
                    'name'   => 'Тестовый короткий адрес',
                    'values' => [
                        [
                            'value' => 'СПб Звеня 12',
                        ],
                    ],
                ],
            ],
        ], $customer->getModifiedForApi());
    }
}