<?php

use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\ContactLink;

class ContactLinkTest extends TestCase
{
    /**
     * @var ContactLink
     */
    private $contactLink;

    public function setUp()
    {
        $this->contactLink = ContactLink::create(
            [
                'contact_id'    => '2986625',
                'lead_id'       => '1704227',
                'last_modified' => 1523574064,
            ]
        );
    }

    public function testPlainProperty()
    {
        $this->assertEquals($this->contactLink->getContactId(), 2986625);
        $this->assertEquals($this->contactLink->getLeadId(), 1704227);
        $this->assertInstanceOf(DateTime::class, $this->contactLink->getLastModified());
        $this->assertEquals($this->contactLink->getLastModified()->format('U'), 1523574064);
    }
}