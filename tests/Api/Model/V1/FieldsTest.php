<?php

use PHPUnit\Framework\TestCase;
use Amocrm\Api\Model\CustomField\Field;
use Amocrm\Api\Model\CustomField\Fields;

class FieldsTest extends TestCase
{
    /**
     * @var Fields
     */
    private $entities;

    public function setUp()
    {
        $this->entities = Fields::create([
            [
                'id'           => '1325',
                'origin'       => 'asdf',
            ],
            [
                'id'           => '1219851',
                'name'         => 'Доп. поле',
                'element_type' => '1',
                'type'         => '2',
                'origin'       => 'asdf',
                'disabled'     => '0',
            ],
            [
                'name'         => 'Доп. поле',
                'element_type' => '1',
                'type'         => '2',
                'disabled'     => '1',
            ],
        ]);
    }

    public function testIterates()
    {
        $this->assertInstanceOf(Fields::class, $this->entities);
        $this->assertEquals($this->entities->count(), 3);
        $this->assertEquals($this->entities->key(), 0);

        $this->entities->next();

        $this->assertEquals($this->entities->key(), 1);

        foreach ($this->entities as $entity) {
            $this->assertInstanceOf(Field::class, $entity);
        }

        $this->assertEquals($this->entities->key(), 1);

        $this->entities->rewind();

        $this->assertEquals($this->entities->key(), 0);

        $this->assertInstanceOf(Field::class, $this->entities->current());
    }
}